import chalk from "chalk";
import axios from "axios";
import { API_LOCAL_URL, API_REMOTE_URL } from "../api/config.ts";
import { api, ajax } from "./api-util.ts";
import { list, item, post, put, del } from "../api/index.ts";
import { stringify } from "flatted";
import _ from "lodash";

class DataSyncManager {
    constructor(store, member) {
        console.log(
            chalk.yellow(
                `----------- DataSyncManager Scheduler Create ----------`,
            ),
        );

        this.interval = null;
        this.interval2 = null;

        this.store = store;

        this.member = member;

        this.alreadyStart = false; // 중복 실행 방지 플래그
        this.isRemoteSvrCon = false; // 원격서버 연결 여부(인터넷)

        console.log("---- DataSyncManager constructor", {
            API_LOCAL_URL,
            API_REMOTE_URL,
        });

        if (!this.alreadyStart) this.init();
    }

    // 초기화
    async init() {
        console.log("-----------------------------------");
        console.log("---- DataSyncManager init!");
        console.log("- API_ENDPOINT:", API_LOCAL_URL);
        console.log("-----------------------------------");

        clearInterval(this.interval);
        clearInterval(this.interval2);

        // this.checkInternetConnection();
        // this.syncPendingLogs();
        // this.cleanupOldLogs();

        // 주기적으로 로그 전송
        this.interval = setInterval(() => {
            // if (!this.isRemoteSvrCon) this.checkInternetConnection();
            this.syncPendingLogs();
        }, 1000 * 10); // 10초마다 실행

        // 오래된 로그 삭제
        this.interval2 = setInterval(() => {
            this.cleanupOldLogs();
        }, 1000 * 60); // 1분마다 실행.

        this.alreadyStart = true;
    }

    // 원격 동기화 서버 연결 확인
    async checkInternetConnection() {
        try {
            axios
                .get(API_REMOTE_URL, {
                    timeout: 5000, // 타임아웃
                })
                .then((res) => {
                    if (res) {
                        console.log(
                            "원격 동기화 서버에 연결되어 있습니다. 동기화를 시작 합니다.",
                            API_REMOTE_URL,
                        );
                        this.isRemoteSvrCon = true;
                    }
                });
            return true;
        } catch (err) {
            console.log(
                "원격 동기화 서버 연결이 없습니다. 동기화를 종료 합니다.",
                API_REMOTE_URL,
            );
            this.isRemoteSvrCon = false;
            return false;
        }
    }

    // 전송 대기 중인 로그 동기화
    async syncPendingLogs() {
        let { place_id } = this.member;

        if (!this.isRemoteSvrCon) {
            console.log(
                `------> 원격 동기화 서버 연결이 없습니다.  로그 원격 동기화 안함 ------>`,
            );
            return false;
        }

        // 대기 중인 로그 가져오기
        await this.store
            .dispatch("syncLog/getList", {
                filter: `place_id=${place_id}`,
            })
            .then(({ success, error, synclogs }) => {
                if (success) {
                    if (synclogs.length === 0) {
                        console.log(
                            "syncPendingLogs 전송 대기 중인 로그가 없습니다.",
                        );
                        return false;
                    }

                    console.log(
                        `syncPendingLogs 전송대기 중인 로그 ${synclogs.length}개.`,
                    );

                    for (let synclog of synclogs) {
                        const { method, host, url, config, place_id, data } =
                            synclog;

                        console.log(`------> syncPendingLogs synclog`, {
                            synclog,
                        });

                        // 원격 동기화 실행
                        config,
                            this.doSync(
                                method,
                                config,
                                host,
                                url,
                                data,
                                false,
                            ).then(({ success, synclog }) => {
                                console.log(
                                    `--> syncPendingLogs  doSync success : `,
                                    {
                                        success,
                                        synclog,
                                    },
                                );

                                // 전송 성공 시 로컬 로그 상태 업데이트
                                if (success) {
                                    return this.store
                                        .dispatch(`syncLog/setItem`, {
                                            synclog: {
                                                id: synclog.id,
                                                status: "send",
                                            },
                                            sync: false,
                                        })
                                        .then(
                                            ({
                                                success,
                                                error,
                                                count,
                                                synclog,
                                            }) => {
                                                return success;
                                            },
                                        );
                                }
                            });
                    }
                } else if (error) {
                    console.log("syncPendingLogs  오류가 발생했습니다", error);
                    return;
                }
            });
    }

    // 로그 전송 (from api/index.js 에서 모든 요청 인터셉트 한다.)
    async doSync(method, config, host, url, data, isSync) {
        console.log("--> doSync Start", host + "/api/" + url);

        // 무한 전송 방지 코드.
        if (!isSync) {
            console.log(`------> 로그 원격 동기화 안함 ------>`, { isSync });
            return false;
        }

        console.log("--> doSync params", { method, host, url, isSync });
        console.log(`------> 로그 원격 동기화 실행 시작 ------>`);

        if (!this.isRemoteSvrCon) {
            console.log(
                "--> doSync 원격서버 연결이 없어서 데이터 저장합니다.",
                { host, url },
            );

            data = JSON.stringify(data);

            return await this.addLog(method, config, API_LOCAL_URL, url, data);
        } else {
            data = JSON.parse(data);

            const link =
                method.toUpperCase() === "POST"
                    ? axios.post
                    : method.toUpperCase() === "PUT"
                      ? axios.put
                      : axios.delete;

            console.log("======> doSync link", { host, url, link });

            let fullUrl = "";

            try {
                fullUrl = new URL(
                    url,
                    host.endsWith("/") ? host : `${host}/` + "api/",
                ).toString();
                console.log("- doSync 요청할 전체 URL:", fullUrl);
            } catch (error) {
                console.error("URL 생성 중 오류 발생:", error.message);
            }

            try {
                const response = await link(fullUrl, data, {
                    baseUrl: API_REMOTE_URL,
                });

                const { success, error, body } = response;

                if (success) {
                    console.log("--> doSync SaveLogData success", {
                        success,
                        error,
                        body,
                    });
                }

                return { success, body };
            } catch (error) {
                console.error("#### doSync Error during the API call:", error);
                return { success: false, body: error.message };
            }
        }
    }

    // 로그 데이터 추가
    async addLog(method, config, host, url, data) {
        console.log("- addLog ", { method, host, url, data });

        let { place_id } = this.member;

        // data= JSON.stringify(data);

        let synclog = { method, config, host, url, place_id, data }; // flatted 사용

        console.log("- addLog synclog", synclog);

        let fullUrl = "";

        try {
            fullUrl = new URL(
                url,
                host.endsWith("/") ? host : `${host}/` + "api/",
            ).toString();
            console.log("- addLog 로컬 데이터 저장할 전체 URL:", fullUrl);
        } catch (error) {
            console.error("URL 생성 중 오류 발생:", error.message);
        }

        try {
            await this.store
                .dispatch("syncLog/newItem", { synclog, sync: false })
                .then(({ success, error, info }) => {
                    return success;
                });
        } catch (err) {
            console.error("++++> addLog Error Sync Save logs:", err.message);
            return false;
        }
    }

    // 오래된 로그 삭제
    async cleanupOldLogs() {
        const cutoffDate = new Date();

        try {
            await this.store
                .dispatch("syncLog/delAllOldItems", { sync: false })
                .then(({ success, error, info }) => {
                    return success;
                });
        } catch (err) {
            console.error(
                "++++> cleanupOldLogs Error deleting old logs:",
                err.message,
            );
            return false;
        }
    }
}

export default DataSyncManager;
