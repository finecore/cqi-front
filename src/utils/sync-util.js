import chalk from 'chalk';
import axios from 'axios';
import { API_LOCAL_URL, API_REMOTE_URL } from '../api/config.ts';
import { list, item, post, put, del } from '../api/index.ts';
import { ajax, reCleateAjax } from './api-util.ts';
import { stringify } from 'flatted';
import _ from 'lodash';
import { USER, USER_SYNC, TOKEN_SYNC } from '@/store/mutation_types';

class DataSyncManager {
  constructor(store, user, userSync) {
    console.log(
      chalk.yellow(`----------- DataSyncManager Scheduler Create ----------`)
    );

    this.interval = null;
    this.interval2 = null;

    this.store = store;

    this.user = user;
    this.userSync = userSync;

    this.alreadyStart = false; // 중복 실행 방지 플래그
    this.isRemoteSvrCon = false; // 원격서버 연결 여부(인터넷)

    console.log('---- DataSyncManager constructor', {
      API_LOCAL_URL,
      API_REMOTE_URL,
    });

    if (!this.alreadyStart) this.init();
  }

  // 초기화
  async init() {
    console.log('-----------------------------------');
    console.log('---- DataSyncManager init!');
    console.log('- API_ENDPOINT:', API_REMOTE_URL);
    console.log('-----------------------------------');

    clearInterval(this.interval);
    clearInterval(this.interval2);

    this.checkInternetConnection();

    // 주기적으로 로그 전송
    this.interval = setInterval(() => {
      this.syncPendingLogs();
    }, 1000 * 5); // 5초마다 실행

    // 오래된 로그 삭제
    this.interval2 = setInterval(() => {
      // this.cleanupOldLogs();
    }, 1000 * 6); // 1분마다 실행.

    this.alreadyStart = true;
  }

  // 원격 동기화 서버 연결 확인
  async checkInternetConnection() {
    try {
      let ajax = reCleateAjax(API_REMOTE_URL);

      await axios
        .get(API_REMOTE_URL, {
          timeout: 0, // 타임아웃
        })
        .then((res) => {
          if (res) {
            console.log(
              '원격 동기화 서버에 연결되어 있습니다. 동기화를 시작 합니다.',
              API_REMOTE_URL
            );
            this.isRemoteSvrCon = true;
          }
        });
      return true;
    } catch (err) {
      console.log(
        '원격 동기화 서버 연결이 없습니다. 동기화를 종료 합니다.',
        API_REMOTE_URL
      );
      this.isRemoteSvrCon = false;
      return false;
    }
  }

  // 전송 대기 중인 로그 동기화
  async syncPendingLogs() {
    let { place_id, id, type } = this.user;

    await this.checkInternetConnection();

    const tokenSync = this.store.getters[TOKEN_SYNC];
    console.log('- doLogin tokenSync', { id, tokenSync });

    if (!this.isRemoteSvrCon) {
      console.log(
        `------> 원격 동기화 서버 연결이 없습니다.  로그 원격 동기화 안함 ------>`
      );
      return false;
    } else if (!tokenSync) {
      console.log('- doLogin tokenSync1', tokenSync);

      // 원격 로그인 한다.
      await this.store
        .dispatch('loginSync', {
          id,
          type,
          sync: true,
        })
        .then(({ success, user }) => {
          console.log('- doLogin OK', {
            success,
            user,
          });

          if (success) {
            this.userSync = user;
          } else {
            console.error('- doLogin Failue', {
              success,
              user,
            });
          }
        });
    }

    // 원격 서버 연결 하려면 재 설정 해야한다.
    let ajax = reCleateAjax(API_REMOTE_URL + '/api/');

    // 대기 중인 로그 가져오기
    await this.store
      .dispatch('syncLog/getList', {
        filter: `place_id=${place_id} AND status = 'pending'`,
      })
      .then(({ success, error, synclogs }) => {
        if (success) {
          if (synclogs.length === 0) {
            console.log('syncPendingLogs 전송 대기 중인 로그가 없습니다.');
            return false;
          }

          console.log(
            `syncPendingLogs 전송대기 중인 로그 ${synclogs.length}개.`
          );

          for (let synclog of synclogs) {
            const { method, host, url, config, place_id, data } = synclog;

            console.log(`------> syncPendingLogs synclog`, {
              synclog,
            });

            // 원격 동기화 실행
            config,
              this.doSync(method, config, host, url, data, false).then(
                ({ success }) => {
                  console.log(
                    `--> syncPendingLogs  doSync success : `,
                    success
                  );
                  // 전송 성공 시 로컬 로그 상태 업데이트
                  if (success) {
                    this.store
                      .dispatch(`syncLog/setItem`, {
                        synclog: {
                          id: synclog.id,
                          status: 'send',
                        },
                        sync: false,
                      })
                      .then(({ success, error }) => {
                        return success;
                      });
                  }
                }
              );
          }
        } else if (error) {
          console.log('syncPendingLogs  오류가 발생했습니다', error);
          return;
        }
      });
  }

  // 로그 전송 (from api/index.js 에서 모든 요청 인터셉트 한다.)
  async doSync(method, config, host, url, data, isSync) {
    console.log('--> doSync Start', host + '/api/' + url, { config });

    console.log('--> doSync params', { method, host, url, isSync });
    console.log(`------> 로그 원격 동기화 실행 시작 ------>`);

    let { id, place_id, level, type } = this.store.getters[USER_SYNC];
    let tokenSync = this.store.getters[TOKEN_SYNC];

    console.log('--> doSync USER_SYNC', this.isRemoteSvrCon, {
      id,
      place_id,
      level,
      type,
      tokenSync,
    });

    if (!this.isRemoteSvrCon) {
      // 무한 전송 방지 코드.
      if (!isSync) {
        console.log(`------> 로그 원격 동기화 저장 안함 ------>`, {
          isSync,
        });
        return false;
      }

      console.log('--> doSync 원격서버 연결이 없어서 데이터 저장합니다.', {
        host,
        url,
      });

      try {
        config = JSON.stringify(config);
        data = JSON.stringify(data);
      } catch (e) {
        console.log('--> doSync JSON parse Error', e.message);
      }

      return await this.addLog(method, config, API_REMOTE_URL, url, data);
    } else if (tokenSync) {
      try {
        config = JSON.parse(config);
        data = JSON.parse(data);
      } catch (e) {
        console.log('--> doSync JSON parse Error', e.message);
      }

      const link =
        method.toUpperCase() === 'POST'
          ? ajax.post
          : method.toUpperCase() === 'PUT'
          ? ajax.put
          : ajax.delete;

      console.log('======> doSync link', { host, url, link });

      try {
        const response = await link(url, data, {
          ...config,
        });

        console.log('======> doSync response', response);

        const {
          common: { success, error, body },
        } = response?.data;

        if (response?.status === 200) {
          return { success, body };
        } else {
          return {
            success: false,
            body: error.message,
          };
        }
      } catch (error) {
        console.error('#### doSync Error during the API call:', error);
        return { success: false, body: error.message };
      }
    }
  }

  // 로그 데이터 추가
  async addLog(method, config, host, url, data) {
    console.log('- addLog ', { method, host, url, data });

    let { id, place_id, level, type } = this.user;

    let synclog = { method, config, host, url, place_id, data }; // flatted 사용

    console.log('- addLog synclog', synclog);

    // tokenShnc 눈 조정허자 얺고 전송시 api-utils 에서 분기 한다.

    try {
      await this.store
        .dispatch('syncLog/newItem', { synclog, sync: false })
        .then(({ success, error, info }) => {
          return success;
        });
    } catch (err) {
      console.error('++++> addLog Error Sync Save logs:', err.message);
      return false;
    }
  }

  // 오래된 로그 삭제
  async cleanupOldLogs() {
    const cutoffDate = new Date();

    try {
      await this.store
        .dispatch('syncLog/delAllOldItems', { sync: false })
        .then(({ success, error, info }) => {
          return success;
        });
    } catch (err) {
      console.error(
        '++++> cleanupOldLogs Error deleting old logs:',
        err.message
      );
      return false;
    }
  }
}

export default DataSyncManager;
