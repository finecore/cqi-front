/**
 * 쿼리스트링에서 id 추축해 제공.
 * @param route 
 * @returns 있으면 id(Number), 없으면 null.
 */
export const queryId = (route: any): number | null => {
  let qId: string | null = null;
  if (route.query.id === undefined) {
    qId = null;
  } else if (Array.isArray(route.query.id)) {
    qId = route.query.id.length > 0 ? route.query.id[0] : null;
  } else {
    qId = route.query.id;
  }
  return qId !== null ? Number(qId) : null;
};
