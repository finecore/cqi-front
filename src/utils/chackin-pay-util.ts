import _ from 'lodash';
import dayjs from 'dayjs';
import format from '@/utils/format-util';
import { USER } from '@/store/mutation_types';

// 시즌 프리미엄 요금 적용 여부.
export const getSeason = ({
  room_state, // 숙박 유형 정보 (입실일, 퇴실일, 숙박 종류등)
  season_premiums, // 시즌 프리미엄 정보
}) => {
  // console.log('-> getSeason', { room_state, season_premiums });

  // 이용 기간(숙박/장기만 해당)
  let usePeriod = dayjs(
    dayjs(room_state.check_out_exp).format('YYYYMMDD')
  ).diff(dayjs(dayjs(room_state.check_in_exp).format('YYYYMMDD')), 'day');

  // 0박은 1박으로 한다
  usePeriod = usePeriod || 1;

  // 시즌 프리미엄 요금 적용 여부.
  let season = _.filter(season_premiums, (v, k) => {
    // console.log("- season", v);
    let isSeason = false;
    for (let i = 0; i <= usePeriod; i++) {
      const mmdd = dayjs(room_state.check_in_exp).add(i, 'day').format('MMDD');

      // console.log("- day", mmdd, v.begin, v.end);

      // 기간이 다음 년도 포함 시.
      if (Number(v.begin) > Number(v.end)) {
        isSeason =
          (Number(mmdd) <= Number('1231') && Number(mmdd) >= Number(v.begin)) ||
          (Number(mmdd) >= Number('0101') && Number(mmdd) <= Number(v.end)); // 시즌에 속하는지 여부.
      } else {
        isSeason =
          Number(v.begin) <= Number(mmdd) && Number(v.end) >= Number(mmdd); // 시즌에 속하는지 여부.
      }

      if (isSeason) break;
    }

    //  console.log("- isSeason", isSeason);
    return isSeason;
  })[0];

  console.log('-> getSeason done', season);

  return season;
};

// 요금 계산.
export const getFee = (
  store: any,
  {
    selection, // 객실 유형 (숙박:1/대실:2/장기:3)
    preferences, // 환경 설정 정보
    room_type, // 객실 타입 정보
    room_state, // 객실 상태 정보
    season_premiums, // 시즌 프리미엄 정보
    room_sale, // 신규 객실 매출 정보
    newConf, // 신규 객실 환경 설정 정보
  }
) => {
  // console.log('-> getFee ', {
  //   selection,
  //   preferences, // 환경 설정 정보
  //   room_type, // 객실 타입 정보
  //   room_state, // 객실 상태 정보
  //   season_premiums, // 시즌 프리미엄 정보
  // });

  let newState = { ...room_state };

  newState.sale = selection; // 초기 값으로 진행 시 0으로 설정되어서 selection 넣어준다.

  let { default_add_point_rate } = preferences;
  let { room_id, sale, check_in_exp, check_out_exp } = newState;

  let {
    id: room_type_id,
    default_fee_rent,
    default_fee_stay,
    reserv_discount_fee_rent,
    reserv_discount_fee_stay,
    person_add_fee,
  } = room_type;

  let reserv_fee =
    selection === 2 ? reserv_discount_fee_rent : reserv_discount_fee_stay; // 예약 할인 요금

  // 객실 기본 요금
  let default_fee = selection === 2 ? default_fee_rent : default_fee_stay;

  // 추가 요금(예약 할인 요금 + 시간 추가 + 요일추가 요금 합산
  let add_fee = -reserv_fee;
  // 옵션 요금(시간 추가 + 인원 추가)
  let option_fee = 0;

  const user = store.getters[USER];

  console.log('- ', {
    selection,
    room_type_id,
    default_fee,
    reserv_fee,
    add_fee,
  });

  const day = dayjs().isoWeekday();
  const hour = dayjs().format('HH');
  const hhmm = dayjs().format('HHmm');

  // 기본 숙박 시간
  let stayTime = day > 5 ? preferences.stay_time_weekend : preferences.stay_time;

  // 기본 대실 시간
  let rentTime =
    Number(hour) < 12 // 오전
      ? day > 5 // 주말
        ? preferences.rent_time_am_weekend
        : preferences.rent_time_am
      : day > 5
      ? preferences.rent_time_pm_weekend
      : preferences.rent_time_pm;

  rentTime = rentTime < 1 ? 1 : rentTime;

  // 요일 / 시간 요금.
  let fee = undefined;

  // 객실 타입 요금 조회.
  store
    .dispatch('roomFee/getListByroomTypeId', {
      room_type_id,
    })
    .then(({ success, count, room_fees }) => {
      // 해당 요일의 해당 시간대의 추가 요금 조회.
      _.each(room_fees, (v) => {
        // 적용 채널 (0: 카운터, 1:자판기)
        if (v.channel === 0 && v.stay_type === selection) {
          let st = Number(v.begin);
          let et = Number(v.end);
          let nt = Number(hhmm);

          // console.log('- roomFee v', { v, st, et, nt });

          // 이전일 ~ 다음일 시간 이라면.
          if (st > et) {
            if (st <= nt || et > nt) {
              fee = v;
            }
          } else {
            if (st <= nt && et > nt) {
              fee = v;
            }
          }
        }
      });
    });

  // 현재 요일 시간 추가 요금 적용.
  if (fee) add_fee = fee.add_fee;

  // 이용 기간(숙박/장기만 해당)
  let usePeriod = dayjs(dayjs(check_out_exp).format('YYYYMMDD')).diff(
    dayjs(dayjs(check_in_exp).format('YYYYMMDD')),
    'day'
  );

  // 0박은 1박으로 한다
  usePeriod = usePeriod || 1;

  // 입실 시간 옵션 추가 요금 적용.
  if (newState.time_option_fee) option_fee += newState.time_option_fee;

  // 인원 추가 요금.
  if (room_sale.person > preferences.person)
    option_fee += person_add_fee * (room_sale.person - preferences.person);

  // console.log( '- 옵션 요금 ', option_fee);

  // 시즌 프리미엄 요금 적용 여부.
  let season = getSeason({ room_state, season_premiums });
  let season_fee = 0;

  // 시즌
  if (!newState.room_reserv_id && season && season.premium) {
    // 입실 시간 기준 일때만 성수기/비성수기 이용 시간 추가.
    if (preferences.stay_time_type === 0 && season.add_stay_time !== 0)
      stayTime += season.add_stay_time;

    if (season.add_rent_time !== 0) rentTime += season.add_rent_time;

    // 시즌 추가 요금
    season_fee += season.premium;
  }

  // 숙박 시 사용 일자에 곱한다.
  if (sale !== 2 && usePeriod > 1) {
    default_fee *= usePeriod;
  }

  let newSale = {
    // 매출 정보.
    ...room_sale,

    id: room_state?.room_sale_id || null, // 정보 변경이라면 id 존재.
    channel: 'pad', // 매출 채널 (web, isc, pad)
    room_id,
    user_id: user.id, // 현재 직원 id
    state: 'A', // 정상
    stay_type: sale, // 숙박 형태.
    phone: newConf?.phoneNo, // 전화번호
    member_id: newConf?.memberId, // 회원 id

    check_in_exp: dayjs(room_sale.check_in_exp).format('YYYY-MM-DD HH:mm:ss'), // 입실 예정일시
    check_out_exp: dayjs(room_sale.check_out_exp).format('YYYY-MM-DD HH:mm:ss'), // 퇴실 예정일시
    check_in: dayjs(room_sale.check_in_exp).format('YYYY-MM-DD HH:mm:ss'), // 입실 일시 (바로 입실 처리됨)

    memo: newState.notice, // 메모

    save_point: newConf?.roomFee * (default_add_point_rate / 100), // 적립 포인트
    car_no: newConf?.carNum, // 차량 번호

    // 요금 설정.
    discount_fee: newConf?.discountAmt || 0,
    receive_fee: newConf?.receiveAmt || 0,

    stay_days: usePeriod,
  };

  console.log('- getFee done', { ...newSale });

  // 설정값 저장
  store.dispatch('setData', {
    key: 'newSale',
    value: newSale,
  });

  store.dispatch('setData', {
    key: 'newState',
    value: newState,
  });

  return room_sale;
};

/*
  체크인 현금 결제
*/
export const payCash = ({
  fee, // 결제 금액
  pay_point_amt, // 마일리지 결제 금액
  pay_cash_amt, // 현금 결제 금액
  pay_card_amt, // 카드 결제 금액
}) => {
  fee = Number(format.toNumber(fee));
  pay_cash_amt = Number(format.toNumber(pay_cash_amt));
  pay_card_amt = Number(format.toNumber(pay_card_amt));
  pay_point_amt = Number(format.toNumber(pay_point_amt));

  console.log('- payCash ', {
    fee,
    pay_point_amt,
    pay_cash_amt,
    pay_card_amt,
  });

  fee -= pay_point_amt;

  if (pay_cash_amt > fee) {
    pay_cash_amt = fee;
    pay_card_amt = 0;
  } else {
    pay_card_amt = fee - pay_cash_amt;
  }

  return {
    pay_cash_amt,
    pay_card_amt,
  };
};

/*
  체크인 카드 결제
*/
export const payCard = ({
  fee, // 결제 금액
  pay_point_amt, // 마일리지 결제 금액
  pay_cash_amt, // 현금 결제 금액
  pay_card_amt, // 카드 결제 금액
}) => {
  fee = Number(format.toNumber(fee));
  pay_cash_amt = Number(format.toNumber(pay_cash_amt));
  pay_card_amt = Number(format.toNumber(pay_card_amt));
  pay_point_amt = Number(format.toNumber(pay_point_amt));

  console.log('- payCard ', {
    fee,
    pay_point_amt,
    pay_cash_amt,
    pay_card_amt,
  });

  fee -= pay_point_amt;

  if (pay_card_amt > fee) {
    pay_card_amt = fee;
    pay_cash_amt = 0;
  } else {
    pay_cash_amt = fee - pay_card_amt;
  }

  return {
    pay_cash_amt,
    pay_card_amt,
  };
};

/*
  체크인 마일리지 결제
*/
export const payMileage = ({ point, nowMileagePoint, pay_point_amt }) => {
  console.log('- payMileage ', {
    point,
    nowMileagePoint,
    pay_point_amt,
  });

  nowMileagePoint = Number(format.toNumber(nowMileagePoint));
  pay_point_amt = Number(format.toNumber(pay_point_amt));

  if (pay_point_amt > point) {
    nowMileagePoint = 0;
    pay_point_amt = point;
  } else {
    if (pay_point_amt < point) {
      nowMileagePoint = point - pay_point_amt;
    } else {
      nowMileagePoint = 0;
      pay_point_amt = point;
    }
  }

  console.log('- payMileage done', {
    nowMileagePoint,
    pay_point_amt,
  });

  return {
    nowMileagePoint,
    pay_point_amt,
  };
};
