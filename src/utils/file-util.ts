import moment from "moment";
import _ from "lodash";
import path from "path";
import fs from "fs";

export const readFile = (dir, name, callback) => {
  const filePath = path.join(appRoot, "../public/upload", dir, name);

  console.log("- readFile", filePath);

  fs.exists(filePath, (exists) => {
    if (exists) {
      fs.readFile(uploadDir, "utf8", (err, data) => {
        // console.log(data);
        callback(err, { name, data });
      });
    }
  });
};

export const readDir = (dir, callback) => {
  console.log("__dirname", __dirname); // "/Users/Sam/node-app/src/api"

  const filePath = path.join("../../public/upload", dir);

  console.log("- readDir", filePath);

  const files = [];

  try {
    fs.readdirSync(filePath).forEach((name) => {
      const data = fs.readFileSync(filePath + "/" + name, "utf8");
      console.log(data);
      files.push({ name, data });
    });

    callback(null, files);
  } catch (err) {
    callback(err, files);
  }
};

export const delFile = (dir, name, callback) => {
  const filePath = path.join(appRoot, "../public/upload", dir, name);

  console.log("- delFile", filePath);

  fs.exists(filePath, (exists) => {
    if (exists) {
      fs.unlink(filePath, (err) => {
        callback(err);
      });
    }
  });
};

export const fileRename = (src, dir, org, re, callback) => {
  if (src) {
    const uploadDir = path.join(appRoot, "../public/upload");

    const filePath = src.substring(src.indexOf(dir));
    const fileName = path.join(uploadDir, filePath);
    const fileReName = fileName.replace(org, re);

    console.log("- fileRename", { fileName, fileReName });

    fs.exists(fileName, (exists) => {
      src = src.replace(org, re); // db 저장할 url

      if (exists) {
        fs.rename(fileName, fileReName, (err) => {
          callback({ err, src });
        });
      } else {
        callback({ err: null, src });
      }
    });
  } else {
    callback({ err: null, src });
  }
};

export const fileNotUseDelete = (dir, callback) => {
  const uploadDir = path.join(appRoot, "../public/upload", dir);

  fs.readdir(uploadDir, (err, filelist) => {
    if (err) callback(err);
    else {
      // console.log(filelist);

      _.each(filelist, (name) => {
        if (name.indexOf("-original-") > -1) {
          const sp = name.split("-");
          const sd = sp[sp.length - 3].replace(/[^\d]/g, "");

          // console.log({ sp, sd });

          if (sd.length === 12) {
            const dt = moment(sd, "YYYYMMDDHHmm");

            if (dt.isValid()) {
              const duration = moment.duration(moment().diff(dt));
              const hours = duration.asHours();

              if (hours > 1) {
                console.log({ hours });

                fs.unlink(uploadDir + "/" + name, () => {
                  console.log("- original file delete", uploadDir + "/" + name);
                });
              }
            }
          }
        }

        callback();
      });
    }
  });
};

export const fileDelete = (src, dir, callback) => {
  if (src) {
    const uploadDir = path.join(appRoot, "../public/upload/");

    const filePath = src.substring(src.indexOf(dir));
    const fileName = path.join(uploadDir, filePath);

    console.log("- fileDelete ", fileName);

    fs.exists(fileName, (exists) => {
      if (exists) {
        fs.unlink(fileName, (err) => {
          console.log("- file delete", fileName);
          callback(err);
        });
      }
    });
  } else {
    callback(null);
  }
};
