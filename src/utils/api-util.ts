import axios, { AxiosInstance } from 'axios';
import { SET_API_ERR } from '@/store/mutation_types';
import dayjs from 'dayjs';
import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from '../api/config';
import { UUID } from '@/store/mutation_types';
// Fetching 상태를 로컬 상태로 관리하는 방식으로 변경
let fetching = false;

const api = {
  // error request
  errorRequest: (dispatch: any, err: any) => {
    if (!err?.response) return;

    console.error('- errorRequest', err, '\n err.response', err.response);

    let error: any = {
      code: 'Network Error',
      message: 'HttpRequest ERR_CONNECTION_REFUSED',
      detail: '',
    };

    if (err.response) {
      const comerr: any = err.response.data.common
        ? err.response.data.common.error
        : error;

      error = {
        code: err.response.status,
        message: err.response.statusText,
        detail: comerr.message,
      };
    } else if (err) {
      const errs = String(err).split(':');

      error = {
        type: SET_API_ERR,
        code: errs[0] === 'TypeError' ? 401 : 400,
        message: errs[1] ? errs[1] : err,
      };
    }

    dispatch('setApiErr', error, { root: true });
    dispatch('hideLoader', null, { root: true });

    fetching = false;

    return err;
  },

  // check response
  checkResponse: (dispatch: any, config: any, res: any) => {
    let { common, body = {} } = res.data;
    let { success = false, error = {} } = common;

    if (res.status !== 200) {
      error = {
        type: SET_API_ERR,
        code: res.status,
        message: res?.data?.message || '',
        detail: res.statusText,
      };

      // console.error('- checkResponse', error);
      dispatch('setApiErr', error, { root: true });
    } else if (!success) {
      if (error.detail && !(error.detail instanceof Array))
        error.detail = [error.detail];
      if (res.status !== 200 && !error.detail)
        error.detail.push(res.statusText);

      // console.error('- checkResponse', error);
      dispatch('setApiErr', error, { root: true });
    }

    fetching = false;

    return { common, body };
  },
};

const getFormData = (json: any) => {
  const formData = new FormData();
  for (const key in json) {
    formData.append(key, json[key]);
  }
  return formData;
};

// Ajax As Axios
let ajax: AxiosInstance | null = null;

const reCleateAjax = (baseURL: string) => {
  // 새 인스턴스 생성 시 baseURL을 명확히 설정
  ajax = axios.create({
    baseURL: (baseURL ? API_REMOTE_URL : API_LOCAL_URL) + '/api/', // 기본 baseURL 설정 (나주에 변경되는걸 앞에 놓아야 적용된다.)
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'channel': baseURL ? 'sync' : 'pad', // 채널 정보 전송
    },
  });

  console.log('===> reCleateAjax ', ajax.defaults.baseURL); // 올바른 baseURL 출력

  // Add a request interceptor
  ajax.interceptors.request.use(
    (config: any) => {
      const { headers, url, method, dispatch, data, loading } = config;

      // 로드 시작.
      fetching = true;

      console.log('===> ', {
        headers,
        url,
        method,
        dispatch,
        data,
        loading,
      });

      // 채널 정보 전송
      // headers.channel = "room";

      // 서버 모드 전송
      headers.server_mode = import.meta.env.VITE_MODE;

      const token =
        headers.channel === 'sync'
          ? sessionStorage.getItem('tokenSync')
          : sessionStorage.getItem('token');

      if (token) {
        headers['x-access-token'] = token;
      } else if (url.indexOf('login') === -1 && !token) {
        console.error('토큰이 필요 합니다.');
        return Promise.reject(config);
      }

      const uuid = sessionStorage.getItem(UUID);
      if (uuid) headers.uuid = uuid;

      console.log('>>>>>> uuid >>>>>>', uuid);

      config.url = url;
      config.time = new Date();

      // file POST 는 form data 로 해야 파라메터 전송됨!(multipart/form-data 일때만 파라메터 전송)
      if (headers['Content-Type'] === 'multipart/form-data') {
        config.data = getFormData(data);
      }

      console.log(
        '>>>>>> axios request >>>>>>\n %c' + method.toUpperCase(),
        'background: #222; color: #bada55',
        API_LOCAL_URL + url.replace(/\s+/g, ''),
        dayjs(config.time).format('mm:ss:SSS')
      );

      console.log(
        'headers: %c' + JSON.stringify(headers, null, 2),
        'background: #ececb8; color: blue'
      );

      if (data) {
        console.log(
          'data: %c' + JSON.stringify(data, null, 2),
          'background: #ececb8; color: blue'
        );
      }
      if (dispatch) {
        // 로딩바 보이기.
        if (loading !== false) dispatch('showLoader', null, { root: true });
      }

      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );

  // Add a response interceptor
  ajax.interceptors.response.use(
    (response: any) => {
      let diff = dayjs
        .utc(
          dayjs(new Date(), 'DD/MM/YYYY HH:mm:ss:SSS').diff(
            dayjs(response.config.time, 'DD/MM/YYYY HH:mm:ss:SSS')
          )
        )
        .format('ss:SSS');

      console.log(
        '<<<<<< axios response <<<<<<\n %c' +
          response.config.method.toUpperCase() +
          '%c  %c ' +
          diff +
          ' ',
        'background: #f66; color: #fff',
        'background: #fff; color: #fff',
        'background: #4b49d7; color: #fff',
        API_LOCAL_URL + response.config.url.replace(/\s+/g, ''),
        '(' +
          dayjs(response.config.time).format('mm:ss:SSS') +
          ' ~ ' +
          dayjs(new Date()).format('mm:ss:SSS') +
          ')',
        '\ndata:',
        response.data
      );

      if (
        !response ||
        !response.data?.common ||
        !response.data?.common.success
      ) {
        const comerr: any = response?.data?.common
          ? response?.data?.common?.error
          : '';

        if (comerr.code !== '200')
          console.error('!!!! api response fail !!!!\n', comerr);
      }

      // 로딩바 숨기기
      if (response.config.dispatch)
        response.config.dispatch('hideLoader', null, { root: true });

      // 로드 완료.
      fetching = false;

      return response;
    },
    function (error) {
      fetching = false;
      return Promise.reject(error);
    }
  );

  return ajax;
};

export { api, ajax, reCleateAjax, fetching };
