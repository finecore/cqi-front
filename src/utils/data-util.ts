import _ from 'lodash';
import dayjs from 'dayjs';

export const dataMerge = (arg: { list: any; rows: any; order?: any }) => {
  let { list, rows, order } = arg;

  // console.log("- dataMerge", { list, rows, order }, rows[0].idx);

  const start = rows.length ? _.findIndex(list, { idx: rows[0].idx }) : -1;
  const end = start + rows.length;

  // console.log("- dataMerge before list length", list.length);

  if (start > -1) {
    // console.log("- data replace", { start, end });

    // 기존 목록에 있다면 업데이트
    // list = list.splice(start, rows.length, rows);

    let items = list.slice(0, start);
    items = items.concat(rows);
    items = items.concat(list.slice(end));
    list = items;
    // console.log("- items", list[start].hp, rows[0].hp);
  } else {
    list = list.concat(rows); // 기존 목록에 추가.
  }

  // console.log("- dataMerge after list length", list.length);

  if (list.length) {
    if (order) list = _.orderBy(list, order.target, order.dir);
  }

  return _.cloneDeep(list);
};

export const dataFilter = (options: any[]) => {
  let filter = '';

  // 배열로 변환.
  if (!Array.isArray(options)) options = [options];

  _.map(options, (option, k) => {
    let {
      table,
      name,
      op,
      value = '',
      from = '',
      to = '',
      fmt = '',
      ptn = '',
      isnull = '',
    } = option;

    console.log('- option', { table, name, op, value, isnull });

    if (value || isnull) {
      if (filter) filter += '|';
      filter += `${table}:${name}:${op}:${value}:`;
    }

    if (from || to) {
      if (!from) from = dayjs().format('YYYY-MM-DD');
      if (filter) filter += '|';
      filter += `${table}:${name}:${op}:${from}:${to}`;
    }

    filter += `:${fmt}:${ptn}`;

    filter += `:${isnull}`;
  });

  // console.log("- filter", filter);

  return filter || '1=1';
};

export const ObjectEquals = (x: any, y: any) => {
  if (x === y) return true;
  // if both x and y are null or undefined and exactly the same

  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects

  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.

  for (var p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor

    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined

    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal

    if (typeof x[p] !== 'object') return false;
    // Numbers, Strings, Functions, Booleans must be strictly equal

    if (!ObjectEquals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }

  for (p in y) {
    if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
    // allows x[ p ] to be set to undefined
  }

  return true;
};
