import moment from "moment";
import _ from "lodash";

export const valid = {
  isEng: (v = "") => !/[^a-z]/gi.test(v),
  isKor: (v = "") => !/[^ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/gi.test(v),
  isEngNum: (v = "") => !/[^a-z|0-9]/gi.test(v),
  isNum: (v = "") => !/[^0-9]/g.test(v),
  isRrno: (v = "") =>
    /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-4][0-9]{6}$/g.test(
      v.replace(/[^\d]/g, "")
    ),
  isBizno: (v = "") =>
    /^(?:[0-9]{3})(?:[0-9]{2})(?:[0-9]{5})$/g.test(v.replace(/[^\d]/g, "")),
  isEmail: (v = "") =>
    /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/gi.test(
      v
    ),
  isPhone: (v = "") =>
    /(^02.{0}|^01.{1}|^05.{1,2}|[0-9]{3})([0-9]{3,4})([0-9]{4})/g.test(
      v.replace(/[^\d]/g, "")
    ),
  isMoney: (v = "") => !/[^0-9,]/g.test(v),
  isSpace: (v = "") => /\s/g.test(v),
  isId: (v = "") => !/[^a-z0-9_]/g.test(v),
  isPwd: (v = "") =>
    !/[^a-z0-9\!\@\#\$\%\^\*\(\)\-_\=\+\\\|\[\]\{\}\;\:\'",\.\<\>\/\?]/gi.test(
      v
    ),
  isSpecial: (v = "") =>
    !/[^a-z0-9\!\@\#\$\%\^\*\(\)\-_\=\+\\\|\[\]\{\}\;\:\'",\.\<\>\/\?]/gi.test(
      v
    ),
};

export const validRules = (rules = {}) => {
  // console.log("- validRules", { rules });

  const RULES = {
    require: [
      (v) =>
        (v !== undefined && v !== "" && v !== null) || "필수 입력 항목 입니다.",
    ],
    userid: [
      (v) => valid.isId(v) || "아이디는 영문/숫자만 입력 가능 입니다.",
      (v) => (v && v.length >= 3) || "아이디는 3자 이상 입니다.",
    ],
    username: [(v) => (v && v.length >= 3) || "이름은 2자 이상 입니다."],
    password: [
      (v) =>
        valid.isPwd(v) || "비밀번호는 영문,숫자,특수문자만 입력 가능 입니다.",
      (v) => (v && v.length >= 5) || "비밀번호는 5자 이상 입니다.",
    ],
    phone: [(v) => valid.isPhone(v) || "전화번호가 올바르지 안습니다."],
    cellphone: [(v) => valid.isPhone(v) || "휴대전화번호가 올바르지 안습니다."],
    email: [(v) => valid.isEmail(v) || "이메일이 올바르지 안습니다."],
    bizno: [(v) => valid.isBizno(v) || "사업자번호가 올바르지 안습니다."],
    sms_auth_key: [
      (v) => valid.isNum(v) || "인증번호는 숫자만 입력 가능 입니다.",
      (v) => (v && v.length === 5) || "인증번호는 5자리 숫자 입니다.",
    ],
    image: [
      (v) =>
        (v !== undefined && v !== "" && v !== null) ||
        "이미지를 선택해 주세요.",
    ],
    ...rules,
  };

  _.map(RULES, (v, k) => {
    // console.log("- rule", { v, k });
    if (k !== "require") {
      k = k + "_require";
      v = [
        (v) => (v !== undefined && v !== "") || "필수 입력 항목 입니다.",
        ...v,
      ];
      RULES[k] = v;
      // console.log("- rule", { v, k });
    }
  });

  // 함수 정의 :rules="[rules.requireVal(), rules.minVal(contractItem.commission, 0), rules.maxVal(contractItem.commission, 100)]"
  RULES.requireVal = (v) =>
    (v !== undefined && v !== "") || "필수 입력 항목 입니다.";
  RULES.minVal = (v, min) => !v || v >= min || `최소 ${min} 이상 입니다.`;
  RULES.maxVal = (v, max) => !v || v <= max || `최대 ${max} 이하 입니다.`;

  // console.log("- RULES", RULES);

  return RULES;
};
