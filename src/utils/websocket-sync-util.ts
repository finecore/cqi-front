import { HOST_URL, SOC_REMOTE_URL } from '@/api/config';
import axios from 'axios';
import { Vue } from '../main';

interface Store {
  [x: string]: any;
  state: {
    auth: {
      user: any;
      uuid: string | null;
    };
    place: {
      item: any;
    };
    ws: {
      uuid: string | null;
    };
  };
  dispatch(action: string, payload?: any): void;
  commit(type: string, payload?: any): void;
  subscribe(listener: (mutation: any) => void): void;
}

class WebSocketSyncUtil {
  private socket: WebSocket | null = null;
  private status: 'connected' | 'disconnected' = 'disconnected';
  private store: Store;
  private computed: any;
  private timer = null;
  private isRemoteSvrCon: boolean;

  constructor(store: Store, computed: any) {
    this.store = store;
    this.computed = computed;

    console.log('- WebSocketSyncUtil', { SOC_REMOTE_URL });
  }

  // 원격 동기화 서버 연결 확인
  async checkInternetConnection() {
    this.isRemoteSvrCon =
      Vue.config.globalProperties.$dataSyncManager.isRemoteSvrCon;
  }

  interval(timerDelay: number): void {
    console.log('- WebSocketSyncUtil interval ', { timerDelay });

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      this.checkInternetConnection();

      const {
        auth: { user },
        place: { item },
      } = this.store.state;

      // 웹소켓에 사용자 매핑(Socket Server)
      if (user && item) {
        this.sendMessage('JOIN_REQUESTED', {
          channel: 'pad',
          user,
          user_place: item,
        });

        setTimeout(() => {
          const {
            ws: { uuid },
          } = this.store.state;

          if (this.isRemoteSvrCon) {
            this.store.dispatch('addUuid', { uuid });
          }
        }, 1000);

        this.interval(1000 * 60); // 1분마다 실행
      } else {
        if (this.status !== 'connected') {
          this.connect();
        }
      }
    }, timerDelay);
  }

  connect(): boolean {
    // 동기화 설정 여부!
    const { cloud_sync_yn = 1 } = this.store.getters('preferences/item');

    console.log(`====> 동기화 여부 <====`, { cloud_sync_yn });

    if (!cloud_sync_yn) {
      return false;
    }

    this.socket = new WebSocket(`wss://${SOC_REMOTE_URL}`);

    console.log('- WebSocketSyncUtil connect ', this.socket);

    this.socket.onopen = () => {
      const webSocket = this.socket as WebSocket;

      const {
        auth: { user },
      } = this.store.state;

      this.status = 'connected';

      console.log('- WebSocketSyncUtil onopen ', this.status);

      if (this.status === 'connected') {
        this.store.dispatch('ws/setWebsocket', { webSocket });

        // 웹소켓에 사용자 매핑(Socket Server)
        this.interval(1000);
      }

      // websocket data receive.
      this.socket!.onmessage = ({ data }) => {
        const message = data ? JSON.parse(data) : {};
        const { type = '', payload } = message;

        console.log(
          '%c <<< websocket receive message <<< ',
          'background: #111; color: #f9fc62',
          { SOC_REMOTE_URL, type, payload }
        );

        if (type) {
          console.log('- WebSocketSyncUtil store.commit ', type);
          this.store.commit(type, { ...payload });
        }
      };

      // websocket data send.
      this.store.subscribe((mutation) => {
        if (mutation.type === 'UPDATE_DATA') {
          this.sendMessage('update', mutation.payload);
        }
      });
    };

    this.socket.onclose = () => {
      this.disconnect();

      setTimeout(() => {
        this.connect();
      }, 2000);
    };

    this.socket.onerror = (err) => {
      console.error('- WebSocketSyncUtil onerror ', err);
    };
  }

  disconnect(): void {
    if (this.socket) {
      this.socket.close();
      this.socket = null;
    }
    this.status = 'disconnected';
    console.error('----> WebSocketSyncUtil disconnected  ');
  }

  sendMessage(type: string, payload: any): void {
    if (this.status !== 'connected') {
      console.error('----> WebSocketSyncUtil disconnected  ');
      return;
    }
    console.log(
      '%c >>> websocket sending message >>> ',
      'background: #641059; color: #fff',
      { type, payload }
    );

    this.socket!.send(JSON.stringify({ type, payload }));
  }
}

export default WebSocketSyncUtil;
