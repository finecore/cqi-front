import _ from 'lodash';
import dayjs from 'dayjs';
import Swal from 'sweetalert2';

// 인터럽트 전체 조회
export const getAllInterrupt = async (store: any, place_id: any) => {
  console.log('- getAllInterrupt ', { place_id });

  // 인터럽트 목록 조회.
  return await store.dispatch('roomInterrupt/getList', place_id);
};

// 인터럽트 객실 조회
export const getInterrupt = async (store: any, room_id: any) => {
  // console.log('- getInterrupt ', { room_id });

  // 인터럽트 목록 조회.
  const promise = await store
    .dispatch('roomInterrupt/getItem', room_id)
    .then(({ success, error, all_room_interrupts }) => {
      return { success, error, all_room_interrupts };
    });

  return promise;
};

// 인터럽트 등록
export const newInterrupt = async (store: any, room_interrupt: any) => {
  console.log('- newInterrupt ', { room_interrupt });

  // 인터럽트 목록 조회.
  const promise = await store
    .dispatch('roomInterrupt/newItem', room_interrupt)
    .then(({ success, error, new_room_interrupt }) => {
      return { success, error, new_room_interrupt };
    });

  return promise;
};

// 인터럽트 수정
export const setInterrupt = async (store: any, room_interrupt: any) => {
  console.log('- setInterrupt ', { room_interrupt });

  // 인터럽트 목록 조회.
  const promise = await store
    .dispatch('roomInterrupt/setItem', room_interrupt)
    .then(({ success, error, new_room_interrupt }) => {
      return { success, error, new_room_interrupt };
    });

  return promise;
};

// 객실 인터럽트 삭제
export const delInterrupt = async (store: any, room_id: any) => {
  console.log('- delInterrupt ', { room_id });

  // 인터럽트 목록 조회.
  const promise = await store
    .dispatch('roomInterrupt/delItem', room_id)
    .then(({ success, error, room_interrupt = {} }) => {
      return { success, error, room_interrupt };
    });

  return promise;
};

// 사용자별 인터럽트 삭제
export const delUserInterrupt = async (
  store: any,
  user_id: number,
  channel: number
) => {
  console.log('- delUserInterrupt ', { user_id });

  // 인터럽트 목록 조회.
  const promise = await store
    .dispatch('roomInterrupt/delUserItem', { user_id, channel })
    .then(({ success, error, room_interrupt = {} }) => {
      return { success, error, room_interrupt };
    });

  return promise;
};

// 인터럽트 추가 삭제
export const changeInterrupt = async (state: any, inttrupt: any) => {
  console.log('- changeInterrupt ', { state });

  let { deleted } = inttrupt || {};

  // 다른 채널에서 변경된 경우 재조회
  state.roomList = _.map(state.roomList, (v, k) => {
    let { room_id } = v;

    if (room_id === inttrupt['room_id']) {
      let roomInterrupt = _.find(state.room_interrupts, { room_id });

      console.log('- roomInterrupt ', { room_id, deleted, roomInterrupt });

      if (roomInterrupt && room_id === roomInterrupt['room_id']) {
        v = { ...v, ...roomInterrupt };
      } else if (!deleted) {
        v = { ...v, ...inttrupt };
      } else if (deleted) {
        let orgItem = _.find(state.roomAllList, { room_id });
        v = { ...orgItem, deleted };
        orgItem.deleted = true; // roomAllList 도 반영.
      }
    }
    return v;
  });

  return state;
};

// 인터럽트 객실 여부
export const isRoomInterrupt = async (state: any) => {
  console.log('- isRoomInterrupt ', { state });

  let { room_interrupts } = state;

  // 다른 채널에서 변경된 경우 재조회
  state.roomList = _.map(state.roomList, (v, k) => {
    let { room_id } = v;
    let roomInterrupt = _.find(room_interrupts, { room_id });

    if (roomInterrupt) {
      if (roomInterrupt.deleted) {
        let orgItem = _.find(state.roomAllList, { room_id });
        if (orgItem) {
          v = { ...v, ...orgItem, deleted: true };
          orgItem.deleted = true; // roomAllList 도 반영.
        }
      } else {
        v = { ...v, ...roomInterrupt };
      }
    }
    return v;
  });

  return state;
};
