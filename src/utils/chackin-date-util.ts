import _ from 'lodash';
import dayjs from 'dayjs';
import Swal from 'sweetalert2';
import format from '@/utils/format-util';
import { Preference, RoomSale } from '@/types';

let isAlert = false;

/*
  숙박 체크인, 체크아웃 일자 계산
  1. 0박 : 입실/퇴실일자 동일, 입실시간 < 퇴실 시간 체크
  2. 1박 이상 : 입실 < 퇴실일자 1일 이상, 입실시간 < 퇴실 시간 체크
  3. 5박 이상 : 장기로 변경 안내 (바로가기 링크)
*/
export const makeStayInOutDate = (
  store: any,
  stayState: any,
  preferences: Preference,
  roomSale: RoomSale
) => {
  console.log('- makeStayInOutDate start', { stayState });

  let { stay_days, check_in_exp, check_out_exp, isFirst } = stayState;

  let { stay_time_type, stay_time_weekend, stay_time } = preferences;

  const day = dayjs().isoWeekday();

  // 기본 숙박 시간
  let stayTime = day > 5 ? stay_time_weekend : stay_time;

  const hour = dayjs().hour();
  const diff = dayjs(check_out_exp).diff(check_in_exp, 'hour');

  // 체크아웃 일시가 현재보다 작다면
  if (dayjs(check_out_exp).isBefore(dayjs())) {
    // type === 'out' &&
    check_out_exp = dayjs().add(stayTime, 'hour').set('minute', 0);

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `퇴실 일자는 현재 시간 보다 <br/>작을 수 없습니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }
  }

  if (stay_days > 4) {
    // 숙박 일자가 5일 이상이라면
    stay_days = 4;

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `숙박 기간은 4일 이하로 <br/>선택해 주세요.`,
        html: '4일 이상 숙박은 장기를 선택해 주세요.',
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }

    check_out_exp = dayjs(check_out_exp).add(-1, 'hour');
  }

  if (isFirst) {
    // 체크인 , 체크아웃 시간 게산
    check_in_exp = dayjs(check_in_exp).format('YYYY-MM-DD HH:mm:ss');
    check_out_exp = dayjs(check_in_exp)
      .add(stay_days, 'day')
      .add(stayTime, 'hour');

    // 숙박 시간 타입 (0: 이용 시간 기준, 1:퇴실 시간 기준)
    if (stay_time_type === 1) {
      check_out_exp = dayjs(check_in_exp)
        .set('hour', stayTime)
        .set('minute', 0)
        .add(stayTime > hour ? 0 : 1, 'day'); // 현재 시간이 퇴실 시간 보다 크다면 1일 추가
    }
  } else {
    check_out_exp = dayjs(check_in_exp)
      .add(stay_days, 'day')
      .set('hour', check_out_exp.hour())
      .set('minute', check_out_exp.minute());
  }

  // 체크 아웃 일시가 체크인 일시보다 작다면
  if (dayjs(check_out_exp).isBefore(dayjs(check_in_exp))) {
    check_out_exp = dayjs(check_in_exp)
      .set('hour', check_in_exp.hour() + stayTime)
      .set('minute', check_out_exp.minute());

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `퇴실 일시는 <br/>입실 일시보다 커야 합니다.`,
        html: `자동으로 입실 시간에서 숙박 기본 시간 (${stayTime}시간)을 더합니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }
  }

  // 0 : 이용시간 기준 현재 숙박 시간(기간)이 기본 숙박 시간(기간)보다 작다면
  if (stay_time_type === 0 && stayTime > diff) {
    // 체크 아웃 일시가 체크인 일시와 같다면
    if (dayjs(check_out_exp).isSame(dayjs(check_in_exp))) {
      check_out_exp = dayjs(check_in_exp)
        .set('hour', check_in_exp.hour() + stayTime)
        .set('minute', check_out_exp.minute());

      if (!isAlert) {
        isAlert = true;
        Swal.fire({
          icon: 'info',
          title: `이용시간 기준 퇴실 일시는 <br/>입실 일시보다 커야 합니다.`,
          html: `퇴실 시간은 <br/>입실 시간에서 숙박 기본 시간 (${stayTime})을 더합니다.`,
          showConfirmButton: true,
          confirmButtonText: '확인',
          timer: 7 * 1000,
        }).then((result) => {
          isAlert = false;
        });
      }
    }
  }

  check_in_exp = dayjs(check_in_exp);
  check_out_exp = dayjs(check_out_exp);

  stay_days = dayjs(check_out_exp.format('YYYY-MM-DD')).diff(
    dayjs(check_in_exp.format('YYYY-MM-DD')),
    'day'
  );

  let selStayType =
    (stay_days === 0 ? '무박 ' : stay_days + '박 ') + (stay_days + 1) + '일';

  stayState.check_in_exp = check_in_exp;
  stayState.check_out_exp = check_out_exp;
  stayState.selStayType = selStayType;
  stayState.stay_days = stay_days;
  stayState.stay_type = 1;
  stayState.isFirst = false;

  console.log('-> makeStayInOutDate room_sale', { ...roomSale });

  if (roomSale.id) {
    let new_check_in_exp = dayjs(roomSale.check_in_exp);
    let new_check_out_exp = dayjs(roomSale.check_out_exp);

    // 현재 숙박 기간 저장(현재 요금 계산시 사용)
    let new_stay_days = dayjs(new_check_in_exp.format('YYYY-MM-DD')).diff(
      new_check_out_exp.format('YYYY-MM-DD'),
      'day'
    );

    let new_room_sale = { ...roomSale, stay_days: new_stay_days };

    // 현재 숙박 기간 저장(현재 요금 계산시 사용)
    store.commit('roomSale/item', { room_sale: new_room_sale });
  }

  console.log('-> makeStayInOutDate done', { ...stayState });

  return stayState;
};

// 대실 체크인, 체크아웃 일자 계산
export const makeRentInOutDate = (store: any, rentState: any, type: number) => {
  console.log('- makeRentInOutDate start', { type });

  let {
    preferences: {
      rent_time_am_weekend,
      rent_time_pm_weekend,
      rent_time_am,
      rent_time_pm,
    },
    selRentInDay,
    selRentOutDay,
    selRentInTime,
    selRentOutTime,
    room_sale,
  } = rentState;

  const day = dayjs().isoWeekday();
  const hour = Number(dayjs().format('HH'));

  // 기본 대실 시간
  let defRentTime =
    Number(hour) < 12 // 오전
      ? day > 5 // 주말
        ? rent_time_am_weekend
        : rent_time_am
      : day > 5
      ? rent_time_pm_weekend
      : rent_time_pm;

  selRentInDay = dayjs().year() + selRentInDay; // 년도 추가 안하면 2021 로 인식함.
  selRentOutDay = dayjs().year() + selRentOutDay;

  let check_in_exp = dayjs(selRentInDay, 'YYYY/MM/DD').add(
    selRentInTime,
    'hour'
  );

  let check_out_exp = dayjs(selRentOutDay, 'YYYY/MM/DD').add(
    selRentOutTime,
    'hour'
  );

  if (type) {
    rentState.selRentOutTime = selRentInTime + type;
    check_out_exp = dayjs(selRentOutDay, 'MM/DD').add(
      rentState.selRentOutTime,
      'hour'
    );
  }

  let rent_time = dayjs(check_out_exp).diff(check_in_exp, 'hour');

  // 체크 아웃 일시가 체크인 일시보다 작다면
  if (rent_time < 1) {
    rent_time = defRentTime;
    check_out_exp = dayjs(check_in_exp).add(rent_time, 'hour');

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `이용시간 기준 퇴실 시간은 <br/>입실 시간보다 커야 합니다.`,
        html: `자동으로 입실 시간에서 대실 기본 시간 (${defRentTime})을 더합니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }
  }
  // 대실 시간 6시간 이상 이라면.
  else if (rent_time > 6) {
    rent_time = 6;
    check_out_exp = dayjs(check_in_exp).add(rent_time, 'hour');

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `대실 시간은 6시간 이내로 <br/>선택해 주세요.`,
        html: ' 6시간 이상 대실은 숙박을 선택해 주세요.',
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }
  }

  rentState.selRentInDay = dayjs(check_in_exp).format('MM/DD');
  rentState.selRentOutDay = dayjs(check_out_exp).format('MM/DD');
  rentState.selRentInTime = dayjs(check_in_exp).hour();
  rentState.selRentOutTime = dayjs(check_out_exp).hour();

  rentState.check_in_exp = check_in_exp;
  rentState.check_out_exp = check_out_exp;
  rentState.rentTime = rent_time;
  rentState.stay_days = 1; // 요금 계산을 위해 1일로 친다.
  rentState.stay_type = 2;
  rentState.isFirst = false;

  if (room_sale.id) {
    let new_room_sale = { ...room_sale, stay_days: 1 };

    // 현재 숙박 기간 저장(현재 요금 계산시 사용)
    store.commit('roomSale/item', { room_sale: new_room_sale });
  }

  console.log('- makeRentInOutDate done', { rentState });

  return rentState;
};

// 장기 체크인, 체크아웃 일자 계산
export const makeLongInOutDate = (store: any, longState: any, type: string) => {
  console.log('- makeLongInOutDate start', { type });

  let {
    preferences: { stay_time_weekend, stay_time },
    stay_months,
    stay_days,
    check_in_exp,
    check_out_exp,
    room_sale,
  } = longState;

  const day = dayjs().isoWeekday();

  // 기본 숙박 시간
  let useTime =
    day > 5 // 주말
      ? stay_time_weekend
      : stay_time;

  if (!check_in_exp) {
    check_in_exp = dayjs();
  }

  stay_days = Number(format.toNumber(stay_days));

  if (type === 'input') {
    stay_months = -1;
  } else if (type === 'month') {
    stay_days = 0;

    _.times(stay_months, (i) => {
      stay_days += check_in_exp.add(i, 'month').daysInMonth();
    });
  }

  check_out_exp = dayjs().add(stay_days, 'day').add(useTime, 'hour');

  // 체크 아웃 일자가 체크인 일자보다 같거나 작다면
  if (stay_days < 1) {
    check_out_exp = dayjs(check_in_exp)
      .add(1, 'day')
      .set('hour', check_in_exp.hour() + useTime)
      .set('minute', check_out_exp.minute());

    if (!isAlert) {
      isAlert = true;
      Swal.fire({
        icon: 'info',
        title: `이용시간 기준 퇴실 일시는 <br/>입실 일시보다 커야 합니다.`,
        html: `퇴실 시간은 <br/>입실 시간에서 숙박 기본 시간 (${useTime})을 더합니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
        timer: 7 * 1000,
      }).then((result) => {
        isAlert = false;
      });
    }
  }

  longState.check_in_exp = check_in_exp;
  longState.check_out_exp = check_out_exp;
  longState.stay_months = stay_months;
  longState.stay_days = stay_days;
  longState.stay_type = 3;
  longState.isFirst = false;

  if (room_sale.id) {
    let new_check_in_exp = dayjs(room_sale.check_in_exp);
    let new_check_out_exp = dayjs(room_sale.check_out_exp);

    // 현재 숙박 기간 저장(현재 요금 계산시 사용)
    let new_stay_days = dayjs(new_check_in_exp.format('YYYY-MM-DD')).diff(
      new_check_out_exp.format('YYYY-MM-DD'),
      'day'
    );

    let new_room_sale = { ...room_sale, stay_days: new_stay_days };

    // 현재 숙박 기간 저장(현재 요금 계산시 사용)
    store.commit('roomSale/item', { room_sale: new_room_sale });
  }

  console.log('- makeLongInOutDate done', { longState });

  return longState;
};
