import _ from 'lodash';
import dayjs from 'dayjs';
import { keyToValue } from '@/constants/key-map';
import Swal, { SweetAlertOptions, SweetAlertIcon, SweetAlertResult } from 'sweetalert2';

/** 콤마 */
const COMMA = ',';

/**
 * UpDown
 */
export enum UpDown {
  DOWN,
  UP,
}

/**
 * UpDownConfig
 */
export interface UpDownConfig {
  min: number | null;  // null 이면 음수 무한대
  max: number | null;  // null 이면 양수 무한대
  step: number;
  stepFirstSlow: boolean; // 첫 스텝은 1씩 느리게.
}

/**
 * UpDownConfig 클래스.
 */
export class UpDownConfigClass implements UpDownConfig {
  min: number | null;
  max: number | null;
  step: number;
  stepFirstSlow: boolean;

  constructor(min: number, max: number, step: number, stepFirstSlow: boolean = false) {
    this.min = min;
    this.max = max;
    this.step = step;

    if (stepFirstSlow) this.stepFirstSlow = stepFirstSlow;
    else this.stepFirstSlow = false;
  }
}

/** 숙박일 UpDownConfig */
export const upDownStayConfig: UpDownConfig = new UpDownConfigClass(0, 4, 1);

/** 대실 UpDownConfig */
export const upDownStayShortConfig: UpDownConfig = new UpDownConfigClass(1, 6, 1);

/** 장기 UpDownConfig. 5박 이상은 장기. */
export const upDownStayLongConfig: UpDownConfig = new UpDownConfigClass(5, 100, 1);

/** 장기 UpDownConfig */
export const upDownStayLong2Config: UpDownConfig = new UpDownConfigClass(5, 100, 5);

/** 인원 UpDownConfig */
export const upDownPersonCountConfig: UpDownConfig = new UpDownConfigClass(1, 20, 1);

/** 시간 시 UpDownConfig */
export const upDownHourConfig: UpDownConfig = new UpDownConfigClass(0, 23, 1);

/** 시간 분 UpDownConfig */
export const upDownMinConfig: UpDownConfig = new UpDownConfigClass(0, 55, 5);

/** 횟수 UpDownConfig */
export const upDownRepeatConfig: UpDownConfig = new UpDownConfigClass(0, 3, 1);

/** 온도 UpDownConfig */
export const upDownTempConfig: UpDownConfig = new UpDownConfigClass(10, 36, 1);

/** 금액 UpDownConfig */
export const upDownAmountConfig: UpDownConfig = new UpDownConfigClass(null, null, 1000);

/** 불투명도 UpDownConfig */
export const upDownOpacityConfig: UpDownConfig = new UpDownConfigClass(0, 10, 1);


/**
 * namespace viewUtil
 */
export namespace viewUtil {
  /**
   * 업/다운 처리.
   * @param val 
   * @param upDown 
   * @param config 
   * @returns 
   */
  export const doUpDown = (
    val: number, 
    upDown: UpDown,
    config: UpDownConfig
    ) => {
      if (upDown === UpDown.UP) {

        if (config.stepFirstSlow && val < config.step) {
          val += 1;
        }
        else {
          val += config.step;
        }

        if (config.max !== null && val > config.max) {
          if (config.min !== null) { // 최소값 설정되어 있으면 최소값으로.
            val = config.min;
          } else { // 최소값 설정되어 있지 않으면 최대값으로.
            val = config.max === null ? 0 : config.max;
          }
        }
      }
      else if (upDown === UpDown.DOWN) {
        if (config.stepFirstSlow && val < config.step) {
          val -= 1;
        }
        else {
          val -= config.step;
        }

        if (config.min !== null && val < config.min) {
          if (config.max !== null) { // 최대값 설정되어 있으면 최대값으로.
            val = config.max;
          } else { // 최대값 설정되어 있지 않으면 최소값으로.
            val = config.min === null ? 0 : config.min;
          }
        }
      }
      return val;
  };
  
  /**
   * comma 처리.
   * @param val 
   * @param defaultVal 
   * @returns 
   */
  export const comma = (val: number | string, unit?: string): string => {
    if (val === undefined || val === null) {
      return '';
    }

    const numVal = typeof val === 'string' ? Number(val.trim()) : val;
    const isNegative = numVal < 0;

    if ((!isNegative && numVal < 1000) || (isNegative && numVal > -1000)) {
      if (unit) {  
        return numVal.toString() + ' ' + unit;
      } else {
        return numVal.toString();
      }
    }
  
    const s = Math.abs(numVal).toString();
    const rest = s.length % 3; // xxx=0, xx=2, x=1
    const arr = [];
  
    if (rest !== 0) {
      arr.push(s.substring(0, rest));
    }
    for (let i = rest; i < s.length; i += 3) {
      arr.push(s.substring(i, i + 3));
    }

    let v = '';
    if (unit) {
      v = arr.join(',') + ' ' + unit;
    } else {
      v = arr.join(',');
    }

    if (isNegative) {
      v = '-' + v;
    }
    return v;
  };

  /**
   * comma 처리. 단위는 원.
   * @param val 
   * @returns 
   */
  export const commaWon = (val: number | string, hideZero: boolean = false): string => {
    if (val === undefined || val === null) return null;
    if (hideZero && val === 0) return null;
    return comma(val, '원');
  };

  /**
   * comma 처리. 단위는 P.
   * @param val 
   * @returns 
   */
  export const commaPoint = (val: number | string): string => {
    return comma(val, 'P');
  };

  /**
   * 숙박일 포맷.
   * @param val 
   * @returns 
   */
  export const fmtStay = (val: number): string => {
    if (val === undefined || val === null) {
      return '';
    }
    if (val === 0) {
      return '무박';
    } else {
      return `${val}박 ${val + 1}일`;
    }
  }

  /**
   * 날짜포맷. YYYY-MM-DD
   * @param date 
   * @returns 
   */
  export const dpFormat = (date: any) => {
    return dayjs(date).format('YYYY-MM-DD');
  };

  /**
   * 날짜포맷. YY년 M월 D일 (ddd)
   * @param date 
   * @returns 
   */
  export const dpFormat2 = (date: any) => {
    return dayjs(date).format('YY년 M월 D일 (ddd)');
  };

  /**
   * 스페이스바 키 막기.
   * @param e 
   */
  export const kbNoSpace = (e: KeyboardEvent) => {
    if (e.code === 'Space') {
      e.preventDefault();
    }
  };

  /**
   * 키보드 숫자, 조작키만 허용.
   * @param e 
   */
  export const kbDigit = (e: KeyboardEvent) => {
    // console.log(`*** kbDigit - e.key: `, e);
    let isPass = false;
    if (isNumberKey(e) || isJojakKey(e)) {
      isPass = true;
    }
    if (!isPass) {
      e.preventDefault();
    }
  };

  /** 숫자인지 확인. */
  const isNumberKey = (e: KeyboardEvent): boolean => {
    if (e.key === '0' || e.key === '1' || e.key === '2' || e.key === '3' || e.key === '4'
      || e.key === '5' || e.key === '6' || e.key === '7' || e.key === '8' || e.key === '9'
    ) {
      return true;
    }
    return false;
  };

  /**
   * 조작키인지 확인.
   * @param e 
   * @returns 
   */
  const isJojakKey = (e: KeyboardEvent): boolean => {
    // console.log(`*** isJojakKey - e.code: ${e.code}`);
    return e.key === 'Backspace' ||
          e.key === 'Delete' ||
          e.key === 'ArrowLeft' ||
          e.key === 'ArrowRight' || 
          e.key === 'Tab' ||
          e.key === 'Enter';
  }

  /**
   * 이동전화번호 포맷.
   * @param val 
   * @returns 
   */
  export const fmtMobileNum = (val: string, delemiter: string = ' '): string => {
    if (val === undefined || val === null) return null;

    const s = val.trim();
    if (s.length >= 11) {
      return `${s.substring(0, 3)}${delemiter}${s.substring(3, 7)}${delemiter}${s.substring(7)}`;
    }
    else if (s.length >= 6) {
      return `${s.substring(0, 3)}${delemiter}${s.substring(3, 6)}${delemiter}${s.substring(6)}`;
    }
    else if (s.length > 3) {
      return `${s.substring(0, 3)}${delemiter}${s.substring(3)}`;
    }
    else if (s.length === 3) {
      return `${s.substring(0, 3)}`;
    }
    else {
      return s;
    }
  };

  export const fmtDate = (value: string): string => {
    if (value === undefined || value === null || value === '') return '';
    return dayjs(value).format('YYYY-MM-DD');
  };

  export const fmtTime = (value: string): string => {
    if (value === undefined || value === null || value === '') return '';
    return dayjs(value).format('HH:mm');
  };

  export const fmtDateTime = (value: string): string => {
    if (value === undefined || value === null || value === '') return '';
    return dayjs(value).format('YY-MM-DD / HH:mm');
  };

  export const fmtDateTimeSec = (value: string): string => {
    if (value === undefined || value === null || value === '') return '';
    return dayjs(value).format('YY-MM-DD / HH:mm:ss');
  };
  

  /**
   * 문자열 내의 모든 공백 제거.
   * @param val 
   * @returns 
   */
  export const removeSpace = (val: string) => {
    return val.replace(/\s/g, '');
  };


  /** 사용자 ID 패턴. */
  const USER_ID_PATTERN = new RegExp(/^[a-zA-Z][a-zA-Z0-9]{3,20}$/);

  /**
   * 사용자 ID 유효성 검사하기.
   * @param val 
   * @returns 
   */
  export const isValidUserID = (val: string): boolean => {
    if (val === undefined || val === null) return false;
    return USER_ID_PATTERN.test(val);
  };


  /** 사용자 PWD 패턴. */
  const USER_PWD_PATTERN = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$/);

  /**
   * 사용자 PWD 유효성 검사하기.
   * @param val 
   * @returns 
   */
  export const isValidUserPwd = (val: string): boolean => {
    if (val === undefined || val === null) return false;
    return USER_PWD_PATTERN.test(val);
  };

  
  /** 이메일 패턴. */
  const EMAIL_PATTERN = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/);

  /**
   * 이메일 유효성 검사하기.
   * @param val 
   * @returns 
   */
  export const isValidEmail = (val: string): boolean => {
    if (val === undefined || val === null) return false;
    return EMAIL_PATTERN.test(val);
  };

  
  /** 전화번호 패턴. */
  const PHONE_PATTERN = new RegExp(/^[0-9]{2,3}[-]?[0-9]{3,4}[-]?[0-9]{4}$/);

  /**
   * 전화번호 유효성 검사하기.
   * @param val 
   * @returns 
   */
  export const isValidPhone = (val: string): boolean => {
    if (val === undefined || val === null) return false;
    return PHONE_PATTERN.test(val);
  };


  /**
   * key-map 에서 table, type, key 값을 찾아 value를 제공한다.
   * @param table 
   * @param type 
   * @param key 
   * @returns 
   */
  export const keyText = (table: string, type: string, key: string | number) => {
    return keyToValue(table, type, key);
  };

  /**
   * 문자열로 제공한다.
   * @param value 
   * @param deafultValue 
   * @returns 
   */
  export const toString = (value: any, deafultValue: string = null): string => {
    if (value === undefined || value === null) { // 리턴 deafultValue.
      return deafultValue;
    }
    else if (typeof value === 'string') {
      return value;
    }
    else if (typeof value === 'number') {
      return String(value);
    }
    else if (typeof value === 'object') {
      return JSON.stringify(value);
    }
    else if (typeof value === 'function') { // 리턴 deafultValue.
      return deafultValue;
    }
    else {
      return String(value);
    }
  };

  export const textFilter = (dataObj: any, text: string, key?: string): boolean => {
    // const re = /[-,\s]/gi;
    // text = String(text || "").replace(re, "");
    if (dataObj === undefined || dataObj === null) {
      return false;
    }

    const type = typeof dataObj;

    if (type === 'object') {
      for (const [k, v] of Object.entries(dataObj)) {
        if (v) {
          const value = toString(v);
          if (value.indexOf(text) > -1) {
            // console.log(`*** textFilter - k: ${k}, v: ${v}, value: ${value}`);
            return true;
          }
        }
      }
    }
    else if (type === 'function') {
      return false;
    }
    else {
      const str = toString(dataObj);
      if (str.indexOf(text) > -1) {
        return true;
      }
    }

    return false;
  };

  /**
   * onOff 값에 따라 sentence 에 text 를 추가 또는 제거한 결과를 제공한다.
   * @param sentence 
   * @param text 
   * @param onOff 
   * @param multiLine 
   * @returns 
   */
  export const appendRemoveText = (sentence: string, text: string, onOff: boolean, multiLine: boolean = false): string => {
    const delimiter = multiLine ? '\n' : ', ';
    if (onOff) {
      if (sentence === undefined || sentence === null || sentence.length === 0) {
        sentence = text;
      }
      else if (sentence.indexOf(text) === -1) {
        sentence += delimiter + text;
      }
    } else {
      if (sentence === undefined || sentence === null || sentence.length === 0) {
        sentence = null;
      }
      else {
        sentence = sentence.replace(delimiter + text, '');
        sentence = sentence.replace(text + delimiter, '');
        sentence = sentence.replace(text, '');
        sentence = sentence.trim();
      }
    }
    return sentence;
  };

  /**
   * 빈 값이면 null로 제공.
   * @param val 
   * @returns 
   */
  export const stringNull = (val: string| number): string | null => {
    if (val === undefined || val === null) return null;

    if (typeof val === 'string') {
      val = val.trim();
      if (val.length === 0) return null;
      return val;
    }
    else if (typeof val === 'number') {
      return String(val);
    }
    else {
      return val;
    } 
  };

  /**
   * 빈 값이면 null로 제공.
   * @param val 
   * @returns 
   */
  export const numberNull = (val: string | number): number | null => {
    if (val === undefined || val === null) return null;

    if (typeof val === 'string') {
      val = val.trim();
      if (val.length === 0) return null;
      return Number(val);
    }
    else if (typeof val === 'number') {
      return val;
    }
    else {
      return val;
    } 
  };

  /**
   * Swal alert 창 띄우기.
   * @param icon 
   * @param title 
   * @param message 
   */
  export const swalAlert = (icon: SweetAlertIcon = 'info', title: string, message?: string) => {
    const fireItem = {
      icon,
      title,
      html: message ? message : '',
      showConfirmButton: true,
      confirmButtonText: '확인',      
    } as SweetAlertOptions;

    if (message === undefined || message === null || message.length === 0) {
      delete fireItem.html;
    }

    Swal.fire(fireItem);
  };

  /**
   * 변경된 데이터 없음 알리기.
   * @param title 
   */
  export const swalAlertNoChange = (title: string) => {
    swalAlert('info', title, '변경된 데이터가 없습니다. 확인 후 다시 시도해 주세요.');
  };

  /**
   * C, U, D 진행할지 확인하기.
   * @param title 
   * @param message 
   * @returns 
   */
  export const swalConfirm = async (title: string, message?: string): Promise<SweetAlertResult> => {
    const fireItem = {
      icon: 'question',
      title,
      html: message ? message : '',
      showCancelButton: true,
      showConfirmButton: true,
      cancelButtonText: '취소',
      confirmButtonText: '확인',
    } as SweetAlertOptions;

    if (message === undefined || message === null || message.length === 0) {
      delete fireItem.html;
    }

    return await Swal.fire(fireItem);
  };

  /**
   * 입력된 데이터가 올바르지 않습니다. 메시지 띄우기.
   * @param title 
   * @param msg 
   */
  export const swalInvalidData = (title: string, msg: string[]) => {
    Swal.fire({
      icon: 'info',
      title: title,
      html: `입력된 데이터가 올바르지 않습니다.<br/>아래 항목을 확인해 주세요.<br/><br/>${msg.join('<br/>')}`,
      showConfirmButton: true,
      confirmButtonText: '확인',
    });
  };

  /**
   * newItem, setItem, delItem 결과 alert 창 띄우기.
   * @param title 
   * @param name 
   * @param cud 
   * @param success 
   */
  export const swalAlertResult = (title: string, name: string, cud: string, success: boolean) => {
    let msg = '';
    if (success) {
      msg = `<span class='t-info'>[${name}]</span> ${cud} 되었습니다.`;
    } else {
      msg = `<span class='t-info'>[${name}]</span> ${cud}에 <span class='t-warn'>실패 하였습니다.</span> 확인 후 다시 시도해 주세요.`;
    }
    swalAlert(success ? 'success' : 'error', title, msg);
  };

}
