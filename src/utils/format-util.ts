import dayjs, { Dayjs } from "dayjs";
import _, { toInteger } from "lodash";
import * as Hangul from "hangul-js";

const format = {
    /**
     * 기본 포멧.
     *
     * @param type (rrno, phone, post)
     * @param text
     * @return
     */
    do: (type: string, text: any, delimiter: string) => {
        let formatTxt = text;
        delimiter = delimiter || "-";

        if (type === "rrno") formatTxt = format.toRrno(text, delimiter);
        else if (type === "phone") formatTxt = format.toPhone(text, delimiter);
        else if (type === "date") formatTxt = format.toDate(text, delimiter);
        else if (type === "datetime")
            formatTxt = format.toDateTime(text, delimiter);
        else if (type === "acct") formatTxt = format.toAccount(text, delimiter);
        else if (type === "card") formatTxt = format.toCard(text, delimiter);
        else if (type === "money") formatTxt = format.toMoney(text);
        else {
            console.log("- format : " + type + " delimiter : " + delimiter);

            const ts = type.match(/\(.*?\)/g);

            // console.log( '- ts : ' + ts);

            let pattern = "";
            let replace = "";

            const txt = formatTxt.replace(
                new RegExp("[" + delimiter + "]", "g"),
                "",
            );

            for (const n in ts) {
                pattern += ts[n];
                replace +=
                    "$" +
                    (Number(n) + 1) +
                    (Number(n) < ts.length - 1 ? delimiter : "");

                const re = new RegExp(pattern, "g");

                if (txt.match(re) && txt.match(re).length === 1) {
                    // console.log( '- txt : ' + txt + ' re : ' + re + ' replace : ' + replace + ' test : ' + txt.match(re));
                    formatTxt = txt.replace(re, replace); // RegExp.$1
                    // console.log( '- formatTxt : ' + formatTxt);
                }
            }
        }

        return formatTxt;
    },
    /**
     * Null to Void
     *
     * @param data
     * @returns
     */
    nullToVoid: (data: { [x: string]: any }, name: string | number) => {
        if (data) {
            try {
                if (
                    !data[name] ||
                    data[name] === "null" ||
                    data[name] === "undefined"
                )
                    data[name] = "";
            } catch (e) {
                data[name] = "";
            }
            return data[name];
        }
        return "";
    },
    /**
     * json object -> array
     *
     * @param object
     * @return
     */
    JSONtoArray: (object: { [x: string]: any }) => {
        const results = [];
        for (const property in object) {
            const value = object[property];
            if (value) results.push(property.toString() + ":" + value);
        }
        return results;
    },
    /**
     * array 에서 name 값 반환.
     *
     * @param array ({name=value,name=value...} 형식의 1차원 배열.)
     * @param dilimiter ±¸ºÐÀÚ
     * @return
     */
    ArrayToJSON: (array: { [x: string]: string }, dilimiter: string) => {
        if (dilimiter === undefined) dilimiter = "=";
        const sub = {};
        for (const inx in array) {
            const data = array[inx].split(dilimiter);
            if (data.length > 1) {
                sub[_.trim(data[0])] = _.trim(data[1]);
            }
        }
        return sub;
    },
    /**
     * html 제거.
     */
    stripTags: (str: string) => {
        const RegExpTag = /[<][^>]*[>]/gi;
        str = str.replace(RegExpTag, "");

        const RegExpJS = "<script[^>]*>(.*?)</script>";
        str = str.replace(RegExpJS, "");

        const RegExpCSS = "<style[^>]*>(.*?)";
        str = str.replace(RegExpCSS, "");

        const RegExpDS = /<!--[^>](.*?)-->/gi;
        str = str.replace(RegExpDS, "");

        const RegExpPh = /document.|object|cookie|&/gi;
        str = str.replace(RegExpPh, "");

        return str;
    },
    /**
     * 숫자 컴마 추가.
     *
     * @param numString
     */
    toCurruncy: (numString: string = "0") => {
        let num = toInteger(numString);
        return num.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    },
    /**
     * ISO to UTC Time.
     *
     * @param timeString
     */
    toISO: (
        timeString: Date = new Date(),
        format: string = "YYYY-MM-DD HH:mm:ss",
    ) => {
        return dayjs(timeString).format(format);
    },
    /**
     * 컴마 제거.
     *
     * @param {String} numString
     */
    stripCommas: (numString: string) => {
        return numString ? numString.replaceAll(",", "") : "";
    },
    /**
     * 숫자형으로 변환.
     *
     * @param {String} numStr
     */
    toNumber: (numStr: string) => {
        if (!numStr) return "";

        numStr = numStr + "";

        const m = numStr.substring(0, 1) === "-";
        const ret = numStr.replace(/[^\d]/g, "");

        return m ? "-" + ret : ret;
    },
    /**
     * 실수형으로 변환.
     *
     * @param {String} numStr
     * @param {int} point 소숫점 자릿수.
     */
    toFloat: (numStr: string, point: number) => {
        if (numStr === undefined) numStr = "";
        numStr = numStr + "";
        if (numStr === "") return 0;
        if (point === undefined || point === undefined) point = 2;

        const data = numStr.split(".");
        data[0] = String(parseInt(format.stripCommas(data[0]), 10));
        data[1] = format.stripCommas(data[1]) + "000000000000";
        data[1] = data[1].substring(0, point);
        return parseFloat(data[0] + "." + data[1]);
    },
    /**
     * 날자 형식으로 변환.
     *
     * @param lastDate
     * @param se
     * @returns
     */
    toDateTime: (lastDate: any, se = "-") => {
        const dateStrNoDash = String(format.toNumber(lastDate));
        const date = format.toDate(dateStrNoDash.substring(0, 8), se);
        const time = format.toTime(dateStrNoDash.substring(8), ":");
        return date + " " + time;
    },
    /**
     * 주민번호 형식으로 변환.
     *
     * @param num
     * @param _se
     * @returns
     */
    toRrno: (num: string, _se = "-") => {
        if (num)
            return num.replace(/[^\d]/g, "").replace(/(\d{6})(\d+)/g, "$1-$2");
        else return "";
    },
    /**
     * 전화번호 형식으로 변환.
     *
     * @param num
     * @param se
     * @returns
     */
    toPhone: (num: string, se = "-") => {
        num = String(format.toNumber(num));

        return num
            .substring(0, 11)
            .replace(/^(\d{3})(\d{3,4})(\d{4})/g, "$1" + se + "$2" + se + "$3")
            .replace(/^(\d{3})(\d{3,4})(\d*)/g, "$1" + se + "$2" + se + "$3")
            .replace(/^(\d{3})(\d{1,3})/g, "$1" + se + "$2")
            .trim();
    },
    /**
     * 계좌번호 형식으로 변환.
     *
     * @param num
     * @param se
     * @returns
     */
    toAccount: (num: string, se = "-") => {
        if (num && num !== "") {
            num = num.replace(/[^\d]/g, "");
            if (num.length > 11) {
                num = num.replace(
                    /([0-9]{3})([0-9]{5})([0-9]{3})([0-9]+)/,
                    "$1" + se + "$2" + se + "$3" + se + "$4",
                );
            } else if (num.length > 8) {
                num = num.replace(
                    /([0-9]{3})([0-9]{5})([0-9]+)/,
                    "$1" + se + "$2" + se + "$3",
                );
            } else if (num.length > 3) {
                num = num.replace(/([0-9]{3})([0-9]+)/, "$1" + se + "$2");
            }
            return num;
        } else return "";
    },
    /**
     * 사업자번호 형식으로 변환.
     *
     * @param num
     * @param se
     * @returns
     */
    toBizno: (num: string, se = "-") => {
        if (num)
            return num
                .replace(/[^\d]/g, "")
                .replace(
                    /([0-9]{3})([0-9]{2})([0-9]{4})/,
                    "$1" + se + "$2" + se + "$3",
                );
        else return "";
    },

    /**
     * 카드번호 형식으로 변환.
     *
     * @param num
     * @param se
     * @returns
     */
    toCard: (num: string, se = "-") => {
        if (num)
            return num
                .replace(/[^\d]/g, "")
                .replace(
                    /([0-9]{4})([0-9]{4})([0-9]{4})([0-9]+)/,
                    "$1" + se + "$2" + se + "$3" + se + "$4",
                );
        else return "";
    },
    /**
     * 날자형식으로 변환.
     *
     * @param lastDate
     * @param se
     * @returns
     */
    toDate: (lastDate: any, se = "-") => {
        let re = null;
        let replace = "";

        const dateStrNoDash = String(format.toNumber(lastDate));

        if (dateStrNoDash.length === 4) {
            re = /(\d{4})/;
            replace = "$1";
        } else if (dateStrNoDash.length === 5) {
            re = /(\d{4})/;
            replace = "$1" + se;
        } else if (dateStrNoDash.length === 6) {
            re = /(\d{4})(\d{2})/;
            replace = "$1" + se + "$2";
        } else if (dateStrNoDash.length === 7) {
            re = /(\d{4})(\d{2})/;
            replace = "$1" + se + "$2" + se;
        } else {
            re = /(\d{4})(\d{2})(\d{2})/;
            replace = "$1" + se + "$2" + se + "$3";
        }

        return dateStrNoDash.replace(re, replace);
    },
    /**
     * 남은 시간을 계산하여 일/시/분/초로 반환.
     *
     * @param lastDate - 계산할 목표 시간 (YYYYMMDDHHmmSS 형식)
     * @param _se - 날짜 형식 구분자 (기본값: "-")
     * @returns {days: number, hours: number, minutes: number, seconds: number}
     */
    remainTime: (startDate: string, lastDate: string, _se = "-"): any => {
        const now = new Date(); // 현재 시간
        const dateStrNoDash = String(lastDate).replace(/[^0-9]/g, ""); // 숫자만 추출

        // 입력된 날짜를 Date 객체로 변환
        const year = parseInt(dateStrNoDash.slice(0, 4), 10);
        const month = parseInt(dateStrNoDash.slice(4, 6), 10) - 1; // 월은 0부터 시작
        const day = parseInt(dateStrNoDash.slice(6, 8) || "1", 10);
        const H = parseInt(dateStrNoDash.slice(8, 10) || "0", 10);
        const M = parseInt(dateStrNoDash.slice(10, 12) || "0", 10);
        const S = parseInt(dateStrNoDash.slice(12, 14) || "0", 10);

        const sDate = new Date(startDate);
        const targetDate = new Date(year, month, day, H, M, S);
        const str = sDate || now;

        // 남은 시간 계산
        const diff = targetDate.getTime() - str.getTime();

        // console.log("------>remainTime diff ", { targetDate, now, diff });

        if (diff <= 0) {
            return { days: 0, hours: 0, minutes: 0, seconds: 0 }; // 시간이 이미 지난 경우
        }

        const days = Math.floor(diff / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
            (diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
        );
        const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((diff % (1000 * 60)) / 1000);

        return { days, hours, minutes, seconds };
    },
    // 날짜 포맷팅 함수
    formatDate: (isoDate: string): string => {
        return dayjs(isoDate).format("YYYY년 M월 D일 HH시 mm분");
    },

    /**
     * 시분초 현식으로 변환.
     *
     * @param time
     * @param se
     * @returns
     */
    toTime: (time: string, se = "-") => {
        if (time)
            if (time.length === 6)
                return String(format.toNumber(time)).replace(
                    /(\d{2})(\d{2})(\d{2})/,
                    "$1" + se + "$2" + se + "$3",
                );
            else
                return String(format.toNumber(time)).replace(
                    /(\d{2})(\d{2})(\d{2})(\d{3})/,
                    "$1" + se + "$2" + se + "$3" + "." + "$4",
                );
        else return "";
    },
    /**
     * 금액 형식으로 변환.
     *
     * @param numStr
     */
    toMoney: (numStr: any = "0", _min = undefined, _max = undefined) => {
        return format.toCurruncy(String(numStr));
    },
    /**
     * 한글/영문 바이트 체크.
     */
    strCharByte: (char: string) => {
        if (char.substring(0, 2) === "%u")
            return char.substring(2, 4) === "00" ? 1 : 2;
        else if (char.substring(0, 1) === "%")
            return parseInt(char.substring(1, 3), 16) > 127 ? 2 : 1;
        else return 1;
    },

    /**
     * 문자 배열 검식.
     *
     * @param data
     * @param text
     * @param key
     */
    textFilter: (data: any, text: string, key: string | number) => {
        const re = /[-,\s]/gi;

        text = String(text || "").replace(re, "");

        const list = text
            ? _.filter(data, (item) => {
                  let isFind = false;

                  _.each(item, (v) => {
                      const value = String((key ? v[key] : v) || "").replace(
                          re,
                          "",
                      );
                      if (value) {
                          if (Hangul.search(value, text) > -1) {
                              isFind = true;
                              return false;
                          }
                      }
                  });
                  return isFind;
              })
            : data;

        return list;
    },

    /**
     * 한글 포함 여부 체크.
     */
    isHangul: (str: string) => {
        if (_.trim(str).length === 0) return false;

        let rtnData = false;

        for (let idx = 0; idx < str.length; idx++) {
            const c = escape(str.charAt(idx));
            if (c.indexOf("%u") > -1) {
                rtnData = true;
                break;
            }
        }
        return rtnData;
    },
    /**
     * 종성 여부 체크.
     *
     * @param {String} wd
     */
    isJongsong: (wd: any) => {
        const INDETERMINATE = 0;
        const NOJONGSONG = 1;
        const JONGSONG = 2;

        const word = String(wd); /* 숫자 대비해서 문자열로 변환. */
        const numStr1 = "013678lmnLMN";
        const numStr2 = "2459aefhijkoqrsuvwxyzAEFHIJKOQRSUVWXYZ";

        if (word === null || word.length < 1) return INDETERMINATE;

        const lastChar = word.charAt(word.length - 1);
        const lastCharCode = word.charCodeAt(word.length - 1);

        if (numStr1.indexOf(lastChar) > -1) return JONGSONG;
        else if (numStr2.indexOf(lastChar) > -1) return NOJONGSONG;

        if (lastCharCode < 0xac00 || lastCharCode > 0xda0c) {
            return INDETERMINATE;
        } else {
            const lastjongseong = ((lastCharCode - 0xac00) % (21 * 28)) % 28;
            if (lastjongseong === 0) return NOJONGSONG;
            else return JONGSONG;
        }
    },
    /* 내부함수 (을/를) */
    ul: (s: string) => {
        if (!format.isHangul(s)) return s;
        const ul0 = ["(을)를", "를", "을"];
        return s + ul0[format.isJongsong(s)];
    },
    /* 내부함수 (이/가) */
    ka: (s: string) => {
        if (!format.isHangul(s)) return s;
        const ka0 = ["(이)가", "가", "이"];
        return s + ka0[format.isJongsong(s)];
    },
    /* 내부함수 (은/는) */
    un: (s: string) => {
        if (!format.isHangul(s)) return s;
        const un0 = ["(은)는", "는", "은"];
        return s + un0[format.isJongsong(s)];
    },
    /* 내부함수 (와/과) */
    wa: (s: string) => {
        if (!format.isHangul(s)) return s;
        const arr = ["(와)과", "와", "과"];
        return s + arr[format.isJongsong(s)];
    },
    /**
     * 전각문자로 변환.
     *
     * @param : is 변환할 문자열.
     * @param : isAllNum 모든 문자가 숫자인지 여부.
     * @return
     * @see
     */
    toFullChar: (is: string, isAllNum: any) => {
        if (!is) return;

        if (isAllNum && !format.isHangul(is)) return is;

        let os = "";
        for (let i = 0; i < is.length; i++) {
            const c = is.charCodeAt(i);
            if (c >= 32 && c <= 126) {
                // 전각으로 변환 될 수 있는 문자의 범위.
                if (c === 32)
                    // 스페이스인 경우 ascii 코드 32
                    os = os + unescape("%u" + (12288).toString(16));
                else os = os + unescape("%u" + (c + 65248).toString(16));
            } else {
                os = os + is.charAt(i);
            }
        }
        return os;
    },
    /**
     * 전각을 반각으로 변환.
     *
     * @param is
     * @returns
     */
    toHalfChar: (is: string) => {
        let os = String();
        const len = is.length;
        for (let i = 0; i < len; i++) {
            const c = is.charCodeAt(i);
            if (c >= 65281 && c <= 65374 && c !== 65340) {
                os += String.fromCharCode(c - 65248);
            } else if (c === 8217) {
                os += String.fromCharCode(39);
            } else if (c === 8221) {
                os += String.fromCharCode(34);
            } else if (c === 12288) {
                os += String.fromCharCode(32);
            } else if (c === 65507) {
                os += String.fromCharCode(126);
            } else if (c === 65509) {
                os += String.fromCharCode(92);
            } else {
                os += is.charAt(i);
            }
        }
        return os;
    },

    toUtf8: (s: string) => {
        let c: number;
        let d = "";
        for (let i = 0; i < s.length; i++) {
            c = s.charCodeAt(i);
            if (c <= 0x7f) {
                d += s.charAt(i);
            } else if (c >= 0x80 && c <= 0x7ff) {
                d += String.fromCharCode(((c >> 6) & 0x1f) | 0xc0);
                d += String.fromCharCode((c & 0x3f) | 0x80);
            } else {
                d += String.fromCharCode((c >> 12) | 0xe0);
                d += String.fromCharCode(((c >> 6) & 0x3f) | 0x80);
                d += String.fromCharCode((c & 0x3f) | 0x80);
            }
        }
        return d;
    },
    fromUtf8: (s: string) => {
        let c: number;
        let d = "";
        let flag = 0;
        let tmp = null;
        for (let i = 0; i < s.length; i++) {
            c = s.charCodeAt(i);
            if (flag === 0) {
                if ((c & 0xe0) === 0xe0) {
                    flag = 2;
                    tmp = (c & 0x0f) << 12;
                } else if ((c & 0xc0) === 0xc0) {
                    flag = 1;
                    tmp = (c & 0x1f) << 6;
                } else if ((c & 0x80) === 0) {
                    d += s.charAt(i);
                } else {
                    flag = 0;
                }
            } else if (flag === 1) {
                flag = 0;
                d += String.fromCharCode(tmp | (c & 0x3f));
            } else if (flag === 2) {
                flag = 3;
                tmp |= (c & 0x3f) << 6;
            } else if (flag === 3) {
                flag = 0;
                d += String.fromCharCode(tmp | (c & 0x3f));
            } else {
                flag = 0;
            }
        }
        return d;
    },
    hexToStr: (hex: string) => {
        hex = hex.toString(); // force conversion
        let str = "";
        for (let i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    },
};

export default format;
