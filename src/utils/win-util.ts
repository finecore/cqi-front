import store from '../store';
import moment from 'moment';
import _ from 'lodash';

// let interval: any = [];

// 열린 창의 위치 가져오기
export const getWindowPos = (newWindow: Window) => {
  // console.log("- getWindowPos", newWindow);

  var X = newWindow.screenLeft;
  var Y = newWindow.screenTop;
  var W = newWindow.outerWidth;
  var H = newWindow.outerHeight;

  // console.log("- getWindowPos", { X, Y, W, H });

  return { X, Y, W, H };
};

export const getWindowStyle = (style: any = {}) => {
  console.log('- getWindowStyle', style);

  const dualScreenLeft =
    window.screenLeft !== undefined ? window.screenLeft : window.screenX;
  const dualScreenTop =
    window.screenTop !== undefined ? window.screenTop : window.screenY;

  const browserWidth = window.innerWidth;
  const browserHeight = window.innerHeight;

  let { width = 0, height = 0, left = 0, top = 0 } = style;

  const systemZoom = width / window.screen.availWidth;

  if (width === -1) width = window.screen.width;
  if (height === -1) height = window.screen.height;
  if (left === -1) left = -window.screen.width; // 좌측 모니터
  if (left === -2) left = window.screen.width; // 우측 모니터

  if (!left)
    left += width / 2 / systemZoom + (dualScreenLeft - browserWidth / 2);
  if (!top) top += height / 2 / systemZoom + (dualScreenTop - browserHeight);

  console.log('- res style', {
    width,
    height,
    dualScreenTop,
    dualScreenLeft,
    browserWidth,
    browserHeight,
    systemZoom,
    left,
    top,
  });

  return `scrollbars=auto,
          width=${width},
          height=${height},
          top=${top},
          left=${left}`;
};

export const openwin = async (url: string, target: string, style: any) => {
  console.log('- openwin', { url, target, style });

  let winPop = store.state.com.winRefs[url];
  // let winPos = store.state.com.winPos[url];

  // console.log('- store', winPop);

  if (!winPop || winPop.closed) {
    // style = getWindowStyle(style);

    // if (winPos)
    //   style = {
    //     ...style,
    //     left: winPos.X,
    //     top: winPos.Y,
    //     width: winPos.W,
    //     height: winPos.H,
    //   };

    const winFeatures = `width=${style.width},height=${style.height},popup=1`;
    console.log(`*** openwin - options: ${winFeatures}`);

    winPop = window.open('/' + url, target, winFeatures);

    store.dispatch('addWinRefs', { key: url, value: winPop });

    // TEST 출력.
    if (store.state.com.winRefs) {
      for (const [key, winPop] of Object.entries(store.state.com.winRefs)) {
        console.log(`*** winPop :: key: ${key} --->`, winPop);
      }
    }

    // let intv = setInterval(() => {
    //   let winPos = store.state.com.winPos[url];
    //   let pos = getWindowPos(winPop); // 새 창의 현재 위치

    //   if (
    //     !winPos ||
    //     winPos.X !== pos.X ||
    //     winPos.Y !== pos.Y ||
    //     winPos.W !== pos.W ||
    //     winPos.H !== pos.H
    //   ) {
    //     store.dispatch('addWinPos', { key: url, pos });

    //     // console.log("- addWinPos", { url, pos });
    //   }
    // }, 1000 * 10);
    // interval[url] = intv;
  } else {
    winPop.focus();
  }

  return winPop;
};

export const closewin = async (url: string) => {
  console.log('- closewin', { url });
  let winPop = store.state.com.winRefs[url];

  if (winPop) {
    winPop.close();
    store.dispatch('delWinRefs', { key: url });
  }

  // let intv = interval[url];
  // if (intv) clearInterval(intv);
};
