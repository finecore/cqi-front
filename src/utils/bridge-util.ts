// import { useStore } from "vuex";
import store from '@/store';
import { roomStateBiz } from '@/biz/roomStateBiz';
import { roomViewBiz } from '@/biz/roomViewBiz';
import { roomViewItemBiz } from '@/biz/roomViewItemBiz';
import { interruptBiz } from '@/biz/interruptBiz';
import { popupBiz, SyncType } from '@/biz/popupBiz';
import { roomSaleBiz } from '@/biz/roomSaleBiz';

/** global 선언 */
declare global {
  interface Window {
    syncWindow: Function;
    syncPopup: Function;
    syncOperner: Function;
    closePopup: Function;
  }
}

/** 채널 FRONT */
const CHANNEL_FONT = 1;

window.closePopup = (link: string) => {
  // console.log(`*** closePopup - link::`, link);
  popupBiz.doClosePopup(store, link);
};

/**
 * 각 팝업 마다 호출해 동기 시킨다.
 * @param syncType
 * @param value
 */
window.syncPopup = (syncType: SyncType, value: any) => {
  console.log(`*** ----- syncPopup - syncType: ${syncType}, value: ${value}`);

  if (syncType === SyncType.SOC_ROOM_STATE_ITEM) {
    const place_id: number = value;
    roomStateBiz.roomStateAll(store, place_id);
    roomSaleBiz.roomSaleListByPlaceId(store, place_id);
  }
  else if (syncType === SyncType.SOC_ROOM_INTERRUPT_ITEM) {
    const place_id: number = value;
    interruptBiz.getAllInterrupt(store, place_id);
  }
  else if (syncType === SyncType.ROOM_VIEWS) {
    const place_id: number = value;
    roomViewBiz.roomViews(store, place_id);
  }
  else if (syncType === SyncType.ROOM_VIEW_ITEMS) {
    const view_id: number = value;
    roomViewItemBiz.roomViewItems(store, view_id);
  }
  else if (syncType === SyncType.ROOM_INTERRUPT_DEL_USER) {
    const user_id: string = value;
    interruptBiz.delUserInterrupt(store, user_id, CHANNEL_FONT);
  }
  else if (syncType === SyncType.PING) {
    const message = value;
    console.log('*** syncPopup - ping 받음! ---> 퐁!. message: ', message);
  }
};

/**
 * oopener 동기화 호출.
 * @param syncType 
 * @param value 
 */
window.syncOperner = (store: any, syncType: SyncType, value?: any) => {

  window.syncPopup(syncType, value); // opener 동기화.

  const winRefs = store.getters['winRefs'];
  console.log(`*** syncOperner - winRefs ::`, winRefs);
  if (winRefs !== undefined) {
    for (const [key, winPop] of Object.entries(winRefs)) {
      console.log(`*** winPop :: key: ${key} --->`, winPop);
      const pop = winPop as any;
      if (pop.syncPopup) {
        pop.syncPopup(syncType, value); // popup 동기화.
      }
    }
  }
};

export default {
};
