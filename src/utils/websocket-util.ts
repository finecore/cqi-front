import { HOST_URL, SOC_LOCAL_URL } from '@/api/config';

interface Store {
  state: {
    auth: {
      user: any;
      uuid: string | null;
    };
    place: {
      item: any;
    };
    ws: {
      uuid: string | null;
    };
  };
  dispatch(action: string, payload?: any): void;
  commit(type: string, payload?: any): void;
  subscribe(listener: (mutation: any) => void): void;
}

class WebSocketUtil {
  private socket: WebSocket | null = null;
  private status: 'connected' | 'disconnected' = 'disconnected';
  private store: Store;
  private computed: any;
  private timer = null;
  private socLocalUrl: string;

  constructor(store: Store, computed: any) {
    this.store = store;
    this.computed = computed;

    this.socLocalUrl =
      (import.meta.env.VITE_USE_HTTPS === 'Y' ? 'wss://' : 'ws://') +
      SOC_LOCAL_URL;

    console.log('- WebSocketUtil Created', this.socLocalUrl);
  }

  interval(timerDelay: number): void {
    console.log('- WebSocketUtil interval ', { timerDelay });

    if (this.timer) clearInterval(this.timer);

    this.timer = setInterval(() => {
      const {
        auth: { user },
        place: { item },
      } = this.store.state;

      // 웹소켓에 사용자 매핑(Socket Server)
      if (user && item) {
        this.sendMessage('JOIN_REQUESTED', {
          channel: 'pad',
          user,
          user_place: item,
        });

        setTimeout(() => {
          const {
            ws: { uuid },
          } = this.store.state;

          this.store.dispatch('addUuid', { uuid });
        }, 1000);

        this.interval(timerDelay * 60);
      } else {
        if (this.status !== 'connected') {
          this.connect();
        }
      }
    }, timerDelay);
  }

  connect(): void {
    this.socket = new WebSocket(this.socLocalUrl);

    console.log('- WebSocketUtil connect ', this.socket);

    this.socket.onopen = () => {
      const webSocket = this.socket as WebSocket;

      const {
        auth: { user },
      } = this.store.state;

      this.status = 'connected';

      console.log('- WebSocketUtil onopen ', this.status);

      if (this.status === 'connected') {
        this.store.dispatch('ws/setWebsocket', { webSocket });

        // 웹소켓에 사용자 매핑(Socket Server)
        this.interval(1000);
      }

      // websocket data receive.
      this.socket!.onmessage = ({ data }) => {
        const message = data ? JSON.parse(data) : {};
        const { type = '', payload } = message;

        console.log(
          '%c <<< websocket receive message <<< ',
          'background: #111; color: #f9fc62',
          { SOC_LOCAL_URL, type, payload }
        );

        if (type) {
          console.log('- WebSocketUtil store.commit ', type);
          this.store.commit(type, { ...payload });
        }
      };

      // websocket data send.
      this.store.subscribe((mutation) => {
        if (mutation.type === 'UPDATE_DATA') {
          this.sendMessage('update', mutation.payload);
        }
      });
    };

    this.socket.onclose = () => {
      this.disconnect();

      setTimeout(() => {
        this.connect();
      }, 2000);
    };

    this.socket.onerror = (err) => {
      console.error('- WebSocketUtil onerror ', err);
    };
  }

  disconnect(): void {
    if (this.socket) {
      this.socket.close();
      this.socket = null;
    }
    this.status = 'disconnected';
    console.error('----> WebSocketUtil disconnected  ');
  }

  sendMessage(type: string, payload: any): void {
    if (this.status !== 'connected') {
      console.error('----> WebSocketUtil disconnected  ');
      return;
    }
    console.log(
      '%c >>> websocket sending message >>> ',
      'background: #641059; color: #fff',
      { type, payload }
    );

    this.socket!.send(JSON.stringify({ type, payload }));
  }
}

export default WebSocketUtil;
