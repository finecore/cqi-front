import { Low, LowSync, JSONFileSync, LocalStorage, JSONFile } from "lowdb";
import lodash from "lodash";
import { Post } from "./scheme";

export const init = async () => {
  console.log("lowdb init start!");

  type Data = {
    posts: Post[];
  };

  // Extend Low class with a new `chain` field
  class LowWithLodash<T> extends Low<T> {
    chain: lodash.ExpChain<this["data"]> = lodash.chain(this).get("data");
  }

  const adapter = new JSONFile<Data>("db.json");
  const db = new LowWithLodash(adapter);

  await db.read();

  const dbcon = db.chain;
  const data = db.data;

  // Instead of db.data use db.chain to access lodash API
  const post = dbcon.get("posts").find({ id: 1 }).value();

  console.log("lowdb init success!");

  return { dbcon, data };
};
