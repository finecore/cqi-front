import dayjs from "dayjs";

/**
 * enum 객체의 value를 name으로 바꿔 제공하기.
 * @param enumObj 
 * @param value 
 * @returns 
 */
export const enumName = (enumObj: object, value: number | string): string => {
  if (!Object.hasOwn(enumObj, '_name_')) {
    return null;
  }

  const name = enumObj['_name_'];
  const nameParts = name.split('-');
  if (nameParts.length !== 2) {
    console.warn(`*** enum 객체의 _name_ 형식이 잘못 되었습니다.('-'없음) -> 이름: ${name}`);
    return null;
  }

  const cat1 = nameParts[0];
  const cat2 = nameParts[1];

  switch (cat1) {
    case 'booking': return booking.nameOf(cat2, value);
    case 'subscribe': return subscribe.nameOf(cat2, value);
    default: return null;
  }
};


// /**
//  * --------------------------------------------------------
//  * roomType.
//  * --------------------------------------------------------
//  */
// export namespace eRoomType {
  
// }


/**
 * --------------------------------------------------------
 * booking.
 * --------------------------------------------------------
 */
export namespace booking {

  /** 예약상태 */
  export enum State {
    _name_ = 'booking-State',
    NORMAL = 'A', // 정상예약.
    CANCEL = 'B', // 취소예약.
    USED = 'C',   // 사용완료.
  }

  /** 숙박형태 */
  export enum StayType {
    _name_ = 'booking-StayType',
    STAY = 1,   // 숙박.
    SHORT = 2,  // 대실.
    LONG = 3,   // 장기.
  }

  /** OTA */
  export enum Ota {
    _name_ = 'booking-Ota',
    YANOLJA = 1,  // 야놀자.
    YEUGINN = 2,  // 여기어때.
    NAVER = 3,    // 네이버.
    AIRBNB = 4,   // 에어비앤비.
    HOTELNOW = 5, // 호텔나우.
    ETC = 8,      // 기타.
    DIRECT = 9,   // 직접예약.
  }

  /** 방문형태 */
  export enum VisitType {
    _name_ = 'booking-VisitType',
    WALK = 1,   // 도보방문.
    CAR = 2,    // 차량방문.
    PUBLIC = 3, // 대중교통.
  }


  /** enum 이름 제공하기 */
  export const nameOf = (cat2: string, value: number | string): string => {
    if (cat2 === 'State') {
      switch (value) {
        case State.NORMAL: return '정상예약';
        case State.CANCEL: return '취소예약';
        case State.USED: return '사용완료';
      }
    }
    else if (cat2 === 'StayType') {
      switch (value) {
        case StayType.STAY: return '숙박';
        case StayType.SHORT: return '대실';
        case StayType.LONG: return '장기';
      }
    }
    else if (cat2 === 'Ota') {
      switch (value) {
        case Ota.YANOLJA: return '야놀자';
        case Ota.YEUGINN: return '여기어때';
        case Ota.NAVER: return '네이버';
        case Ota.AIRBNB: return '에어비앤비';
        case Ota.HOTELNOW: return '호텔나우';
        case Ota.ETC: return '기타';
        case Ota.DIRECT: return '직접예약';
      }
    }
    else if (cat2 === 'VisitType') {
      switch (value) {
        case VisitType.WALK: return '도보방문';
        case VisitType.CAR: return '차량방문';
        case VisitType.PUBLIC: return '대중교통';
      }
    }

    return null;
  };
}

/**
 * --------------------------------------------------------
 * subscribe.
 * --------------------------------------------------------
 */
export namespace subscribe {
  
  /** 구독 서비스의 구독 상태. */
  export enum State {
    _name_ = 'subscribe-State',
    UNSUBSCRIBE = 0,  // 미구독.
    SUBSCRIBE = 1,    // 구독중.
    CANCEL = 2,       // 구독취소.
  }
  
  /** 결재 주기 */
  export enum PayTerm {
    _name_ = 'subscribe-PayTerm',
    MONTH = 'M',      // 매월.
    // YEAR = 2,      // 매년. (TOBE: 차후 논의)
  }

  /** 업소구독기록의 기록상태. */
  export enum HistState {
    _name_ = 'subscribe-HistState',
    FIRST_SUBSCRIBE = 1,  // 첫구독.
    RE_SUBSCRIBE = 2,     // 재구독.
    CONTINUED = 3,        // 연속구독.
    CANCEL = 4,           // 구독취소.
  }


  /** enum 이름 제공하기 */
  export const nameOf = (cat2: string, value: number | string): string => {
    if (cat2 === 'State') {
      switch (value) {
        case State.UNSUBSCRIBE: return '미구독';
        case State.SUBSCRIBE: return '구독중';
        case State.CANCEL: return '구독취소';
      }
    }
    else if (cat2 === 'PayTerm') {
      switch (value) {
        case PayTerm.MONTH: return '매월';
        // case PayTerm.YEAR: return '매년';
      }
    }
    else if (cat2 === 'HistState') {
      switch (value) {
        case HistState.FIRST_SUBSCRIBE: return '첫구독';
        case HistState.RE_SUBSCRIBE: return '재구독';
        case HistState.CONTINUED: return '연속구독';
        case HistState.CANCEL: return '구독취소';
      }
    }

    return null;
  };
}
