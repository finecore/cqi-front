//==============================================================
// 상수 정의.
//==============================================================
export namespace Consts {
  
  /** 채널 */
  export const Channel = {
    FRONT: 'front',
    MOBILE: 'mobile',
    ISC: 'isc',
    DEVICE: 'device',
    API: 'api',
    PMS: 'pms',
    PAD: 'pad',
    WATCH: 'watch',
  }

  /** 채널 숫자형태 */
  export const ChannelNumber = {
    FRONT: 1,
  }

  /** 숙박 유형 */
  export enum StayType {
    EMPTY = 0,
    STAY = 1,
    SHORT_TIME = 2,
    LONG_TIME = 3,
  }

  /** 객실상태 서브상태. 긴급성이 높은것 부터. */
  export enum SubState {
    INSPECT_WAIT = 1, // 점검대기.
    INSPECT = 2,      // 점검중.
    INSPECTED = 3,    // 점검완료.

    CLEANED = 11,     // 청소완료.
    CLEANING = 12,    // 청소중.
    CLEAN = 13,       // 청소요청.

    THEFT = 21,       // 도난.
    OUTING = 22,      // 외출.
    ALARM = 23,       // 알람.
    RESERVATION = 24, // 예약.

    NONE = 0,         // 없음.
  }

  /** 판매 상태 */
  export enum SaleState {
    ING = 'A',       // 매출등록.
    CANCEL = 'B',    // 입실취소.
    COMPLETE = 'C',  // 정산완료.
  }

  /** 예약 상태 */
  export enum ReservationState {
    ING = 'A',       // 정상예약.
    CANCEL = 'B',    // 취소예약.
    COMPLETE = 'C',  // 사용완료.
  }

};
