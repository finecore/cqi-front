import subprocess

def find_and_move_window_to_monitor(title, monitor_number):
    print(f"Window find a window with the title: {title}, monitor_number: {monitor_number}")

    # Shell 명령어로 창 타이틀과 위치 가져오기
    get_window_list_cmd = "osascript -e 'tell application \"System Events\" to get {name, position} of every window of every process'"
    output = subprocess.check_output(get_window_list_cmd, shell=True).decode("utf-8").strip()

    print(f"get_window_list_cmd output: {output}")

    # 창 타이틀과 위치를 분리하여 파싱
    windows = output.replace(",,", ", ").split(", ")

    print(f"output -> windows: {windows}")

    found_window = False
    window_position = None
    for i in range(0, len(windows) - 1, 2):
        print(f"title: {title}, windows: {windows[i]}")

        windows_names = windows[i].split(" - ")

        if len(windows_names) == 2:
            if title in windows_names[0]:
                found_window = True
                wwindow_position = windows[i].replace("}", "").replace("{", "").split(" ")
                print(f"found_window window_position: {window_position}")
                break

    if not found_window:
        print(f"Cannot find a window with the title: {title}")
        return

    # 모니터 번호를 기준으로 창 이동
    # monitor_offset = monitor_number * int(window_position[0])
    # move_window_cmd = f"osascript -e 'tell application \"System Events\" to set position of front window of process \"{title}\" to {{{monitor_offset}, {window_position[1]}}}'"
    #  move_window_cmd = f"osascript -e 'tell application \"System Events\" to set position of front window of process \"{title}\" to {{0, {monitor_offset}}}'"
    # subprocess.run(move_window_cmd, shell=True)
    # print("Window successfully moved!")

if __name__ == "__main__":
    title = "NAVER"  # 찾으려는 창의 타이틀을 입력하세요
    monitor_number = 1  # 원하는 모니터의 번호를 입력하세요 (0부터 시작)
    find_and_move_window_to_monitor(title, monitor_number)
