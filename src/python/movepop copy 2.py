import pygetwindow as gw
import pyautogui
from screeninfo import get_monitors

def move_window_to_monitor(title, monitor_number):
    # 모니터 정보 가져오기
    monitors = get_monitors()

    if monitor_number > len(monitors) - 1:
        print("Invalid monitor number. Please choose a valid monitor number.")
        return

    # 창을 찾기 위해 타이틀 일부를 사용
    target_window = None
    for window in gw.getAllTitles():
        if title in window:
            target_window = gw.getWindowsWithTitle(window)[0]
            break

    if target_window is None:
        print(f"Cannot find a window with the title: {title}")
        return

    # 선택한 모니터로 창 이동
    monitor = monitors[monitor_number]
    target_window.moveTo(monitor.x, monitor.y)

if __name__ == "__main__":
    title = "NAVER" # 찾으려는 창의 타이틀을 입력하세요
    monitor_number = 1 # 원하는 모니터의 번호를 입력하세요 (0부터 시작)
    move_window_to_monitor(title, monitor_number)
