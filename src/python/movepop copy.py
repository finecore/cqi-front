import subprocess
import re
import applescript
import time

def find_window(title):
    """윈도우 제목(title)에 해당하는 윈도우 아이디를 찾는 함수"""
    cmd = ['osascript', '-e', f'tell application "System Events" to get the id of windows of process "Google Chrome" whose name contains "{title}"']
    result = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
    return result

def move_window(window_id, monitor_num):
    """윈도우 아이디(window_id)에 해당하는 윈도우를 모니터(monitor_num)로 이동시키는 함수"""
    applescript_str = f'tell application "System Events" to set the bounds of window id {window_id} to {{(screen {monitor_num})}}'
    applescript.run(applescript_str)

# 예시: "Google Chrome" 브라우저에서 "NAVER" 탭을 찾아서 모니터 2로 이동시키는 코드
window_ids = find_window('NAVER').split(', ')
for window_id in window_ids:
    move_window(window_id, 2)
    print(f'window id {window_id} moved to monitor 2')
