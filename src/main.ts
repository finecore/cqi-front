import { createApp } from 'vue';

import './styles/style.css';

import App from './App.vue';
import router from './router';
import store from './store';

import vuetify from './plugins/vuetify';
import { loadFonts } from './plugins/webfontloader';
import bridge from './utils/bridge-util';

import mitt from 'mitt';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import isoWeek from 'dayjs/plugin/isoWeek';
import timezone from 'dayjs/plugin/timezone';
import dayOfYear from 'dayjs/plugin/dayOfYear';
import weekOfYear from 'dayjs/plugin/weekOfYear';

import format from '@/utils/format-util';
import DataSyncManager from '@/utils/sync-util';

import 'dayjs/locale/ko';

dayjs.locale('ko');
dayjs.extend(utc);
dayjs.extend(isoWeek);
dayjs.extend(timezone);
dayjs.extend(dayOfYear); //dayOfYear 플러그인 추가 (dayjs().dayOfYear() 사용 가능)
dayjs.extend(weekOfYear); //weekOfYear 플러그인 추가 (dayjs().week() 사용 가능)

// mitt 인스턴스 생성
const emitter = mitt();

loadFonts();

const Vue = createApp(App);

// 글로벌 속성 설정
Vue.config.globalProperties.$theme = store.getters['theme'];
Vue.config.globalProperties.bridge = bridge;

Vue.config.globalProperties.$format = format;
Vue.config.globalProperties.$emitter = emitter;

// DataSyncManager 싱글톤 인스턴스 관리
let dataSyncManagerInstance = null;
const getDataSyncManager = () => dataSyncManagerInstance;

// DataSyncManager 초기화 함수
const initializeDataSyncManager = () => {
  const user = store?.state?.auth?.user;
  const user_sync = store?.state?.auth?.userSync;

  if (user?.id && !dataSyncManagerInstance) {
    dataSyncManagerInstance = new DataSyncManager(store, user, user_sync);
    console.log('DataSyncManager 인스턴스 생성 완료');
  }
};

// store 상태를 구독하여 DataSyncManager 초기화
store.subscribe(() => {
  const user = store.state.auth?.user;
  if (user?.id && !dataSyncManagerInstance) {
    initializeDataSyncManager();
  }
});

// Vue 전역 속성에 DataSyncManager 설정
Vue.config.globalProperties.$dataSyncManager = getDataSyncManager();

// 전역 제공
Vue.provide('emitter', emitter);

// 플러그인 사용
Vue.use(router).use(store).use(vuetify);

// Vue 앱 마운트
Vue.mount('#app');

export { Vue, store, getDataSyncManager };
