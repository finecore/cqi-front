import { WIN_REFS } from '@/store/mutation_types';
import { menuBiz } from '@/biz/menuBiz';

/**
 * 윈도우간 싱크를 위한 타입.
 */
export enum SyncType {
  SOC_ROOM_STATE_ITEM = 'roomState/item', // 소켓으로 받은 'roomState/item'
  SOC_ROOM_INTERRUPT_ITEM = 'roomInterrupt/item', // 소켓으로 받은 'roomInterrupt/item'

  PING = 'PING', // PING - PONG 테스트.
  ROOM_VIEWS = 'ROOM_VIEWS', // 객실목록 뷰 목록 동기화.
  ROOM_VIEW_ITEMS = 'ROOM_VIEW_ITEMS', // 객실목록 뷰의 아이템 목록 동기화.
  ROOM_INTERRUPT_DEL_USER = 'ROOM_INTERRUPT_DEL_USER', // 사용자의 모든 객실-인터럽트 삭제.
}

/** namespace popupBiz */
export namespace popupBiz {
  /**
   * 링크 별칭. 링크 마지막에 있는 'room/xxx/1' 과 같이 숫자가 붙은 부분을 제거해 'room/xxx' 로 만든다.
   * @param link
   * @returns
   */
  export const linkAlias = (link: string): string => {
    const idx = link.lastIndexOf('/');
    if (idx > 0) {
      const lastPart = link.substring(idx + 1);
      // lastPart가 숫자이면 처리한다.
      if (!isNaN(Number(lastPart))) {
        const ret = link.substring(0, idx);
        // console.log(`*** linkAlias - link: ${link} ---> alias: ${ret}`);
        return ret;
      }
    }
    return link;
  };

  /**
   * 현재 창 목록 출력.
   * @param store
   */
  const printWindows = (store: any, prefix: string = '') => {
    const winRefs = store.getters[WIN_REFS];
    if (winRefs) {
      let no = 1;
      for (const [key, winPop] of Object.entries(winRefs)) {
        console.log(
          `*** 열린 창 확인 - ${prefix}[${no++}] :: key: [${key}] --->`,
          winPop ? '있음' : '없음'
        );
      }
    } else {
      console.log(
        `*** 열린 창 확인 - ${prefix}:: winRefs is ${winRefs ? '있음' : '없음'}`
      );
    }
  };

  /**
   * 창 열기. 같은 창이 있으면 focus.
   * link 가 달라도 하나의 창으로 동작하게 하려면 linkAlias 를 동일하게 설정한다.
   *  예) link: 'reservation/1', linkAlias: 'reservation'
   * @param store
   * @param link
   * @param target
   * @param winFeatures
   * @param linkAlias
   * @returns
   */
  const openWin = (
    store: any,
    link: string,
    target: string,
    winFeatures: string,
    linkAlias?: string
  ): any => {
    console.log('==========> openWin', { link, target, winFeatures });

    const winRefs = store.getters[WIN_REFS];
    console.log(`*** openWin winRefs: `, winRefs);

    if (winRefs) {
      const storeTarget = linkAlias ? linkAlias : link;

      const winPop = winRefs[storeTarget]; // 이미 열린 창 확인.

      console.log(`*** openWin winPop: `, winPop);

      if (winPop && !winPop.closed) {
        winPop.focus();
        // printWindows(store, 'openWin 이후 ');
        return winPop;
      } else {
        const winPop = window.open('/' + link, target, winFeatures);
        store.dispatch('addWinRefs', { key: storeTarget, value: winPop });
        // console.log(`*** 열린 창 추가 :: openWin key: [${storeTarget}]`);
        winPop.focus();
        // printWindows(store, 'openWin 이후 ');
        return winPop;
      }
    }
    // printWindows(store, 'openWin 이후 ');
    return null;
  };

  /**
   * 열린 창 닫기.
   * @param store
   * @param linkAlias 링크 또는 링크 별칭.
   * @returns 창을 찾아 닫으면 true, 창이 없으면 false.
   */
  const closeWin = (store: any, linkAlias: string): boolean => {
    const winRefs = store.getters[WIN_REFS];
    if (winRefs) {
      const winPop = winRefs[linkAlias]; // 이미 열린 창 확인.
      if (winPop) {
        winPop.close();
        store.dispatch('delWinRefs', { key: linkAlias });
        // printWindows(store, 'closeWin 이후 ');
        return true;
      } else {
        console.log(
          `*** closeWin - key 가 없어 window.close() 함. :: key: [${linkAlias}]`
        );
        window.close();
      }
    }
    // printWindows(store, 'closeWin 이후 ');
    return false;
  };

  /** link 의 창을 닫는다. */
  export const doClosePopup = (store: any, link: string): boolean => {
    return closeWin(store, link);
  };

  /** 사용자 전환 창 열기. */
  export const doOpenUserChange = (store: any): any => {
    //'login/user-change', 400, 338)">사용자 전환
    const link = `login/user-change`;
    const target = `user-change`;
    const winFeatures =
      'width=400,height=338,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 객실 화면 창 열기. */
  export const doOpenRoom = (store: any, roomId: number): any => {
    const link = `room/room_in/${roomId}`;
    const linkAlias = popupBiz.linkAlias(link);
    const target = `room_in`;
    const winFeatures =
      'width=1000,height=991,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures, linkAlias);
  };

  /** 객실목록 뷰 창 열기. */
  export const doOpenRoomView = (
    store: any,
    roomViewId: number,
    tabNo?: number
  ): any => {
    menuBiz.setShowMenu(store, false);
    const link = `room_view/${roomViewId}`;
    const target = `room_view_${roomViewId}`;
    const winFeatures =
      'width=1890,height=970,scrollbars=no,resizable=no,popup=yes';

    closeWin(store, link);
    return openWin(store, link, target, winFeatures);
  };

  /** 객실목록 뷰 설정 창 열기. */
  export const doOpenRoomViewSetting = (store: any): any => {
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_setting';
    const target = 'room_setting';
    const winFeatures =
      'width=800,height=745,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 매출 조회 창열기. */
  export const doOpenSales = (store: any): any => {
    //'room/check_sales', 1600, 984)">매출 조회
    menuBiz.setShowMenu(store, false);
    const link = 'room/check_sales';
    const target = 'check_sales';
    const winFeatures =
      'width=1600,height=984,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 객실 이력조회 창열기. */
  export const doOpenRoomHistory = (store: any): any => {
    //'room/room_inquiry', 1040, 926.4)">객실 이력조회
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_inquiry';
    const target = 'room_inquiry';
    const winFeatures =
      'width=1040,height=926.4,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 포인트 이력조회 창열기. */
  export const doOpenPoint = (store: any): any => {
    //'room/point_list', 1110, 830)">포인트 이력조회
    menuBiz.setShowMenu(store, false);
    const link = 'room/point_list';
    const target = 'point_list';
    const winFeatures =
      'width=1110,height=830,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 카드데크 키 현황 창열기. */
  export const doOpenKeyStatus = (store: any): any => {
    //'setting/key_status', 1010, 784)">카드데크 키 현황
    menuBiz.setShowMenu(store, false);
    const link = 'setting/key_status';
    const target = 'key_status';
    const winFeatures =
      'width=1010,height=784,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 객실정보 관리 창열기. */
  export const doOpenRoomManage = (store: any): any => {
    //'management/room_info', 1194, 811)">객실정보 관리
    menuBiz.setShowMenu(store, false);
    const link = 'management/room_info';
    const target = 'room_info';
    const winFeatures =
      'width=1194,height=811,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 객실타입 관리 창열기. */
  export const doOpenRoomTypeSetting = (store: any): any => {
    //'room/room_type', 800, 823)">객실타입 관리
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_type';
    const target = 'room_type';
    const winFeatures =
      'width=800,height=823,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 일괄처리 창열기. */
  export const doOpenRoomBatch = (store: any): any => {
    //'room/room_status', 1360, 861)">일괄처리
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_status';
    const target = 'room_status';
    const winFeatures =
      'width=1360,height=861,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 포인트 관리 창열기. */
  export const doOpenManagePoint = (store: any): any => {
    //'room/point_management', 800, 734)">포인트 관리
    menuBiz.setShowMenu(store, false);
    const link = 'room/point_management';
    const target = 'point_management';
    const winFeatures =
      'width=800,height=734,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 포인트 이력 창열기. */
  export const doOpenPointHistory = (store: any): any => {
    //'room/point_management', 800, 734)">포인트 관리
    menuBiz.setShowMenu(store, false);
    const link = 'room/point_list';
    const target = 'point_list';
    const winFeatures =
      'width=1110,height=830,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 예약 목록 창열기. */
  export const doOpenReservationList = (store: any): any => {
    //'room/room_reservation/1', 1360, 800)">예약 목록
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_reservation/1';
    const linkAlias = popupBiz.linkAlias(link);
    const target = 'room_reservation';
    const winFeatures =
      'width=1360,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures, linkAlias);
  };

  /** 예약 등록 창열기. */
  export const doOpenReservation = (store: any): any => {
    //'room/room_reservation/2', 1000, 630)">예약 등록
    menuBiz.setShowMenu(store, false);
    const link = 'room/room_reservation/2';
    const linkAlias = popupBiz.linkAlias(link);
    const target = 'room_reservation';
    const winFeatures =
      'width=1360,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures, linkAlias);
  };

  /** 공지 게시판 창열기. */
  export const doOpenNotice = (store: any): any => {
    //	'dashboard/notice_news', 800, 793)">공지 게시판
    menuBiz.setShowMenu(store, false);
    const link = 'dashboard/notice_news';
    const target = 'notice_news';
    const winFeatures =
      'width=800,height=793,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 업주 게시판 창열기. */
  export const doOpenBoardAalarm = (store: any): any => {
    //'room/board_alarm', 950, 800)">업주 게시판
    menuBiz.setShowMenu(store, false);
    const link = 'room/board_alarm';
    const target = 'board_alarm';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** A/S 건의 게시판 창열기. */
  export const doOpenBoardAS = (store: any): any => {
    //'room/board_as', 950, 800)">A/S 건의 게시판
    menuBiz.setShowMenu(store, false);
    const link = 'room/board_as';
    const target = 'board_as';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 요금설정 창열기. */
  export const doOpenSettingCharge = (store: any): any => {
    //'setting/charge', 950, 800)">요금설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/charge';
    const target = 'setting_charge';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 운영규칙 설정 창열기. */
  export const doOpenSettingOperation = (store: any): any => {
    //'setting/operation', 950, 840)">운영규칙 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/operation';
    const target = 'setting_operation';
    const winFeatures =
      'width=950,height=840,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 입실시간 옵션 창열기. */
  export const doOpenSettingTimeOption = (store: any): any => {
    //'setting/time_option', 950, 800)">입실시간 옵션
    menuBiz.setShowMenu(store, false);
    const link = 'setting/time_option';
    const target = 'setting_time_option';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 무인판매 설정 창열기. */
  export const doOpenSettingUnmanned = (store: any): any => {
    //'setting/unmanned', 950, 800)">무인판매 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/unmanned';
    const target = 'setting_unmanned';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 예약설정 창열기. */
  export const doOpenSettingReservation = (store: any): any => {
    //'setting/reservation', 950, 800)">예약설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/reservation';
    const target = 'setting_reservation';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 포인트 설정 창열기. */
  export const doOpenSettingPoint = (store: any): any => {
    //'setting/point', 950, 800)">포인트 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/point';
    const target = 'setting_point';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 잠금 설정 창열기. */
  export const doOpenSettingLock = (store: any): any => {
    //'setting/lock', 950, 800)">잠금 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/lock';
    const target = 'setting_lock';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 냉/난방 설정 창열기. */
  export const doOpenSettingAir = (store: any): any => {
    //'setting/air', 950, 800)">냉/난방 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/air';
    const target = 'setting_air';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 화면 설정 창열기. */
  export const doOpenSettingScreen = (store: any): any => {
    //'setting/view', 950, 800)">화면 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/view';
    const target = 'setting_view';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 음성 설정 창열기. */
  export const doOpenSettingVoice = (store: any): any => {
    //'setting/voice', 950, 800)">음성 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/voice';
    const target = 'setting_voice';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 전원 설정 창열기. */
  export const doOpenSettingPower = (store: any): any => {
    //'setting/power', 950, 800)">전원 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/power';
    const target = 'setting_power';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 알림 설정 창열기. */
  export const doOpenSettingAlarm = (store: any): any => {
    //'setting/alarm', 950, 800)">알림 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/alarm';
    const target = 'setting_alarm';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** PMS 설정 창열기. */
  export const doOpenSettingPms = (store: any): any => {
    //'setting/pms', 950, 800)">PMS 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/pms';
    const target = 'setting_pms';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 도어락 설정 창열기. */
  export const doOpenSettingDoorLock = (store: any): any => {
    //'setting/door', 950, 800)">도어락 설정
    menuBiz.setShowMenu(store, false);
    const link = 'setting/door';
    const target = 'setting_door';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 데이터 관리 창열기. */
  export const doOpenSettingData = (store: any): any => {
    //'setting/data', 950, 800)">데이터 관리
    menuBiz.setShowMenu(store, false);
    const link = 'setting/data';
    const target = 'setting_data';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 직원 관리 창열기. */
  export const doOpenSettingEmployee = (store: any): any => {
    //'setting/employee', 950, 816)">직원 관리
    menuBiz.setShowMenu(store, false);
    const link = 'setting/employee';
    const target = 'setting_employee';
    const winFeatures =
      'width=950,height=816,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 구독 관리 창열기. */
  export const doOpenSettingSubscribe = (store: any): any => {
    //'setting/subscribe', 950, 800)">구독 관리
    menuBiz.setShowMenu(store, false);
    const link = 'setting/subscribe';
    const target = 'setting_subscribe';
    const winFeatures =
      'width=950,height=800,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };

  /** 시뮬레이터 창열기. */
  export const doOpenSimulator = (store: any): any => {
    //'room/ccu_simulator', 550, 446)">시뮬레이터 <!--, 550, 505-->
    menuBiz.setShowMenu(store, false);
    const link = 'room/ccu_simulator';
    const target = 'ccu_simulator';
    const winFeatures =
      'width=550,height=446,scrollbars=no,resizable=no,popup=yes';
    return openWin(store, link, target, winFeatures);
  };
}
