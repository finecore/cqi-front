import Swal from 'sweetalert2';
import { Ref } from 'vue';
import { RoomView, Snackbar } from '@/types';
import { fromPairs } from 'lodash';

/**
 * RoomView 클래스
 */
export class RoomViewClass implements RoomView {
  id: number;
  place_id: number;
  theme_id: number = 1;
  title: string;
  comment: string;
  all_room: number = 0;
  room_order: number = 1;
  rows: number = 1;
  cols: number;
  col_gap: number = 5;
  row_gap: number = 5;
  width: number = 100;
  height: number = 100;
  order: number = 1;
  reg_date: string = null;
  mod_date: string = null;

  constructor(place_id: number, title: string, cols: number) {
    this.place_id = place_id;
    this.title = title;
    this.comment = title;
    this.cols = cols;
  }
}

/**
 * namespace roomViewBiz
 */
export namespace roomViewBiz {
  /**
   * 업소의 room_view 목록을 가져오고 store에 저정한다.
   * @param store
   * @param place_id
   */
  export const roomViews = (store: any, place_id: number) => {
    return store.dispatch('roomView/getList', place_id);
  };
  /**
   * 업소의 room_view 목록을 가져오고 store에 저정한다.
   * @param store
   * @param place_id
   */
  export const roomTheme = async (store: any) => {
    return await store.dispatch('roomTheme/getList');
  };

  /**
   * store 에서 room_view 목록을 가져온다.
   * @param store
   * @returns
   */
  export const getterRoomViews = (store: any) => {
    return store.getters['roomView/list'];
  };

  /**
   * 지정한 id의 room_view를 가져오고 store에 저장한다.
   * @param store
   * @param id
   */
  export const roomView = async (store: any, id: number) => {
    return store.dispatch('roomView/getItem', id);
  };

  /**
   * store 에서 room_view 한개를 가져온다.
   * @param store
   * @returns
   */
  export const getterRoomView = (store: any) => {
    return store.getters['roomView/item'];
  };

  /**
   * 객실뷰 이름을 변경한다.
   * @param store
   * @param view_id
   * @param name
   * @param snackbar
   */
  export const changeRoomViewName = (
    store: any,
    roomView: Ref<RoomView>,
    changeName: Ref<string>,
    callback?: any
  ) => {
    let ret = true;
    const list = getterRoomViews(store);
    for (const item of list) {
      if (item.title === name) {
        ret = false;
        break;
      }
    }

    if (ret) {
      const room_view = {
        id: roomView.value.id,
        title: changeName.value,
      } as RoomView;
      store.dispatch('roomView/setItem', room_view).then(() => {
        roomViews(store, roomView.value.place_id); // room_views 다시 조회한다.
        if (callback) {
          callback();
        }
      });
    } else {
      Swal.fire({
        icon: 'warning',
        title: `이름 중복`,
        html: `같은 이름이 존재합니다. <br/> 다른 이름으로 변경해 주세요.`,
        allowEnterKey: false,
        showCloseButton: false,
        confirmButtonText: '확인',
      }).then((result) => {});
    }
  };

  /**
   * room_view를 추가하고 store에 저장한다.
   * @param store
   * @param newRoomView
   */
  export const insertRoomView = async (
    store: any,
    newRoomView: RoomView,
    callback?: any
  ) => {
    const result = await Swal.fire({
      icon: 'question',
      title: `객실목록 뷰 생성`,
      html: `[${newRoomView.title}] 객실목록 뷰를 생성합니다.`,
      allowEnterKey: false,
      showCloseButton: false,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    });

    if (result.isConfirmed) {
      const ret = await store.dispatch('roomView/newItem', newRoomView);
      console.log(`*** insertRoomView - ret ::`, ret);
      roomViews(store, newRoomView.place_id); // room_views 다시 조회한다.
      if (callback) callback(newRoomView);
    }
  };

  /**
   * room_view를 수정하고 store에 저장한다.
   * @param store
   * @param roomView
   */
  export const updateRoomView = (store: any, roomView: RoomView) => {
    return store.dispatch('roomView/setItem', roomView);
  };

  /**
   * room_view를 삭제하고 store에서 삭제한다. store 목록에서도 제거한다.
   * @param store
   * @param id
   */
  export const deleteRoomView = (
    store: any,
    roomView: Ref<RoomView>,
    callback?: any
  ) => {
    if (roomView.value === null) return;

    Swal.fire({
      icon: 'question',
      title: `객실뷰 삭제 확인`,
      html: `[${roomView.value.title}]을 삭제하시겠습니까?`,
      allowEnterKey: false,
      showCloseButton: false,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    }).then((result) => {
      // console.log(`*** Swal - result ::`, result);
      if (result.isConfirmed) {
        store.dispatch('roomView/delItem', roomView.value.id).then(() => {
          roomViews(store, roomView.value.place_id); // room_views 다시 조회한다.
          if (callback) callback();
        });
      }
    });
  };
}
