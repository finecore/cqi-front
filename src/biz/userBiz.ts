import { User, CudResult } from '@/types';
import { CudType } from '@/biz/dataBiz';
import { viewUtil } from '@/utils/view-util';

/**
 * namespace userBiz.
 */
export namespace userBiz {

  /**
   * 업소의 사용자(직원) 목록을 조회하고 store 에 저장한다.
   * @param store 
   * @param place_id 
   */
  export const usersByPlaceId = async (store: any, place_id: number) => {
    const filter = `a.place_id=${place_id}`;
    const order = `a.id`;
    const desc = `asc`;

    await store.dispatch('user/getList', { filter, order, desc })
  };

  /**
   * store 에서 사용자(직원) 목록 제공하기.
   * @param store 
   * @returns 
   */
  export const getterUsers = (store: any): User[] => {
    return store.getters['user/list'];
  };

  /**
   * 전송할 데이터 정리하기.
   * @param sendData
   */
  const sendDataCut = (sendData: User) => {
    delete sendData.use_yn;
    delete sendData.last_login_date;
  };

  /**
   * ------------------------------------------------------
   * 전송할 데이터 확인하기.
   * @param sendData 
   * @returns 
   * ------------------------------------------------------
   */
  const validSendData = (sendData: User, cudType: CudType, retryPwd?: string): string[] => {
    const msg: string[] = [];

    if (cudType === CudType.New) {
      sendData.id = sendData.id?.trim();
      if (!viewUtil.isValidUserID(sendData.id)) {
        msg.push('[아이디]가 올바르지 않습니다.(4자 이상의 영문자,숫자 조합)');
      }
    }

    if (cudType === CudType.New || cudType === CudType.Set) {
      sendData.name = sendData.name ? sendData.name.trim() : null;
      if (sendData.name === null || sendData.name?.length < 2) {
        msg.push('[이름]이 올바르지 않습니다.(2자 이상)');
      }
  
      if (sendData.level === null) {
        msg.push('[권한설정]을 선택해 주세요.');
      }
  
      sendData.email = sendData.email ? sendData.email.trim() : null;
      if (!viewUtil.isValidEmail(sendData.email)) {
        msg.push('[이메일]이 올바르지 않습니다.');
      }
  
      if (!viewUtil.isValidPhone(sendData.hp)) {
        msg.push('[휴대폰]번호가 올바르지 않습니다.');
      }
  
      if (!viewUtil.isValidPhone(sendData.tel)) {
        msg.push('[연락처]번호가 올바르지 않습니다.');
      }
  
      sendData.dept = sendData.dept ? sendData.dept.trim() : null;
      if (sendData.dept === null) {
        msg.push('[부서]가 올바르지 않습니다.(2자 이상)');
      }
    }

    if (cudType === CudType.New || cudType === CudType.Pwd) {
      if (!viewUtil.isValidUserPwd(sendData.pwd)) {
        msg.push('[비밀번호]가 올바르지 않습니다.(8자 이상의 영문자,숫자,특수문자 조합)');
      }
  
      if (sendData.pwd != retryPwd)  {
        msg.push('[비밀번호]와 [비밀번호 확인]이 맞지 않습니다.');
      }
    }

    return msg;
  };

  /**
   * ------------------------------------------------------
   * 사용자 추가하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const newUser = async (store: any, sendData: User, retryPwd: string, callback?: (_result: CudResult<User>) => void) => {
    const title = '직원 추가';

    sendDataCut(sendData); // 전송할 데이터 정리.

    // 1. 확인.
    const msg = validSendData(sendData, CudType.New, retryPwd);
    if (msg.length > 0) {
      viewUtil.swalInvalidData(title, msg);
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${sendData.id}]</span> 직원을 추가 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);
    
    if (swalResult.isConfirmed) {
      // 2. 전송.
      let result = false;
      await store.dispatch('user/newItem', sendData)
      .then((cudResult: CudResult<User>) => {
        result = cudResult.success;
        if (callback) callback(cudResult);
      });

      if (result) {
        await usersByPlaceId(store, sendData.place_id);
      }
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, sendData.id, '추가', result);
    }
  };

  /**
   * ------------------------------------------------------
   * 사용자 정보 변경하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const setUser = async (store: any, sendData: User, callback?: (_success: boolean) => void) => {
    const title = '직원 정보 변경';

    sendDataCut(sendData); // 전송할 데이터 정리.

    // 1. 확인.
    const msg = validSendData(sendData, CudType.Set);
    if (msg.length > 0) {
      viewUtil.swalInvalidData(title, msg);
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${sendData.id}]</span> 직원 정보를 변경 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);
  
    if (swalResult.isConfirmed) {
      // 2. 전송.
      let result = false;
      await store.dispatch('user/setItem', sendData)
      .then((success: boolean) => {
        result = success;
      });

      if (result) {
        await usersByPlaceId(store, sendData.place_id);
      }

      if (callback) callback(result);
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, sendData.id, '변경', result);
    }
  };

  /**
   * 
   * @param store 
   * @param sendData 
   * @param retryPwd 
   * @param callback 
   * @returns 
   */
  export const setUserPwd = async (store: any, sendData: any, retryPwd: string, callback: () => void) => {
    const title = '직원 비밀번호 변경';

    // 1. 확인.
    const msg = validSendData(sendData, CudType.Pwd, retryPwd);
    if (msg.length > 0) {
      viewUtil.swalInvalidData(title, msg);

      if (callback) callback();
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${sendData.id}]</span> 직원 비밀번호를 변경 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);
  
    if (swalResult.isConfirmed) {
      // 2. 전송.
      let result = false;
      await store.dispatch('user/setItem', sendData)
      .then((success: boolean) => {
        result = success;
      });

      if (callback) callback();
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, sendData.id, '변경', result);
    }
  };

  /**
   * ------------------------------------------------------
   * 사용자 삭제하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const delUser = async (store: any, sendData: User, callback?: (_success: boolean) => void) => {
    const title = '직원 삭제';

    // 1. 확인.
    const textHtml = `<span style='font-weight: 800;'>[${sendData.id}]</span> 직원을 삭제 하시겠습니까?<br/><br/>
                      <span style='color: red;'>삭제된 데이터는 복구할 수 없습니다.</span>`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);
  
    if (swalResult.isConfirmed) {  
      // 2. 전송.
      let result = false;
      await store.dispatch('user/delItem', sendData)
      .then((success: boolean) => {
        result = success;
      })

      if (result) {
        await usersByPlaceId(store, sendData.place_id);
      }

      if (callback) callback(result);
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, sendData.id, '삭제', result);
    }
  };
}
