import { RoomStateLog } from '@/types';

/**
 * namespace roomStateLogBiz
 */
export namespace roomStateLogBiz {

  /**
   * day는 대부분 -1로, 하루 이전부터 현재까지의 로그 목록을 가져온다.
   * @param store 
   * @param place_id 
   * @param day 
   */
  export const roomStateDayLogs = (store: any, place_id: number, day: number = -1) => {
    store.dispatch('roomStateLog/getRoomStateDayLogs', { place_id, day });
  };

  /**
   * last_id 보다 큰 로그 목록을 가져온다.
   * @param store 
   * @param place_id 
   * @param last_id 
   */
  export const roomStateLastLogs = (store: any, place_id: number, last_id: number) => {
    store.dispatch('roomStateLog/getRoomStateLastLogs', { place_id, last_id });
  };

  /**
   * store 에서 객실 상태 로그 목록을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterRoomStateLogs = (store: any): RoomStateLog[] => {
    return store.getters['roomStateLog/list'];
  };

  /**
   * store 에서 객실 상태 로그 목록을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterRoomStateEvents = (store: any): any[] => {
    return store.getter['roomStateLog/roomStateEvents'];
  };
}