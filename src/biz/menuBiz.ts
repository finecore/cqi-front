/**
 * namespace menuBiz
 */
export namespace menuBiz {
  /**
   * store에 저장된 showMenu를 반환한다.
   * @param store 
   * @returns 
   */
  export const getterShowMenu = (store: any): boolean => {
    return store.getters['showMenu'];
  };

  /**
   * store에 showMenu를 저장한다.
   * @param store 
   * @param showMenu 
   */
  export const setShowMenu = (store: any, showMenu: boolean) => {
    store.dispatch('setShowMenu', showMenu);
  };

  /**
   * store에 저장된 showMenu를 토글한다.
   */
  export const toggleMenu = (store: any) => {
    const before = store.getters['showMenu'];
    const after = !before;
    store.dispatch('setShowMenu', after);
    // console.log(`*** toggleMenu - before: ${before}, after: ${after}`);
  };
}
