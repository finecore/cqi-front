import { Notice } from '@/types';

export namespace noticeBiz {
  export const notices = (store: any, pageSize: number = 35, isAll: boolean) => {
    store.dispatch('notice/getList', {
      page: { no: 1, size: pageSize },
      all: isAll ? '1' : '0',
    });
  };
}
