import { Device, IscState, StayCount, PayAmount } from '@/types';
import { SalePaySum } from '@/types/sale';

/**
 * 키오스크 State
 */
export interface KioskHolder {
  kioskSerialno: string;                //선택한 serialno
  kioskSaleId: number;                  //선택한 매출 ID
  kioskRoomId: number;                  //선택한 객실 ID
  kioskRoomTypeId: number;              //키오스크 선택 객실타입 ID
  kioskIscState: number;                //키오스크 판매상태 (0:판매대기중, 1:판매 가능한 객실없음 또는 판매중지 상태)
  kioskMinor_mode: number;					    //키오스크 성인인증모드 (0:해제, 1:설정)
  kioskFloors: Map<string, number>;     //키오스크별 선택 층
  kioskStay1s: Map<string, number>;     //키오스크별 선택 숙박타입1
  kioskStay2s: Map<string, number>;     //키오스크별 선택 숙박타입2
  kiosksRoomType: Map<string, number>;  //키오스크별 선택 객실타입
}

/**
 * namespace kioskBiz
 */
export namespace kioskBiz {
  
  /**
   * 키오스크 디바이스 목록 조회하고 store 에 저장.
   * @param store 
   * @param place_id 
   * @param callback 
   */
  export const reqKioskDevices = async (store: any, place_id: number, callback?: any) => {
    store.dispatch('kiosk/getKioskDevices', { place_id  })
    .then((kioskDevices: Device[]) => {
      if (callback) callback(kioskDevices);
    });
  };
  
  /**
   * 키오스크 디바이스 목록 조회. holder 사용.
   * @param store 
   * @param place_id 
   * @param holder 
   */
  export const kioskDevicesHolder = (store: any, place_id: number, holder?: KioskHolder): void => {
    store.dispatch('kiosk/getKioskDevices', { place_id  })
    .then((kiosk_devices: Device[]) => {
      if (kiosk_devices.length > 0) {
        if (holder)
          holder.kioskSerialno = kiosk_devices[0].serialno; // 첫번째 것 설정
      }
    });
  };

  /**
   * store 에서 키오스크 디바이스 목록 가져오기.
   * @param store 
   * @returns 
   */
  export const getterKioskDevices = (store: any): Device[] => {
    return store.getters['kiosk/kioskDevices'];
  };

  /**
   * store 에서 키오스크 serialno 디바이스 가져오기.
   * @param store 
   * @param serialno 
   * @returns 
   */
  export const getterMyKioksDevice = (store: any, serialno: string): Device => {
    const kioskDevices = store.getters['kiosk/kioskDevices'];
    return kioskDevices.find((device: Device) => device.serialno === serialno);
  };

  /**
   * 키오스크 ISC 상태 목록 조회
   * @param store 
   * @param place_id 
   * @param holder
   * @returns 
   */
  export const kioskIscStates = (store: any, place_id: number, holder?: KioskHolder): void => {
    store.dispatch('kiosk/getKioskIscStates', { place_id })
    .then((kioskStates: IscState[]) => {
      if (kioskStates.length > 0) {
        if (holder) {
          for (const kioskIscState of kioskStates) {
            if (holder.kioskSerialno === kioskIscState.serialno) {
              holder.kioskIscState = kioskIscState.state;
              holder.kioskMinor_mode = kioskIscState.minor_mode;
              console.log(`*** getKioskIscStates - holder.kioskIscState 설정함. ::`, holder.kioskIscState);
              console.log(`*** getKioskIscStates - holder.kioskMinor_mode 설정함. ::`, holder.kioskMinor_mode);
              break;
            }
          }
        }
      }
    });
  };

  /**
   * store 에서 키오스크 ISC state 목록 가져오기.
   * @param store 
   * @returns 
   */
  export const getterKioskStates = (store: any): IscState[] => {
    return store.getters['kiosk/kioskStates'];
  };

  /**
   * store 에서 키오스크 serialno ISC state 가져오기.
   * @param store 
   * @param serialno 
   * @returns 
   */
  export const getterMyKioskIscState = (store: any, serialno: string): IscState => {
    const kioskStates = store.getters['kiosk/kioskIscStates'];
    // console.log(`*** kiosk getterMyKioskIscState - ${serialno} ::`, kioskStates);
    return kioskStates.find((iscstate: IscState) => iscstate.serialno === serialno);
  };

  /**
   * 키오스크 매출 조회
   * @param store 
   * @param place_id 
   * @param begin 
   * @param end 
   */
  export const kioskSalePaySums = (store: any, place_id: number, begin: string, end: string): void => {
    store.dispatch('kiosk/getKioskSalePaySums', { place_id, begin, end });
    // .then((kioskSalePaySums: SalePaySum[]) => {
    //   if (kioskState) {
    //     if (kioskSalePaySums?.length > 0) {
    //       kioskState.kioskSaleId = kioskSalePaySums[0].sale_id; // 첫번째 것 설정
    //     }
    //   }
    // });
  };

  /**
   * store 에서 키오스크 매출 목록 가져오기.
   * @param store 
   * @returns 
   */
  export const getterKioskSalePaySums = (store: any): SalePaySum[] => {
    return store.getters['kiosk/kioskSalePaySums'];
  };

  /**
   * store 에서 키오스크 serialno 매출 목록 가져오기.
   * @param store 
   * @param serialno 
   * @returns 
   */
  export const getterMyKioskSalePaySums = (store: any, serialno: string): SalePaySum[] => {
    const kioskSalePaySums = store.getters['kiosk/kioskSalePaySums'];
    const myList = kioskSalePaySums.filter((salePaySum: SalePaySum) => salePaySum.serialno === serialno);
    kioskSaleStayCount(store, myList); // 키오스크 매출 내역에서 숙박 타입별 카운트
    return myList;
  };

  /**
   * 키오스크 매출 내역에서 숙박 타입별 카운트 구해 commit.
   * @param store 
   * @param kioskSalePaySums 
   */
  const kioskSaleStayCount = (store: any, kioskSalePaySums: SalePaySum[]): void => {

    let standard: number = 0;
    let shortTime: number = 0;
    let longTime: number = 0;

    kioskSalePaySums.forEach((salePaySum: SalePaySum) => {
      if (salePaySum.stay_type === 1) standard++;
      else if (salePaySum.stay_type === 2) shortTime++;
      else if (salePaySum.stay_type === 3) longTime++;
    });

    const obj = {  shortTime, standard, longTime } as StayCount;
    store.commit('kiosk/kiosSaleStayCount', obj);
    
  };

  /**
   * 키오스크 매출 내역의 숙박 타입별 카운트 조회
   * @param store 
   * @param serialno 
   * @returns 
   */
  export const getterMyKioskSaleStayCount = (store: any, serialno: string): StayCount => {
    return store.getters['kiosk/kiosSaleStayCount'];
  };




  /**
   * ==================================================
   * 공통 기능
   * ==================================================
   */

  /** 
   * 키오스크 선택
   * @param holder
   * @param currentKioskState
   * @param serialno
   */
  export const doKioskSelect = (holder: KioskHolder, currentKioskState: IscState, serialno: string) => {
    holder.kioskSerialno = serialno;
    console.log(`*** doKioskSelect - iscState ::`, currentKioskState);
    if (currentKioskState) {
      holder.kioskIscState = currentKioskState.state;
      holder.kioskMinor_mode = currentKioskState.minor_mode;
      console.log(`*** doKioskSelect - holder.kioskIscState 설정함. ::`, holder.kioskIscState);
      console.log(`*** doKioskSelect - holder.kioskMinor_mode 설정함. ::`, holder.kioskMinor_mode);
    }
  };

  /** 
   * 사용된 결제 수단 표시
   * 예) 'ota/현금/카드/포인트'
   */
  export const doTopPayUnit = (item: PayAmount): string => {
    let ret = [];

    if (item.prepay_ota_amt > 0) {
      ret.push('OTA');
    }
    if (item.prepay_cash_amt > 0 || item.pay_cash_amt > 0) {
      ret.push('현금');
    }
    if (item.prepay_card_amt > 0 || item.pay_card_amt > 0) {
      ret.push('카드');
    }
    if (item.prepay_point_amt > 0 || item.pay_point_amt > 0) {
      ret.push('포인트');
    }
    return ret.join('/');
  };
    
}
