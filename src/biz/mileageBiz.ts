import { CudResult, Mileage, MileageLog } from '@/types';
import { viewUtil } from '@/utils/view-util';

/** AddPointSign */
export enum AddPointSign {
  ADD = 1,
  SUB = 2,
}


/**
 * namespace mileageBiz
 */
export namespace mileageBiz {

  /**
   * 업소의 마일리지를 조회하고 store에 저장한다.
   * callback 또는 리턴값으로 마일리지를 제공 받을 수 있다.
   * @param store 
   * @param place_id 
   * @returns 
   */
  export const mileages = async (store: any, place_id: number): Promise<Mileage[]> => {
    return await store.dispatch('mileage/getList', place_id);
  };

  /**
   * store 에 저장된 마일리지 목록을 제공한다.
   * @param store 
   * @returns 
   */
  export const getterMileages = (store: any): Mileage[] => {
    return store.getters['mileage/list'];
  };

  /**
   * 업소, 휴대전화의 마일리지를 조회하고 store에 저장한다.
   * callback 또는 리턴값으로 마일리지를 제공 받을 수 있다.
   * @param store 
   * @param place_id 
   * @param phone 
   * @param callback 
   * @returns 
   */
  export const mileage = async (store: any, place_id: number, phone: string, callback?: (_mileage: Mileage) => void): Promise<Mileage> => {
    return store.dispatch('mileage/getItem', { place_id, phone })
    .then((mileage: Mileage) => {
      if (callback) callback(mileage);
    })
  };

  /**
   * ------------------------------------------------------
   * 포인트 휴대전화 번호 등록.
   * @param store 
   * @param item 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const newMileage = async (store: any, item: Mileage, callback?: (_result: CudResult<Mileage>) => void): Promise<boolean> => {
    item.point = 0;
    const cudResult: CudResult<Mileage> = await store.dispatch('mileage/newItem', item);

    if (cudResult.success) {
      await mileages(store, item.place_id);
    }

    // 3 결과 안내.
    viewUtil.swalAlertResult('휴대전화 번호 추가', viewUtil.fmtMobileNum(item.phone), '휴대전화 번호 추가', cudResult.success);

    // callback.
    if (callback) callback(cudResult);

    return cudResult.success;
  };

  /**
   * ------------------------------------------------------ 
   * 마일리지 바꾸기.
   * @param store 
   * @param item 
   * @param addPointSign 1:추가, 2:차감.
   * @param addPoint
   * @param userId
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const setMileage = async (store: any, item: Mileage, addPointSign: number, addPoint: number, userId: string, callback?: (_success: boolean) => void): Promise<boolean> => {
    // 1 확인.
    const title = '포인트 부여';
    let { phone } = item;
    phone = viewUtil.fmtMobileNum(phone);

    console.log(`*** setMileage: addPoint: ${addPoint}, typeof addPoint: ${typeof addPoint}`);

    if (typeof addPoint === 'string') {
      addPoint = Number(addPoint);
    }

    if (addPoint > 10000) {
      viewUtil.swalAlert('warning', title, '포인트는 최대 1만 포인트까지만 부여 가능합니다.');
      return;
    }

    const currentPoint = item.point;
    let signName = '';

    if (addPointSign === AddPointSign.ADD) { // 추가.
      item.point += addPoint;
      signName = '추가';
    }
    else if (addPointSign === AddPointSign.SUB) { // 차감.
      item.point -= addPoint;
      signName = '차감';
    }
    else { // 추가.
      viewUtil.swalAlert('warning', title, '추가 또는 차감을 선택해 주세요.');
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${phone}]</span>
     번호에 ${viewUtil.comma(addPoint)} 포인트를 ${signName} 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    let success = false;
    if (swalResult.isConfirmed) {

      // 2 전송.
      const sendData = {
        ...item,
        type: 1,          // 1: 관리자 변경, 2: 사용자 적립, 3: 사용자 사용
        user_id: userId,
      }

      const success = await store.dispatch('mileage/setItem', sendData);

      if (success) {
        await mileages(store, item.place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(title, phone, '포인트가 부여', success);

      // callback.
      if (callback) callback(success);
    }

    return success;
  }

  /**
   * ------------------------------------------------------
   * 마일리지 삭제하기.
   * @param store 
   * @param item 
   * @param userId
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const delMileage = async (store: any, item: Mileage, userId: string, callback?: (_success: boolean) => void): Promise<boolean> => {
    // 1 확인.
    const title = '포인트 삭제';
    let { phone } = item;
    phone = viewUtil.fmtMobileNum(phone);

    const textHtml = `<span style='font-weight: 800;'>[${phone}]</span> 포인트를 삭제 하시겠습니까?
      <br><br><span style='font-weight: 800; color: red;'>[주의]</span> 삭제된 포인트는 복구할 수 없습니다.`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    let success = false;
    if (swalResult.isConfirmed) {
      // 2 전송.
      const param = { mileage_id: item.id, user_id: userId };
      success = await store.dispatch('mileage/delItem', param);

      if (success) {
        await mileages(store, item.place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(title, phone, '포인트가 삭제', success);

      // callback.
      if (callback) callback(success);

    }
    return success;
  };
}