import { RoomInterrupt } from '@/types';

export class RoomInterruptClass implements RoomInterrupt {
  room_id: number;
  channel: number;
  user_id: string;
  token: string;
  state: number;
  sale: number;
  reg_date?: string;
  mod_date?: string;

  constructor(room_id: number, channel: number, user_id: string, token: string, state: number, sale: number) {
    this.room_id = room_id;
    this.channel = channel;
    this.user_id = user_id;
    this.token = token;
    this.state = state;
    this.sale = sale;
  }
}

export namespace interruptBiz {

  export const getterRoomInterrupt = (store: any): RoomInterrupt => {
    return store.getters['roomInterrupt/item'];
  };

  export const getterRoomInterrupts = (store: any): RoomInterrupt[] => {
    return store.getters['roomInterrupt/list'];
  };

  /**
   * 모든 객실 인터럽트 조회하고 store.list 에 저장한다.
   * @param store 
   * @param place_id 
   * @param callback?
   */
  export const getAllInterrupt = async (store: any, place_id: number, callback?: any) => {
    store.dispatch('roomInterrupt/getList', place_id)
    .then(({ all_room_interrupts }) => {
      if (callback) callback(all_room_interrupts);
    });
  };

  /**
   * 지정한 객실 인터럽트 조회하고 store.item 에 저장한다.
   * @param store 
   * @param room_id 
   */
  export const getInterrupt = (store: any, room_id: number) => {
    store.dispatch('roomInterrupt/getItem', room_id);
  };

  /**
   * 객실 인터럽트를 설정한다. 같은 유저 & 채널의 다른 인터럽트는 해제(삭제)한다. store.item 에 저장한다.
   * @param store 
   * @param room_interrupt 
   */
  export const newInterrupt = async (store: any, room_interrupt: RoomInterrupt) => {
    delUserInterrupt(store, room_interrupt.user_id, room_interrupt.channel, (res: any) => {
      store.dispatch('roomInterrupt/newItem', room_interrupt);
    });
  };

  /**
   * 객실 인터럽트를 수정한다 하고 store.item 에 저장한다.
   * @param store 
   * @param room_interrupt 
   */
  export const setInterrupt = (store: any, room_interrupt: RoomInterrupt) => {
    store.dispatch('roomInterrupt/setItem', room_interrupt);
  };

  /**
   * 객실 인터럽트를 삭제한다. store.list 에서 삭제한다.
   * @param store 
   * @param room_id 
   */
  export const delInterrupt = (store: any, room_id: number) => {
    store.dispatch('roomInterrupt/delItem', room_id);
  };

  /**
   * 같은 사용자, 채널의 인터럽트를 삭제한다.
   * @param store
   * @param user_id 
   * @param channel 
   * @param callback 
   */
  export const delUserInterrupt = async (store: any, user_id: string, channel: number, callback?: any) => {
    store.dispatch('roomInterrupt/delUserItem', { user_id, channel })
    .then((res: any) => {
      console.log(`*** delUserInterrupt: ${user_id}, ${channel}`);
      if (callback) callback(res);
    });
  };

}