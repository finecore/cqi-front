import _ from 'lodash';
import { Consts } from '@/constants/Consts';
import { RoomState, RoomSale, Preference } from '@/types';

/**
 * 객실과 상태에 따른 수
 */
export interface RoomCounter {
  total: number; // 전체 객실 수
  stay: number; // 숙박 객실 수
  rent: number; // 대실 객실 수
  long: number; // 장기 객실 수
  reservd: number; // 예약 객실 수
  empty: number; // 빈 객실 수
  requestClean: number; // 청소요청 객실 수
  cleaning: number; // 청소 중 객실 수
}

/**
 * RoomCounter 클래스
 */
export class RoomCounterClass implements RoomCounter {
  total: number = 0;
  stay: number = 0;
  rent: number = 0;
  long: number = 0;
  reservd: number = 0;
  empty: number = 0;
  requestClean: number = 0;
  cleaning: number = 0;
}

/**
 * RoomState 클래스.
 * DB default 값으로 설정.
 */
export class RoomStateClass implements RoomState {
  readonly room_id: number = null;
  readonly channel: string = null;

  id: number = null;
  room_sale_id: number = null;
  dnd: number = 0;
  clean: number = 0;
  clean_change_time: string = null;
  fire: number = 0;
  emerg: number = 0;
  sale: number = 0;
  sale_change_time: string = null;
  // isc_sale: number = null;
  isc_sale_1: number = 0;
  isc_sale_2: number = 0;
  isc_sale_3: number = 0;
  key: number = 0;
  key_change_time: string = null;
  outing: number = 0;
  signal: number = 0;
  theft: number = 0;
  emlock: number = 0;
  door: number = 0;
  car_call: number = 0;
  airrcon_relay: number = 0;
  main_relay: number = 1;
  use_auto_power_off: number = 1;
  bath_on_delay: number = 0;
  num_light: number = 0;
  chb_led: number = 0;
  car_ss1: number = 0;
  car_ss2: number = 0;
  car_ss3: number = 0;
  shutter: number = 0;
  toll_gate: number = 0;
  entrance: number = 0;
  air_sensor_no: number = 0;
  air_set_temp: number = 0;
  air_set_min: number = 0;
  air_set_max: number = 50;
  air_temp: number = 0;
  air_preheat: number = 0;
  air_heat_type: number = 0;
  air_fan: number = 0;
  air_power: number = 0;
  air_power_type: number = 0;
  main_power_type: number = 0;
  light: number = null;
  dimmer: string = null;
  curtain: string = null;
  boiler_no: number = 0;
  boiler_set_temp: number = 0;
  boiler_temp: number = 0;
  boiler_type: number = 0;
  boiler_heating: number = 0;
  boiler_thermo: number = 0;
  boiler_power: number = 0;
  notice: string = null;
  notice_opacity: number = 7;
  notice_display: number = 0;
  temp_key_1: number = 10;
  temp_key_2: number = 10;
  temp_key_3: number = 10;
  temp_key_4: number = 10;
  temp_key_5: number = 10;
  inspect: number = 0;
  qr_key_yn: number = 0;
  keyless: number = 0;
  reg_date: string = null;
  mod_date: string = null;

  place_id: number = null;
  name: string = null;

  room_type_id: number = null;
  floor: number = null;
  state: string = null;
  sale_channel: string = null;
  stay_type: number = null;
  check_in: string = null;
  check_out: string = null;
  type_name: string = null;
  sale_pay_id: number = null;
  rollback: string = null;

  constructor(room_id: number, channel: string) {
    this.room_id = room_id;
    this.channel = channel;
  }
}

/** 객실 상태 필터 모음 */
export interface RoomFilter {
  hasStay: boolean;
  hasRent: boolean;
  hasLong: boolean;
  hasReserved: boolean;
  hasEmpty: boolean;
}

/**  */
export interface RepresentState {
  stayType: Consts.StayType;
  subState: Consts.SubState;
}

/**
 * namespace roomStateBiz
 */
export namespace roomStateBiz {
  /**
   * 객실 대표 상태를 반환한다.
   * @param roomState
   * @returns
   */
  export const representState = (
    roomState: RoomState,
    roomSale: RoomSale,
    preferences: Preference
  ): RepresentState => {
    /*
    clean = 0:없음, 1:있음 (청소요청)
    sale  = 0:업음 1:숙박 2:대실 3:장기
    key   = 0:없음, 1:청소키,2:마스터키 3:고객키
    */
    const { inspect_use } = preferences;
    const { clean, sale, key, outing, theft, inspect } = roomState;
    const { alarm = 0, check_out = null } = roomSale || {};

    const represent = {
      stayType: Consts.StayType.EMPTY,
      subState: Consts.SubState.NONE,
    };

    if (roomState === undefined || roomState === null) {
      return represent;
    }

    // stayType (0:공실, 1:숙박, 2:대실, 3:장기)
    if (sale === 0) {
      represent.stayType = Consts.StayType.EMPTY;
    } else if (sale === 1) {
      represent.stayType = Consts.StayType.STAY;
    } else if (sale === 2) {
      represent.stayType = Consts.StayType.SHORT_TIME;
    } else if (sale === 3) {
      represent.stayType = Consts.StayType.LONG_TIME;
    }

    // subState
    if (clean === 1) {
      // 청소요청
      represent.subState = Consts.SubState.CLEAN;
    }
    if (key === 2) {
      represent.subState = Consts.SubState.INSPECT;
    }
    if (key === 3) {
      // 0:없음, 1:고객키, 2:마스터키 3:청소키 (청소중)
      represent.subState = Consts.SubState.CLEANING;
    }
    if (sale > 0 && key === 6 && outing !== 1) {
      // 청소키 제거. 청소완료.
      represent.subState = Consts.SubState.CLEANED;
    }

    if (theft === 1) {
      // 도난.
      represent.subState = Consts.SubState.THEFT;
    } else if (outing === 1) {
      // 외출.
      represent.subState = Consts.SubState.OUTING;
    }

    // 알람과 예약 상태.
    if (represent.subState === Consts.SubState.NONE) {
      if (alarm === 1) {
        // 알람.
        represent.subState = Consts.SubState.ALARM;
      }

      // TODO: 예약 상태.
    }

    // 객실 인스펙트 사용 여부 (0: 사용 안함, 1: 퇴실 상태만 사용, 2: 항상 사용)
    if (key !== 3) {
      if ((inspect_use === 1 && check_out !== null) || inspect_use === 2) {
        if (key === 6)
          represent.subState = Consts.SubState.INSPECT_WAIT; // 인스펙트대기
        else if (key === 2)
          represent.subState = Consts.SubState.INSPECT; // 인스펙트 중
        else if (key === 5)
          represent.subState = Consts.SubState.INSPECTED; // 인스펙트 완료
        else {
          // 점검 상태 이고 고객키 없을때 점검 표시.
          if (inspect && key !== 1) {
            if (inspect == 1) {
              represent.subState = Consts.SubState.INSPECT_WAIT; // 인스펙트대기
            } else if (inspect === 2) {
              represent.subState = Consts.SubState.INSPECT; // 인스펙트 중
            } else if (inspect === 3) {
              represent.subState = Consts.SubState.INSPECTED; // 인스펙트 완료
            }
          }
        }
      }
    }

    return represent;
  };

  /**
   *  전체 객실 상태 매출 조회.
   * @param store
   * @param place_id
   * @param state
   */
  export const roomStateSaleAll = async (store: any, place_id: number) => {
    return await store.dispatch('roomState/getAllRoomStatesSale', { place_id });
  };

  /**
   * 전체 객실 상태 조회.
   * @param store
   * @param place_id
   */
  export const roomStateAll = async (store: any, place_id: number) => {
    await store.dispatch('roomState/getAllRoomStates', { place_id });
  };

  export const getterRoomStateList = (store: any): RoomState[] => {
    return store.getters['roomState/list'];
  };

  /**
   * room_id로 객실 상태 조회 하고 store에 저장한다.
   * @param store
   * @param room_id
   * @param callback
   */
  export const roomStateByRoomId = async (
    store: any,
    room_id: number,
    callback?: any
  ) => {
    store
      .dispatch('roomState/getItem', room_id)
      .then((room_state: RoomState[]) => {
        let ret = null;
        if (Array.isArray(room_state) && room_state.length > 0) {
          ret = room_state[0];
        }
        if (callback) callback(ret);
      });
  };

  /**
   * store 에서 RoomState 가져오기.
   * @param store
   * @returns
   */
  export const getterRoomState = (store: any): RoomState => {
    return store.getters['roomState/item'];
  };

  export const setRoomState = async (
    store: any,
    changeValues: any,
    callback?: any
  ): Promise<boolean> => {
    return store
      .dispatch('roomState/setItem', changeValues)
      .then((success: boolean) => {
        if (callback) callback(success);
      });
  };

  /**
   * target 에 source 의 값을 복사한다.
   * @param target
   * @param source
   */
  export const copyFromRoomState = (target: RoomState, source: RoomState) => {
    target.id = source.id;
    target.room_id = source.room_id;
    target.room_sale_id = source.room_sale_id;
    target.channel = source.channel;
    target.dnd = source.dnd;
    target.clean = source.clean;
    target.clean_change_time = source.clean_change_time;
    target.fire = source.fire;
    target.emerg = source.emerg;
    target.sale = source.sale;
    target.sale_change_time = source.sale_change_time;
    target.isc_sale_1 = source.isc_sale_1;
    target.isc_sale_2 = source.isc_sale_2;
    target.isc_sale_3 = source.isc_sale_3;
    target.key = source.key;
    target.key_change_time = source.key_change_time;
    target.outing = source.outing;
    target.signal = source.signal;
    target.theft = source.theft;
    target.emlock = source.emlock;
    target.door = source.door;
    target.car_call = source.car_call;
    target.airrcon_relay = source.airrcon_relay;
    target.main_relay = source.main_relay;
    target.use_auto_power_off = source.use_auto_power_off;
    target.bath_on_delay = source.bath_on_delay;
    target.num_light = source.num_light;
    target.chb_led = source.chb_led;
    target.car_ss1 = source.car_ss1;
    target.car_ss2 = source.car_ss2;
    target.car_ss3 = source.car_ss3;
    target.shutter = source.shutter;
    target.toll_gate = source.toll_gate;
    target.entrance = source.entrance;
    target.air_sensor_no = source.air_sensor_no;
    target.air_set_temp = source.air_set_temp;
    target.air_set_min = source.air_set_min;
    target.air_set_max = source.air_set_max;
    target.air_temp = source.air_temp;
    target.air_preheat = source.air_preheat;
    target.air_heat_type = source.air_heat_type;
    target.air_fan = source.air_fan;
    target.air_power = source.air_power;
    target.air_power_type = source.air_power_type;
    target.main_power_type = source.main_power_type;
    target.light = source.light;
    target.dimmer = source.dimmer;
    target.curtain = source.curtain;
    target.boiler_no = source.boiler_no;
    target.boiler_set_temp = source.boiler_set_temp;
    target.boiler_temp = source.boiler_temp;
    target.boiler_type = source.boiler_type;
    target.boiler_heating = source.boiler_heating;
    target.boiler_thermo = source.boiler_thermo;
    target.boiler_power = source.boiler_power;
    target.notice = source.notice;
    target.notice_opacity = source.notice_opacity;
    target.notice_display = source.notice_display;
    target.temp_key_1 = source.temp_key_1;
    target.temp_key_2 = source.temp_key_2;
    target.temp_key_3 = source.temp_key_3;
    target.temp_key_4 = source.temp_key_4;
    target.temp_key_5 = source.temp_key_5;
    target.inspect = source.inspect;
    target.qr_key_yn = source.qr_key_yn;
    target.keyless = source.keyless;
    target.place_id = source.place_id;
    target.name = source.name;
    target.room_type_id = source.room_type_id;
    target.floor = source.floor;
    target.state = source.state;
    target.sale_channel = source.sale_channel;
    target.stay_type = source.stay_type;
    target.check_in = source.check_in;
    target.check_out = source.check_out;
    target.type_name = source.type_name;
    target.sale_pay_id = source.sale_pay_id;
    target.rollback = source.rollback;
  };
}
