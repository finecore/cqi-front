import Swal from 'sweetalert2';
import _ from 'lodash';
import { RoomView, RoomViewItem } from '@/types';

/** 객실 리소스 아이템 */
export interface ResourceRoomViewItem extends RoomViewItem {
  used: boolean;
  floor: number;
  type_id: number;
}

/** 객실목록 뷰 클래스 */
export class RoomViewItemClass implements RoomViewItem {
  id: number;
  view_id: number;
  room_id: number;
  row: number;
  col: number;
  room_name: string;
  type_name: string;
  drag_fixed: boolean = false;
  drag_moved: boolean = false;
}

/**
 * namespace roomViewBiz
 */
export namespace roomViewItemBiz {
  /**
   * roomViewItem 복사본을 만든다. store 에서 가져온 객체를 조작할때 사용한다.
   * @param roomViewItem 복사 할 원본.
   * @returns
   */
  export const copyOf = (roomViewItem: RoomViewItem): RoomViewItem => {
    const newItem = new RoomViewItemClass();
    newItem.id = roomViewItem.id;
    newItem.view_id = roomViewItem.view_id;
    newItem.room_id = roomViewItem.room_id;
    newItem.row = roomViewItem.row;
    newItem.col = roomViewItem.col;
    newItem.room_name = roomViewItem.room_name;
    newItem.type_name = roomViewItem.type_name;
    if (
      roomViewItem.drag_fixed !== undefined ||
      roomViewItem.drag_fixed !== null
    ) {
      newItem.drag_fixed = roomViewItem.drag_fixed;
    }
    if (
      roomViewItem.drag_moved !== undefined ||
      roomViewItem.drag_moved !== null
    ) {
      newItem.drag_moved = roomViewItem.drag_moved;
    }
    return newItem;
  };

  /**
   * 업소의 room_view_item 목록을 가져오고 store에 저정한다.
   * @param store
   * @param view_id
   * @param callback
   */
  export const roomViewItems = async (
    store: any,
    view_id: number,
    callback?: any
  ) => {
    await store
      .dispatch('roomViewItem/getList', view_id)
      .then((roomViewItems: RoomViewItem[]) => {
        // console.log(`*** roomViewItemBiz.roomViewItems() roomViewItems:`, roomViewItems);
        if (callback) callback(roomViewItems);
      });
  };

  /**
   * store 에서 room_view_item 목록을 가져온다.
   * @param store
   * @returns
   */
  export const getterRoomViewItemList = (store: any) => {
    return store.getters['roomViewItem/list'];
  };

  /**
   * store 에서 room_view_item 목록에서 지정한 row, col의 room_view_item을 가져온다.
   * @param store
   * @param row
   * @param col
   * @returns
   */
  export const getterRoomViewItemByRowCol = (
    store: any,
    row: number,
    col: number
  ) => {
    const list = getterRoomViewItemList(store);
    return (
      list.find((item: RoomViewItem) => item.row === row && item.col === col) ||
      ({} as RoomViewItem)
    );
  };

  /**
   * 지정한 id의 room_view_item 를 가져오고 store에 저장한다.
   * @param store
   * @param id
   * @param holder
   */
  export const roomViewItem = (store: any, id: number) => {
    store.dispatch('roomViewItem/getItem', id);
  };

  /**
   * store 에서 room_view_item 한개를 가져온다.
   * @param store
   * @returns
   */
  export const getterRoomViewItem = (store: any) => {
    return store.getters['roomViewItem/item'];
  };

  // /**
  //  * room_view_item 를 추가하고 store에 저장한다.
  //  * @param store
  //  * @param newRoomViewItem
  //  */
  // export const insertRoomViewItem = (store: any, newRoomViewItem: RoomViewItem) => {
  //   store.dispatch('roomViewItem/newItem', newRoomViewItem);
  // };

  // /**
  //  * room_view_item 를 수정하고 store에 저장한다.
  //  * @param store
  //  * @param roomView
  //  * @param holder
  //  */
  // export const updateRoomViewItem = (store: any, roomViewItem: RoomViewItem) => {
  //   store.dispatch('roomViewItem/setItem', roomViewItem);
  // }

  // /**
  //  * room_view_item 를 삭제하고 store에서 삭제한다. store 목록에서도 제거한다.
  //  * @param store
  //  * @param id
  //  * @param holder
  //  */
  // export const deleteRoomViewItem = (store: any, id: number) => {
  //   store.dispatch('roomView/delItem', id);
  // };

  /**
   * 객실목록 뷰 아이템 목록 변경하기.
   * @param store
   * @param view_id
   * @param roomViewItems
   * @param callback
   */
  export const changeRoomViewAllItems = async (
    store: any,
    roomView: RoomView,
    roomViewItems: RoomViewItem[],
    callback?: any
  ) => {
    const result = await Swal.fire({
      icon: 'question',
      title: `객실 구성`,
      html: `[${roomView.title}] 객실 구성을 적용하시겠습니까?`,
      allowEnterKey: false,
      showCloseButton: false,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    }).then((result) => {
      if (result.isConfirmed) {
        store
          .dispatch('roomViewItem/changeItems', {
            view_id: roomView.id,
            room_view_items: roomViewItems,
          })
          .then((ret: boolean) => {
            roomViewItemBiz.roomViewItems(store, roomView.id); // 변경된 목록을 다시 가져온다.
            if (callback) callback(ret);
          });
      }
    });
  };
}
