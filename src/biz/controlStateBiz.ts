//================================================================
// 객실 상태 조작 관련 비즈니스 로직
//================================================================

import { Ref } from "vue";
import { Room, RoomState, RoomStateLog } from "@/types";
import { Consts } from '@/constants/Consts';
import { roomBiz } from "./roomBiz";
import { viewUtil } from "@/utils/view-util";

/**
 * 객실 제어 상태 인터페이스.
 */
export interface ControlState {
  log_id: number;     // 로그ID
  channel: string;    // 채널
  room_id: number;    // 객실ID
  room_name: string;  // 객실이름
  type_name: string;  // 객실유형
  floorNo: number;    // 층번호
  reg_date: string;   // 등록일시
  msg: string;        // 메세지
  msgs: string[];     // 메세지 목록
  highlight: boolean; // 강조
  data: any;          // 데이터
}

/**
 * 객실 제어 상태 클래스.
 */
export class ControlStateClass implements ControlState {
  log_id: number = 0;
  channel: string = Consts.Channel.FRONT;
  room_id: number = 0;
  room_name: string = '';
  type_name: string = '';
  floorNo: number = 0;
  reg_date: string = '';
  msg: string = '';
  msgs: string[] = [];
  highlight: boolean = false;
  data: any = {};

constructor(data?: object) {
    if (data) this.data = data;
  }
}

/**
 * 선택한 객실 상태.
 */
export interface ChoseStates {
  stay1: boolean; // 숙박
  stay2: boolean; // 대실
  stay3: boolean; // 장기
  key: boolean;
  door: boolean;
  emlock: boolean;
  car_call: boolean
  emerg: boolean;
  outing: boolean;
}

/**
 * 선택한 객실 상태 클래스.
 */
export class ChoseStatesClass implements ChoseStates {
  stay1: boolean = false; // 숙박
  stay2: boolean = false; // 대실
  stay3: boolean = false; // 장기
  key: boolean = false;
  door: boolean = false;
  emlock: boolean = false;
  car_call: boolean = false;
  emerg: boolean = false;
  outing: boolean = false;

  constructor(initValue: boolean = false) {
    this.stay1 = initValue;
    this.stay2 = initValue;
    this.stay3 = initValue;
    this.key = initValue;
    this.door = initValue;
    this.emlock = initValue;
    this.car_call = initValue;
    this.emerg = initValue;
    this.outing = initValue;
  }
}

/**
 * namespace roomStateLogBiz
 */
export namespace controlStateBiz {
  /**
   * store.list 에서 조작 상태 목록을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterControlStateList = (store: any): ControlState[] => {
    return store.getters['controlState/list'];
  };

  /**
   * store.item 을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterControlState = (store: any): ControlState => {
    return store.getters['controlState/item'];
  };

  /**
   * websocket payload를 ControlState 객체로 변환한다.
   * @param store 
   * @param payload 
   * @returns 
   */
  export const valueOfSocketPayload = (store: any, payload: any): ControlState => {

    const { room_state } = payload;
    const controlState = new ControlStateClass(room_state);
    extractData(controlState);
    // console.log(`*** valueOfSocketPayload :: controlState: `, controlState);
    
    let room: Room | null = null;
    const rooms = roomBiz.getterRooms(store); // store에서 찾기.
    for (const roomItem of rooms) {
      if (roomItem.id === controlState.room_id) {
        room = roomItem;
        // console.log(`*** Room store 에서 찾음 - valueOfSocketPayload :: roomItem: `, room);
        break;
      }
    }

    // if (room === null) { // store에 없으면 DB에서 찾기.
    // }
    
    if (room !== null) { // room 정보 설정.
      controlState.room_name = room.name;
      controlState.type_name = room.type_name;
      controlState.floorNo = room.floor;
    }
    return controlState;
  }

  export const valueOfRoomStateLog = (roomStateLog: RoomStateLog, room: Room): ControlState => {
      const controlState = new ControlStateClass(roomStateLog.data);
      controlState.log_id = roomStateLog.id;
      controlState.room_id = roomStateLog.room_id;
      controlState.reg_date = roomStateLog.reg_date;
      extractData(controlState);

      if (room) {
        controlState.room_name = room.name;
        controlState.type_name = room.type_name;
        controlState.floorNo = room.floor;
      }
      return controlState;
  };

  const extractData = (controlState: ControlState) => {
    if (!controlState.channel && controlState.data.channel) {
      controlState.channel = controlState.data.channel;
    }
    if (!controlState.room_id && controlState.data.room_id) {
      controlState.room_id = controlState.data.room_id;
    }
    if (!controlState.reg_date && controlState.data.reg_date) {
      controlState.reg_date = controlState.data.reg_date;
    }
  }

  const setMsg = (cs: ControlState, msg: string, highlight?: boolean) => {
    if (cs.msg === null || cs.msg === '') {
      cs.msg = msg; 
    }
    cs.msgs.push(cs.msg);

    if (highlight !== undefined) {
      cs.highlight = highlight;
      if (highlight === true) {
        cs.msg = msg;
      }
    }
  };

  export const makeMessage = (cs: ControlState) => {
    const { data } = cs;

    const channel = cs.channel;
    const key = data?.key !== undefined ? data.key : null;
    const door = data?.door !== undefined ? data.door : null;
    const emlock = data?.emlock !== undefined ? data.emlock : null;
    const car_call = data?.car_call !== undefined ? data.car_call : null;
    const emerg = data?.emerg !== undefined ? data.emerg : null;

    const sale = data?.sale !== undefined ? data.sale : null;
    const clean = data?.clean !== undefined ? data.clean : null;
    const outing = data?.outing !== undefined ? data.outing : null;
    const check_in = data?.check_in !== undefined ? data.check_in : null;
    const check_out = data?.check_out !== undefined ? data.check_out : null;
    const change_room_id = data?.change_room_id !== undefined ? data.change_room_id : null;
    const stay_type = data?.stay_type !== undefined ? data.stay_type : null;
    const user_id = data?.user_id !== undefined ? data.user_id : '';
    const prev_sale = data?.prev_sale !== undefined ? data.prev_sale : null;
    const main_relay = data?.main_relay !== undefined ? data.main_relay : null;

    const state = data?.state !== undefined ? data.state : null;
    const signal = data?.signal !== undefined ? data.signal : null;
    const air_temp = data?.air_temp !== undefined ? data.air_temp : null;
    const inspect = data?.inspect !== undefined ? data.inspect : null;
    const rollback = data?.rollback !== undefined ? data.rollback : null;


    // 장비에서 오는 상태변경(시뮬레이션 대응).
    if (key !== null) {
      const message = key === 1 ? '고객키가 삽입 되었습니다.'
          : key === 2 ? '마스터키가 삽입 되었습니다.'
          : key === 3 ? '청소키가 삽입 되었습니다.'
          : key === 4 ? '고객키가 제거 되었습니다.'
          : key === 5 ? '마스터키가 제거 되었습니다.'
          : key === 6 ? '청소키가 제거 되었습니다.'
          : key === 0 ? 'NO KEY'
          : '';
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    if (door !== null) {
      const message = door === 1 ? '문이 열렸습니다.' : '문이 닫혔습니다.';
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    if (emlock !== null) {
      const message = emlock === 1 ? 'EM LOCK 열렸습니다.' : 'EM LOCK 닫혔습니다.';
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    if (car_call !== null) {
      const message = viewUtil.keyText('room_state', 'car_call', car_call);
      // return { message, highlight: true };
      setMsg(cs, message);
    }

    if (emerg !== null) {
      const message = viewUtil.keyText('room_state', 'emerg', emerg);
      // return { message, highlight: true };
      setMsg(cs, message, true);
    }

    // 장비에서 오는 상태변경(시뮬레이션 미대응).
    if ((sale !== null || stay_type !== null) && Number(sale || stay_type) > 0) {
      if (prev_sale !== null ) {
        const message = prev_sale === 1 ? "대실로 변경 됨." : "숙박으로 변경 됨.";
        // return { message, highlight: false };
        setMsg(cs, message);
      }

      if (change_room_id !== null ) {
        const message = '객실이 이동 되었습니다.';
        // return { message, highlight: false };
        setMsg(cs, message);
      }
    }

    if (check_in !== null || check_out !== null) { // 대실/숙박/장기 (공실은 제외)
      if (sale !== null || stay_type !== null) {
        const message = viewUtil.keyText("room_state", "sale", sale || stay_type);
        // return { message, highlight: false };
        setMsg(cs, message);
      }

      if (check_in !== null) {
        const message = user_id === "auto" ? "자동 입실" : "입실";
        // return { message, highlight: false };
        setMsg(cs, message);
      } else if (check_out !== null) {
        const message = user_id === "auto" ? "자동 퇴실" : "퇴실";
        // return { message, highlight: false };
        setMsg(cs, message);
      }
    }

    if (sale !== null) {
      const message = viewUtil.keyText('room_state', 'sale', sale) + ' 상태';
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    if (outing !== null) { // 외출/외출 복귀
      const message = viewUtil.keyText('room_state', 'outing', outing);
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    if (clean !== null) { // 청소요청/청소완료
      const message = viewUtil.keyText('room_state', 'clean', clean);
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    // if (inspect_use !== null && inspect !== null) { // 인스펙트
    //   message = viewUtil.keyText('room_state', 'inspect', inspect);
    //   return { message, highlight: false };
    // }

    if (main_relay !== null) { // 메인 전원
      const message = viewUtil.keyText('room_state', 'main_relay', main_relay);
      // return { message, highlight: false };
      setMsg(cs, message);
    }

    // console.warn(`*** 객실상태 미정의 이벤트 발생 :: `, data);
    // return { message: '미정의 이벤트', highlight: false };
  };

  export const filtering = (
    csList: ControlState[], 
    choseStates: Ref<ChoseStates>, 
    floorNo: Ref<number>, 
    roomId: Ref<number>, 
    roomsFilter: Ref<string>
  ): ControlState[] => {
    csList = csList.filter((cs: ControlState) => {
      let isOK = false;
      if (choseStates.value.key === false && 
          choseStates.value.door === false && 
          choseStates.value.outing === false &&
          choseStates.value.stay1 === false && 
          choseStates.value.stay2 === false &&
          choseStates.value.stay3 === false
      ) {
        isOK = true;
      }
      else {
        if (choseStates.value.key && cs.data.key !== undefined) isOK = true;
        else if (choseStates.value.door && cs.data.door !== undefined) isOK = true;
        else if (choseStates.value.outing && cs.data.outing !== undefined) isOK = true;
        else if (choseStates.value.stay1 && cs.data.stay_type !== undefined && cs.data.stay_type === 1) isOK = true;
        else if (choseStates.value.stay2 && cs.data.stay_type !== undefined && cs.data.stay_type === 2) isOK = true;
        else if (choseStates.value.stay3 && cs.data.stay_type !== undefined && cs.data.stay_type === 3) isOK = true;
      }
      return isOK;
    });
  
    if (roomId.value !== 0) {
      csList = csList.filter((cs: ControlState) => {
        return cs.room_id === roomId.value;
      });
    }
    else if (floorNo.value !== 0) {
      csList = csList.filter((cs: ControlState) => {
        return cs.floorNo === floorNo.value;
      });
    }
  
    csList.forEach((controlState: ControlState) => {
      controlStateBiz.makeMessage(controlState);
    });
    if (roomsFilter.value !== null) {
      csList = csList.filter((cs: ControlState) => {
        // console.log(`*** MainEvents - roomsFilter ::`, roomsFilter.value, ', cs.msg:', cs.msg);
        if (cs.msg.indexOf(roomsFilter.value) > -1) {
          return true;
        } else if (cs.room_name !== undefined && cs.room_name.indexOf(roomsFilter.value) > -1) {
          return true;
        } else {
          return false;
        }
      });
    }
  
    return csList;
  };
}
