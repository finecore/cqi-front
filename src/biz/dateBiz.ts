import dayjs, { Dayjs } from 'dayjs';

/** 기간 유형. */
export enum RangeType {
  TODAY = 1,      // 오늘
  YESTERDAY = 2,  // 어제
  THIS_WEEK = 3,  // 이번주
  THIS_MONTH = 4, // 이번달
}

/**
 * 기간별 조회 단위(MySQL DATE_FORMAT)
 */
export enum RangeUnit {
  DAY = 'e',              //1..31
  DAY_OF_YEAR = 'j',      //1..366
  WEEK_OF_YEAR = 'v',     //1..53
  MONTH_OF_YEAR = 'Ym',   //..202301..202312..
  YEAR = 'Y',             //..2021, 2022, 2023..
}

/**
 * 시작일과 종료일을 YYY-MM-DD 형식으로 제공.
 */
export interface BeginEnd {
  begin: string;
  end: string;
}

/**
 * namespace dateBiz
 */
export namespace dateBiz {

  /**
   * 매출조회 기간 이름.
   * @param unit 
   * @returns 
   */
  export const rangeUnitName = (unit: RangeUnit): string => {
    if (unit === RangeUnit.DAY_OF_YEAR) return '일별';
    else if (unit === RangeUnit.WEEK_OF_YEAR) return '주별';
    else if (unit === RangeUnit.MONTH_OF_YEAR) return '월별';
    else if (unit === RangeUnit.YEAR) return '년별';
    else return '';
  };

  /**
   * date를 dayjs.Dayjs로 변환. date가 주어지지 않으면 오늘을 기준으로 함.
   * @param date 
   * @returns 
   */
  export const dayjsFrom = (date?: dayjs.Dayjs | string): dayjs.Dayjs => {
    return date ? (dayjs.isDayjs(date) ? date : dayjs(date)) : dayjs();
  };

  /**
   * 오늘과 어제를 BeginEnd로 제공. date가 주어지면 그 날을 기준으로 함.
   * previous 가 1이면 어제, 2이면 그제, 3이면 그그제...
   * @param date 
   * @param previous
   * @returns 
   */
  export const beginEndDay = (date?: dayjs.Dayjs | string, previous: number = 1): BeginEnd => {
    const current = dayjsFrom(date);
    const from = current.subtract(previous, 'day');
    return {
      begin: from.format('YYYY-MM-DD'),
      end: current.format('YYYY-MM-DD'),
    };
  };

  /**
   * 이번주와 지난주를 BeginEnd로 제공. date가 주어지면 그 날을 기준으로 함.
   * previous 가 1이면 지난주, 2이면 그지난주, 3이면 그그지난주...
   * rangeFit 이 true 이면 주의 주기를 일..토 맞줘 제공한다.
   * @param date 
   * @param previous
   * @param rangeFit
   * @returns 
   */
  export const beginEndWeek = (date?: dayjs.Dayjs | string, previous: number = 1, rangeFit: boolean = false): BeginEnd => {
    let current = (dayjsFrom(date));
    let from = current.subtract(previous, 'week');

    if (rangeFit) {
      from = from.startOf('week');
      current = current.endOf('week');
    }
    return {
      begin: from.format('YYYY-MM-DD'),
      end: current.format('YYYY-MM-DD'),
    };
  };

  /**
   * 이번달과 지난달을 BeginEnd로 제공. date가 주어지면 그 날을 기준으로 함.
   * previous 가 1이면 지난달, 2이면 그지난달, 3이면 그그지난달...
   * @param date 
   * @param previous
   * @returns 
   */
  export const beginEndMonth = (date?: dayjs.Dayjs | string, previous: number = 1, rangeFit: boolean = false): BeginEnd => {
    let current = (dayjsFrom(date));
    let from = current.subtract(previous, 'month');

    if (rangeFit) {
      from = from.startOf('month');
      current = current.endOf('month');
    }
    return {
      begin: from.format('YYYY-MM-DD'),
      end: current.format('YYYY-MM-DD'),
    };
  };

  /**
   * 이번년도와 지난년도을 BeginEnd로 제공. date가 주어지면 그 날을 기준으로 함.
   * previous 가 1이면 지난년도, 2이면 그지난년도, 3이면 그그지난년도...
   * @param date 
   * @param previous
   * @returns 
   */
  export const beginEndYear = (date?: dayjs.Dayjs | string, previous: number = 1, rangeFit: boolean = false): BeginEnd => {
    let current = (dayjsFrom(date));
    let from = current.subtract(previous, 'year');

    if (rangeFit) {
      from = from.startOf('year');
      current = current.endOf('year');
    }
    return {
      begin: from.format('YYYY-MM-DD'),
      end: current.format('YYYY-MM-DD'),
    };
  };

  /**
   * 5분 단위로 떨어지는 미래 dayjs 제공.
   * @param day 
   * @returns 
   */
  export const expectDate = (day: Dayjs = dayjs()): Dayjs => {
    const min = day.minute();
    const quotient = Math.floor(min / 10);
    let remainder = min % 10;
    if (remainder !== 0 && remainder !== 5) {
      if (remainder < 5 )
        remainder = 5;
      else
        remainder = 10;
    }
    const afterDay = day.minute(quotient * 10 + remainder);
    return afterDay;
  };

  /**
   * 오전 (0~11) 인가.
   * @param now 
   * @returns 
   */
  export const isAm = (now: Dayjs): boolean => {
    return now.hour() < 12;
  }

  /**
   * 주말인가.
   * @param now 
   * @returns 
   */
  export const isWeekend = (now: Dayjs): boolean => {
    const day = now.day();
    return day === 0 || day === 6; //일요일(0) 또는 토요일(6)
  }
}
