import _ from 'lodash';
import dayjs from 'dayjs';
import Swal, { SweetAlertResult } from 'sweetalert2';
import { Consts } from '@/constants/Consts';
import { RoomType, RoomState, RoomSale, Preference, User } from '@/types';
import { dateBiz } from '@/biz/dateBiz';
import { roomStateBiz } from './roomStateBiz';
import { roomSaleBiz } from '@/biz/roomSaleBiz';
import { viewUtil } from '@/utils/view-util';
import { set } from 'lodash';

// /** 판매 계산기 */
// export class SaleCalculator {

//   /** 체크인 전체 비용 계산. */
//   static calcTotal(roomSale: RoomSale, roomType: RoomType, preferences: Preference, stayNum: number) {
//     const beginTime = dayjs(roomSale.check_in_exp);
//     const endTime = dayjs(roomSale.check_out_exp);
//     const isWeekend = dateBiz.isWeekend(beginTime);
//     const isAm = dateBiz.isAm(beginTime);

//     let overTime = 0;

//     // 숙박 기본 비용 설정.
//     if (roomSale.stay_type === Consts.StayType.STAY) { // 숙박.
//       roomSale.default_fee = roomType.default_fee_stay;
//     }
//     else if (roomSale.stay_type === Consts.StayType.SHORT_TIME) { // 대실.
//       roomSale.default_fee = roomType.default_fee_rent;
//     }
//     else if (roomSale.stay_type === Consts.StayType.LONG_TIME) { // 장기.
//       roomSale.default_fee = roomType.default_fee_stay;
//     }

//     let oneTimeFee = roomSale.default_fee;

//     const addPerson = roomSale.person > roomType.person ? roomSale.person - roomType.person : 0;
//     if (addPerson > 0) {
//       oneTimeFee += addPerson * roomType.person_add_fee; // 인원 추가 비용.
//     }

//     let totalFee = oneTimeFee;
//     if (roomSale.stay_type === Consts.StayType.STAY || roomSale.stay_type === Consts.StayType.LONG_TIME) { // 숙박, 장기.

//       totalFee = oneTimeFee * stayNum;

//     }
//     else if (roomSale.stay_type === Consts.StayType.SHORT_TIME) { // 대실.
//       const defaultHour = isWeekend
//         ? (isAm ? preferences.rent_time_am_weekend : preferences.rent_time_pm_weekend)
//         : (isAm ? preferences.rent_time_am : preferences.rent_time_pm);
//       if (stayNum > defaultHour) {
//         const gapHour = stayNum - defaultHour;
//         totalFee = oneTimeFee + (gapHour * roomType.over_time_fee_rent); // 시간당 추가비용 추가.
//       }
//     }

//   }

//   /** 기본 비용 * 숙박일 계산. */
//   static calcStayAddFee(roomSale: RoomSale, roomType: RoomType, preferences: Preference, stayNum: number
//     , isWeekend: boolean, isAm: boolean
//   ) {
//     let addFee = 0;
//     if (roomSale.stay_type === Consts.StayType.SHORT_TIME) { // 대실은 대실 기본 비용으로 계산.
//       const defaultHour = isWeekend
//                             ? (isAm ? preferences.rent_time_am_weekend : preferences.rent_time_pm_weekend)
//                             : (isAm ? preferences.rent_time_am : preferences.rent_time_pm);
//       if (stayNum > defaultHour) {
//         const gap = stayNum - defaultHour;
//         addFee = gap * roomType.over_time_fee_rent;
//       }
//       return addFee;
//     }
//     else { // 숙박, 장기는 일수로 계산.
//       if (stayNum >= 2) {
//         const gap = stayNum - 1;
//         addFee = gap * roomType.default_fee_stay;
//       }
//       return addFee;
//     }
//   }

// //   /** 인원수가 객실유형보다 많으면 추가 비용 계산. */
// //   static calcPersonAddFee(roomSale: RoomSale, roomType: RoomType, preferences: Preference, stayNum: number
// //     , isWeekend: boolean, isAm: boolean
// // ) {
// //     let addFee = 0;
// //     if (roomSale.person > roomType.person) {
// //       const gap = roomSale.person - roomType.person;
// //       addFee = gap * roomType.person_add_fee;
// //     }
// //     return addFee
// //   }

//   /** 예약 할인 계산. */
//   static calcReservationFee(roomSale: RoomSale, roomType: RoomType, preferences: Preference, isWeekend: boolean, isAm: boolean) {
//   };

//   /** 타임 옵션 계산. */
//   static calcTimeOptionFee(roomSale: RoomSale, roomType: RoomType, preferences: Preference, isWeekend: boolean, isAm: boolean) {
//   };

//   /** 추가 비용 계산. (체크아웃시) */
//   static calcOverTimeFee(roomSale: RoomSale, roomType: RoomType, preferences: Preference, isWeekend: boolean, isAm: boolean) {
//   };

// }

/**
 * 상태변경 가능성 값.
 */
export enum ChangebleStateValue {
  ABLE = 1, // 상태변경 가능.
  CANCEL = 2, // 상태변경 취소 가능.
  DISABLE = 3, // 상태변경 불가능.
}

/**
 * 상태변경 가능성.
 */
export interface ChangebleState {
  checkInState: ChangebleStateValue;
  checkOutState: ChangebleStateValue;
  outtingState: ChangebleStateValue;
  setCleanState: ChangebleStateValue;
  setEmptyState: ChangebleStateValue;
  setShortTimeState: ChangebleStateValue;
  setExtendState: ChangebleStateValue;
  setMoveRoomState: ChangebleStateValue;

  isDisableCheckIn(): boolean;
  isDisableCheckOut(): boolean;
  isDisableOutting(): boolean;
  isDisableSetClean(): boolean;
  isDisableSetEmpty(): boolean;
  isDisableSetShortTime(): boolean;
  isDisableSetExtend(): boolean;
  isDisableSetMoveRoom(): boolean;
}

export class ChangebleStateClass implements ChangebleState {
  checkInState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  checkOutState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  outtingState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  setCleanState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  setEmptyState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  setShortTimeState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  setExtendState: ChangebleStateValue = ChangebleStateValue.DISABLE;
  setMoveRoomState: ChangebleStateValue = ChangebleStateValue.DISABLE;

  isDisableCheckIn(): boolean {
    return this.checkInState === ChangebleStateValue.DISABLE;
  }
  isDisableCheckOut(): boolean {
    return this.checkOutState === ChangebleStateValue.DISABLE;
  }
  isDisableOutting(): boolean {
    return this.outtingState === ChangebleStateValue.DISABLE;
  }
  isDisableSetClean(): boolean {
    return this.setCleanState === ChangebleStateValue.DISABLE;
  }
  isDisableSetEmpty(): boolean {
    return this.setEmptyState === ChangebleStateValue.DISABLE;
  }
  isDisableSetShortTime(): boolean {
    return this.setShortTimeState === ChangebleStateValue.DISABLE;
  }
  isDisableSetExtend(): boolean {
    return this.setExtendState === ChangebleStateValue.DISABLE;
  }
  isDisableSetMoveRoom(): boolean {
    return this.setMoveRoomState === ChangebleStateValue.DISABLE;
  }
}

/**
 * 체크인 관련 비즈니스 로직을 제공한다.
 */
export namespace checkInBiz {
  /**
   * 상황에 맞게 상태변경 가능성을 제공한다.
   * @param roomState
   * @param preferences
   * @param roomSale
   * @returns
   */
  export const changebleState = (
    roomState: RoomState,
    preferences: Preference,
    roomSale: RoomSale = null
  ): ChangebleState => {
    const changeble = new ChangebleStateClass();
    const isEmptyRoom = roomState.sale === null || roomState.sale === 0;

    const {
      cancel_time, // 입실취소 활성화 유지 기간 (분)
      check_out_cancle_time, // 퇴실 취소 가능 시간(분)
    } = preferences;

    const now = dayjs(); // 현재시간.

    // 청소지시/취소는 언제든 가능하다? TODO: 확인 할 것.
    if (roomState.key === 3) {
      Swal.fire({
        icon: 'info',
        title: '청소 중 입니다.',
        html: `청소가 완료된 후 상태변경 가능합니다.`,
        showConfirmButton: true,
      });
      return;
    }

    if (roomState.clean === 0) {
      changeble.setCleanState = ChangebleStateValue.ABLE; // 청소지시 가능.
    } else {
      changeble.setCleanState = ChangebleStateValue.CANCEL; // 청소지시 취소 가능.
    }

    // 공실 또는 사용중.
    if (isEmptyRoom) {
      // 공실이면.
      changeble.checkInState = ChangebleStateValue.ABLE; // 체크인 가능.

      if (roomSale?.check_out) {
        const checkOut = roomSale?.check_out ? dayjs(roomSale.check_out) : null; // 체크아웃 시간.
        const checkOutAfterMinute = now.diff(checkOut, 'minute'); // 체크아웃 후 경과 시간(분).
        if (checkOutAfterMinute > check_out_cancle_time) {
          changeble.checkOutState = ChangebleStateValue.CANCEL; // 체크아웃 취소 가능.
        }
      }
      if (changeble.checkOutState !== ChangebleStateValue.CANCEL) {
        // 체크아웃 취소 할수 없으면 비활성.
        changeble.checkOutState = ChangebleStateValue.DISABLE; // 체크아웃 취소 불가능.
      }
    } else {
      // 공실이 아니면.

      const checkIn = dayjs(roomState.check_in); // 체크인 시간.
      const checkInAfterMinute = now.diff(checkIn, 'minute'); // 체크인 후 경과 시간(분).
      console.log(
        `*** checkInAfterMinute: ${checkInAfterMinute},  cancel_time: ${cancel_time}`
      );
      if (checkInAfterMinute <= cancel_time) {
        changeble.checkInState = ChangebleStateValue.CANCEL; // 체크인 취소 가능.
      } else {
        changeble.checkInState = ChangebleStateValue.DISABLE; // 체크인 취소 불가능.
      }

      changeble.checkOutState = ChangebleStateValue.ABLE; // 체크아웃 가능.
      if (roomState.outing === 0) {
        changeble.outtingState = ChangebleStateValue.ABLE; // 외출 가능.
      } else {
        changeble.outtingState = ChangebleStateValue.CANCEL; // 외출 취소 가능.
      }
      changeble.setEmptyState = ChangebleStateValue.ABLE; // 공실처리 가능.
      if (roomState.sale !== Consts.StayType.SHORT_TIME) {
        // 대실이면.
        changeble.setShortTimeState = ChangebleStateValue.ABLE; // 대실변경 가능.
      }
      changeble.setExtendState = ChangebleStateValue.ABLE; // 연장 가능.
      changeble.setMoveRoomState = ChangebleStateValue.ABLE; // 이동 가능.
    }

    return changeble;
  };

  /**
   * 숙박/장기 체크아웃 시간을 설정 제공한다.
   * @param checkInDay  체크인 일시 Dayjs.
   * @param stayNum     숙박일수(1~4) 또는 장기 숙박일수. 5박 이상은 장기이다.
   * @param preferences 환경설정.
   * @returns
   */
  export const checkOutExpByStay = (
    checkInDay: dayjs.Dayjs,
    stayNum: number,
    preferences: Preference = null
  ): dayjs.Dayjs => {
    if (preferences === null) {
      return null;
    }

    const { stay_time_type, stay_time, stay_time_weekend } = preferences;
    let endDay: dayjs.Dayjs = dayjs();

    // 숙박시간 타입 (0:이용시간기준, 1:퇴실시간기준)
    if (stay_time_type === 0) {
      const isWeekendStart = checkInDay.isoWeekday() > 5; // 입실시간 기준. 1-7(월-일)
      const useHour = isWeekendStart ? stay_time_weekend : stay_time; // checkInDay 기준 주말이면 주말 시간 적용.

      // 이용시간 기준, 시작 시간에 머물시간(stay_time...)을 더해 종료시간을 구한다.
      if (stayNum === 0) {
        // 무박1일.
        endDay = checkInDay.add(useHour, 'hour'); // 현재 시간에 숙박시간 더하기.
      } else if (stayNum === 1) {
        // 1박2일.
        endDay = checkInDay.add(useHour, 'hour'); // 현재 시간에 숙박시간 더하기. TODO: 무박이랑 동일?
      } else {
        // 2박 이상이면 추가 날짜를 더한다.
        endDay = checkInDay.add(useHour, 'hour'); // 현재 시간에 숙박시간 더하기.
        endDay = endDay.add(stayNum - 1, 'day'); // 남은 숙박일 더하기.
      }
    } else {
      // stay_time_type === 1
      // 퇴실시간 기준. 숙박 마지막 날에 퇴실시간((stay_time...))을 설정해 종료시간을 구한다.
      if (stayNum > 0) {
        endDay = checkInDay.add(stayNum, 'day'); // 숙발일 더하고. 무박은 안더함.
      }
      const isWeekendEnd = endDay.isoWeekday() > 5; // 퇴실날자가 주말인가? 1-7(월-일)
      const useHour = isWeekendEnd ? stay_time_weekend : stay_time; // 퇴실날자가 주말이면 주말 시간 적용.
      endDay = endDay.hour(useHour).minute(0); //  퇴실시간 설정.
    }

    endDay = endDay.second(0).millisecond(0); // 초, 밀리초는 0으로 설정.
    // console.log(`*** checkOutExpByStay - endDay ::`, endDay);
    return endDay;
  };

  /**
   * 대실 체크아웃 시간을 설정 제공한다.
   * @param checkInDay  체크인 일시 Dayjs.
   * @param preferences 환경설정.
   * @returns
   */
  export const checkOutExpByShortTime = (
    checkInDay: dayjs.Dayjs,
    preferences: Preference
  ): dayjs.Dayjs => {
    if (preferences === null) {
      return null;
    }
    const useHour = stayShortHour(checkInDay, preferences);
    let endDay: dayjs.Dayjs = checkInDay.add(useHour, 'hour');
    endDay = endDay.second(0).millisecond(0); // 초, 밀리초는 0으로 설정.
    return endDay;
  };

  /**
   * 지정 시간 기준 대실 시간 제공.
   * @param targetTime
   * @param preferences
   * @returns
   */
  export const stayShortHour = (
    targetTime: dayjs.Dayjs,
    preferences: Preference
  ): number => {
    const {
      rent_time_am,
      rent_time_pm,
      rent_time_am_weekend,
      rent_time_pm_weekend,
    } = preferences;
    const isWeekend = targetTime.isoWeekday() > 5; // 입실시간 기준. 1-7(월-일)
    const isAM = targetTime.hour() < 12; // 오전인가?

    const useHour = isWeekend
      ? isAM
        ? rent_time_am_weekend
        : rent_time_pm_weekend // 주말 대실 시간.
      : isAM
      ? rent_time_am
      : rent_time_pm; // 평일 대실 시간.
    console.log(
      `*** stayShortHour - 결과: ${useHour}시간. 대상시간: ${targetTime.format(
        'YYYY-MM-DD HH:mm'
      )} -`,
      `(주말오전:${rent_time_am_weekend}시간, 주말오후:${rent_time_pm_weekend}시간, 평일오전:${rent_time_am}시간, 평일오후:${rent_time_pm}시간)`
    );
    return useHour;
  };

  /**
   * -----------------------------------------------------
   * 입실 처리.
   * -----------------------------------------------------
   * @param store
   * @param mRoomState
   * @param mRoomSale
   * @param userId
   * @returns
   */
  export const doCheckIn = async (
    store: any,
    mRoomState: RoomState,
    mRoomSale: RoomSale,
    timeOptionFee: number,
    callback?: any
  ) => {
    const { pay_cash_amt, pay_card_amt, pay_point_amt } = mRoomSale;

    const payAmount = pay_cash_amt + pay_card_amt + pay_point_amt; // 결재금액.
    const priceTotal = mRoomSale.default_fee + mRoomSale.add_fee; // 판매가.
    const unpaidTotal =
      priceTotal -
      (mRoomSale.pay_cash_amt +
        mRoomSale.pay_card_amt +
        mRoomSale.pay_point_amt); // 미수금.
    console.log(
      `*** doCheckIn - payAmount:${payAmount}, priceTotal:${priceTotal}, unpaidTotal:${unpaidTotal}`
    );

    // 1. 확인.
    if (payAmount === 0) {
      // 결재금액이 0.
      Swal.fire({
        icon: 'warning',
        title: '결재금액이 0원 입니다.',
        text: '결재 금액을 확인하세요.',
        showConfirmButton: true,
      });
      return;
    }

    let isUnpaidOk = false;
    if (unpaidTotal > 0) {
      // 미수금 있음.
      await Swal.fire({
        icon: 'question',
        title: '미수금이 있습니다.',
        html: `<span style="color: red";>미수금(${viewUtil.comma(
          unpaidTotal
        )})</span> 있는 상태로 처리하시겠습니까?`,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: '확인',
        cancelButtonText: '취소',
      }).then((result: SweetAlertResult) => {
        isUnpaidOk = result.isConfirmed;
      });
      if (!isUnpaidOk) return;
    } else if (unpaidTotal < 0) {
      // 추가 결재됨.
      await Swal.fire({
        icon: 'info',
        title: '결제 금액이 판매가보다 많습니다.',
        html: `<span style="color: red";>결제 금액(${viewUtil.comma(
          payAmount
        )})</span>을 판매가(${viewUtil.comma(priceTotal)})에 맞춰 주세요.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
      });
      return;
    }

    let isGo = false;

    await Swal.fire({
      icon: 'question',
      title: '입실처리 하시겠습니까?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    }).then((result: SweetAlertResult) => {
      console.log(`*** 입실처리 하시겠습니까? -  result:: `, result);
      if (result.isConfirmed) {
        isGo = true;
      }
    });

    if (isGo) {
      // 2. 데이터 준비.
      //   option_fee: mRoomSale.option_fee,
      //   season_fee: compSeassonFee.value,
      //   person_fee: compPersonFee.value,
      //   discount_fee: compStayValue.value,
      //   stay_days: stayNum.value,

      const clone = _.cloneDeep(mRoomSale);
      clone.state = 'A';
      clone.check_in = dayjs().format('YYYY-MM-DD HH:mm:ss');

      const sendData = {
        add_fee: clone.add_fee,
        alarm: clone.alarm,
        alarm_hour: clone.alarm_hour,
        alarm_min: clone.alarm_min,
        channel: clone.channel,
        check_in: clone.check_in,
        check_in_exp: clone.check_in_exp,
        check_out: clone.check_out,
        check_out_exp: clone.check_out_exp,
        default_fee: clone.default_fee,
        id: clone.id,
        option_fee: clone.option_fee,
        pay_card_amt: clone.pay_card_amt,
        pay_cash_amt: clone.pay_cash_amt,
        pay_point_amt: clone.pay_point_amt,
        person: clone.person,
        phone: clone.phone,
        prepay_card_amt: clone.prepay_card_amt,
        prepay_cash_amt: clone.prepay_cash_amt,
        prepay_ota_amt: clone.prepay_ota_amt,
        prepay_point_amt: clone.prepay_point_amt,
        room_id: clone.room_id,
        room_reserv_id: clone.room_reserv_id,
        save_point: clone.save_point,
        state: clone.state,
        stay_type: clone.stay_type,
        time_option_fee: timeOptionFee,
        user_id: clone.user_id,
      };

      // 3. 전송.
      let saleOK = false;
      await roomSaleBiz.newRoomSale(store, sendData, (resultItem: RoomSale) => {
        console.log(`*** doCheckIn - 입실처리 데이터 전송 결과 ::`, resultItem);
        if (resultItem !== null) {
          saleOK = true;
          mRoomSale.id = resultItem.id;
          mRoomSale.state = clone.state;
          mRoomSale.check_in = clone.check_in;
        }
      });

      if (saleOK) {
        mRoomState.room_sale_id = mRoomSale.id;
        mRoomState.sale = mRoomSale.stay_type;
        mRoomState.inspect = 0;

        const sendData = {
          id: mRoomState.id,
          room_id: mRoomState.room_id,
          air_set_temp: mRoomState.air_set_temp,
          inspect: mRoomState.inspect,
          room_sale_id: mRoomState.room_sale_id,
          sale: mRoomState.sale,
        };

        // 3. 전송.
        await roomStateBiz.setRoomState(store, sendData, (success: boolean) => {
          // 결과 처리.
          console.log(`*** doCheckIn - roomState 데이터 전송 결과 ::`, success);
        });

        // 4. 안내.
        await Swal.fire({
          icon: 'success',
          title: '입실처리 되었습니다.',
          showConfirmButton: true,
        }).then((result: SweetAlertResult) => {
          if (result.isConfirmed) {
            if (callback) callback();
          }
        });
      }
    }
  };

  /**
   * -----------------------------------------------------
   * 공실처리.
   * -----------------------------------------------------
   * @param store
   * @param mRoomState
   * @param userId
   * @param callback
   */
  export const doSetEmpty = async (
    store: any,
    mRoomState: RoomState,
    userId: string,
    callback?: any
  ) => {
    let isGo = false;

    // 1. 확인.
    await Swal.fire({
      icon: 'question',
      title: '공실처리 하시겠습니까?',
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    }).then((result: SweetAlertResult) => {
      isGo = result.isConfirmed;
    });

    if (isGo) {
      // 2. 데이터 준비.
      const clone = _.cloneDeep(mRoomState);
      clone.air_power_type = 0;
      clone.air_set_temp = clone.temp_key_3;
      clone.inspect = 0;
      clone.sale = 0;

      const sendData = {
        id: clone.id,
        room_id: clone.room_id,
        user_id: userId,
        channel: clone.channel,
        air_power_type: clone.air_power_type,
        air_set_temp: clone.air_set_temp,
        inspect: clone.inspect,
        sale: clone.sale,
      };
      console.log(`*** doSetClean - 공실처리 전송 전 데이터확인.`, sendData);

      // 3. 전송.
      await roomStateBiz.setRoomState(store, sendData, (success: boolean) => {
        // 결과 처리.
        if (success) {
          mRoomState = clone;
          // mRoomState.air_power_type = 0;
          // mRoomState.air_set_temp = mRoomState.temp_key_3;
          // mRoomState.inspect = 0;
          // mRoomState.sale = 0;
        }
      });

      // 4. 안내.
      await Swal.fire({
        icon: 'success',
        title: `공실처리 되었습니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
      }).then((result: SweetAlertResult) => {
        if (result.isConfirmed) {
          if (callback) callback();
        }
      });
    }
  };

  /**
   * -----------------------------------------------------
   * 청소지시. 또는 청소지시 중 청소지시 취소.
   * -----------------------------------------------------
   * @param store
   * @param mRoomState
   * @param callback
   */
  export const doSetClean = async (
    store: any,
    mRoomState: RoomState,
    userId: string,
    callback?: any
  ) => {
    let isGo = false;
    let title = mRoomState.clean === 1 ? '청소지시 취소' : '청소지시';

    // 1. 확인.
    await Swal.fire({
      icon: 'question',
      title: `${title}`,
      html: `${title}를 하시겠습니까?`,
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: '확인',
      cancelButtonText: '취소',
    }).then((result: SweetAlertResult) => {
      isGo = result.isConfirmed;
    });

    if (isGo) {
      // 2. 데이터 준비.
      const clone = _.cloneDeep(mRoomState);
      clone.clean = clone.clean === 1 ? 0 : 1; // 청소지시 하기 또는 취소하기.
      clone.clean_change_time = dayjs().format('YYYY-MM-DD HH:mm:ss');
      clone.air_set_temp = mRoomState.temp_key_4;

      const sendData = {
        id: clone.id,
        room_id: clone.room_id,
        user_id: userId,
        channel: clone.channel,
        clean: clone.clean,
        clean_change_time: clone.clean_change_time,
        air_set_temp: clone.temp_key_4,
      };
      console.log(`*** doSetClean - 청소지시 전송 전 데이터확인.`, sendData);

      // 3. 전송.
      let result = false;
      await roomStateBiz.setRoomState(store, sendData, (success: boolean) => {
        // 결과 처리.
        result = success;
        if (success) {
          mRoomState = clone;
          // mRoomState.clean = clone.clean;
          // mRoomState.clean_change_time = clone.clean_change_time;
          // mRoomState.air_set_temp = clone.temp_key_4;
        }
      });

      // 4. 안내.
      await Swal.fire({
        icon: 'success',
        title: `${title} 되었습니다.`,
        showConfirmButton: true,
        confirmButtonText: '확인',
      }).then((result: SweetAlertResult) => {
        if (result.isConfirmed) {
          if (callback) callback(result);
        }
      });
    }
  };
}
