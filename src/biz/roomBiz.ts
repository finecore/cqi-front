import { Floor, FloorRoom, Room } from '@/types';
import { roomStateBiz } from './roomStateBiz';
import { viewUtil } from '@/utils/view-util';

/** Room 의 모든 프로퍼티가 옵션인 RoomOpt type. */
export type RoomOpt = {
  [K in keyof Room]?: Room[K];
};

/**
 * 객실 변경 유형.
 */
export enum RoomChangeType {
  DATA = 1,
  TYPE = 2,
  FLOOR = 3,
  TYPE_FLOOR = 4,
  GID = 5,
}

/**
 * namespace roomBiz
 */
export namespace roomBiz {
  /**
   * 객실 복사하기.
   * @param
   */
  export const copyFrom = (from: Room): Room => {
    const copy: Room = {
      id: from.id,
      place_id: from.place_id,
      room_type_id: from.room_type_id,
      name: from.name,
      count: from.count,
      floor: from.floor,
      gid: from.gid,
      lid: from.lid,
      card_barcode: from.card_barcode,
      reserv_yn: from.reserv_yn,
      doorlock_id: from.doorlock_id,

      type_name: from.type_name !== undefined ? from.type_name : null,
      sale: from.sale !== undefined ? from.sale : null,
      isc_sale_1: from.isc_sale_1 !== undefined ? from.isc_sale_1 : null,
      isc_sale_2: from.isc_sale_2 !== undefined ? from.isc_sale_2 : null,
      isc_sale_3: from.isc_sale_3 !== undefined ? from.isc_sale_3 : null,
    };
    return copy;
  };

  /**
   * 층과 층의 객실 목록을 가져온다.
   * @param store
   * @param place_id
   * @param callback
   */
  export const roomFloors = async (
    store: any,
    place_id: number,
    callback?: (_list: Floor[]) => void
  ) => {
    await store
      .dispatch('room/getRoomsFloors', place_id)
      .then((floors: Floor[]) => {
        if (callback) callback(floors);
      });
  };

  /**
   * store 에서 층 목록을 가져온다.
   * @param store
   * @returns
   */
  export const getterFloors = (
    store: any,
    appendZero: boolean = false,
    defaultText: string = '층'
  ): Floor[] => {
    if (appendZero) {
      const list = [
        { no: 0, name: defaultText, room_count: 0, room_infos: '' },
      ];
      list.push(...store.getters['room/listFloors']);
      return list;
    }
    return store.getters['room/listFloors'];
  };

  /**
   * store 에서 층의 객실 목록을 가져온다.
   * @param store
   * @param holder
   * @returns
   */
  export const getterFloorRooms = (
    store: any,
    floorNo: number,
    roomsFilter?: string
  ): FloorRoom[] => {
    const floors = getterFloors(store);
    const floor = floors.find((floor: Floor) => floor.no === floorNo);

    if (roomsFilter) {
      return (
        floor?.rooms.filter((item: FloorRoom) =>
          item.name.includes(roomsFilter)
        ) || []
      );
    } else {
      return floor?.rooms || [];
    }
  };

  /**
   * 업소의 객실 목록을 조회하고 store 에 저장한다.
   * @param store
   * @param place_id
   */
  export const roomsByPlaceId = async (
    store: any,
    place_id: number,
    callback?: any
  ) => {
    const filter = `a.place_id=${place_id}`;
    await store.dispatch('room/getList', { filter }).then(({ rooms }) => {
      if (callback) callback(rooms);
    });
  };

  /**
   * 업소의 객실 목록을 store 에서 가져온다.
   * @param store
   * @returns
   */
  export const getterRooms = (store: any): Room[] => {
    return store.getters['room/list'];
  };

  /**
   * 객실 정보를 조회하고 store 에 저장한다.
   * @param store
   * @param room_id
   * @param callback
   */
  export const room = async (
    store: any,
    room_id: number,
    callback?: any
  ): Promise<void> => {
    return store.dispatch('room/getItem', room_id).then((room: Room) => {
      if (callback) callback(room);
    });
  };

  /**
   * 객실 정보를 store 에서 가져온다.
   * @param store
   * @returns
   */
  export const getterRoom = (store: any): Room => {
    return store.getters['room/item'];
  };

  /**
   * 객실이름 마지막에 '호'가 없으면 '호'를 붙인다.
   * @param roomName
   * @returns
   */
  export const roomNameHo = (roomName: string) => {
    if (roomName) {
      const lastWord = roomName.charAt(roomName.length - 1);
      if (lastWord === '호') return roomName;
      else return roomName + '호';
    }
    return '';
  };

  /**
   * ----------------------------------------------------
   * 객실 추가하기.
   * @param store
   * @param item
   * @param callback
   * @returns
   * ----------------------------------------------------
   */
  export const newRooms = async (
    store: any,
    items: Room[],
    isClone: boolean = false,
    callback?: (_success: boolean) => void
  ): Promise<boolean> => {
    const title = isClone ? '객실 복제' : '객실 추가';
    const { place_id } = items[0];
    const name =
      items.length === 1
        ? items[0].name
        : items.map((item) => item.name).join(', ');

    let isConfirmed = false;

    if (isClone) {
      // 1 확인.
      const textHtml = `<span class='t-warn'>[${name}]</span> ${
        items.length
      }개 객실을 ${isClone ? '복제' : '추가'} 하시겠습니까?`;
      const swalResult = await viewUtil.swalConfirm(title, textHtml);
      isConfirmed = swalResult.isConfirmed;
    } else {
      isConfirmed = true;
    }

    let success = false;
    if (isConfirmed) {
      // 2 전송.
      for (const item of items) {
        const ret = await store.dispatch('room/newItem', item);
        success = success || ret;
      }

      if (success) {
        await roomsByPlaceId(store, place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(
        title,
        name,
        `${items.length}개 객실이 정상적으로 ${isClone ? '복제' : '추가'}`,
        success
      );

      // callback.
      if (callback) callback(success);
    }
    return success;
  };

  /**
   * 객실 정보 변경하기.
   * @param store
   * @param sendDatas
   * @param callback
   * @returns
   */
  export const setRooms = async (
    store: any,
    sendDatas: RoomOpt[],
    changeType: RoomChangeType = RoomChangeType.DATA,
    toValue: string,
    callback?: (_success: boolean) => void
  ): Promise<boolean> => {
    // 1 확인.
    const title = '객실 변경';
    const { place_id } = sendDatas[0];
    let name =
      sendDatas.length === 1
        ? sendDatas[0].name
        : sendDatas.map((item) => item.name).join(', ');

    let textHtml = '';

    if (changeType === RoomChangeType.DATA) {
      textHtml = `<span class='t-info'>[${name}]</span> ${sendDatas.length}개 객실 정보를 변경 하시겠습니까?`;
    } else if (changeType === RoomChangeType.TYPE) {
      textHtml = `<span class='t-info'>[${name}]</span> ${sendDatas.length}개 객실타입을
          <span class='t-info'>${toValue}</span> 타입으로 변경 하시겠습니까?`;
    } else if (changeType === RoomChangeType.FLOOR) {
      textHtml = `<span class='t-info'>[${name}]</span> ${sendDatas.length}개 객실의 층을
          <span class='t-info'>${sendDatas[0].floor}층</span>으로 변경 하시겠습니까?`;
    } else if (changeType === RoomChangeType.TYPE_FLOOR) {
      textHtml = `<span class='t-info'>[${name}]</span> ${sendDatas.length}개 객실을
        <span class='t-info'>${toValue}</span>, <span class='t-info'>${sendDatas[0].floor}층</span>으로 변경 하시겠습니까?`;
    } else if (changeType === RoomChangeType.GID) {
      textHtml = `<span class='t-info'>[${name}]</span> ${sendDatas.length}개 객실 GID를 <span class='t-info'>${toValue}</span>로 변경 하시겠습니까?`;
    }

    let success = false;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);
    if (swalResult.isConfirmed) {
      // 2 전송.

      let success = false;
      for (const item of sendDatas) {
        const ret = await store.dispatch('room/setItem', item);
        success = success || ret;

        // 객실 상태 변경.
        if (
          item.isc_sale_1 !== undefined ||
          item.isc_sale_2 !== undefined ||
          item.isc_sale_3 !== undefined
        ) {
          const room_state: RoomOpt = {
            room_id: item.id,
          };
          if (item.isc_sale_1 !== undefined)
            room_state.isc_sale_1 = item.isc_sale_1;
          if (item.isc_sale_2 !== undefined)
            room_state.isc_sale_2 = item.isc_sale_2;
          if (item.isc_sale_3 !== undefined)
            room_state.isc_sale_3 = item.isc_sale_3;
          roomStateBiz.setRoomState(store, room_state);
        }
      }

      if (success) {
        await roomsByPlaceId(store, place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(
        title,
        name,
        `${sendDatas.length}개 객실이 정상적으로 변경`,
        success
      );

      // callback.
      if (callback) callback(success);
    }
    return success;
  };

  /**
   * 객실 삭제하기.
   * @param store
   * @param item
   * @param callback
   * @returns
   */
  export const delRooms = async (
    store: any,
    items: Room[],
    callback?: (_success: boolean) => void
  ): Promise<boolean> => {
    // 1 확인.
    const title = '객실 삭제';
    const { place_id } = items[0];
    let name =
      items.length === 1
        ? items[0].name
        : items.map((item) => item.name).join(', ');

    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> ${items.length}개 객실을 삭제 하시겠습니까?
        <br><br><span class='t-warn'>[주의]</span> 매출을 포함한 모든 객실관련 정보가 <span class='t-warn'>영구적으로 삭제</span> 됩니다.`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    let success = false;
    if (swalResult.isConfirmed) {
      // 2 전송.

      let success = false;
      for (const item of items) {
        const ret = await store.dispatch('room/delItem', item.id);
        success = success || ret;
      }

      if (success) {
        await roomsByPlaceId(store, place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(
        title,
        name,
        `${items.length}개 객실이 정상적으로 삭제`,
        success
      );

      // callback.
      if (callback) callback(success);
    }
    return success;
  };
}
