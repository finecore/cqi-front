import { Preference, StayItem } from '@/types';
import { enumName, booking } from '@/constants/enums';

export namespace preferencesBiz {
  /**
   * Preference 조회 및 store 에 저장.
   * @param store
   * @param place_id
   * @param callback
   */
  export const preferences = async (
    store: any,
    place_id: number,
    callback?: any
  ): Promise<void> => {
    return store
      .dispatch('preferences/getItem', place_id)
      .then((preferences: Preference) => {
        if (callback) callback(preferences);
      });
  };

  /**
   * store 에서 Preference 가져오기.
   * @param store
   * @returns
   */
  export const getterPreference = (store: any): Preference => {
    return store.getters['preferences/item'];
  };

  /**
   * Preference 변경.
   * @param store
   * @param changeValues
   * @param callback
   */
  export const setPreference = async (
    store: any,
    changeValues: any,
    callback?: any
  ) => {
    let result = false;
    await store
      .dispatch('preferences/setItem', changeValues)
      .then((success: boolean) => {
        result = success;
      });

    await preferences(store, changeValues.place_id);

    if (callback) {
      callback(result);
    }
  };

  /**
   * 숙박형태 아이템.
   * @param stayType
   * @param pref
   * @returns
   */
  export const stayItem = (stayType: number, pref: Preference): StayItem => {
    const result = { name: '', color: '' };
    result.name = enumName(booking.StayType, stayType);
    const stayTypeColor = JSON.parse(pref.stay_type_color);

    if (stayType === booking.StayType.STAY) {
      result.color = stayTypeColor['1'];
    } else if (stayType === booking.StayType.SHORT) {
      result.color = stayTypeColor['2'];
    } else if (stayType === booking.StayType.LONG) {
      result.color = stayTypeColor['3'];
    }
    return result;
  };
}
