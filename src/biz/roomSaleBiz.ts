import _ from 'lodash';
import dayjs from 'dayjs';

import { Consts } from '@/constants/Consts';
import { Preference, RoomSale, RoomType } from '@/types';
import { dateBiz } from '@/biz/dateBiz';

// import dayOfYear from 'dayjs/plugin/dayOfYear'; // TODO main.ts 에서 글로벌 처리.
// import weekOfYear from 'dayjs/plugin/weekOfYear';
// dayjs.extend(dayOfYear); //dayOfYear 플러그인 추가 (dayjs().dayOfYear() 사용 가능)
// dayjs.extend(weekOfYear); //weekOfYear 플러그인 추가 (dayjs().week() 사용 가능

/** RoomSale 클래스 */
export class RoomSaleClass implements RoomSale {
  readonly room_id: number = null;
  readonly channel: string = null;

  id: number = null;
  user_id: string = null;
  phone: string = null;
  member_id: string = null;
  serialno: string = null;
  state: string = null;
  stay_type: number = Consts.StayType.STAY;
  prev_stay_type: number = null;
  person: number = 2;
  time_option_id: number = null;
  room_reserv_id: number = null;
  check_in_exp: string = null;
  check_out_exp: string = null;
  check_in: string = null;
  check_out: string = null;
  default_fee: number = 0;
  add_fee: number = 0;
  option_fee: number = 0;
  prepay_ota_amt: number = 0;
  prepay_cash_amt: number = 0;
  prepay_card_amt: number = 0;
  prepay_point_amt: number = 0;
  pay_card_amt: number = 0;
  pay_cash_amt: number = 0;
  pay_point_amt: number = 0;
  save_point: number = 0;
  alarm: number = 0;
  alarm_hour: number = 9;
  alarm_min: number = 0;
  alarm_memo_play: number = 0;
  alarm_term: number = 10;
  alarm_repeat: number = 1;
  memo: string = null;
  car_no: string = null;
  card_approval_num: string = null;
  card_merchant: string = null;
  card_no: string = null;
  card_accepter_name: string = null;
  comment: string = null;
  qr_key_phone: string = null;
  room_name: string = null;
  room_type_id: number = null;
  place_id: string | number = null;
  sale: number = null;
  user_name: string = null;
  reg_date: string = null;
  mod_date: string = null;

  constructor(room_id: number, channel: string = Consts.Channel.FRONT) {
    this.room_id = room_id;
    this.channel = channel;
  }
}

/**
 * namespace
 */
export namespace roomSaleBiz {
  /**
   * placeId 로 RoomSale 목록 조회 및 store 에 저장.
   * @param store
   * @param place_id
   * @param callback
   */
  export const roomSaleListByPlaceId = async (
    store: any,
    place_id: number,
    callback?: any
  ) => {
    store
      .dispatch('roomSale/getList', place_id)
      .then((roomSales: RoomSale[]) => {
        if (callback) callback(roomSales);
      });
  };

  /**
   * sotre 에서 RoomSale 목록 가져오기.
   * @param store
   * @returns
   */
  export const getterRoomSaleList = (store: any): RoomSale[] => {
    return store.getters['roomSale/list'];
  };

  /**
   * roomId 로 RoomSale 조회 및 store 에 저장.
   * @param store
   * @param room_id
   * @param callback
   */
  export const roomSaleByRoomId = async (
    store: any,
    room_id: number,
    callback?: any
  ) => {
    store.dispatch('roomSale/getItem', room_id).then((roomSale: RoomSale) => {
      if (callback) callback(roomSale);
    });
  };

  /**
   * store 에서 RoomSale 가져오기.
   * @param store
   * @returns
   */
  export const getterRoomSale = (store: any): RoomSale => {
    return store.getters['roomSale/item'];
  };

  /**
   * target 에 source 값을 복사한다.
   * @param target
   * @param source
   */
  export const copyFromRoomSale = (target: RoomSale, source: RoomSale) => {
    target.id = source.id;
    target.room_id = source.room_id;
    target.user_id = source.user_id;
    target.phone = source.phone;
    target.member_id = source.member_id;
    target.channel = source.channel;
    target.serialno = source.serialno;
    target.state = source.state;
    target.stay_type = source.stay_type;
    target.prev_stay_type = source.prev_stay_type;
    target.person = source.person;
    target.time_option_id = source.time_option_id;
    target.room_reserv_id = source.room_reserv_id;
    target.check_in_exp = source.check_in_exp;
    target.check_out_exp = source.check_out_exp;
    target.check_in = source.check_in;
    target.check_out = source.check_out;
    target.default_fee = source.default_fee;
    target.add_fee = source.add_fee;
    target.option_fee = source.option_fee;
    target.prepay_ota_amt = source.prepay_ota_amt;
    target.prepay_cash_amt = source.prepay_cash_amt;
    target.prepay_card_amt = source.prepay_card_amt;
    target.prepay_point_amt = source.prepay_point_amt;
    target.pay_card_amt = source.pay_card_amt;
    target.pay_cash_amt = source.pay_cash_amt;
    target.pay_point_amt = source.pay_point_amt;
    target.save_point = source.save_point;
    target.alarm = source.alarm;
    target.alarm_hour = source.alarm_hour;
    target.alarm_min = source.alarm_min;
    target.alarm_memo_play = source.alarm_memo_play;
    target.alarm_term = source.alarm_term;
    target.alarm_repeat = source.alarm_repeat;
    target.memo = source.memo;
    target.car_no = source.car_no;
    target.card_approval_num = source.card_approval_num;
    target.card_merchant = source.card_merchant;
    target.card_no = source.card_no;
    target.card_accepter_name = source.card_accepter_name;
    target.comment = source.comment;
    target.qr_key_phone = source.qr_key_phone;
    target.room_name = source.room_name;
    target.room_type_id = source.room_type_id;
    target.place_id = source.place_id;
    target.sale = source.sale;
    target.user_name = source.user_name;
  };

  export const applyPreference = (
    store: any,
    roomSale: RoomSale,
    preferences: Preference
  ) => {};

  /**
   * 입실 설정 변경 설정.
   * @param store
   * @param roomSale
   */
  export const changeRoomSale = (
    store: any,
    roomSale: RoomSale,
    roomType: RoomType,
    preferences: Preference
  ) => {
    const now = dayjs();
    roomSale.check_in = now.format('YYYY-MM-DD HH:mm:ss');

    const isWeekend = dateBiz.isWeekend(now);
    const isAm = dateBiz.isAm(now);

    if (roomSale.stay_type === 1) {
      // 숙박.
      if (isWeekend) {
        const end = now
          .add(1, 'day')
          .hour(preferences.stay_time_weekend)
          .minute(0)
          .second(0); //주말 체크아웃 시간.
        roomSale.check_out_exp = end.format('YYYY-MM-DD HH:mm:ss');
      } else {
        const end = now
          .add(1, 'day')
          .hour(preferences.stay_time)
          .minute(0)
          .second(0); //주중 체크아웃 시간.
        roomSale.check_out_exp = end.format('YYYY-MM-DD HH:mm:ss');
      }
      roomSale.default_fee = roomType.default_fee_stay; //숙박 요금.
    } else if (roomSale.stay_type === 2) {
      // 대실.
      let endTime = null;
      if (isWeekend && isAm) {
        preferences.rent_time_am_weekend;
        endTime = now.add(preferences.rent_time_am_weekend, 'hour'); //주말 오전 대실 체크아웃 시간.
      } else if (isWeekend && !isAm) {
        preferences.rent_time_pm_weekend;
        endTime = now.add(preferences.rent_time_pm_weekend, 'hour'); //주말 오후 대실 체크아웃 시간.
      } else if (!isWeekend && isAm) {
        preferences.rent_time_am;
        endTime = now.add(preferences.rent_time_am, 'hour'); //주중 오전 대실 체크아웃 시간.
      } else if (!isWeekend && !isAm) {
        preferences.rent_time_pm;
        endTime = now.add(preferences.rent_time_pm, 'hour'); //주중 오후 대실 체크아웃 시간.
      }
      roomSale.default_fee = roomType.default_fee_rent; //대실 요금.
    } else if (roomSale.stay_type === 3) {
      // 장기.
      roomSale.default_fee = roomType.default_fee_stay; //숙박 요금.
    }
  };

  export const newRoomSale = async (
    store: any,
    roomSale: any,
    callback?: any
  ): Promise<RoomSale> => {
    return store
      .dispatch('roomSale/newItem', roomSale)
      .then((iRoomSale: RoomSale) => {
        roomSale.id = iRoomSale.id; // 생성된 ID 설정.
        if (callback) callback(iRoomSale);
        return iRoomSale;
      });
  };
}
