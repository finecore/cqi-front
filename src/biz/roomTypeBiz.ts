import { RoomType, CudResult } from '@/types';
import { dataBiz, CudType } from './dataBiz';
import { viewUtil } from '@/utils/view-util';

export interface RoomTypeExt extends RoomType {
  focus: boolean;
}

export type RoomTypeSet = {
  [K in keyof RoomType]?: RoomType[K];
};

/**
 * namespace roomBiz
 */
export namespace roomTypeBiz {

  /**
   * 객실타입 조회 및 store 에 저장.
   * @param store 
   * @param place_id 
   * @param holder 
   */
  export const roomTypes = async (store: any, place_id: number, callback?: (_list: RoomType[]) => void) => {
    return store.dispatch('roomType/getList', { place_id })
    .then((roomTypes: RoomType[]) => {
      if (callback) callback(roomTypes);
    });
  };

  /**
   * 객실타입 목록 store 에서 가져오기.
   * @param store 
   * @returns 
   */
  export const getterRoomTypes = (store: any, appendZero: boolean = false, defaultText: string = '객실유형'): RoomType[] => {
    if (appendZero) {
      const obj = {} as RoomType;
      obj.id = 0;
      obj.name = defaultText;

      const list = [ obj ];
      list.push(...store.getters['roomType/list']);
      return list;
    }
    return store.getters['roomType/list'];
  }

  /**
   * 객실ID로 객실유형 조회.
   * @param store 
   * @param room_id 
   */
  export const roomTypeByRoomId = async (store: any, room_id: number, callback?: any): Promise<void> => {
    return store.dispatch('roomType/getRoomTypeByRoomId', room_id)
    .then((roomType: RoomType) => {
      if (callback) callback(roomType);
    })
    .catch((error: any) => {
      console.log(error);
    });
  };

  /**
   * 객실타입 store 에서 가져오기.
   * @param store 
   * @returns 
   */
  export const getterRoomType = (store: any): RoomType => {
    return store.getters['roomType/item'];
  };


  /**
   * ------------------------------------------------------
   * 입력 값 확인.
   * @param roomType 
   * @returns 
   * ------------------------------------------------------
   */
  const validSendData = (roomType: RoomType): string[] => {
    const msg: string[] = [];

    roomType.name = roomType.name ? roomType.name.trim() : null;
    if (roomType.name === null || roomType.name?.length < 2) {
      msg.push('[타입명]은 2자 이상 입력해야 합니다.');
    }
    if (roomType.default_fee_stay === null) {
      msg.push('[기본요금 숙박] 값이 필요합니다.');
    }
    if (roomType.default_fee_rent === null) {
      msg.push('[기본요금 대실] 값이 필요합니다.');
    }
    if (roomType.card_add_fee === null) {
      msg.push('[추가요금 카드결제] 값이 필요합니다.');
    }
    if (roomType.card_add_fee === null) {
      msg.push('[추가요금 인원추가] 값이 필요합니다.');
    }
    if (roomType.reserv_discount_fee_stay === null) {
      msg.push('[예약할인 숙박] 값이 필요합니다.');
    }
    if (roomType.reserv_discount_fee_rent === null) {
      msg.push('[예약할인 대실] 값이 필요합니다.');
    }

    return msg;
  };

  /**
   * ------------------------------------------------------
   * 객실타입 추가하기.
   * @param store 
   * @param item 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const newRoomType = async (store: any, item: RoomType, callback?: (_success: boolean) => void) => {
    // 1 확인.
    const title = '객실타입 추가';
    const { name } = item;

    const msgs = validSendData(item);
    if (msgs.length > 0) {
      viewUtil.swalInvalidData(title, msgs);
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> 객실타입을 추가 하시겠습니까?<br/><br/>
          <span style="color: red">투숙인원과 각 요금이 맞는지 한번 더 확인하고 진행해 주세요.</span>`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      // 2 전송.
      const success = await store.dispatch('roomType/newItem', item);

      if (success) {
        await roomTypes(store, item.place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(title, name, title, success);

      // callback.
      if (callback) callback(success);
    }
  };

  /**
   * ------------------------------------------------------
   * 객실타입 변경하기.
   * @param store 
   * @param item 
   * @param originData 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const setRoomType = async (store: any, item: RoomType, originData: RoomType, callback?:(_success: boolean) => void) => {
    // 1 확인.
    const title = '객실타입 변경';
    const { name } = item;

    if (!dataBiz.hasChanged(item, originData)) {
      viewUtil.swalAlertNoChange(title);
      return;
    }

    const msgs = validSendData(item);
    if (msgs.length > 0) {
      viewUtil.swalInvalidData(title, msgs);
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> 객실타입을 변경 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      const sendData = dataBiz.extractChangedData(item, originData);

      // 2 전송.
      const success = await store.dispatch('roomType/setItem', sendData);
      // const result: CudResult<RoomType> = { success, item: roomTypeSet as RoomType };

      if (success) {
        await roomTypes(store, item.place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(title, name, title, success);

      // callback.
      if (callback) callback(success);
    }
  };

  /**
   * ------------------------------------------------------
   * 객실타입 삭제하기.
   * @param store 
   * @param item 
   * @param callback 
   * @returns 
   * ------------------------------------------------------
   */
  export const delRoomType = async (store: any, item: RoomType, callback?: (_success: boolean) => void) => {
    // 1 확인.
    const title = '객실타입 삭제';
    const { name } = item;

    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> 객실타입을 삭제 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      // 2 전송.
      const success = await store.dispatch('roomType/delItem', item.id);

      if (success) {
        await roomTypes(store, item.place_id);
      }

      // 3 결과 안내.
      viewUtil.swalAlertResult(title, name, title, success);

      // callback.
      if (callback) callback(success);
    }
  };

}
