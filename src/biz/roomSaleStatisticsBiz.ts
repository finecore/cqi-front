import { SaleRangeSum, RoomSaleSum } from '@/types/sale';
import { RangeUnit } from '@/biz/dateBiz';
import { NowAndPrevious } from '@/types';
import { dateBiz } from '@/biz/dateBiz';
import dayjs from 'dayjs';
import _ from 'lodash';

// import dayOfYear from 'dayjs/plugin/dayOfYear'; // TODO main.ts 에서 글로벌 처리.
// import weekOfYear from 'dayjs/plugin/weekOfYear';
// dayjs.extend(dayOfYear); //dayOfYear 플러그인 추가 (dayjs().dayOfYear() 사용 가능)
// dayjs.extend(weekOfYear); //weekOfYear 플러그인 추가 (dayjs().week() 사용 가능

/**
 * 매출 조회 모드.
 */
export enum StatisticsMode {
  SALE = 'sale', // 매출
  ROOM = 'room', // 객실
}

export class SaleRangeSumClass implements SaleRangeSum {
  place_id: number;
  group_num: number;
  group_year: number;
  group_sub: number;
  group_display: string;
  count: number = 0;
  default_fee: number = 0;
  add_fee: number = 0;
  pay_cash_amt: number = 0;
  pay_card_amt: number = 0;
  pay_point_amt: number = 0;
  prepay_ota_amt: number = 0;
  prepay_cash_amt: number = 0;
  prepay_card_amt: number = 0;
  prepay_point_amt: number = 0;
  total_fee: number = 0;
  total_amt: number = 0;
  unpaid_amt: number = 0;

  constructor(
    place_id: number,
    group_num: number,
    group_year: number,
    group_sub: number,
    group_display: string
  ) {
    this.place_id = place_id;
    this.group_num = group_num;
    this.group_year = group_year;
    this.group_sub = group_sub;
    this.group_display = group_display;
  }
}

/**
 * 기간별 매출 홀더.
 */
export interface SaleRangeSumHolder {
  saleSumNowPrevious: NowAndPrevious<SaleRangeSum[]>;
}

/**
 * namespace
 */
export namespace roomSaleStatisticsBiz {
  export const getSaleRangeSumNowAndPrevious = async (
    store: any,
    place_id: number,
    unit: RangeUnit,
    begin: string,
    end: string,
    holder?: SaleRangeSumHolder
  ): Promise<NowAndPrevious<SaleRangeSum[]>> => {
    const beginDay = dateBiz.dayjsFrom(begin);
    const endDay = dateBiz.dayjsFrom(end);

    let now: SaleRangeSum[] = [];
    let previous: SaleRangeSum[] = [];

    if (unit === RangeUnit.DAY_OF_YEAR) {
      // 일별 매출 조회
      now = await getSaleRangeSum(store, place_id, unit, begin, end);

      // 1년전 같은기간 매출 조회
      let preBegin = beginDay.subtract(1, 'year').format('YYYY-MM-DD');
      let preEnd = endDay.subtract(1, 'year').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.WEEK_OF_YEAR) {
      // 주별 매출 조회
      now = await getSaleRangeSum(store, place_id, unit, begin, end);

      // 1년전 같은주별 매출 조회
      let preBegin = beginDay.subtract(1, 'year').format('YYYY-MM-DD');
      let preEnd = endDay.subtract(1, 'year').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.MONTH_OF_YEAR) {
      // 월별 매출 조회
      now = await getSaleRangeSum(store, place_id, unit, begin, end);

      // 1년전 같은기간 매출 조회
      let preBegin = beginDay.subtract(1, 'year').format('YYYY-MM-DD');
      let preEnd = endDay.subtract(1, 'year').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.YEAR) {
      // 3년간 연별 매출 조회
      now = await getSaleRangeSum(store, place_id, unit, begin, end);
      previous = [];
    }

    const obj = { now, previous };
    // console.log(`*** SaleRangeSum - ${unit} => obj ::`, obj);

    if (holder) {
      holder.saleSumNowPrevious = obj;
    }
    return obj;
  };

  /**
   * @deprecated 사용안함. getSaleRangeSumNowAndPrevious 사용.
   * 기간별 현재/지난 매출을 조회한다.
   * @param store
   * @param place_id
   * @param unit
   * @param date
   * @param holder
   * @returns
   */
  export const getSaleRangeSumNowAndPrevious2 = async (
    store: any,
    place_id: number,
    unit: RangeUnit,
    date: dayjs.Dayjs | string = null,
    holder?: SaleRangeSumHolder
  ): Promise<NowAndPrevious<SaleRangeSum[]>> => {
    const day = dateBiz.dayjsFrom(date); // 기준일

    let now: SaleRangeSum[] = [];
    let previous: SaleRangeSum[] = [];

    let begin = day;
    let end = day;

    if (unit === RangeUnit.DAY_OF_YEAR) {
      // 7일간 일별 매출 조회
      begin = day.subtract(7, 'day');
      end = day;
      now = await getSaleRangeSum(
        store,
        place_id,
        unit,
        begin.format('YYYY-MM-DD'),
        end.format('YYYY-MM-DD')
      );

      // 이전달 같은기간 매출 조회
      let preBegin = begin.subtract(1, 'month').format('YYYY-MM-DD');
      let preEnd = end.subtract(1, 'month').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.WEEK_OF_YEAR) {
      // 7주간 주별 매출 조회
      begin = day.subtract(7, 'week').startOf('week');
      end = day;
      now = await getSaleRangeSum(
        store,
        place_id,
        unit,
        begin.format('YYYY-MM-DD'),
        end.format('YYYY-MM-DD')
      );

      // 1년전 같은주별 매출 조회
      let preBegin = begin.subtract(1, 'year').format('YYYY-MM-DD');
      let preEnd = end.subtract(1, 'year').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.MONTH_OF_YEAR) {
      // 12개월간 월별 매출 조회
      begin = day.subtract(12, 'month').startOf('month');
      end = day;
      now = await getSaleRangeSum(
        store,
        place_id,
        unit,
        begin.format('YYYY-MM-DD'),
        end.format('YYYY-MM-DD')
      );

      // 1년전 같은기간 매출 조회
      let preBegin = begin.subtract(1, 'year').format('YYYY-MM-DD');
      let preEnd = end.subtract(1, 'year').format('YYYY-MM-DD');
      previous = await getSaleRangeSum(store, place_id, unit, preBegin, preEnd);
    } else if (unit === RangeUnit.YEAR) {
      // 3년간 연별 매출 조회
      end = day;
      begin = day.subtract(3, 'year').startOf('year');
      now = await getSaleRangeSum(
        store,
        place_id,
        unit,
        begin.format('YYYY-MM-DD'),
        end.format('YYYY-MM-DD')
      );
      previous = [];
    }

    const obj = {
      now,
      previous,
      begin: begin.format('YYYY-MM-DD'),
      end: end.format('YYYY-MM-DD'),
    };
    // console.log(`*** SaleRangeSum - ${unit} => obj ::`, obj);

    if (holder) {
      holder.saleSumNowPrevious = obj;
    }
    return obj;
  };

  /**
   * begin ~ end 기간의 매출을 조회한다.
   * 기간 중 값이 없는 날은 0으로 채워준다.
   * @param store
   * @param place_id
   * @param begin
   * @param end
   * @param rangeUnit
   * @param holder
   * @returns
   */
  export const getSaleRangeSum = async (
    store: any,
    place_id: number,
    unit: RangeUnit,
    begin: string,
    end: string
  ) => {
    let ret = [];
    const promise = await store
      .dispatch('roomSale/getSaleRangeSum', {
        place_id,
        begin,
        end,
        unit,
      })
      .then((sale_range_sums: SaleRangeSum[]) => {
        // console.log(`*** sale_range_sums - unit(${unit}), begin(${begin}), end(${end}) ::`, sale_range_sums);
        if (sale_range_sums !== null && sale_range_sums?.length > 0) {
          ret = sale_range_sums;
        }

        if (unit !== RangeUnit.YEAR) {
          // 연별 조회는 빈값을 채우지 않는다.
          ret = makeFill(ret, place_id, begin, end, unit);
          // console.log(`*** fillSaleRangeSum - unit(${unit}), begin(${begin}), end(${end}) ::`, ret);
        }
        return ret;
      })
      .catch((error: any) => {
        console.error(`*** roomSale/getSaleRangeSum - error ::`, error);
        return ret;
      });
    return promise;
  };

  /**
   * 빈 데이터 채우기.
   * @param reals
   * @param place_id
   * @param begin
   * @param end
   * @param unit
   * @returns
   */
  const makeFill = (
    reals: SaleRangeSum[],
    place_id: number,
    begin: string,
    end: string,
    unit: RangeUnit
  ): SaleRangeSum[] => {
    let ret = [];

    const beginDay = dayjs(begin);
    const endDay = dayjs(end);

    const yearRange = endDay.year() - beginDay.year(); //0:같은년도, 1:1년차, 2:2년차, 3:3년차...

    if (yearRange === 0) {
      // 같은 년도 기간에 데이터 채우기.
      ret = makeFillSameYear(
        endDay.year(),
        beginDay,
        endDay,
        place_id,
        unit,
        reals
      );
    } else {
      // 다른 년도 기간에 데이터 채우기.
      const beginYear = beginDay.year();
      const endYear = endDay.year();

      for (let year = beginYear; year <= endYear; year++) {
        const innerBegin =
          year == beginYear ? beginDay : dayjs().year(year).month(0).date(1); // month는 0 base.
        const innerEnd =
          year === endYear
            ? endDay
            : dayjs().year(year).month(11).endOf('month'); // month는 0 base.

        const yearRet = makeFillSameYear(
          year,
          innerBegin,
          innerEnd,
          place_id,
          unit,
          reals
        );
        ret.push(...yearRet);
      }
    }
    return ret;
  };

  /**
   * RangeUnit 별 데이터 채우기.
   * @param year
   * @param begin
   * @param end
   * @param place_id
   * @param unit
   * @param reals
   * @returns
   */
  const makeFillSameYear = (
    year: number,
    begin: dayjs.Dayjs,
    end: dayjs.Dayjs,
    place_id: number,
    unit: RangeUnit,
    reals: SaleRangeSum[]
  ): SaleRangeSum[] => {
    let ret = reals;
    if (unit === RangeUnit.DAY_OF_YEAR) {
      return makeFillDates(
        year,
        begin.dayOfYear(),
        end.dayOfYear(),
        place_id,
        reals
      );
    } else if (unit === RangeUnit.WEEK_OF_YEAR) {
      return makeFillWeeks(year, begin.week(), end.week(), place_id, reals);
    } else if (unit === RangeUnit.MONTH_OF_YEAR) {
      return makeFillMonths(year, begin.month(), end.month(), place_id, reals);
    }
    return ret;
  };

  /**
   * 날짜 채우기.
   * @param year
   * @param beginOfYear
   * @param endOfYear
   * @param place_id
   * @param reals
   * @returns
   */
  const makeFillDates = (
    year: number,
    beginOfYear: number,
    endOfYear: number,
    place_id: number,
    reals: SaleRangeSum[]
  ): SaleRangeSum[] => {
    const ret = [];
    let index = 0;
    for (let doy = beginOfYear; doy <= endOfYear; doy++) {
      const real = reals[index];
      if (real && real.group_num === doy) {
        ret.push(real);
        index++;
      } else {
        const empty = dayjs().dayOfYear(doy).year(year);
        // console.log(`*** DATE :: `, empty.format('YYYY-MM-DD'));
        ret.push(
          new SaleRangeSumClass(
            place_id,
            doy,
            year,
            empty.date(),
            empty.format('M/D')
          )
        );
      }
    }
    // 남은 reals 채우기.
    if (index <= reals.length - 1) {
      //처리안된 index가 있다면.
      for (let i = index; i < reals.length; i) ret.push(reals[i]);
    }
    return ret;
  };

  /**
   * 주 채우기.
   * @param year
   * @param beginOfYear
   * @param endOfYear
   * @param place_id
   * @param reals
   * @returns
   */
  const makeFillWeeks = (
    year: number,
    beginOfYear: number,
    endOfYear: number,
    place_id: number,
    reals: SaleRangeSum[]
  ): SaleRangeSum[] => {
    const ret = [];
    let index = 0;
    for (let woy = beginOfYear; woy <= endOfYear; woy++) {
      const real = reals[index];
      if (real && real.group_num === woy) {
        ret.push(real);
        index++;
      } else {
        let empty = dayjs().week(woy).year(year).endOf('week');
        // console.log(`*** WEEK :: `, empty.format('YYYY-MM-DD'));
        ret.push(
          new SaleRangeSumClass(
            place_id,
            woy,
            year,
            woy,
            `${woy}주${empty.format('(M/D)')}`
          )
        );
      }
    }
    // 남은 reals 채우기.
    if (index <= reals.length - 1) {
      //처리안된 index가 있다면.
      for (let i = index; i < reals.length; i) ret.push(reals[i]);
    }
    return ret;
  };

  /**
   * 달 채우기.
   * @param year
   * @param beginOfYear
   * @param endOfYear
   * @param place_id
   * @param reals
   * @returns
   */
  const makeFillMonths = (
    year: number,
    beginOfYear: number,
    endOfYear: number,
    place_id: number,
    reals: SaleRangeSum[]
  ): SaleRangeSum[] => {
    const ret = [];
    let indexAndNext = 0;
    for (let moy = beginOfYear; moy <= endOfYear; moy++) {
      const real = reals[indexAndNext];
      if (real && real.group_num === moy) {
        ret.push(real);
        indexAndNext++;
      } else {
        const empty = dayjs()
          .month(moy - 1)
          .year(year)
          .endOf('month');
        // console.log(`*** MONTH :: `, empty.format('YYYY-MM-DD'));
        ret.push(
          new SaleRangeSumClass(
            place_id,
            moy,
            year,
            moy,
            empty.format('YY/M월')
          )
        );
      }
    }
    // 남은 reals 채우기.
    if (indexAndNext < reals.length - 1) {
      //처리안된 index가 있다면.
      for (let i = indexAndNext; i < reals.length; i) ret.push(reals[i]);
    }
    return ret;
  };

  export const getPlaceSum = (
    store: any,
    place_id: number,
    begin: string,
    end: string
  ) => {
    store
      .dispatch('roomSale/getPlaceSum', {
        place_id,
        begin,
        end,
      })
      .then((saleSums: SaleRangeSum[]) => {
        console.log(`*** roomSale/getPlaceSum - saleSums ::`, saleSums);
        if (saleSums !== null && saleSums.length > 0) {
          return saleSums[saleSums.length - 1];
        }
        return null;
      })
      .catch((error: any) => {
        console.error(`*** roomSale/getPlaceSum - error ::`, error);
      });
  };

  export const getRoomStaySums = (
    store: any,
    place_id: number,
    begin: string,
    end: string
  ) => {
    store
      .dispatch('roomSale/getRoomStaySums', {
        place_id,
        begin,
        end,
      })
      .then((saleSums: RoomSaleSum[]) => {
        // console.log(`*** roomSale/getRoomStaySums - result ::`, saleSums);
        return saleSums;
      })
      .catch((error: any) => {
        console.error(`*** roomSale/getRoomStaySums - error ::`, error);
      });
  };

  /**
   * 객실별 매출 조회.
   * @param store
   * @param place_id
   * @param begin
   * @param end
   */
  export const roomSaleSums = (
    store: any,
    place_id: number,
    begin: string,
    end: string
  ) => {
    store.dispatch('roomSale/getRoomSums', {
      place_id,
      begin,
      end,
    });
  };

  /**
   * store 에서 객실별 매출 가져오기.
   * @param store
   * @returns
   */
  export const getterRoomSaleSums = (store: any) => {
    return store.getters['roomSale/roomSumList'];
  };

  /**
   * 빈 객실 매출 합계.
   */
  export const EmptyRoomSaleSum: RoomSaleSum = {
    room_id: 0,
    room_name: '',
    stay_type: 0,
    count: 0,
    prepay_ota_cnt: 0,
    prepay_cash_cnt: 0,
    prepay_card_cnt: 0,
    prepay_point_cnt: 0,
    pay_cash_cnt: 0,
    pay_card_cnt: 0,
    pay_point_cnt: 0,
    default_fee: 0,
    add_fee: 0,
    option_fee: 0,
    prepay_ota_amt: 0,
    prepay_cash_amt: 0,
    prepay_card_amt: 0,
    prepay_point_amt: 0,
    pay_cash_amt: 0,
    pay_card_amt: 0,
    pay_point_amt: 0,
    total_fee: 0,
    total_amt: 0,
    unpaid_amt: 0,
  };
}
