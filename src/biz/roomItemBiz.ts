import { RoomItem, RoomViewItem } from '@/types';

/**
 * namespace roomItemBiz
 */
export namespace roomItemBiz {
  /**
   * 객실화면에 사용하는 객실아이템 목록을 가져오고 store에 저정한다.
   * @param store 
   * @param place_id 
   * @param holder 
   */
  export const roomItems = (store: any, place_id: number) => {
    store.dispatch('roomItem/getList', place_id);
  };

  /**
   * store 에서 객실아이템 목록을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterRoomItems = (store: any) => {
    return store.getters['roomItem/list'];
  };

  /**
   * store 에서 객실아이템 목록에서 지정한 room_id의 객실아이템을 가져온다.
   * @param store
   * @param roomId 
   * @returns 
   */
  export const getterRoomItemByRoomId = (store: any, roomId: number) => {
    const list = getterRoomItems(store);
    return list.find((item: RoomItem) => item.room_id === roomId) || {} as RoomItem;
  };

}
