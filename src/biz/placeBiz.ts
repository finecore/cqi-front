import dayjs from 'dayjs';
import { Place, PlaceSubscribe, Subscribe } from '@/types';
import { viewUtil } from '@/utils/view-util';
import { dataBiz } from './dataBiz';

/** 있는 프로퍼티('?')만 사용하는 수정 데이터 type. */
export type SetPlaceSubscribe = {
  [K in keyof PlaceSubscribe]?: PlaceSubscribe[K];
}

/**
 * namespace placeBiz
 */
export namespace placeBiz {
  /**
   * 업소 조회.
   * state 를 지정하면 state.place 에 업소 정보를 설정한다.
   * @param store 
   * @param place_id 
   * @param state 
   */
  export const place = async (store: any, place_id: number) => {
    store.dispatch('place/getItem', place_id);
  };

  /**
   * store 에서 업소 정보를 가져온다.
   * @param store 
   * @returns 
   */
  export const getterPlace = (store: any): Place => {
    return store.getters['place/item'];
  };

  /**
   * 업소에서 구독중인 서비스 목록을 가져오고 store 에 저장한다.
   * @param store 
   * @param place_id 
   */
  export const placeSubscribes = async (store: any, place_id: number) => {
    store.dispatch('placeSubscribe/getList', place_id);
  };

  /**
   * store 에서 업소에서 구독중인 서비스 목록을 가져온다.
   * @param store 
   * @returns 
   */
  export const getterPlaceSubscribes = (store: any): PlaceSubscribe[] => {
    return store.getters['placeSubscribe/list'];
  };


  /**
   * 전송할 데이터 정리하기.
   * @param sendData
   */
  const sendDataCut = (sendData: PlaceSubscribe) => {
    delete sendData.name;
    delete sendData.pay_yn;
    delete sendData.pay_type;
    delete sendData.code;
    delete sendData.default_fee;
    delete sendData.license_yn;
  };

  /**
   * ------------------------------------------------------
   * 업소 구독 추가하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * ------------------------------------------------------
   */
  export const newPlaceSubscribe = async (store: any, sendData: PlaceSubscribe, callback?: (_success: boolean) => void) => {
    const title = '구독 신청';
    const { name } = sendData;

    sendDataCut(sendData);

    // 1. 확인.
    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> 서비스를 구독신청 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      // 2. 전송.
      const result = await store.dispatch('placeSubscribe/newItem', sendData);

      if (result) {
        await placeSubscribes(store, sendData.place_id);
      }

      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, name, '추가', result);

      if (callback) callback(result);
    }
  };

  /**
   * ------------------------------------------------------
   * 구독 변경, 재신청 하기.
   * @param store 
   * @param isReSubscribe
   * @param sendData 
   * @param callback 
   * ------------------------------------------------------
   */
  export const setPlaceSubscribe = async (store: any, isReSubscribe: boolean, sendData: SetPlaceSubscribe, originData: PlaceSubscribe, callback?: (_success: boolean) => void) => {
    // 1. 확인.
    const { name } = sendData;
    const title = isReSubscribe ? '구독 신청' : '구독 변경';

    if (!isReSubscribe) {
      if (sendData.license_copy === originData.license_copy) {
        viewUtil.swalAlertNoChange(title);
        return;
      }
    }
  
    const textHtml = isReSubscribe 
        ? `<span style='font-weight: 800;'>[${name}]</span> 구독을 신청 하시겠습니까?`
        : `<span style='font-weight: 800;'>[${name}]</span> 구독 수를 변경 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      sendData = dataBiz.extractChangedData(sendData, originData);

      // 2. 전송.
      const result = await store.dispatch('placeSubscribe/setItem', sendData);
      if (result) {
        await placeSubscribes(store, sendData.place_id);
      }

      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, name, isReSubscribe ? '구독신청' : '변경', result);

      // callback.
      if (callback) callback(result);
    }
  };

  /**
   * ------------------------------------------------------
   * 구독 취소하기.
   * @param store 
   * @param placeSubcribe 
   * @param callback 
   * ------------------------------------------------------
   */
  export const cancelPlaceSubscribe = async (store: any, sendData: SetPlaceSubscribe, callback?: (_success: boolean) => void) => {
    // 1. 확인.
    const title = '구독 취소';
    const { name } = sendData;

    const today = dayjs().hour(0).minute(0).second(0);
    const trialEndDate = dayjs(sendData.trial_end_date);
    const isTrial = today.isBefore(trialEndDate);
  
    let trialMsg = '';

    if (isTrial) {
      sendData.trial_end_date = sendData.end_date;
      trialMsg = `<br/><br/><span style='color: red;'>* 무료 평가판 기간이 남아 있습니다.<br/>구독 취소시 무표 평가판 기간이 소멸됩니다.</span>`;
    }

    const textHtml = `<span style='font-weight: 800;'>[${name}]</span> 구독을 취소 하시겠습니까?${trialMsg}`;
    const swalResult = await viewUtil.swalConfirm(title, textHtml);

    if (swalResult.isConfirmed) {
      // 2. 전송.  
      const result = await store.dispatch('placeSubscribe/setItem', sendData);
      if (result) {
        await placeSubscribes(store, sendData.place_id);
      }

      // 3. 결과 안내.
      viewUtil.swalAlertResult(title, name, '구독 취소', result);
      
      // callback.
      if (callback) callback(result);
    }
  };
};
