import { CudResult, TimeOption } from '@/types';
import { viewUtil } from '@/utils/view-util';

/** C, U, D 제목. */
const CUD_TITLE = '입실시간 옵션';

/** timeOptionBiz */
export namespace timeOptionBiz {

  /**
   * 입실옵션 목록 조회.
   * @param store 
   * @param place_id 
   */
  export const timeOptions = async (store: any, place_id: number) => {
    await store.dispatch('timeOption/getList', place_id);
  }

  /**
   * 입실옵션 목록을 store 에서 제공하기.
   * @param store 
   * @returns 
   */
  export const getterTimeOptions = (store: any): TimeOption[] => {
    return store.getters['timeOption/list'];
  }

  /**
   * ------------------------------------------------------
   * 전송할 데이터 확인하기.
   * @param sendData 
   * @returns 
   * ------------------------------------------------------
   */
  const validSendData = (sendData: TimeOption): string[] => {
    const msg: string[] = [];

    if (sendData.name === null || sendData.name.trim().length < 2)
      msg.push('[옵션명]이 올바르지 않습니다.(2자 이상)');

    if (sendData.stay_type === null)
      msg.push('[투숙형태]를 선택해 주세요.');

    if (sendData.begin >= sendData.end)
      msg.push('[옵션시작] 시간은 [옵션종료] 시간보다 작아야 합니다.');

    if (sendData.room_type_ids.length === 0)
      msg.push('[적용 객실타입]을 선택해 주세요.');

    return msg;
  };

  /**
   * ------------------------------------------------------
   * 입실옵션 추가하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * ------------------------------------------------------
   */
  export const newTimeOption = async (store: any, sendData: TimeOption, callback?: (_cudResult: CudResult<TimeOption>) => void) => {
    // 1. 확인.
    const msg = validSendData(sendData);
    if (msg.length > 0) {
      viewUtil.swalInvalidData(CUD_TITLE, msg);
      return;
    }
  
    const textHtml = `<span style='font-weight: 800;'>[${sendData.name}]</span> 옵션을 추가 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(CUD_TITLE, textHtml);
  
    if (swalResult.isConfirmed) {
      delete sendData.room_type_ids; // 전송시 삭제.

      // 2. 전송.
      let result = false;
      await store.dispatch('timeOption/newItem', sendData)
      .then((cudResult: CudResult<TimeOption>) => {
        result = cudResult.success;
        if (callback) callback(cudResult);
      });

      if (result) {
        await timeOptions(store, sendData.place_id); // 목록 갱신. begin 정렬값 변경으로 인해.
      }
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(CUD_TITLE, sendData.name, '추가', result);
    }
  };

  /**
   * ------------------------------------------------------
   * 입실옵션 변경하기.
   * @param store 
   * @param sendData 
   * @param callback 
   * ------------------------------------------------------
   */
  export const setTimeOption = async (store: any, sendData: TimeOption, callback?: (_cudResult: CudResult<TimeOption>) => void) => {
    // 1. 확인.
    const msg = validSendData(sendData);
    if (msg.length > 0) {
      viewUtil.swalInvalidData(CUD_TITLE, msg);
      return;
    }

    const textHtml = `<span style='font-weight: 800;'>[${sendData.name}]</span> 옵션을 변경 하시겠습니까?`;
    const swalResult = await viewUtil.swalConfirm(CUD_TITLE, textHtml);
  
    if (swalResult.isConfirmed) {
      delete sendData.room_type_ids; // 전송시 삭제.

      // 2. 전송.
      let result = false;
      await store.dispatch('timeOption/setItem', sendData)
      .then((cudResult: CudResult<TimeOption>) => {
        result = cudResult.success;
        if (callback) callback(cudResult);
      });

      if (result) {
        await timeOptions(store, sendData.place_id); // 목록 갱신. begin 정렬값 변경으로 인해.
      }
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(CUD_TITLE, sendData.name, '변경', result);
    }
  };

  /**
   * ------------------------------------------------------
   * 입실옵션 삭제하기.
   * @param store
   * @param sendData 
   * @param callback 
   * ------------------------------------------------------
   */
  export const delTimeOption = async (store: any, sendData: TimeOption, callback?: (_success: boolean) => void) => {
    // 1. 확인.
    const textHtml = `<span style='font-weight: 800;'>[${sendData.name}]</span> 옵션을 삭제 하시겠습니까?<br/><br/>
                      <span style='color: red;'>삭제된 데이터는 복구할 수 없습니다.</span>`;
    const swalResult = await viewUtil.swalConfirm(CUD_TITLE, textHtml);
  
    if (swalResult.isConfirmed) {  
      // 2. 전송.
      let result = false;
      await store.dispatch('timeOption/delItem', sendData.id)
      .then((success: boolean) => {
        result = success;
        if (callback) callback(success);
      })
  
      // 3. 결과 안내.
      viewUtil.swalAlertResult(CUD_TITLE, sendData.name, '삭제', result);
    }
  };

}