import { Subscribe, CudResult } from '@/types';
import { CudType } from '@/biz/dataBiz';
import { viewUtil } from '@/utils/view-util';

/**
 * namespace subsBiz.
 */
export namespace subsBiz {

  /**
   * 구독 서비스 목록을 가져오고 store 에 저장한다.
   * @param store 
   */
  export const subscribes = async (store: any) => {
    store.dispatch('subscribe/getList', {});
  };

  export const getterSubscribes = (store: any): Subscribe[] => {
    return store.getters['subscribe/list'];
  };
}
