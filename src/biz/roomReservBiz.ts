import dayjs from 'dayjs'
import _ from 'lodash';
import { RoomReserv, NowAndPrevious } from "@/types";
import { ReservCount, OtaReservCount, StayReservCount } from "@/types/reserv";
import { RangeUnit } from "@/biz/dateBiz";

/**
 * ReservCount 클래스 생성
 */
export class ReservCountClass implements ReservCount {
  place_id: number = 0;
  group1: number = 0;
  group2: number = 0;
  count: number = 0;

  constructor(place_id: number, group1: number, group2: number, count: number = 0) {
    this.place_id = place_id;
    this.group1 = group1;
    this.group2 = group2;
    this.count = count;
  }
};

export class OtaReservCountClass implements OtaReservCount {
  readonly _tp_ = "OtaReservCount";
  place_id: number = 0;
  group_num: number = 0;
  total_count: number = 0;
  ota_1_count: number = 0;
  ota_2_count: number = 0;
  ota_3_count: number = 0;
  ota_etc_count: number = 0;

  constructor(place_id: number, group_num: number) {
    this.place_id = place_id;
    this.group_num = group_num;
  }
}

/**
 * 기간별 예약건수 저장 홀더.
 */
export interface ReservCountHolder {
  reservCountDay: NowAndPrevious<ReservCount[]>;
  reservCountWeek: NowAndPrevious<ReservCount[]>;
  reservCountMonth: NowAndPrevious<ReservCount[]>;
}

/**
 * 기간별 OTA 예약 건수 저장 홀더.
 */
export interface OtaReservCountHolder {
  otaReservCountDay: OtaReservCount[];
  otaReservCountWeek: OtaReservCount[];
  otaReservCountMonth: OtaReservCount[];
}

/**
 * 기간별 숙박형태 예약 건수 저장 홀더.
 */
export interface StayReservCountHolder {
  stayReservCountDay: StayReservCount[];
  stayReservCountWeek: StayReservCount[];
  stayReservCountMonth: StayReservCount[];
}


export interface ReservCounter {
  total: number;    // 총수
  entered: number;  // 처리된 수
  pending: number;  // 대기중 수
  cancel: number;   // 취소 수
}

export class ReservCounterClass implements ReservCounter {
  total = 0;
  entered = 0;
  pending = 0;
  cancel = 0;
}

// export interface RoomReservHolder {
//   reservs: ReservCounter;
// }

/**
 * namespace
 */
export namespace roomReservBiz {
  /**
   * 
   * @param store 
   * @param place_id 
   * @param begin 
   * @param end 
   */
  export const roomReservs = async (store: any, place_id: number, begin: string, end: string) => {
    store.dispatch('roomReserv/getListByDate', { place_id, begin, end });
  };

  export const getterReservList = (store: any): RoomReserv[] => {
    return store.getters['roomReserv/list'];
  };

  export const getterReservCounter = (store: any): ReservCounter => {
    return store.getters['roomReserv/reservCounter'];
  };

  /**
   * [일] 시간별 예약 건수 조회.
   * 오늘과, 어제의 에약 건수를 조회하고 없는 시간대는 0으로 채워서 반환한다.
   * @param store 
   * @param place_id 
   * @param date 
   * @returns 
   */
  export const getReservCountByDay = async (store: any, place_id: number, date: dayjs.Dayjs = null, holder?: ReservCountHolder): Promise<NowAndPrevious<ReservCount[]>> => {
    const anyDay = date !== null ? date : dayjs();
    const begin: string = anyDay.subtract(1, 'day').format('YYYY-MM-DD');
    const end: string = anyDay.format('YYYY-MM-DD');

    let now: ReservCount[] = [];
    let previous: ReservCount[] = [];

    //오늘과 어제 예약 건수 조회.
    await store.dispatch('roomReserv/getReservCountByDay', { place_id, begin, end })
    .then((reserv_counts: ReservCount[]) => {
      // console.log(`*** roomReserv/getReservCountByDay - reserv_counts ::`, reserv_counts);
      //현재일
      const dateNum = anyDay.date(); // 1 to 31.
      let prevNum = dateNum - 1;
      for (const reservCount of reserv_counts) {
        if (reservCount.group1 == dateNum) {
          now.push(reservCount); //기준일 데이터에 넣는다.
        } else {
          previous.push(reservCount); //기준 이전일 데이터에 넣는다.
          prevNum = reservCount.group1;
        }
      }

      now = fill24Hours(now, place_id, dateNum); //없는 시간대는 0으로 채운다.
      previous = fill24Hours(previous, place_id, prevNum); //없는 시간대는 0으로 채운다. dataNum -1은 어제. 값을
    })
    .catch((error: any) => {
      console.error(`*** roomReserv/getReservCountByDay - error ::`, error);
    });

    const obj = { now, previous };
    if (holder) {
      holder.reservCountDay = obj;
    }
    return obj;
  };

  /**
   * [주] 요일별 예약 건수 조회.
   * 이번주와, 지난주의 에약 건수를 조회하고 없는 요일은 0으로 채워서 반환한다.
   * @param store 
   * @param place_id 
   * @param date 
   * @returns 
   */
  export const getReservCountByWeek = async (store: any, place_id: number, date: dayjs.Dayjs = null, holder?: ReservCountHolder): Promise<NowAndPrevious<ReservCount[]>> => {
    const anyDay = date != null ? date : dayjs();
    const begin: string = anyDay.subtract(1, 'week').startOf('week').format('YYYY-MM-DD'); //지난주 시작일(일요일)
    const end: string = anyDay.endOf('week').format('YYYY-MM-DD'); //이번주 마지막일(토요일)

    let now: ReservCount[] = [];
    let previous: ReservCount[] = [];

    //이번주와 지난주 예약 건수 조회.
    await store.dispatch('roomReserv/getReservCountByWeek', { place_id, begin, end })
    .then((reserv_counts: ReservCount[]) => {
      // console.log(`*** roomReserv/getReservCountByWeek - reserv_counts ::`, reserv_counts);
      //이번주
      const weekNum = anyDay.week(); // 1 to 53.
      let prevNum = weekNum - 1;
      for (const reservCount of reserv_counts) {
        if (reservCount.group1 == weekNum) {
          now.push(reservCount); //이번주 데이터에 넣는다.
        } else {
          previous.push(reservCount); //지난주 데이터에 넣는다.
          prevNum = reservCount.group1;
        }
      }

      now = fillWeek(now, place_id, weekNum); //없는 요일은 0으로 채운다.
      previous = fillWeek(previous, place_id, prevNum); //없는 요일은 0으로 채운다.
    })
    .catch((error: any) => {
      console.error(`*** roomReserv/getReservCountByWeek - error ::`, error);
    });
    
    const obj = { now, previous };
    if (holder) {
      holder.reservCountWeek = obj;
    }
    return obj;
  };

  /**
   * [월] 날자별 예약 건수 조회.
   * 이번달과, 지난달의 에약 건수를 조회하고 없는 날자는 0으로 채워서 반환한다.
   * @param store 
   * @param place_id 
   * @param date 
   * @returns 
   */
  export const getReservCountByMonth = async (store: any, place_id: number, date: dayjs.Dayjs = null, holder?: ReservCountHolder): Promise<NowAndPrevious<ReservCount[]>> => {
    const anyDay = date != null ? date : dayjs();
    const begin: string = anyDay.subtract(1, 'month').startOf('month').format('YYYY-MM-DD'); //지난달 시작일(1일)
    const end: string = anyDay.endOf('month').format('YYYY-MM-DD'); //이번달 마지막일

    let now: ReservCount[] = [];
    let previous: ReservCount[] = [];

    //이번달과 지난달 예약 건수 조회.
    await store.dispatch('roomReserv/getReservCountByMonth', { place_id, begin, end })
    .then((reserv_counts: ReservCount[]) => {
      // console.log(`*** roomReserv/getReservCountByMonth - reserv_counts ::`, reserv_counts);
      //이번달
      const monthNum = anyDay.month() + 1; // 1 to 12
      let prevNum = monthNum - 1;
      for (const reservCount of reserv_counts) {
        if (reservCount.group1 == monthNum) {
          now.push(reservCount); //이번달 데이터에 넣는다.
        } else {
          previous.push(reservCount); //지난달 데이터에 넣는다.
          prevNum = reservCount.group1;
        }
      }

      now = fillMonth(now, place_id, monthNum, anyDay.daysInMonth()); //없는 날자는 0으로 채운다.
      previous = fillMonth(previous, place_id, prevNum, anyDay.subtract(1, 'month').daysInMonth()); //없는 날자는 0으로 채운다.
    })
    .catch((error: any) => {
      console.error(`*** roomReserv/getReservCountByMonth - error ::`, error);
    });

    const obj = { now, previous };
    if (holder) {
      holder.reservCountMonth = obj;
    }
    return obj;
  };

  /**
   * 0 ~ 23시까지 빈 시간 데이터 채우기.
   * @param reservCounts 
   * @param place_id 
   * @param dateNum 
   * @returns 
   */
  const fill24Hours = (reservCounts: ReservCount[], place_id: number, dateNum: number): ReservCount[] => {
    const fills: ReservCount[] = [];
    let itemIndex = 0;
    for (let idx = 0; idx < 24; idx++) {
      const item = reservCounts[itemIndex];
      if (item?.group2 == idx) {
        fills.push(new ReservCountClass(place_id, dateNum, idx, item.count));
        itemIndex++;
      } else {
        fills.push(new ReservCountClass(place_id, dateNum, idx));
      }
    }
    return fills;
  };

  /**
   * 0=sunday ~ 6=saturday까지 빈 요일 데이터 채우기.
   * @param reservCounts 
   * @param place_id 
   * @param weekNum 
   * @returns 
   */
  const fillWeek = (reservCounts: ReservCount[], place_id: number, weekNum: number): ReservCount[] => {
    const fills: ReservCount[] = [];
    let itemIndex = 0;
    for (let idx = 0; idx < 7; idx++) {
      const item = reservCounts[itemIndex];
      if (item?.group2 == idx) {
        fills.push(new ReservCountClass(place_id, weekNum, idx, item.count));
        itemIndex++;
      } else {
        fills.push(new ReservCountClass(place_id, weekNum, idx));
      }
    }
    return fills;
  };

  /**
   * 1 ~ daysInMonth(28, 29, 30, 31)까지 빈 날짜 데이터 채우기.
   * @param reservCounts 
   * @param place_id 
   * @param monthNum 
   * @param daysInMonth 
   * @returns 
   */
  const fillMonth = (reservCounts: ReservCount[], place_id: number, monthNum: number, daysInMonth: number): ReservCount[] => {
    const fills: ReservCount[] = [];
    let itemIndex = 0;
    for (let idx = 0; idx < daysInMonth; idx++) {
      const item = reservCounts[itemIndex];
      if (item?.group2 == idx) {
        fills.push(new ReservCountClass(place_id, monthNum, idx, item.count));
        itemIndex++;
      } else {
        fills.push(new ReservCountClass(place_id, monthNum, idx));
      }
    }
    return fills;
  };


  /**
   * 기간별 OTA 예약 건수 조회.
   * @param store 
   * @param place_id 
   * @param date 
   * @param unit 
   * @param holder 
   * @returns 
   */
  export const getOtaReservCount = async (store: any, place_id: number, date: dayjs.Dayjs = null, unit: RangeUnit = RangeUnit.DAY_OF_YEAR, holder?: OtaReservCountHolder): Promise<OtaReservCount[]> => {
    const anyDay = date != null ? date : dayjs();

    let begin: string = '';
    if (unit == RangeUnit.DAY_OF_YEAR) {
      begin = anyDay.subtract(6, 'day').format('YYYY-MM-DD'); //기준일 기준 6일전까지.
    } else if (unit == RangeUnit.WEEK_OF_YEAR) {
      begin = anyDay.subtract(4, 'week').startOf('week').format('YYYY-MM-DD'); //기준일 기준 4주전까지.
    } else if (unit == RangeUnit.MONTH_OF_YEAR) {
      begin = anyDay.subtract(2, 'month').startOf('month').format('YYYY-MM-DD'); //기준일 기준 2달전까지.
    }
    const end: string = anyDay.format('YYYY-MM-DD'); //이번달 마지막일

    const otaReservCounts: OtaReservCount[] = [];
    await store.dispatch('roomReserv/getOtaReservCount', { place_id, begin, end, unit })
    .then((ota_reserv_counts: OtaReservCount[]) => {
      console.log(`*** roomReserv/getOtaReservCount - unit: ${unit} => reserv_counts ::`, ota_reserv_counts);

      otaReservCounts.push(...ota_reserv_counts);
      if (holder) {
        if (unit == RangeUnit.DAY_OF_YEAR) {
          holder.otaReservCountDay = otaReservCounts;
          holder.otaReservCountDay = [ // TEST
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 224, ota_1_count: 10, ota_2_count: 15, ota_3_count: 9, ota_etc_count: 15, total_count: 49 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 223, ota_1_count: 23, ota_2_count: 13, ota_3_count: 8, ota_etc_count: 14, total_count: 58 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 222, ota_1_count: 17, ota_2_count: 18, ota_3_count: 6, ota_etc_count: 15, total_count: 56 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 221, ota_1_count: 15, ota_2_count: 16, ota_3_count: 8, ota_etc_count: 14, total_count: 53 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 220, ota_1_count: 13, ota_2_count: 19, ota_3_count: 6, ota_etc_count: 15, total_count: 53 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 219, ota_1_count: 21, ota_2_count: 15, ota_3_count: 9, ota_etc_count: 14, total_count: 59 },
            { _tp_: 'OtaReservCount', place_id: 34, group_num: 218, ota_1_count: 19, ota_2_count: 11, ota_3_count: 7, ota_etc_count: 15, total_count: 52 },
          ];
        } else if (unit == RangeUnit.WEEK_OF_YEAR) {
          holder.otaReservCountWeek = otaReservCounts;
        } else if (unit == RangeUnit.MONTH_OF_YEAR) {
          holder.otaReservCountMonth = otaReservCounts;
        }
      }
    });

    return otaReservCounts;
  };


  export const EmptyOtaReservCount: OtaReservCount = {} as OtaReservCount;

  export const EmptyOtaReservCountNowPrevious: NowAndPrevious<OtaReservCount> = { now: new OtaReservCountClass(0, 0), previous: new OtaReservCountClass(0, 0) }

  /** */
  export const otaReservNowAndPrevious = (list: OtaReservCount[], rangeNum: number = 0): NowAndPrevious<OtaReservCount> => {
    if (list == null || list.length == 0) {
      return EmptyOtaReservCountNowPrevious;
    }
    const reverseList = _.reverse(list);
    const now = reverseList[rangeNum];
    let previous = null;
    if (rangeNum + 1 <= reverseList.length - 1) {
      previous = reverseList[rangeNum + 1];
    } else {
      previous = new OtaReservCountClass(0, 0);
    }
    return { now, previous };
  };

  /**
   * 기간별 숙박형태 예약 건수 조회.
   * @param store 
   * @param place_id 
   * @param date 
   * @param unit 
   * @param holder 
   * @returns 
   */
  export const getStayReservCount = async (store: any, place_id: number, date: dayjs.Dayjs = null, unit: RangeUnit = RangeUnit.DAY_OF_YEAR, holder?: StayReservCountHolder): Promise<StayReservCount[]> => {
    const anyDay = date != null ? date : dayjs();

    let begin: string = '';
    if (unit == RangeUnit.DAY_OF_YEAR) {
      begin = anyDay.subtract(6, 'day').format('YYYY-MM-DD'); //기준일 기준 6일전까지.
    } else if (unit == RangeUnit.WEEK_OF_YEAR) {
      begin = anyDay.subtract(4, 'week').startOf('week').format('YYYY-MM-DD'); //기준일 기준 4주전까지.
    } else if (unit == RangeUnit.MONTH_OF_YEAR) {
      begin = anyDay.subtract(2, 'month').startOf('month').format('YYYY-MM-DD'); //기준일 기준 2달전까지.
    }
    const end: string = anyDay.format('YYYY-MM-DD'); //이번달 마지막일

    const otaReservCounts: StayReservCount[] = [];
    await store.dispatch('roomReserv/getStayReservCount', { place_id, begin, end, unit })
    .then((stay_reserv_counts: StayReservCount[]) => {
      // console.log(`*** roomReserv/getStayReservCount - unit: ${unit} => stay_reserv_counts ::`, stay_reserv_counts);

      otaReservCounts.push(...stay_reserv_counts);
      if (holder) {
        if (unit == RangeUnit.DAY_OF_YEAR) {
          holder.stayReservCountDay = otaReservCounts;
          holder.stayReservCountDay = [ // TEST
            { _tp_: 'StayReservCount', place_id: 34, group_num: 224, stay_1_count: 10, stay_2_count: 15, stay_3_count: 9, total_count: 34 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 223, stay_1_count: 23, stay_2_count: 13, stay_3_count: 8, total_count: 44 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 222, stay_1_count: 17, stay_2_count: 18, stay_3_count: 6, total_count: 31 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 221, stay_1_count: 15, stay_2_count: 16, stay_3_count: 8, total_count: 39 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 220, stay_1_count: 13, stay_2_count: 19, stay_3_count: 6, total_count: 38 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 219, stay_1_count: 21, stay_2_count: 15, stay_3_count: 9, total_count: 45 },
            { _tp_: 'StayReservCount', place_id: 34, group_num: 218, stay_1_count: 19, stay_2_count: 11, stay_3_count: 7, total_count: 37 },
          ];
        } else if (unit == RangeUnit.WEEK_OF_YEAR) {
          holder.stayReservCountWeek = otaReservCounts;
        } else if (unit == RangeUnit.MONTH_OF_YEAR) {
          holder.stayReservCountMonth = otaReservCounts;
        }
      }
    });

    return otaReservCounts;
  };


  export const EmptyStayReservCount: StayReservCount = {} as StayReservCount;

  export const EmptyStayReservCountNowPrevious: NowAndPrevious<StayReservCount> = { now: EmptyStayReservCount, previous: EmptyStayReservCount }

  /** */
  export const stayReservNowAndPrevious = (list: StayReservCount[], rangeNum: number = 0): NowAndPrevious<StayReservCount> => {
    if (list == null || list.length == 0) {
      return EmptyStayReservCountNowPrevious;
    }
    const reverseList = _.reverse(list);
    const now = reverseList[rangeNum];
    let previous = null;
    if (rangeNum + 1 <= reverseList.length - 1) {
      previous = reverseList[rangeNum + 1];
    } else {
      previous = EmptyStayReservCount;
    }
    return { now, previous };
  };

}