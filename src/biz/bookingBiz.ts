import dayjs, { Dayjs } from 'dayjs'
import _ from 'lodash';
import { Booking, StayItem, CudResult } from "@/types";
import { RangeType } from '@/biz/dateBiz';


/** Booling 확장. */
interface BookingExt extends Booking {
  otaName: string;
  stateName: string;
  stayItem: StayItem;
}

/** 모든 필드가 옵션(?)인 BookingSet. */
export type BookingSet = {
  [K in keyof BookingExt]?: BookingExt[K];
};

/** 예약 목록/등록(수정) 제어 */
export interface BookingControl {
  isNewMode: boolean;     // 신규 등록 모드 여부.
  isFirstSearch: boolean; // 최초 조회 여부.
  tabNo: number;          // 탭 번호.
  bookingId: number;      // 예약 ID.
}


export interface BookingListState {
  rangeType: number;    // 기간 유형. dataBiz.RangeType 참조.
  rangeBegin: Dayjs;   // 기간 시작일, 종료일.
  rangeEnd: Dayjs;
  bookingState: string; // 예약상태.
  roomType: number;     // 객실유형.
  filterText: string;   // 검색어.
}

/**
 * namespace
 */
export namespace bookingBiz {

  export const newBookingListState = (): BookingListState => {
    const today = dayjs();
    return {
      rangeType: RangeType.TODAY,
      rangeBegin: today,
      rangeEnd: today,
      bookingState: null,
      roomType: 0,
      filterText: null,
    }
  };

  /**
   * 
   * @param store 
   * @param place_id 
   * @param begin 
   * @param end 
   */
  export const bookings = async (store: any, place_id: number, begin: string, end: string) => {
    await store.dispatch('roomReserv/getListByDate', { place_id, begin, end });
  };

  export const getterBookings = (store: any): Booking[] => {
    return store.getters['roomReserv/list'];
  };

  export const bookingItem = async (store: any, booking_id: number, callback?:(_bookingData: Booking) => void) => {
    const result = await store.dispatch('roomReserv/getItem', booking_id);
    const { success, room_reserv } = result;
    if (callback) callback(room_reserv);
  };

  export const getterBookingItem = (store: any): Booking => {
    return store.getters['roomReserv/item'];
  }

}