/** CudType */
export enum CudType {
  New = 'new',
  Set = 'set',
  Del = 'del',
  Pwd = 'pwd',
}

/**
 * namespace dataBiz.
 */
export namespace dataBiz {
  
  /**
   * 변경된 데이터가 있는지 확인한다.
   * @param data 
   * @param originalData 
   * @returns 
   */
  export const hasChanged = (data: object, originalData: object): boolean => {
    for (const [key, value] of Object.entries(data)) {
      // console.log(`*** dataBiz.hasChanged() key: ${key}, value type: ${typeof value}, value: ${value} <-> orgin: ${originalData[key]}`);

      if (key === 'mod_date') { // 이 키는 무시.
        continue;
      }
      if (value === undefined || value === null) { // 값이 없으면 무시.
        continue;
      }
      
      // 내부 값이 객체이면, 객체의 내부 값들을 비교한다.
      if (typeof value === 'object') {

        return hasChanged(value, originalData[key]);

        // // const orginObj = JSON.parse(originalData[key]);
        // console.log(`*** hasChanged() - key: ${key}, value:: `, value);
        // for (const [k, v] of Object.entries(value)) {
        //   // console.log(`*** dataBiz.hasChanged()2 - object ${key}[${k}]: ${v} <-> orgin: ${originalData[k]}`);
        //   if (k === 'mod_date') { // 이 키는 무시.
        //     continue;
        //   }
        //   if (v === undefined || v === null) { // 값이 없으면 무시.
        //     continue;
        //   }
          
        //   if (v !== originalData[k]) {
        //     return true;
        //   }
        // }
      }
      else if (value !== originalData[key]) {
        return true;
      }
    }
    return false;
  };

  /**
   * 'id', '.._id', 그리고 변경된 데이터만 추출한다.
   * @param data 
   * @param originalData 
   * @returns 
   */
  export const extractChangedData = (data: object, originalData: object): object => {
    const extractData = {};
    for (const [key, value] of Object.entries(data)) {
      if (key === 'id' || key.endsWith('_id')) {
        extractData[key] = value;
      }
      else if (value !== originalData[key]) {
        extractData[key] = value;
      }
    }
    return extractData;
  };
}