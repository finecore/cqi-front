import { RoomFee } from '@/types';

export namespace roomFeeBiz {

  export const roomFees = async (store: any, place_id: number, begin: string, end: string) => {
    return store.dispatch('seasonPremium/getList', { place_id, begin, end });
  };

  export const getterSeasonPremiums = (store: any) => {
    return store.getters['seasonPremium/list'];
  };

  export const roomFee = async (store: any, id: number) => {
    return store.dispatch('seasonPremium/getItem', { id });
  };

  export const getterSeasonPremium = (store: any) => {
    return store.getters['seasonPremium/item'];
  };

}