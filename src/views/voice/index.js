import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Wave } from "better-react-spinkit";
import cx from 'classnames';
import _ from 'lodash';

// Actions.
import { actionCreators as voiceAction } from 'actions/voice';

import audioPlayer from 'tts';
import createApp from 'canvas-loop';
import createAnalyser from 'web-audio-analyser';
import createAudioContext from 'ios-safe-audio-context';
// import detectAutoplay from "detect-audio-autoplay";
// import detectMediaSource from "detect-media-element-source";
import average from 'analyser-frequency-average';

import { TweenMax, Power1 } from 'gsap';

class Voice extends Component {
  constructor(props) {
    super(props);

    this.state = {
      leftMenuHeight: 0,
      sound: [], // build/ (dev server 는 public/),
      mute: true,
      on: undefined,
    };

    // this.audio = new Audio(this.state.track);

    // get our canvas element & 2D context
    this.canvas = null;
    this.ctx = null;
    this.app = {};
    this.maxRadius = 0;

    this.queue = [];
    this.playInterrupt = false;
    this.isFirstQueue = true;
    this._isMounted = false;
  }

  canplay = (callback) => {
    // Create an iOS-safe AudioContext which fixes
    // potential sampleRate bugs with playback
    // (The hack needs to be called on touchend for iOS!)
    this.audioContext = createAudioContext();

    // Detect whether createMediaElementSource() works
    // as expected. You can also use userAgent sniffing here.
    // detectMediaSource(supportsMediaElement => {
    //   // No media element support -> we should buffer
    //   this.shouldBuffer = !supportsMediaElement;
    //   if (callback) callback();
    // }, this.audioContext);
  };

  start = (source, mute, callback) => {
    if (!source) return;

    // Create a looping audio player with our audio context.
    // On mobile, we use the "buffer" mode to support AudioAnalyser.
    let player = audioPlayer(source, {
      context: this.audioContext,
      buffer: true,
      loop: false,
    });

    // Set up our AnalyserNode utility
    // Make sure to use the same AudioContext as our player!
    this.audioUtil = createAnalyser(player.node, player.context, {
      stereo: false,
    });

    // The actual AnalyserNode
    this.analyser = this.audioUtil.analyser;

    //  console.log("- this.app", this.app);

    // start the render loop
    this.app.on('tick', this.draw);
    this.app.start();

    // This is triggered on mobile, when decodeAudioData begins.
    player.once('decoding', (amount) => {
      //loading.innerText = "Decoding...";
    });

    // Only gets called when loop: false
    player.on('end', () => {
      console.log('Audio ended');
      if (callback) callback(true);
      player.dispose(); // 리스너 제거.
    });

    // If there was an error loading the audio
    player.on('error', (err) => {
      console.error('- player error', err.message);
      player.dispose(); // 리스너 제거.
      if (callback) callback(false);
    });

    // This is called with 'canplay' on desktop, and after
    // decodeAudioData on mobile.
    player.on('load', () => {
      //  console.log("Source:", player.element ? "MediaElement" : "Buffer");

      // start audio node
      if (!mute) {
        console.log('Playing', Math.round(player.duration) + 's of audio...');
        player.play();
      }
    });

    this.player = player;
  };

  draw = () => {
    let width = this.app.shape[0];
    let height = this.app.shape[1];

    // retina scaling
    this.ctx.save();
    this.ctx.scale(this.app.scale, this.app.scale);
    // this.ctx.clearRect(0, 0, width, height); // circle wave 에서 지움.

    // grab our byte frequency data for this frame
    let freqs = this.audioUtil.frequencies();

    // find an average signal between two Hz ranges
    let minHz = 40;
    let maxHz = 100;
    let avg = average(this.analyser, freqs, minHz, maxHz);

    let radius = (Math.min(width, height) / 3) * avg;

    // draw a circle
    this.ctx.beginPath();
    this.ctx.arc(width / 2, height / 2, radius, 0, Math.PI * 2);
    this.ctx.fillStyle = '#FF0040';
    this.ctx.fill();
    this.ctx.restore();

    if (this.maxRadius === 0 && radius > 5) {
      this.maxRadius = radius;

      console.log('- new Circle', radius);

      let circle = new Circle(
        this.ctx,
        this.width,
        this.height,
        this.width / 2,
        this.height / 2,
        1.5,
        radius,
        30,
        '#DF3A01',
        () => {
          this.maxRadius = 0;
        }
      );

      circle.start();
    }
  };

  voiceOnOff = (flag, play) => {
    const { sound } = this.state;

    this._isMounted &&
      this.setState(
        {
          on: flag,
        },
        () => {
          if (this.player && this.player.playing) this.player.pause();

          // canvas resize
          this.app = createApp(this.canvas, {
            scale: 1,
          });

          this.width = this.app.shape[0];
          this.height = this.app.shape[1];

          console.log('- voice onoff ', flag, play, sound[this.state.on]);
          if (play) this.start(sound[this.state.on], false);
        }
      );
  };

  componentDidMount() {
    this._isMounted = true;

    // get our canvas element & 2D context
    this.ctx = this.canvas.getContext('2d');

    this.app = createApp(this.canvas, {
      scale: 1,
    });

    this.width = this.app.shape[0];
    this.height = this.app.shape[1];

    this.canplay(() => {});

    if (this.ceicleTimer) clearInterval(this.ceicleTimer);

    let circle = null;

    // 웨이브 타이머.
    this.ceicleTimer = setInterval(() => {
      circle = null;

      if (!this.player || !this.player.playing) {
        circle = new Circle(
          this.ctx,
          this.width,
          this.height,
          this.width / 2,
          this.height / 2,
          2.0,
          5,
          30,
          this.state.on ? '#fff' : '#666'
        );

        circle.start();
      }
    }, 5 * 1000);

    if (this.playTimer) clearInterval(this.playTimer);

    let playIn = 0;
    let playOk = 0;
    let clearTimer = null;

    // 음성 큐 재생.
    this.playTimer = setInterval(() => {
      if (this.queue.length)
        console.log(
          '- voice queue playTimer',
          this.playInterrupt,
          this.isFirstQueue
        );

      // 한 문장씩 재생.
      if (
        this.audioContext &&
        !this.playInterrupt &&
        !this.isFirstQueue &&
        this.queue.length
      ) {
        let { list } = this.props.voice;

        let {
          voice_opt: { queue_size },
        } = this.props.preferences;

        playIn++;

        // [중복 음성 사용] 시 현재 음성 출력 완료 까지대기.
        // [중복 음성 사용 안함] 시 바로 음성 출력.
        if (queue_size) this.playInterrupt = true;

        // 오래된 음성 부터 출력.
        let track = this.queue.shift(); // 첫 번째 요소를 제거하고, 제거된 요소를 반환

        console.log(
          '- voice queue timer track',
          track,
          ' length',
          this.queue.length
        );

        // 음성 파일이 존재 하는지 여부.(브라우저 캐시)
        let cash = _.find(list, { name: track.name });

        // 한 문장 플레이.
        new Speech(this, cash, _.cloneDeep(track), () => {
          // 문장 재생 완료.
          this.playInterrupt = false;
          playOk++;
        });

        if (clearTimer) clearTimeout(clearTimer);

        // 음성 재생 오류 시 자동 해제.
        clearTimer = setTimeout(() => {
          if (playIn !== playOk) this.playInterrupt = false;
          playIn = playOk = 0;
        }, 5000);
      }
    }, 500);
  }

  componentWillUnmount = () => {
    if (this.playTimer) clearInterval(this.playTimer);
    if (this.ceicleTimer) clearInterval(this.ceicleTimer);
    if (this.player && this.player.playing) this.player.pause();

    this._isMounted = false;
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (
      nextProps.preferences &&
      nextProps.preferences !== this.props.preferences &&
      nextProps.preferences.voice !== this.props.preferences.voice
    ) {
      let { voice } = nextProps.preferences;

      console.log('- preferences voice', voice);

      if (this.state.on === undefined) {
        this.voiceOnOff(voice, false);
      }
    }

    if (nextProps.room && nextProps.room.list !== this.props.room.list) {
      let { list } = nextProps.room;

      const {
        user: { place_id },
      } = this.props;

      this._isMounted &&
        this.setState({
          sound: [
            './voice/' + place_id + '/off.mp3',
            './voice/' + place_id + '/on.mp3',
          ],
        });

      let voices = [];

      voices.push({ name: 'on', text: '음성 지원 기능을 시작 합니다.' });
      voices.push({ name: 'off', text: '음성 지원 기능을 중지 합니다.' });
      voices.push({ name: 'door_0', text: '문이 닫혔습니다.' });
      voices.push({ name: 'door_1', text: '문이 열렸습니다.' });
      voices.push({ name: 'clean_0', text: '청소 완료' });
      voices.push({ name: 'clean_1', text: '청소 요청' });
      voices.push({ name: 'outing_0', text: '외출 복귀' });
      voices.push({ name: 'outing_1', text: '외출' });
      voices.push({ name: 'key_1', text: '고객키가 삽입 되었습니다.' });
      voices.push({ name: 'key_2', text: '마스터키가 삽입 되었습니다.' });
      voices.push({ name: 'key_3', text: '청소키가 삽입 되었습니다.' });
      voices.push({ name: 'key_4', text: '고객키가 제거 되었습니다.' });
      voices.push({ name: 'key_5', text: '마스터키가 제거 되었습니다.' });
      voices.push({ name: 'key_6', text: '청소키가 제거 되었습니다.' });
      voices.push({ name: 'sale_1', text: '숙박' });
      voices.push({ name: 'sale_2', text: '대실' });
      voices.push({ name: 'sale_3', text: '장기' });
      voices.push({ name: 'check_in', text: '입실' });
      voices.push({ name: 'check_out', text: '퇴실' });
      voices.push({ name: 'auto_check_in', text: '자동 입실' });
      voices.push({ name: 'auto_check_out', text: '자동 퇴실' });
      voices.push({ name: 'inspect_1', text: '점검대기' });
      voices.push({ name: 'inspect_2', text: '점검 중' });
      voices.push({ name: 'inspect_3', text: '점검 완료' });
      voices.push({ name: 'main_relay_0', text: '전원이 차단 되었습니다.' });
      voices.push({ name: 'main_relay_1', text: '전원이 공급 되었습니다.' });
      voices.push({ name: 'car_call_1', text: '배차 요청 입니다.' });
      voices.push({ name: 'car_call_2', text: '배차 완료 되었습니다.' });
      voices.push({ name: 'car_call_3', text: '배차 요청이 취소 되었습니다.' });

      // 모든 객실 타입명 체크(없으면 서버에 생성)
      _.map(list, (item) => {
        let text = item.type_name;
        voices.push({ name: 'room_type_' + item.room_type_id, text });
      });

      // 모든 객실명 체크(없으면 서버에 생성)
      _.map(list, (item) => {
        let text = item.name + (isNaN(item.name) ? '' : ' 호, ');
        voices.push({ name: 'room_' + item.id, text });
      });

      setTimeout(() => {
        this.props.makeVoiceFileList(voices, false, (voices) => {
          if (voices) {
            _.map(voices, (item) => {
              //const { name } = item;
              //let url = "./voice/" + place_id + "/" + name + ".mp3";
              // console.log("- check voice room", url);
              // this.start(url, true, () => {
              //   console.log("- voice cashing", url);
              // });
            });
          }
        });
      }, 5 * 1000);
    }

    /*
        음성 지원.
      1. 단어를 조합하여 문장을 만든다.
      2. 해당 단어를 서버에서 조회 한다.
      3. 서버에 없으면 Google TTS 를 사용 하여 음성 파일을 서버에 생성 한다.
      4. 모든 단어 파일을 Queue 에 넣고 Audio 를 사용하여 Play 한다.
    */
    if (
      this.state.on &&
      nextProps.voice &&
      nextProps.voice.tracks !== this.props.voice.tracks
    ) {
      let { tracks } = nextProps.voice;

      let {
        voice_opt: { queue_size },
      } = this.props.preferences;

      console.log('- voice componentWillReceiveProps tracks', tracks);

      let tracksCopy = _.cloneDeep(tracks);

      console.log('- voice queue ', this.queue, ' tracks', tracks);

      let lastInx = this.queue.length - 1;
      let lastQueue = this.queue.length ? this.queue[lastInx] : null;

      // 첫번째 큐 여부.(다음 음성이 같은 객실일때 한번에 처리 하려고 딜레이를 준다.)
      this.isFirstQueue = lastInx === 0;

      const { name: _name } = tracksCopy[0];

      let isMerged = false;

      //  큐 사이즈가 동시 음성 갯수 넘어가면 머지 안함.
      if (queue_size && this.queue.length < queue_size) {
        // 바로 전 큐에 동일 객실 정보가 있다면 merge.
        if (lastQueue && lastQueue[0].name === _name) {
          let mergeTracks = _.unionBy(this.queue[lastInx], tracksCopy, 'name'); // tracks merge..

          let sortTrack = _.sortBy(mergeTracks, ['inx']); // 순서 정렬.

          this.queue[lastInx] = sortTrack;

          isMerged = true;

          console.log('- voice track merge ', this.queue[lastInx]);
        }
      }

      if (!isMerged) {
        /*
          1. 로그가 들어오면 음성 큐 배열에 담는다.
          2. 큐 사이즈 오버 시
            1. [중복 음성 사용] 시 left shift 하며 왼쪽 음성 출력,
            2. [중복 음성 사용 안함] 시 left shift 한며 삭제.(왼쪽 삭제)
          3. 현재 큐에 같은 객실이 있다면 머지 한다.
          4. 큐는 delay_time 시간 지나면 left sift (먼저 들어온 순서로 음성 발신)
        */

        // 동시 음성 갯수 사용 시
        if (queue_size) {
          console.log(
            '- voice queue size',
            queue_size,
            ' length',
            this.queue.length
          );
          //  큐 사이즈가 동시 음성 갯수 넘어가면 첫번째 음성(다음 재생 음성) 제거.
          if (this.queue.length + 1 > queue_size) {
            this.queue.shift();
            console.log('- voice queue shift ', this.queue.length, this.queue);
          }
        }

        // 큐에 담는다.
        this.queue.push(tracksCopy);
      }

      // 첫 큐 셋팅 시 지연 시간을 줘서 다음 음성이 같은 객실 이라면 merge 할 시간을 준다.
      setTimeout(
        () => {
          this.isFirstQueue = false;
        },
        this.isFirstQueue ? 1000 : 0
      );
    }

    // 파일 생성 하지 않고 바로 음성 재생.
    if (this.state.on && nextProps.voice && nextProps.voice.speech) {
      let { speech } = nextProps.voice;

      if (speech) {
        const {
          contents: { data },
        } = speech;

        if (data) {
          console.log('- voice componentWillReceiveProps tts contents', data);

          var Sound = (function () {
            var df = document.createDocumentFragment();
            return function Sound(src) {
              var snd = new Audio(src);
              df.appendChild(snd); // keep in fragment until finished playing

              var removed = false;
              snd.addEventListener('ended', function () {
                if (removed) return;
                removed = true;
                df.removeChild(snd);
              });

              snd.playbackRate = 1.0;
              snd.play();

              return snd;
            };
          })();

          var base64data = Buffer.from(data, 'binary').toString('base64');
          console.log('- base64data', base64data.length);

          // var originaldata = Buffer.from(base64data, "base64");
          // console.log(originaldata.toString());

          // then do it
          Sound('data:audio/mp3;base64,' + base64data);
        }
      }
    }
  }

  render() {
    return (
      <div className={cx('voice', this.state.on && 'on')}>
        <canvas ref={(ref) => (this.canvas = ref)} />
        <div className="btn-box">
          <button
            onClick={(evt) => this.voiceOnOff(this.state.on ? 0 : 1, true)}
            type="button"
          >
            {!this.state.on ? 'Voice Off' : 'Voice On'}
          </button>
        </div>
      </div>
    );
  }
}

class Speech {
  constructor($this, cash, tracks, callback) {
    this.props = $this.props;
    this.start = $this.start;

    this.cash = cash;
    this.tracks = _.cloneDeep(tracks); // 음성 목록.
    this.callback = callback;

    this.voice = []; // 문장 파일 목록.
    this.inx = 0;

    this.load();
  }

  load = () => {
    console.log('- voice Speech load', this.tracks, this.tracks.length);

    const {
      user: { place_id },
    } = this.props;

    _.map(this.tracks, (track, inx) => {
      // console.log("- voice cash", cash, track.name, inx);

      if (!this.cash) {
        this.props.makeVoiceFile(track.name, track.text, false, (name, url) => {
          if (name) {
            let url = './voice/' + place_id + '/' + name + '.mp3';
            console.log(
              '- voice no cash url',
              url,
              this.tracks.length,
              this.voice.length
            );

            this.voice[inx] = { name, url }; // 원래 순서대로.

            if (this.tracks.length === this.voice.length) {
              this.play();
            }
          }
        });
      } else {
        let url = './voice/' + place_id + '/' + track.name + '.mp3';
        console.log(
          '- voice cash url',
          url,
          this.tracks.length,
          this.voice.length
        );

        this.voice[inx] = { name: track.name, url };

        if (this.tracks.length === this.voice.length) {
          this.play();
        }
      }
    });
  };

  // 한 문장 플레이.
  play = () => {
    let source = this.voice[this.inx]; // 단어
    if (source) {
      console.log('- voice Speech play', source.name, source.url);

      this.start(source.url, false, () => {
        this.inx++;
        if (this.inx < this.voice.length) {
          this.play();
        } else {
          this.tracks = [];
          this.voice = [];
          if (this.callback) this.callback();
        }
      });
    } else {
      if (this.callback) this.callback();
    }
  };
}

const LERP_THRESHOLD = 0.01;

class Circle {
  constructor(ctx, w, h, x, y, duration, radius, endScale, color, callback) {
    this.ctx = ctx;
    this.w = w;
    this.h = h;
    this.x = x;
    this.y = y;
    this.scale = 1;
    this.radius = radius;
    this.color = color;
    this.callback = callback;

    this.path = new Path2D();
    this.path.arc(0, 0, radius, 0, Math.PI * 2);

    this.tween = TweenMax.to(this, duration || 1.25, {
      scale: endScale,
      radius: radius * endScale,
      paused: true,
      ease: Power1.easeInOut,
      onComplete: () => {
        if (this.callback) this.callback();
      },
    });
  }

  wave = () => {
    this.ctx.clearRect(0, 0, this.w, this.h);

    // Reset previous transform
    // this.ctx.setTransform(1, 0, 0, 1, 0, 0);

    // this.ctx.clearRect(0, 0, this.width, this.height);

    // this.ctx.setTransform(this.circle.scale, 0, 0, this.circle.scale, this.circle.x, this.circle.y);
    // this.ctx.strokeStyle = "#ffffff";
    // this.ctx.stroke(this.circle.path);

    // deaw a circle line.
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    this.ctx.strokeStyle = this.color || '#ffffff';
    this.ctx.lineWidth = 0.7;
    this.ctx.stroke();
    this.ctx.closePath();
  };

  start = () => {
    TweenMax.ticker.addEventListener('tick', this.wave);
    this.tween.play();
  };

  stop = () => {
    TweenMax.ticker.removeEventListener('tick', this.wave);
    this.tween.pause();
  };

  lerp(endX, endY, amt) {
    const dx = endX - this.x;
    const dy = endY - this.y;

    if (Math.abs(dx) > LERP_THRESHOLD) {
      this.x += dx * amt;
    } else {
      this.x = endX;
    }

    if (Math.abs(dy) > LERP_THRESHOLD) {
      this.y += dy * amt;
    } else {
      this.y = endY;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    layout,
    auth,
    preferences,
    placeLock,
    room,
    roomView,
    roomSale,
    roomState,
    voice,
  } = state;

  return {
    layout,
    user: auth.user,
    preferences: preferences.item,
    stopLoad: preferences.stopLoad,
    placeLock,
    roomView,
    room,
    roomSale,
    roomState,
    voice,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    makeVoiceFileList: (voices, reset, callback) =>
      dispatch(voiceAction.makeVoiceFileList(voices, reset, callback)),

    makeVoiceFile: (name, text, reset, callback) =>
      dispatch(voiceAction.makeVoiceFile(name, text, reset, callback)),
  };
};

module.exports = connect(mapStateToProps, mapDispatchToProps)(Voice);
