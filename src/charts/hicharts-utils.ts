enum Align {
  Left = 'left',
  Right = 'right',
  Center = 'center',
}

interface ChartOptions {
  chart: object;
  title: {
    text: string;
    align: Align;
  },
  subtitle: {
    text: string;
    align: Align;
  },
}
interface ChartTitle {
  text: string;
  align: Align;
}
interface ChartAxis {
  title: string;
  // labels: s
}
interface CharSeries {
  name: string;
  data: number[];
}

class ChartOptionsBuilder {
  private chartType: string;
  private title: ChartTitle;
  private subtitle: ChartTitle;
  private xAxis: ChartAxis;
  private yAxis: ChartAxis;
  private series: CharSeries[];

  setChartType(type: string) {
    this.chartType = type;
    return this;
  }

  setTitle(text: string, align: Align = Align.Left) {
    this.title = { text, align };
    return this;
  }

  setSubtitle(text: string, align: Align = Align.Left) {
    this.subtitle = { text, align };
    return this;
  }


}

const chartOptions = {
        chart: {
          // type: "column",
        },
        title: {
            text: 'U.S Solar Employment Growth',
            align: 'left',
        },
        subtitle: {
            text: 'By Job Category. Source: <a href="https://irecusa.org/programs/solar-jobs-census/" target="_blank">IREC</a>.',
            align: 'left'
        },
        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2010 to 2020'
            }
        },
        yAxis: {
            title: {
                text: 'Number of Employees'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },
        accessibility: {
          enabled: false,
        },
            
        series: [
        {
            name: 'Installation & Developers',
            data: [43934, 48656, 65165, 81827, 112143, 142383,
                171533, 165174, 155157, 161454, 154610]
        }, {
            name: 'Manufacturing',
            data: [24916, 37941, 29742, 29851, 32490, 30282,
                38121, 36885, 33726, 34243, 31050]
        }, {
            name: 'Sales & Distribution',
            data: [11744, 30000, 16005, 19771, 20185, 24377,
                32147, 30912, 29243, 29213, 25663]
        }, {
            name: 'Operations & Maintenance',
            data: [null, null, null, null, null, null, null,
                null, 11164, 11218, 10077]
        }, {
            name: 'Other',
            data: [21908, 5548, 8105, 11248, 8989, 11816, 18274,
                17300, 13053, 11906, 10073]
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

      };
