import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, room_fees, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = room_fees;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { room_fee }: any) {
            state.item = room_fee[0] || room_fee;

            mergeList(state.list, state.item, "room_id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "a.reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `room/fee/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, room_fees },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            room_fees,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return room_fees;
                },
            );
            return promise;
        },

        async getListByroomTypeId(
            { state, commit, dispatch, loading = true }: any,
            { room_type_id, channel = "pad" },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/fee/type/${room_type_id}/${channel}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_fees } }) => {
                if (success) {
                    commit(LIST, { room_fees });
                } else {
                    commit(LIST, []);
                }
                return { success, error, room_fees };
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            { id },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/fee/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_fee } }) => {
                if (success) {
                    commit(ITEM, { room_fee });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return room_fee;
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            room_fee: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room/fee`,
                {
                    room_fee,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let room_fees = state.list.concat(room_fee);
                    commit(LIST, { count: state.count++, room_fees });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            room_fee: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/fee/${room_fee.id}`,
                {
                    room_fee,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { room_fee });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `room/fee/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let room_fees = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count--, room_fees });
                }
                return success;
            });
            return promise;
        },
    },
};
