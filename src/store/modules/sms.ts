import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, smss, page }: any,
        ) {
            state.count = count;
            state.list = smss;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { sms }: any) {
            state.item = sms[0] || sms;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 0,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `sms/all/${filter}/${order}/${desc}/${limit}`,
                {
                    page,
                },
                sync,
            ).then(({ common: { success, error }, body: { count, smss } }) => {
                if (success) {
                    commit(LIST, {
                        count: count[0].count,
                        smss,
                        page: _.cloneDeep(page),
                    }); // page 는 화면에서 변경 되므로 clone 한다.
                } else {
                    commit(LIST, []);
                }
                return smss;
            });
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            tr_num: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `sms/${tr_num}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { sms } }) => {
                if (success) {
                    commit(ITEM, { sms });
                } else {
                    commit(ITEM, []);
                }
                return sms;
            });
            return promise;
        },
        async sendAuthNo(
            { state, commit, dispatch, loading = true }: any,
            phoneNo: number,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `sms/authno/${phoneNo}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { info, encAuthNo } }) => {
                    if (success) {
                        const sms: { tr_num } = info.insertId;
                        commit(ITEM, { sms });
                    }
                    return { success, error, encAuthNo };
                },
            );
            return promise;
        },
        async checkAuthNo(
            { state, commit, dispatch, loading = true }: any,
            phoneNo: number,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `sms/check/authno/${phoneNo}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    const sms: { tr_num } = info.insertId;
                    commit(ITEM, { sms });
                }
                return { success, error };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            sms: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `sms`,
                {
                    sms,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    sms.id = info.insertId;
                    commit(ITEM, { sms });
                }
                return { success, error };
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            sms: { tr_num: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `sms/${sms.tr_num}`,
                {
                    sms,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { sms });
                }
                return { success, error };
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            tr_num: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `sms/${tr_num}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let smss = state.list.filter(
                        (item: { id: any }) => item.id !== tr_num,
                    );
                    commit(LIST, { count: state.count - 1, smss });
                }
                return { success, error };
            });
            return promise;
        },
    },
};
