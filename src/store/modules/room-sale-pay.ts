import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, addList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import dayjs from "dayjs";
import _ from "lodash";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any },
            { count, room_sale_pays, page }: any,
        ) {
            state.count = count;
            state.list = room_sale_pays;
        },
        [ITEM](state: { item: any; list: any }, { room_sale_pay }: any) {
            state.item = room_sale_pay[0] || room_sale_pay;

            addList(state.list, room_sale_pay, "desc", 300);
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                member_id,
                page = { no: 1, size: 10 },
                filter = "1=1",
                between = "a.check_in_exp",
                begin,
                end,
            },
            sync = true,
        ) {
            const { no = 1, size = Number(getSessionStroge("rowSize")) } = page;
            let limit = (no - 1) * size + "," + size;

            const promise = await list(
                { dispatch, loading },
                `room/sale/pay/member/list/${member_id}/${filter}/${between}/${begin}/${end}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { room_sale_pays } }) => {
                    if (success) {
                        room_sale_pays = _.each(room_sale_pays, (v) => {
                            v.reg_date = dayjs(v.reg_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.mod_date = dayjs(v.mod_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.reg_date_short = dayjs(v.reg_date).format(
                                "YYYY-MM-DDs",
                            );
                            v.mod_date_short = dayjs(v.mod_date).format(
                                "YYYY-MM-DD",
                            );
                            return v;
                        });
                        commit(LIST, {
                            count: room_sale_pays.length,
                            room_sale_pays,
                        });
                    } else {
                        commit(LIST, { room_sale_pays: [] });
                    }
                    return { success, error, room_sale_pays };
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/sale/pay/member/${id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { room_sale_pay } }) => {
                    if (success) {
                        commit(ITEM, { room_sale_pay });
                    } else {
                        commit(ITEM, { room_sale_pay: {} });
                    }
                    return { success, error, room_sale_pay };
                },
            );
            return promise;
        },

        async delAllItem(
            { state, commit, dispatch, loading = true }: any,
            room_sale_pay: { member_id: any; use_yn: string },

            sync = true,
        ) {
            const promise = await put(
                dispatch,
                `room/sale/pay/member/del/${room_sale_pay.member_id}`,
                { room_sale_pay },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(LIST, { list: [] });
                }
                return { success, error, room_sale_pay, info };
            });
            return promise;
        },

        // 수정/삭제 금지!!
    },
};
