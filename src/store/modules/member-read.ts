import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import format from "@/utils/format-util";

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, member_reads, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = member_reads;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { member_read }: any) {
            state.item = member_read[0] || member_read;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true }: any,
            {
                member_id,
                filter = "1=1",
                order = "a.reg_date",
                desc = "desc",
                limit = "100",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/read/list/${member_id}/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_reads },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            member_reads,
                        });
                    } else {
                        commit(LIST, []);
                    }
                    return { success, member_reads };
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/read/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_read } }) => {
                if (success) {
                    commit(ITEM, { member_read });
                } else {
                    commit(ITEM, []);
                }
                return { success, member_read: member_read[0] };
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_read: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member/read`,
                {
                    member_read,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_read.id = info.insertId;
                    commit(ITEM, { member_read });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            member_read: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/read/${member_read.id}`,
                {
                    member_read,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_read = {
                        ...state[ITEM],
                        member_read,
                    };

                    commit(ITEM, { member_read });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/read/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let member_reads = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, member_reads });
                }
                return success;
            });
            return promise;
        },
    },
};
