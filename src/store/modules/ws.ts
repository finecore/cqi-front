import { UUID, WEB_SOCKET, MEMBER } from "@/store/mutation_types";

const JOIN_REQUESTED = "JOIN_REQUESTED";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        uuid: "",
        webSocket: null,
    },
    getters: {
        [UUID](state: { [x: string]: any }) {
            return state.uuid;
        },
        [WEB_SOCKET](state: { [x: string]: any }) {
            return state.webSocket;
        },
    },
    mutations: {
        [UUID](state: { [x: string]: any }, { uuid }: any) {
            state[UUID] = uuid;
        },
        [WEB_SOCKET](state: { [x: string]: any }, { webSocket }: any) {
            state[WEB_SOCKET] = webSocket;
        },
    },
    actions: {
        async setUuid({ state, commit, dispatch }: any, uuid: any) {
            commit(UUID, { uuid });
        },
        async setWebsocket({ state, commit, dispatch }, { webSocket }) {
            if (webSocket) {
                commit(WEB_SOCKET, { webSocket });
            }
        },
    },
};
