import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1, size: 10 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, preferences, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = preferences;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { preferences }: any) {
            state.item = preferences[0] || preferences;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;

                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `preferences/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, preferences },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            preferences,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return preferences;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `preferences/${place_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { preferences } }) => {
                if (success) {
                    commit(ITEM, { preferences });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return preferences;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            preferences: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `preferences`,
                {
                    preferences,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            preferences: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `preferences/${preferences.id}`,
                {
                    preferences,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let preferences = state.list.map((item: { id: any }) =>
                        item.id !== preferences.id ? item : preferences,
                    );

                    commit(LIST, { count: state.count, preferences });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `preferences/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
    },
};
