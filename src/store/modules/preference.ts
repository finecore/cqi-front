import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1, size: 10 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, preference, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = preference;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { preference }: any) {
            state.item = preference[0] || preference;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;

                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `preference/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, preference },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            preference,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return preference;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `preference/${place_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { preference } }) => {
                if (success) {
                    commit(ITEM, { preference });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return preference;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            preference: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `preference`,
                {
                    preference,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            preference: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `preference/${preference.id}`,
                {
                    preference,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let preference = state.list.map((item: { id: any }) =>
                        item.id !== preference.id ? item : preference,
                    );

                    commit(LIST, { count: state.count, preference });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `preference/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
    },
};
