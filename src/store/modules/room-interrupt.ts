import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { count: number; list: any; page: number },
            { count, all_room_interrupts, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = all_room_interrupts;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { room_interrupt }: any) {
            state.item = room_interrupt;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = false },
            place_id: number,
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `room/interrupt/all/${place_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { all_room_interrupts = [] },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: all_room_interrupts?.length || 0,
                            all_room_interrupts,
                            page: 1,
                        });
                        return { success, error, all_room_interrupts };
                    } else {
                        commit(LIST, []);
                        return { success, error, all_room_interrupts: [] };
                    }
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = false }: any,
            room_id: number,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/interrupt/${room_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { room_interrupt } }) => {
                    if (success) {
                        room_interrupt = room_interrupt
                            ? room_interrupt[0]
                            : {};

                        commit(ITEM, { room_interrupt });
                        return { success, error, room_interrupt };
                    } else {
                        commit(ITEM, {});
                        return { success, error, room_interrupt: {} };
                    }
                },
            );
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = false }: any,
            room_interrupt: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room/interrupt`,
                {
                    room_interrupt,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                room_interrupt.id = info.insertId;

                if (success) {
                    commit(ITEM, { room_interrupt });
                }
                return { success, error, room_interrupt };
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = false }: any,
            room_interrupt: any,
            sync = true,
        ) {
            console.log("- setItem", { room_interrupt });

            const promise = await put(
                { dispatch, loading },
                `room/interrupt/${room_interrupt.room_id}`,
                {
                    room_interrupt,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    room_interrupt = { ...state[ITEM], ...room_interrupt };
                    commit(ITEM, { room_interrupt });
                }
                return { success, error, room_interrupt };
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = false }: any,
            room_id: number,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `room/interrupt/${room_id}`,
                null,
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, room_interrupt },
                }) => {
                    if (success) {
                        // room_interrupt = state.list.filter(
                        //   (item: any) => item.room_id !== room_id
                        // );

                        commit(LIST, {
                            count: state.count - 1,
                            room_interrupt,
                            page: 1,
                        });
                        return { success, error, room_interrupt };
                    } else {
                        return { success, error, room_interrupt: {} };
                    }
                },
            );
            return promise;
        },
        async delUserItem(
            { state, commit, dispatch, loading = false }: any,
            { user_id, channel },
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `room/interrupt/user/${user_id}/${channel}`,
                null,
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, room_interrupt },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: state.count - 1,
                            room_interrupt,
                            page: 1,
                        });
                        return { success, error, room_interrupt };
                    } else {
                        return { success, error, room_interrupt: {} };
                    }
                },
            );
            return promise;
        },
    },
};
