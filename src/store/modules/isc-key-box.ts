import { list, item, post, put, del } from "@/api";
import {
    LIST,
    USE_LIST,
    ITEM,
    COUNT,
    PAGE,
    mergeList,
} from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [USE_LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [USE_LIST](state: { [x: string]: any }) {
            return state[USE_LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { [x: string]: any },
            { count, isc_key_boxs, page }: any,
        ) {
            state[COUNT] = count;
            state[LIST] = isc_key_boxs;
            if (page) state[PAGE] = page;
        },
        [USE_LIST](state: { [x: string]: any }, { isc_key_boxs }: any) {
            state[USE_LIST] = isc_key_boxs;
        },
        [ITEM](
            state: { [x: string]: any; list: any[]; item: any },
            { isc_key_box }: any,
        ) {
            state[ITEM] = isc_key_box[0] || isc_key_box;

            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "gid,did",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `isc/key/box/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, isc_key_boxs },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            isc_key_boxs,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return isc_key_boxs;
                },
            );
            return promise;
        },
        async getUseList(
            { state, commit, dispatch, loading = true },
            { sync },
        ) {
            const promise = await list(
                { dispatch, loading },
                `isc/key/box/use/list`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { isc_key_boxs } }) => {
                if (success) {
                    commit(USE_LIST, {
                        isc_key_boxs,
                    });
                } else {
                    commit(USE_LIST, []);
                }
                return isc_key_boxs;
            });
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `isc/key/box/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { isc_key_box } }) => {
                if (success) {
                    commit(ITEM, { isc_key_box });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return isc_key_box;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            isc_key_box: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `isc/key/box`,
                {
                    isc_key_box,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let isc_key_boxs = state.list.concat(isc_key_box);
                    commit(LIST, { count: state.count++, isc_key_boxs });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            isc_key_box: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `isc/key/box/${isc_key_box.id}`,
                {
                    isc_key_box,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { isc_key_box });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `isc/key/box/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let isc_key_boxs = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count--, isc_key_boxs });
                }
                return success;
            });
            return promise;
        },
    },
};
