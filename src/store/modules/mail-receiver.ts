import { LIST, ITEM, AUTH, COUNT } from "@/store/mutation_types";
import { list, item, post, put, del } from "@/api";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        count: 0,
        list: [],
        item: {},
        page: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state[COUNT];
        },
        [LIST](state) {
            return state[LIST];
        },
        [ITEM](state) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](state, { count, mail_receivers, page }) {
            state.count = count;
            state.list = mail_receivers;
            if (page) state.page = page;
        },
        [ITEM](state, { mail_receiver }) {
            state.item = mail_receiver;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "id",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `mail/receiver/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, mail_receivers },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            mail_receivers,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return mail_receivers;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `mail/receiver/${id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { mail_receiver } }) => {
                    if (success) {
                        commit(ITEM, { mail_receiver });
                    } else {
                        commit(ITEM, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return mail_receiver;
                },
            );
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            mail_receiver,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `mail_receiver`,
                {
                    mail_receiver,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            mail_receiver,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `mail/receiver/${mail_receiver.id}`,
                {
                    mail_receiver,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let list = state.list.map((item) =>
                        item.id !== mail_receiver.id ? item : mail_receiver,
                    );

                    commit(LIST, { count: state.count, list });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `mail/receiver/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
    },
};
