import {
  USER,
  IS_LOGINED,
  TOKEN,
  LOGIN_ID,
  DUP_ACCESS_LOGOUT,
  DUP_ACCESS_FAILUE,
  IS_SUPERVISOR,
  IS_ADMIN,
  IS_VIEWER,
  IS_ALIVE,
} from '@/store/mutation_types';

import { post, item } from '@/api';
import router from '@/router/index';
import format from '@/utils/format-util';

function ParseJsonString(str: string) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return null;
  }
}

export default {
  state: {
    [USER]: ParseJsonString(sessionStorage.getItem(USER)) || {},
    [TOKEN]: sessionStorage.getItem(TOKEN) || '',
    [IS_LOGINED]: sessionStorage.getItem(IS_LOGINED) === 'true',
    [IS_ALIVE]: false,
    [DUP_ACCESS_LOGOUT]: false,
    [DUP_ACCESS_FAILUE]: false,
  },
  getters: {
    [USER]: (state: { [x: string]: any }) => state[USER] || { id: '', pwd: '' },
    [TOKEN]: (state: { [x: string]: any }) => state[TOKEN],
    [IS_LOGINED]: (state: { [x: string]: any }) => state[IS_LOGINED],
    [IS_ALIVE]: (state: { [x: string]: any }) => state[IS_ALIVE],
    [IS_SUPERVISOR]: (state: {
      [x: string]: {
        type: number;
        level: number;
      };
    }) =>
      state[IS_LOGINED] && state[USER]?.type === 1 && state[USER]?.level === 0,
    [IS_ADMIN]: (state: {
      [x: string]: {
        type: number;
        level: number;
      };
    }) =>
      state[IS_LOGINED] && state[USER]?.type === 1 && state[USER]?.level < 3,
    [IS_VIEWER]: (state: {
      [x: string]: {
        type: number;
        level: number;
      };
    }) =>
      state[IS_LOGINED] && state[USER]?.type === 1 && state[USER]?.level === 9,
    [DUP_ACCESS_LOGOUT]: (state: { [x: string]: any }) =>
      state[DUP_ACCESS_LOGOUT],
    [DUP_ACCESS_FAILUE]: (state: { [x: string]: any }) =>
      state[DUP_ACCESS_FAILUE],
  },
  mutations: {
    [USER](state: any, { user, token }: any) {
      console.log('- mutations commit MEMGER', {
        user,
        token,
      });

      state[USER] = user;
      sessionStorage.setItem(USER, JSON.stringify(user)); // user 정보 저장.
      state[IS_LOGINED] = true;
      sessionStorage.setItem(IS_LOGINED, JSON.stringify(true));
      if (token) {
        state.token = token;
        sessionStorage.setItem(TOKEN, token); // user 정보 저장.
      }
    },
    [TOKEN](state: { [x: string]: any }, token: any) {
      state[TOKEN] = token;
    },
    [IS_LOGINED](state: { [x: string]: any }, isLogin: any) {
      state[IS_LOGINED] = isLogin;
    },
    [IS_ALIVE](state: { [x: string]: any }, alive: any) {
      state[IS_ALIVE] = alive;
    },
    [DUP_ACCESS_LOGOUT](state: { [x: string]: any }, status: any) {
      state[DUP_ACCESS_LOGOUT] = status;
    },
    [DUP_ACCESS_FAILUE](state: { [x: string]: any }, status: any) {
      state[DUP_ACCESS_FAILUE] = status;
    },
  },
  actions: {
    async login({ commit }, { id, pwd, isEnc = true }) {
      const response = await post({}, 'login', {
        id,
        pwd,
        isEnc,
      }).then(({ common: { success, error }, body: { user, token } }) => {
        if (success) {
          user = user[0] || user;

          user.reserv_date = format.toISO(user.reserv_date);
          user.check_in_exp = format.toISO(user.check_in_exp);
          user.check_out_exp = format.toISO(user.check_out_exp);
          user.auth_date = format.toISO(user.auth_date);
          user.last_login_date = format.toISO(user.last_login_date);
          user.reg_date = format.toISO(user.reg_date);
          user.mod_date = format.toISO(user.mod_date);

          console.log('----> login success ', { ...user });

          commit(USER, { user, token });
          commit(IS_LOGINED, true);
          commit(TOKEN, token);

          // 회원정보 영동 업데이트
          // commit("item", { user });

          localStorage.setItem(LOGIN_ID, id);
        } else {
          console.log('----> login fails ', { ...error });

          commit(USER, { user: null, token: null });
          commit(IS_LOGINED, false);
        }
        return { success, error, user };
      });
      return response;
    },

    async shareLogin({ commit }, { id, pwd, isEnc = true }) {
      const response = await post({}, 'share/login', {
        id,
        pwd,
        isEnc,
      }).then(({ common: { success, error }, body: { user, token } }) => {
        if (success) {
          user = user[0] || user;

          user.reserv_date = format.toISO(user.reserv_date);
          user.check_in_exp = format.toISO(user.check_in_exp);
          user.check_out_exp = format.toISO(user.check_out_exp);
          user.auth_date = format.toISO(user.auth_date);
          user.last_login_date = format.toISO(user.last_login_date);
          user.reg_date = format.toISO(user.reg_date);
          user.mod_date = format.toISO(user.mod_date);

          commit(USER, { user, token });
          commit(IS_LOGINED, true);
          commit(TOKEN, token);

          // 회원정보 영동 업데이트
          // commit("item", { user });

          localStorage.setItem(LOGIN_ID, id);
        } else {
          commit(USER, { user: null, token: null });
          commit(IS_LOGINED, false);
        }
        return { success, error, user };
      });
      return response;
    },

    async addUuid({ state, commit }, { uuid }) {
      commit(USER, { user: { ...state[USER], uuid } });
    },

    async logout({ commit }) {
      commit(USER, { user: null, token: null });
      commit(IS_LOGINED, false);
      sessionStorage.removeItem(IS_LOGINED);
      sessionStorage.removeItem(USER);
      sessionStorage.removeItem(TOKEN);

      commit('item', {});

      router.push('/login');
    },

    async isAlive({ commit }) {
      const response = await item({}, 'alive', {}, false);
      const { success, error, body } = response.common;
      if (success) {
        commit(IS_ALIVE, body.alive);
      }
      return { success, error, alive: body?.alive };
    },
  },
};
