import { list, item, post, put, del } from "@/api";
import {
    LIST,
    PLACE_LIST,
    ITEM,
    COUNT,
    PAGE,
    SEL_LIST,
    mergeList,
} from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
        [SEL_LIST]: [],
        [PLACE_LIST]: [],
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state[LIST];
        },
        [ITEM](state) {
            return state.item;
        },
        [SEL_LIST](state) {
            return state[SEL_LIST];
        },
        [PLACE_LIST](state) {
            return state[PLACE_LIST];
        },
    },
    mutations: {
        [LIST](state, { count, devices, page }) {
            state[LIST] = devices;
            if (count) state.count = count;
            if (page) state.page = page;
        },
        [ITEM](state, { device }) {
            state.item = device[0] || device;
            mergeList(state[LIST], state.item, "id");
        },
        [SEL_LIST](state, { id, isAdd = false }) {
            let selected = _.find(state[LIST], { id });
            let isSelected = _.find(state[SEL_LIST], { id });
            if (selected) {
                if (isAdd) {
                    if (!isSelected) state[SEL_LIST].push(selected);
                } else {
                    state[SEL_LIST] = _.filter(
                        state[SEL_LIST],
                        (v) => v.id !== id,
                    );
                }
            }
        },
        [PLACE_LIST](state, { devices }) {
            state[PLACE_LIST] = devices;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "id",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `device/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, devices } }) => {
                    if (success) {
                        commit(LIST, {
                            devices,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return devices;
                },
            );
            return promise;
        },
        async getPlaceList(
            { state, commit, dispatch, loading = true },
            { place_id },
            sync = true,
        ) {
            const promise = await list(
                dispatch,
                `device/place/${place_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, devices } }) => {
                    if (success) {
                        commit(PLACE_LIST, { devices });
                    } else {
                        commit(PLACE_LIST, []);
                    }
                    return devices;
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `device/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { device } }) => {
                if (success) {
                    commit(ITEM, { device });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return device;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            device,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `device`,
                {
                    device,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    device.id = info.insertId;
                    let devices = state[LIST].concat(device);
                    commit(LIST, { count: state.count++, devices });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            device,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `device/${device.id}`,
                {
                    device,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let devices = state[LIST].map((item) =>
                        item.id !== device.id ? item : device,
                    );
                    commit(LIST, { count: state.count, devices });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `device/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let devices = state[LIST].filter((item) => item.id !== id);
                    commit(LIST, { count: state.count--, devices });
                }
                return success;
            });
            return promise;
        },
    },
};
