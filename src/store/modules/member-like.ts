import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, addList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import dayjs from "dayjs";
import _ from "lodash";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state: { count: any; list: any }, { count, member_likes }: any) {
            state[COUNT] = count;
            state[LIST] = member_likes;
        },
        [ITEM](state: { item: any; list: any }, { member_like }: any) {
            state[ITEM] = member_like[0] || member_like;

            addList(state[LIST], member_like, "desc", 300);
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                member_id,
                page = { no: 1, size: 10 },
                filter = "1=1",
                between = "a.check_in_exp",
                begin,
                end,
            },
            sync = true,
        ) {
            const { no = 1, size = Number(getSessionStroge("rowSize")) } = page;
            let limit = (no - 1) * size + "," + size;

            const promise = await list(
                { dispatch, loading },
                `member/like/list/${member_id}/${filter}/${between}/${begin}/${end}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_likes } }) => {
                if (success) {
                    member_likes = _.each(member_likes, (v) => {
                        v.reg_date = dayjs(v.reg_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.mod_date = dayjs(v.mod_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.reg_date_short = dayjs(v.reg_date).format(
                            "YYYY-MM-DDs",
                        );
                        v.mod_date_short = dayjs(v.mod_date).format(
                            "YYYY-MM-DD",
                        );
                        return v;
                    });
                    commit(LIST, {
                        count: member_likes.length,
                        member_likes,
                    });
                } else {
                    commit(LIST, { member_likes: [] });
                }
                return { success, error, member_likes };
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/like/member/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_like } }) => {
                if (success) {
                    commit(ITEM, { member_like });
                } else {
                    commit(ITEM, { member_like: {} });
                }
                return { success, error, member_like };
            });
            return promise;
        },

        async getItemByplaceId(
            { state, commit, dispatch, loading = true }: any,
            { place_id, member_id },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/like/place/${place_id}/${member_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_like } }) => {
                if (success) {
                    commit(ITEM, { member_like });
                } else {
                    commit(ITEM, { member_like: {} });
                }
                return { success, error, member_like };
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_like: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member/like`,
                {
                    member_like,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_like.id = info.insertId;
                    commit(ITEM, { member_like });
                }
                return { success, member_like };
            });
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            member_like: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/like/${member_like.id}`,
                {
                    member_like,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let member_likes = state[LIST].map((item: { id: any }) =>
                        item.id !== member_like.id ? item : member_like,
                    );
                    commit(LIST, { count: state.count, member_likes });
                }
                return success;
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/like/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let member_likes = state[LIST].filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    // commit(LIST, { count: state.count--, member_likes });
                }
                return { success, member_likes: state[LIST] };
            });
            return promise;
        },

        async delAllItem(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/like/all/${member_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(LIST, { list: [] });
                }
                return { success, error };
            });
            return promise;
        },
    },
};
