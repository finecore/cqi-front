import {
    MEMBER,
    IS_LOGINED,
    TOKEN,
    LOGIN_ID,
    DUP_ACCESS_LOGOUT,
    DUP_ACCESS_FAILUE,
    IS_SUPERVISOR,
    IS_ADMIN,
    IS_VIEWER,
    IS_ALIVE,
} from "@/store/mutation_types";

import { post, item } from "@/api";
import router from "@/router/index";

import wsAction from "./ws";

function ParseJsonString(str: string, sync = "") {
    try {
        return JSON.parse(str);
    } catch (e) {}
}

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        // 화면 reload 시 로그인 정보 재설정 한다.(로그인 정보만 재설정)
        [MEMBER]: ParseJsonString(sessionStorage.getItem(MEMBER)),
        [TOKEN]: sessionStorage.getItem(TOKEN),
        [IS_LOGINED]: sessionStorage.getItem(IS_LOGINED) === "true",
        [IS_ALIVE]: false,
        [DUP_ACCESS_LOGOUT]: false,
        [DUP_ACCESS_FAILUE]: false,
    },
    getters: {
        [MEMBER](state: any) {
            return state[MEMBER] || { id: "", pwd: "" };
        },
        [TOKEN](state: any) {
            return state[TOKEN];
        },
        [IS_LOGINED](state: any) {
            return state[IS_LOGINED];
        },
        [IS_ALIVE](state: any) {
            return state[IS_ALIVE];
        },
        [IS_SUPERVISOR](state: { [x: string]: { type: any; level: any } }) {
            if (state[IS_LOGINED]) {
                const { type, level } = state[MEMBER];
                return type === 1 && level === 0;
            }
            return false;
        },
        [IS_ADMIN](state: { [x: string]: { type: any; level: any } }) {
            if (state[IS_LOGINED]) {
                const { type, level } = state[MEMBER];
                return type === 1 && level < 3;
            }
            return false;
        },
        [IS_VIEWER](state: { [x: string]: { type: any; level: any } }) {
            if (state[IS_LOGINED]) {
                const { type, level } = state[MEMBER];
                return type === 1 && level === 9;
            }
            return false;
        },
        [DUP_ACCESS_LOGOUT](state: any) {
            return state[DUP_ACCESS_LOGOUT];
        },
        [DUP_ACCESS_FAILUE](state: any) {
            return state[DUP_ACCESS_FAILUE];
        },
    },
    mutations: {
        [MEMBER](state: any, { member, token }: any) {
            state[MEMBER] = member;
            sessionStorage.setItem(MEMBER, JSON.stringify(member)); // member 정보 저장.
            state[IS_LOGINED] = true;
            sessionStorage.setItem(IS_LOGINED, JSON.stringify(true));
            if (token) {
                state.token = token;
                sessionStorage.setItem(TOKEN, token); // member 정보 저장.
            }
        },
        [TOKEN](state: any, token: any) {
            state[TOKEN] = token;
        },
        [IS_LOGINED](state: any, isLogin: any) {
            state[IS_LOGINED] = isLogin;
        },
        [IS_ALIVE](state: any, alive: any) {
            state[IS_ALIVE] = alive;
        },
        // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
        [DUP_ACCESS_LOGOUT](state: any, isMEMBER: any) {
            state[DUP_ACCESS_LOGOUT] = isMEMBER;
        },
        // 현재 사용 안함 추후 동시 접속 라이선스 기능 사용 시 적용.
        [DUP_ACCESS_FAILUE](state: any, isMEMBER: any) {
            state[DUP_ACCESS_FAILUE] = isMEMBER;
        },
    },
    actions: {
        async google(
            { state, commit, dispatch, loading = true },
            { id, pwd },
            sync = true,
        ) {
            // console.log('- actions login', { state, commit, dispatch, loading });

            // call api.
            const promise = await post(
                { dispatch, loading },
                "auth/google",
                {},
                sync,
            ).then(
                ({
                    common: { success = false, error = {} },
                    body: { member = {}, token = "" },
                }) => {
                    if (success) {
                        commit(MEMBER, { member, token });
                    } else {
                        commit(MEMBER, { member: null, token: null });
                    }

                    commit(IS_LOGINED, success);
                    commit(TOKEN, token);

                    localStorage.setItem(LOGIN_ID, id); // 최종 로그인 id 정보 로컬 저장.

                    return { success, error, member };
                },
            );
            return promise;
        },

        async addUuid(
            { state, commit, dispatch, loading = true }: any,
            { uuid },
            sync = true,
        ) {
            commit(MEMBER, { member: { ...state.member, uuid } });
        },

        async logout({ state, commit, dispatch, loading = true }) {
            commit(MEMBER, { member: null, token: null });
            commit(IS_LOGINED, null);

            sessionStorage.removeItem(IS_LOGINED);
            sessionStorage.removeItem(MEMBER); // member 정보 삭제.
            sessionStorage.removeItem(TOKEN); // token 정보 저장.

            router.push("/idle");

            return true;
        },

        async isAlive(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading: false },
                `alive`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { alive } }) => {
                if (success) {
                    commit(IS_ALIVE, { alive });
                }
                return { success, error, alive };
            });
            return promise;
        },
    },
};
