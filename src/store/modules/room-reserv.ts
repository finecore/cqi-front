import { list, item, post, put, del } from "@/api";
import {
    LIST,
    PLACE_LIST,
    MEMBER_LIST,
    MEMGER_CONN_LIST,
    MEMBER_USE_LIST,
    ROOM_LIST,
    ITEM,
    PREV_NEXT_ITEM,
    COUNT,
    PAGE,
    mergeList,
} from "@/store/mutation_types";
import format from "@/utils/format-util";

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [PLACE_LIST]: [],
        [MEMBER_LIST]: [],
        [MEMBER_USE_LIST]: [],
        [MEMGER_CONN_LIST]: [],
        [ROOM_LIST]: [],
        [ITEM]: {},
        [PREV_NEXT_ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: any) {
            return state.count;
        },
        [LIST](state: any) {
            return state[LIST];
        },
        [PLACE_LIST](state: any) {
            return state[PLACE_LIST];
        },
        [MEMBER_LIST](state: any) {
            return state[MEMBER_LIST];
        },
        [MEMBER_USE_LIST](state: any) {
            return state[MEMBER_USE_LIST];
        },
        [MEMGER_CONN_LIST](state: any) {
            return state[MEMGER_CONN_LIST];
        },
        [ROOM_LIST](state: any) {
            return state[ROOM_LIST];
        },
        [ITEM](state: any) {
            return state[ITEM];
        },
        [PREV_NEXT_ITEM](state: any) {
            return state[PREV_NEXT_ITEM];
        },
    },
    mutations: {
        [LIST](state: any, { list }: any) {
            state[LIST] = list;
        },
        [PLACE_LIST](state: any, { list }: any) {
            state[PLACE_LIST] = list;
        },
        [MEMBER_LIST](state: any, { list }: any) {
            state[MEMBER_LIST] = list;
        },
        [MEMBER_USE_LIST](state: any, { list }: any) {
            state[MEMBER_USE_LIST] = list;
        },
        [MEMGER_CONN_LIST](state: any, { list }: any) {
            state[MEMGER_CONN_LIST] = list;
        },
        [ROOM_LIST](state: any, { list }: any) {
            state[ROOM_LIST] = list;
        },
        [ITEM](state: any, { room_reserv }: any) {
            state[ITEM] = room_reserv[0] || room_reserv;
        },
        [PREV_NEXT_ITEM](state: any, { room_reserv }: any) {
            state[PREV_NEXT_ITEM] = room_reserv[0] || room_reserv;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            { place_id },
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                `room/reserv/all/${place_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { all_room_reservs },
                }) => {
                    if (success) {
                        commit(LIST, {
                            list: all_room_reservs,
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    }
                    return { success, all_room_reservs };
                },
            );
            return promise;
        },
        async getListByMember(
            { state, commit, dispatch, loading = true },
            {
                member_id,
                filter = "1=1",
                between = "check_in_exp",
                begin,
                end,
                type = MEMBER_LIST,
            },
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                begin
                    ? `room/reserv/member/${member_id}/${filter}/${between}/${begin}/${end}`
                    : `room/reserv/member/${member_id}/${between}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reservs } }) => {
                if (success) {
                    // useHistoryList 필터링 로직
                    room_reservs = _.each(room_reservs, (v: any) => {
                        // state.memberUseList 타입을 지정해 주세요
                        v.chip = _.chain(state.memberUseList)
                            .groupBy((v) => v.place_name as string) // 명시적 타입 캐스팅
                            .some((group) => group.length >= 3)
                            .value();

                        // `check_in_exp` 날짜 포맷 수정
                        v.reserv_date = format.toISO(v.reserv_date);
                        v.check_in_exp = format.toISO(v.check_in_exp);
                        v.check_out_exp = format.toISO(v.check_out_exp);
                        v.auth_date = format.toISO(v.auth_date);
                        v.last_login_date = format.toISO(v.last_login_date);
                        v.reg_date = format.toISO(v.reg_date);
                        v.mod_date = format.toISO(v.mod_date);

                        v.state_name =
                            v.state === "A"
                                ? "예약 상태"
                                : v.state === "B"
                                  ? "예약 취소"
                                  : "숙박 완료";

                        // console.log("watch filter", { ...v });

                        return v;
                    });
                    commit(type, {
                        list: room_reservs,
                    }); // page 는 화면에서 변경 되므로 clone 한다.
                }
                return { success, room_reservs };
            });
            return promise;
        },
        async getListByPlace(
            { state, commit, dispatch, loading = true },
            { place_id, between, begin, end },
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                begin
                    ? `room/reserv/place/${place_id}/${between}/${begin}/${end}`
                    : `room/reserv/place/${place_id}/${between}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reservs } }) => {
                if (success) {
                    commit(PLACE_LIST, {
                        list: room_reservs || [],
                    }); // page 는 화면에서 변경 되므로 clone 한다.
                }
                return { success, room_reservs };
            });
            return promise;
        },
        async getItemByRoom(
            { state, commit, dispatch, loading = true },
            { room_id, between, begin, end, isPrevNext = false },
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                begin
                    ? `room/reserv/room/${room_id}/${between}/${begin}/${end}`
                    : `room/reserv/room/${room_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    const item = isPrevNext ? PREV_NEXT_ITEM : ITEM;

                    commit(item, {
                        room_reserv,
                    });
                }
                return {
                    success,
                    error,
                    room_reserv: room_reserv[0] || {},
                };
            });
            return promise;
        },

        async getItemByMember(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                `room/reserv/member/${member_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    commit(ITEM, {
                        room_reserv,
                    });
                }
                return {
                    success,
                    error,
                    room_reserv: room_reserv[0] || {},
                };
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/reserv/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    commit(ITEM, { room_reserv });
                }
                return { success, error, room_reserv: room_reserv[0] || {} };
            });
            return promise;
        },

        async getItemById(
            { state, commit, dispatch, loading = true },
            { id, between, begin, end, e },
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                begin
                    ? `room/reserv/${id}/${between}/${begin}/${end}`
                    : `room/reserv/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    const item = ITEM;

                    commit(item, {
                        room_reserv,
                    });
                }
                return {
                    success,
                    error,
                    room_reserv: room_reserv[0] || {},
                };
            });
            return promise;
        },

        async getItemByReservNum(
            { state, commit, dispatch, loading = true }: any,
            num: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/reserv/num/${num}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    commit(ITEM, { room_reserv });
                }
                return { success, error, room_reserv: room_reserv[0] || {} };
            });
            return promise;
        },
        async getItemByMmsNoNum(
            { state, commit, dispatch, loading = true }: any,
            num: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/reserv/mms/mo/${num}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_reserv } }) => {
                if (success) {
                    commit(ITEM, { room_reserv });
                }
                return { success, error, room_reserv: room_reserv[0] || {} };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            room_reserv: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room/reserv`,
                {
                    room_reserv,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    room_reserv.id = info.insertId;
                    commit(ITEM, { room_reserv });
                }
                return { success, error, room_reserv };
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            room_reserv: any,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/reserv/${room_reserv.id}`,
                {
                    room_reserv,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    room_reserv = { ...state[ITEM], room_reserv };
                    commit(ITEM, { room_reserv });
                }
                return { success, error, room_reserv };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            room_reserv: { id: any; type: string },
            sync = true,
        ) {
            let { id, type } = room_reserv;

            console.log("delItem", { id, type });

            const promise = await del(
                dispatch,
                `room/reserv/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let list = state[type].filter(
                        (item: { id: any }) => item.id !== id,
                    );

                    console.log("delItem", { id, type });

                    commit(type, { list });
                }
                return success;
            });
            return promise;
        },

        async delAllItem(
            { state, commit, dispatch, loading = true }: any,
            room_reserv: { member_id: any; type: string },

            sync = true,
        ) {
            let { member_id, type } = room_reserv;

            const promise = await del(
                dispatch,
                `room/reserv/all/member/${member_id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(type, { list: [] });
                }
                return success;
            });
            return promise;
        },
    },
};
