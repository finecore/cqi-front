import {
    LOADER,
    SHOW_LOADER,
    HIDE_LOADER,
    DATA,
    SAVE,
} from "@/store/mutation_types";
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    state: {
        [LOADER]: false,
        [DATA]: [{}],
        [SAVE]: sessionStorage.getItem(SAVE) || [{}],
    },
    getters: {
        [LOADER](state: { [x: string]: any }) {
            return state[LOADER];
        },
        [DATA](state: { [x: string]: any }) {
            return state[DATA];
        },
        [SAVE](state: { [x: string]: any }) {
            return state[SAVE];
        },
    },
    mutations: {
        [SHOW_LOADER](state: { [x: string]: boolean }) {
            state[LOADER] = true;
        },
        [HIDE_LOADER](state: { [x: string]: boolean }) {
            state[LOADER] = false;
        },
        [DATA](state: { [x: string]: boolean }, data: any) {
            let { key, value } = data;
            console.log("---> com action addData", { key, value });
            state[DATA][key] = value;
        },
        [SAVE](state: { [x: string]: boolean }, data: any) {
            let { key, value } = data;
            console.log("---> com action saveData", { key, value });
            state[SAVE][key] = value;
        },
    },
    actions: {
        showLoader({ commit }: any, state: any) {
            commit(SHOW_LOADER);
        },
        hideLoader({ commit, state }) {
            commit(HIDE_LOADER);
        },
        setData({ commit, state }, data: any) {
            commit(DATA, data);
        },
        saveData({ commit, state }, data: any) {
            commit(SAVE, data);
        },
    },
};
