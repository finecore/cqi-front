import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state, { count, mms_mos }) {
            state.count = count;
            state.list = mms_mos;
        },
        [ITEM](state, { mms_mo }) {
            state.item = mms_mo[0] || mms_mo;
            mergeList(state.list, state.item, "MO_KEY");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                no = 1,
                size = getSessionStroge("rowSize"),
                filter = "1=1",
                order = "a.MO_KEY",
                desc = "desc",
            },
            sync = true,
        ) {
            let limit = (no - 1) * Number(size) + "," + size;

            const promise = await list(
                { dispatch, loading },
                `mms/mo/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, mms_mos } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            mms_mos,
                        });
                    }
                    return mms_mos;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `mms/mo/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { mms_mo } }) => {
                if (success) {
                    commit(ITEM, { mms_mo });
                }
                return mms_mo;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            mms_mo,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `mms/mo`,
                {
                    mms_mo,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    mms_mo.id = info.insertId;
                    commit(ITEM, { mms_mo });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            mms_mo,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `mms/mo/${mms_mo.MO_KEY}`,
                {
                    mms_mo,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { mms_mo });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            MO_KEY,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `mms/mo/${MO_KEY}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let mms_mos = state.list.filter(
                        (item) => item.MO_KEY !== MO_KEY,
                    );
                    commit(LIST, { count: state.count - 1, mms_mos });
                }
                return success;
            });
            return promise;
        },
    },
};
