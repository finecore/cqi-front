import { list, item, post, put, del } from '@/api';
import { LIST, ITEM, COUNT, PAGE, mergeList } from '@/store/mutation_types';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from '@/api/config';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
  },
  getters: {
    [COUNT](state: { [x: string]: any }) {
      return state[COUNT];
    },
    [LIST](state: { [x: string]: any }) {
      return state[LIST];
    },
    [ITEM](state: { [x: string]: any }) {
      return state[ITEM];
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, isc_states, page }: any
    ) {
      state.count = count;
      state.list = isc_states;
      if (page) state.page = page;
    },
    [ITEM](state: { item: any; list: any[] }, { isc_state }: any) {
      state.item = isc_state[0] || isc_state;

      mergeList(state.list, state.item, 'id');
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      {
        page,
        filter = '1=1',
        order = 'a.reg_date',
        desc = 'desc',
        limit = '10000',
      },
      sync = true
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `isc/state/list/${filter}/${order}/${desc}/${limit}`,
        {},
        sync
      ).then(({ common: { success, error }, body: { count, isc_states } }) => {
        if (success) {
          commit(LIST, {
            count: count[0].count,
            isc_states,
            page: _.cloneDeep(page),
          }); // page 는 화면에서 변경 되므로 clone 한다.
        } else {
          commit(LIST, []);
          return dispatch('setApiErr', error, { cqi: true });
        }
        return isc_states;
      });
      return promise;
    },
    async getItem(
      { state, commit, dispatch, loading = true }: any,
      id: any,
      sync = true
    ) {
      const promise = await item(
        { dispatch, loading },
        `isc/state/${id}`,
        {},
        sync
      ).then(({ common: { success, error }, body: { isc_state } }) => {
        if (success) {
          commit(ITEM, { isc_state });
        } else {
          commit(ITEM, []);
          return dispatch('setApiErr', error, { cqi: true });
        }
        return isc_state;
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      isc_state: any,
      sync: boolean
    ) {
      const promise = await post(
        { dispatch, loading },
        `isc/state`,
        {
          isc_state,
        },
        sync
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let isc_states = state.list.concat(isc_state);
          commit(LIST, { count: state.count++, isc_states });
        }
        return success;
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      isc_state: { id: any },
      sync: boolean
    ) {
      const promise = await put(
        { dispatch, loading },
        `isc/state/${isc_state.id}`,
        {
          isc_state,
        },
        sync
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          commit(ITEM, { isc_state });
        }
        return success;
      });
      return promise;
    },
    async delItem(
      { state, commit, dispatch, loading = true }: any,
      id: any,
      sync = true
    ) {
      const promise = await del(dispatch, `isc/state/${id}`, null, sync).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            let isc_states = state.list.filter(
              (item: { id: any }) => item.id !== id
            );
            commit(LIST, { count: state.count--, isc_states });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
