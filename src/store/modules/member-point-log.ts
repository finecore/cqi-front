import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import format from "@/utils/format-util";

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state[COUNT];
        },
        [LIST](state: { list: any }) {
            return state[LIST];
        },
        [ITEM](state: { item: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](state: any, { count, member_point_logs }: any) {
            state[COUNT] = count;
            state[LIST] = member_point_logs;
        },
        [ITEM](state: { item: any; list: any[] }, { member_point_log }: any) {
            state[ITEM] = member_point_log[0] || member_point_log;
            mergeList(state[LIST], state[ITEM], "id");
        },
    },
    actions: {
        async increaseMileage(
            { state, commit, dispatch, loading = true }: any,
            member_point_log: any,
            sync = true,
        ) {
            // rollback 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
            let { place_id, phone, user_id, point, rollback } =
                member_point_log;

            const promise = await put(
                { dispatch, loading },
                `member/point/log/increase/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
                { member_point_log },
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, member_point_log },
                }) => {
                    if (success) {
                        commit(ITEM, { member_point_log });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point_log: member_point_log[0] };
                },
            );
            return promise;
        },

        async decreaseMileage(
            { state, commit, dispatch, loading = true }: any,
            member_point_log: any,
            sync = true,
        ) {
            let { place_id, phone, user_id, point, rollback } =
                member_point_log;

            const promise = await put(
                { dispatch, loading },
                `member/point/log/decrease/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
                { member_point_log },
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, member_point_log },
                }) => {
                    if (success) {
                        commit(ITEM, { member_point_log });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point_log: member_point_log[0] };
                },
            );
            return promise;
        },

        async getList(
            { state, commit, dispatch, loading = true },
            {
                member_id,
                filter = "1=1",
                between = "a.check_in_exp",
                begin,
                end,
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/point/log/list/${member_id}/${filter}/${between}/${begin}/${end}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { member_point_logs },
                }) => {
                    if (success) {
                        member_point_logs = _.each(member_point_logs, (v) => {
                            v.reg_date = dayjs(v.reg_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.mod_date = dayjs(v.mod_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.reg_date_short = dayjs(v.reg_date).format(
                                "YYYY-MM-DDs",
                            );
                            v.mod_date_short = dayjs(v.mod_date).format(
                                "YYYY-MM-DD",
                            );
                            return v;
                        });
                        commit(LIST, {
                            count: member_point_logs?.length,
                            member_point_logs,
                        });
                    } else {
                        commit(LIST, { member_point_logs: [] });
                    }
                    return { success, error, member_point_logs };
                },
            );
            return promise;
        },

        async getListByPlace(
            { state, commit, dispatch, loading = true },
            { place_id },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/point/log/place/${place_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_point_logs },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            member_point_logs,
                        });
                    } else {
                        commit(LIST, []);
                    }
                    return { success, member_point_logs };
                },
            );
            return promise;
        },

        async getItemByMember(
            { state, commit, dispatch, loading = true },
            { member_id },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/point/log/member/${member_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { member_point_log },
                }) => {
                    if (success) {
                        if (member_point_log[0]?.point)
                            member_point_log[0].point = format.toCurruncy(
                                member_point_log[0]?.point,
                            );

                        console.log(
                            "=====> member_point_log",
                            member_point_log,
                        );

                        commit(ITEM, { member_point_log });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point_log: member_point_log[0] };
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true },
            { place_id, phone },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/point/log/${place_id}/phone/${phone}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { member_point_log },
                }) => {
                    if (success) {
                        commit(ITEM, { member_point_log });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point_log: member_point_log[0] };
                },
            );
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_point_log: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member_point_log`,
                {
                    member_point_log,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_point_log.id = info.insertId;
                    commit(ITEM, { member_point_log });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            { place_id, phone, member_point_log },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/point/log/${place_id}/phone/${phone}`,
                {
                    member_point_log,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_point_log = {
                        ...state[ITEM],
                        member_point_log,
                    };

                    commit(ITEM, { member_point_log });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            { id, userId },
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/point/log/${id}/${userId}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let member_point_logs = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, member_point_logs });
                }
                return success;
            });
            return promise;
        },
    },
};
