import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import _ from "lodash";
import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, synclog, page }: any,
        ) {
            state.count = count;
            state.list = synclog;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { synclog }: any) {
            state.item = synclog[0] || synclog;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 0,
                filter = "1=1",
                order = "reg_date",
                desc = "asc",
                limit = "100",
            },
            sync = false,
        ) {
            const promise = await list(
                { dispatch, loading },
                `sync/log/list/${filter}/${order}/${desc}/${limit}`,
                { page },
                false,
            ).then(
                ({ common: { success, error }, body: { count, synclogs } }) => {
                    if (success) {
                        commit(LIST, {
                            count,
                            synclogs,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }

                    // console.log("<===== ", success, error, count, synclogs);

                    return { success, error, count, synclogs };
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `sync/log/${id}`,
                {},
                false,
            ).then(({ common: { success, error }, body: { synclog } }) => {
                if (success) {
                    commit(ITEM, { synclog });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return { success, error, synclog };
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            { sync, synclog },
        ) {
            console.log("====>  원격서버 동기화 저장 newItem", { ...synclog });

            const promise = await post(
                { dispatch, loading },
                `sync/log`,
                { synclog },
                false,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    synclog.id = info.insertId;
                    commit(ITEM, { synclog });
                }
                return { success, error, synclog };
            });
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            { sync, synclog },
        ) {
            const promise = await put(
                { dispatch, loading },
                `sync/log/${synclog.id}`,
                {
                    synclog,
                },
                false,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { synclog });
                }
                return { success, error, info };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            { sync, id },
        ) {
            const promise = await del(
                { dispatch, sync },
                `sync/log/${id}`,
                null,
                false,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let synclog = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, synclog });
                }
                return { success, error, info };
            });
            return promise;
        },

        async delAllOldItems({ state, commit, dispatch, loading = true }: any) {
            const promise = await del(
                dispatch,
                `sync/old/logs`,
                null,
                false,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    // let synclog = state.list.filter(
                    //     (item: { id: any }) => item.id !== id,
                    // );
                    // commit(LIST, { count: state.count - 1, synclog });
                }
                return { success, error, info };
            });
            return promise;
        },
    },
};
