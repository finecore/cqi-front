import { list, item, post, put, del } from "@/api";
import {
    LIST,
    ITEM,
    COUNT,
    PAGE,
    SELECTED,
    mergeList,
} from "@/store/mutation_types";
import _ from "lodash";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        count: 0,
        list: [],
        item: {},
        page: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { [x: string]: any; count: any; page: any },
            { count, list, page }: any,
            sync = true,
        ) {
            state.count = count;
            state[LIST] = list;
            if (page) state.page = page;
        },
        [ITEM](state: { [x: string]: any }, { item }: any) {
            state[ITEM] = item;
            mergeList(state[LIST], state[ITEM], "member_id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 1,
                filter = "1=1",
                order = "member_id",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/preferences/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_preferences },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            list: member_preferences[0] || member_preferences,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return { success, error, member_preferences };
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/preferences/${member_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { member_preference },
                }) => {
                    member_preference =
                        member_preference[0] || member_preference;

                    if (success) {
                        commit(ITEM, { item: member_preference });
                    } else {
                        commit(ITEM, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return { success, error, member_preference };
                },
            );
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_preference: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member/preferences`,
                { member_preference },
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, member_preference },
                }) => {
                    if (success) {
                        commit(ITEM, { item: member_preference });
                    }
                    return { success, member_preference };
                },
            );
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            member_preference: { member_id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/preferences/${member_preference.member_id}`,
                { member_preference },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { item: member_preference });
                }
                return { success, info, member_preference };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/preferences/${member_id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return success;
            });
            return promise;
        },
    },
};
