import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, list, page }: any,
        ) {
            state.list = list;
        },
        [ITEM](state: { item: any; list: any[] }, { item }: any) {
            state.item = item;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = true,
        ) {
            const promise = await list(
                dispatch,
                `room/type/all/${place_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { all_room_types } }) => {
                    if (success) {
                        commit(LIST, {
                            list: all_room_types,
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    }
                    return { success, error, list: all_room_types };
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/type/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_type } }) => {
                if (success) {
                    const item = room_type[0] || room_type;
                    commit(ITEM, { item });
                }
                return { success, error, item };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            room_type,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room/type`,
                {
                    room_type,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    const item = room_type;
                    item.id = info.insertId;
                    commit(ITEM, { item });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            room_type,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/type/${room_type.id}`,
                {
                    room_type,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    const item = room_type;
                    commit(ITEM, { item });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `room/type/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let list = state.list.filter((item) => item.id !== id);
                    commit(LIST, { count: state.count - 1, list });
                }
                return success;
            });
            return promise;
        },
    },
};
