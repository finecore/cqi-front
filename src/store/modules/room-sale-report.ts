import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state[COUNT];
        },
        [LIST](state) {
            return state[LIST];
        },
        [ITEM](state) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](state, { count, report, page }) {
            state.count = count;
            state.list = report;
            if (page) state.page = page;
        },
        [ITEM](state, { report }) {
            state.item = report[0] || report;

            mergeList(state.list, state.item, "room_id");
        },
    },
    actions: {
        async getHoursList(
            { state, commit, dispatch, loading = true },
            {
                no,
                size,
                placeId = 0,
                begin,
                end,
                order = "a.reg_date",
                desc = "desc",
                limit = "10",
            },
            sync = true,
        ) {
            if (no) {
                limit = (no - 1) * size + "," + size;
            }

            const promise = await list(
                { dispatch, loading },
                `room/sale/report/hours/${placeId}/${begin}/${end}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, report } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            report,
                        });
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return report;
                },
            );
            return promise;
        },
        async getDaysList(
            { state, commit, dispatch, loading = true },
            {
                no,
                size,
                placeId = 0,
                begin,
                end,
                order = "a.reg_date",
                desc = "desc",
                limit = "10",
            },
            sync = true,
        ) {
            if (no) {
                limit = (no - 1) * size + "," + size;
            }

            const promise = await list(
                { dispatch, loading },
                `room/sale/report/days/${placeId}/${begin}/${end}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, report } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            report,
                        });
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return report;
                },
            );
            return promise;
        },
        async getMonthsList(
            { state, commit, dispatch, loading = true },
            {
                no,
                size,
                placeId = 0,
                begin,
                end,
                order = "a.reg_date",
                desc = "desc",
                limit = "10",
            },
            sync = true,
        ) {
            if (no) {
                limit = (no - 1) * size + "," + size;
            }

            const promise = await list(
                { dispatch, loading },
                `room/sale/report/months/${placeId}/${begin}/${end}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, report } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            report,
                        });
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return report;
                },
            );
            return promise;
        },
        async getYearsList(
            { state, commit, dispatch, loading = true },
            {
                no,
                size,
                placeId = 0,
                begin,
                end,
                order = "a.reg_date",
                desc = "desc",
                limit = "10",
            },
            sync = true,
        ) {
            if (no) {
                limit = (no - 1) * size + "," + size;
            }

            const promise = await list(
                { dispatch, loading },
                `room/sale/report/years/${placeId}/${begin}/${end}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, report } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            report,
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return report;
                },
            );
            return promise;
        },
    },
};
