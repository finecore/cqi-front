import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import format from "@/utils/format-util";

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, member_points, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = member_points;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { member_point }: any) {
            state.item = member_point[0] || member_point;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async increaseMileage(
            { state, commit, dispatch, loading = true }: any,
            member_point: any,
            sync = true,
        ) {
            // rollback 1: 관리자 변경, 2: 고객 적립, 3: 고객 사용, 4: 적립 취소, 5: 사용 취소
            let { place_id, phone, user_id, point, rollback } = member_point;

            const promise = await put(
                { dispatch, loading },
                `member/point/increase/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
                { member_point },
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, member_point },
                }) => {
                    if (success) {
                        commit(ITEM, { member_point });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point: member_point[0] };
                },
            );
            return promise;
        },

        async decreaseMileage(
            { state, commit, dispatch, loading = true }: any,
            member_point: any,
            sync = true,
        ) {
            let { place_id, phone, user_id, point, rollback } = member_point;

            const promise = await put(
                { dispatch, loading },
                `member/point/decrease/${place_id}/${phone}/${user_id}/${point}/${rollback}`,
                { member_point },
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { info, member_point },
                }) => {
                    if (success) {
                        commit(ITEM, { member_point });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, member_point: member_point[0] };
                },
            );
            return promise;
        },

        async getList(
            { state, commit, dispatch, loading = true },
            { place_id },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/point/place/${place_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_points },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            member_points,
                        });
                    } else {
                        commit(LIST, []);
                    }
                    return { success, member_points };
                },
            );
            return promise;
        },

        async getItemByMember(
            { state, commit, dispatch, loading = true },
            { member_id },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/point/member/${member_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_point } }) => {
                if (success) {
                    if (member_point[0]?.point)
                        member_point[0].point = format.toCurruncy(
                            member_point[0]?.point,
                        );

                    console.log("=====> member_point", member_point);

                    commit(ITEM, { member_point });
                } else {
                    commit(ITEM, []);
                }
                return { success, member_point: member_point[0] };
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true },
            { place_id, phone },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/point/${place_id}/phone/${phone}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member_point } }) => {
                if (success) {
                    commit(ITEM, { member_point });
                } else {
                    commit(ITEM, []);
                }
                return { success, member_point: member_point[0] };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_point: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member/point`,
                {
                    member_point,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_point.id = info.insertId;
                    commit(ITEM, { member_point });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            { place_id, phone, member_point },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/point/${place_id}/phone/${phone}`,
                {
                    member_point,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_point = {
                        ...state[ITEM],
                        member_point,
                    };

                    commit(ITEM, { member_point });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            { id, userId },
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/point/${id}/${userId}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let member_points = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, member_points });
                }
                return success;
            });
            return promise;
        },
    },
};
