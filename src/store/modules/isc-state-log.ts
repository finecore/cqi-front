import { list, item } from "@/api";
import { LIST, ITEM, COUNT, PAGE, addList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state, { count, isc_state_logs, page }) {
            state.count = count;
            state.list = isc_state_logs;
        },
        [ITEM](state, { isc_state_log }) {
            state.item = isc_state_log[0] || isc_state_log;

            addList(state.list, isc_state_log, "desc", 300);
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            { page, filter = "1=1", order = "a.id", desc = "desc" },
            sync = true,
        ) {
            const { no = 1, size = getSessionStroge("rowSize") } = page;
            let limit = (no - 1) * size + "," + size;

            const promise = await list(
                { dispatch, loading },
                `isc/state/log/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, isc_state_logs },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            isc_state_logs,
                        });
                    } else {
                        commit(LIST, { isc_state_logs: [] });
                    }
                    return isc_state_logs;
                },
            );
            return promise;
        },
        async getListLimit(
            { state, commit, dispatch, loading = true },
            { filter = "1=1", order = "a.id", desc = "desc", limit = 100 },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `isc/state/log/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, isc_state_logs },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            isc_state_logs,
                        });
                    } else {
                        commit(LIST, { isc_state_logs: [] });
                    }
                    return isc_state_logs;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `isc/state/log/${id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { isc_state_log } }) => {
                    if (success) {
                        commit(ITEM, { isc_state_log });
                    } else {
                        commit(ITEM, { isc_state_log: {} });
                    }
                    return isc_state_log;
                },
            );
            return promise;
        },
    },
};
