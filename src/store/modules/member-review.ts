import { list, item, post, put, del } from "@/api";
import {
    LIST,
    ITEM,
    COUNT,
    BEST_LIST,
    MEMBER_LIST,
    mergeList,
} from "@/store/mutation_types";
import dayjs from "dayjs";
import _ from "lodash";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        count: 0,
        list: [],
        [BEST_LIST]: [],
        [MEMBER_LIST]: [],
        item: {},
        page: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [BEST_LIST](state: { [x: string]: any }) {
            return state[BEST_LIST];
        },
        [MEMBER_LIST](state: { [x: string]: any }) {
            return state[MEMBER_LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { [x: string]: any; count: any; page: any },
            { count, list, page }: any,
        ) {
            state.count = count;
            state[LIST] = list;
            if (page) state.page = page;
        },
        [BEST_LIST](state: { [x: string]: any }, { list }: any) {
            state[BEST_LIST] = list;
        },
        [MEMBER_LIST](state: { [x: string]: any }, { list }: any) {
            state[MEMBER_LIST] = list;
        },
        [ITEM](state: { [x: string]: any }, { item }: any) {
            state[ITEM] = item;
            mergeList(state[LIST], state[ITEM], "member_id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 1,
                filter = "1=1",
                order = "reg_date",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/review/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_reviews },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            list: member_reviews[0] || member_reviews,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                    }
                    return { success, error, member_reviews };
                },
            );
            return promise;
        },

        async getBestList(
            { state, commit, dispatch, loading = true },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/review/main/best`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, member_reviews },
                }) => {
                    if (success) {
                        commit(BEST_LIST, {
                            list: member_reviews,
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(BEST_LIST, []);
                    }
                    return { success, error, member_reviews };
                },
            );
            return promise;
        },

        async getListByMember(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                `member/review/member/${member_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { member_reviews } }) => {
                    if (success) {
                        const list = _.each(member_reviews, (v) => {
                            v.reg_date = dayjs(v.reg_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.mod_date = dayjs(v.mod_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            return v;
                        });
                        commit(MEMBER_LIST, {
                            list,
                        });
                    } else {
                        commit(MEMBER_LIST, {
                            list: [],
                        });
                    }
                    return { success, member_reviews };
                },
            );
            return promise;
        },

        async getListByPlaceId(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                `member/review/place/${place_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { member_reviews } }) => {
                    if (success) {
                        const list = _.each(member_reviews, (v) => {
                            v.reg_date = dayjs(v.reg_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            v.mod_date = dayjs(v.mod_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            return v;
                        });
                        commit(MEMBER_LIST, {
                            list,
                        });
                    }
                    return { success, member_reviews };
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/review/${member_id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { member_review } }) => {
                    member_review = member_review[0] || member_review;

                    if (success) {
                        commit(ITEM, { item: member_review });
                    } else {
                        commit(ITEM, []);
                    }
                    return { success, error, member_review };
                },
            );
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member_review: any,
            sync = true,
        ) {
            delete member_review.id;

            const promise = await post(
                { dispatch, loading },
                `member/review`,
                { member_review },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member_review.id = info.insertId;
                    commit(ITEM, { item: member_review });
                }
                return { success, member_review };
            });
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            member_review: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/review/${member_review.id}`,
                { member_review },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { item: member_review });
                }
                return { success, error, info, member_review };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/review/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    const list = _.filter(
                        state[MEMBER_LIST],
                        (v) => v.id !== id,
                    );

                    commit(MEMBER_LIST, { list });
                }
                return success;
            });
            return promise;
        },

        async delAllMemberItem(
            { state, commit, dispatch, loading = true }: any,
            member_id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/review/all/member/${member_id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(MEMBER_LIST, { list: [] });
                }
                return success;
            });
            return promise;
        },
    },
};
