import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state: { list: any }, { room_sales }: any) {
            state.list = room_sales;
        },
        [ITEM](state: { item: any; list: any[] }, { room_sale }: any) {
            state.item = room_sale;
            mergeList(state.list, state.item, "id");

            console.log("- room/sale/item commit ", { ...room_sale });
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            { room_id, limit = "10000" },
            sync = true,
        ) {
            const promise = await list(
                dispatch,
                `room/sale/list/${room_id}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_sales } }) => {
                if (success) {
                    commit(LIST, {
                        room_sales,
                    });
                    return { success, error, room_sales };
                } else if (error) {
                    commit(LIST, []);
                    return { success, error, room_sales: [] };
                }
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: number,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/sale/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_sale } }) => {
                if (success) {
                    room_sale = room_sale[0] || room_sale;
                    commit(ITEM, { room_sale });
                }
                return { success, error, room_sale };
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            room_sale: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room/sale`,
                {
                    room_sale,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                let new_room_sale = { ...room_sale, id: 0 };
                if (success) {
                    new_room_sale.id = info.insertId;

                    commit(ITEM, { room_sale: new_room_sale });
                }
                return { success, error, new_room_sale, info };
            });
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            room_sale: any,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/sale/${room_sale.room_sale_id}`,
                {
                    room_sale,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { room_sale });
                }
                return { success, error, room_sale, info };
            });
            return promise;
        },

        async checkOut(
            { state, commit, dispatch, loading = true }: any,
            room_sale: any,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/sale/checkout/${room_sale.room_id}`,
                {
                    room_sale,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { room_sale });
                }
                return { success, error, room_sale, info };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `room/sale/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let room_sales = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, room_sales });
                }
                return success;
            });
            return promise;
        },

        async commitItem(
            { state, commit, dispatch, loading = true }: any,
            room_sale: any,
            sync = true,
        ) {
            return commit(ITEM, { room_sale });
        },
    },
};
