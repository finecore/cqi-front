import {
    LIST,
    ITEM,
    COUNT,
    PAGE,
    SELECTED,
    PLACE_SEARCH_TEXT,
} from "@/store/mutation_types";
import { list, item, post, put, del } from "@/api";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import dayjs from "dayjs";
import _ from "lodash";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
        [SELECTED]: 0,
        [PLACE_SEARCH_TEXT]: "",
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
        [SELECTED](state: { [x: string]: any }) {
            return state[SELECTED];
        },
        [PLACE_SEARCH_TEXT](state: { [x: string]: any }) {
            return state[PLACE_SEARCH_TEXT];
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, list, page }: any,
        ) {
            state.count = count;
            state.list = list;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { place_imgs }: any) {
            state.item = place_imgs;
        },
        [SELECTED](state: { [x: string]: any }, { id, text }: any) {
            if (id !== undefined) state[SELECTED] = id;
            if (text !== undefined) state[PLACE_SEARCH_TEXT] = text;
        },
        [PLACE_SEARCH_TEXT](state: { [x: string]: any }, text: any) {
            state[PLACE_SEARCH_TEXT] = text;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "name",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                if (!limit) limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `place/imgs/all/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, place_imgss: list },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            list,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return list;
                },
            );
            return promise;
        },

        async getListByPlaceId(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = false,
        ) {
            const promise = await list(
                dispatch,
                `place/imgs/all/${place_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { place_imgs } }) => {
                if (success) {
                    const list = _.each(place_imgs, (v) => {
                        v.reg_date = dayjs(v.reg_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.mod_date = dayjs(v.mod_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        return v;
                    });
                    commit(LIST, {
                        list,
                    });
                }
                return { success, place_imgs };
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `place/imgs/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { place_imgs } }) => {
                if (success) {
                    place_imgs = place_imgs[0] || place_imgs;
                    commit(ITEM, { place_imgs });
                }
                return { success, error, place_imgs };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            place_imgs: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `place_imgs`,
                {
                    place_imgs,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            place_imgs: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `place/imgs/${place_imgs.id}`,
                {
                    place_imgs,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let list = state.list.map((item: { id: any }) =>
                        item.id !== place_imgs.id ? item : place_imgs,
                    );

                    commit(LIST, { count: state.count, list });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `place/imgs/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: state.page });
                }
                return success;
            });
            return promise;
        },
    },
};
