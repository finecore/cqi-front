import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced로 설정하여 다른 모듈과 격리된 상태를 유지
    state: {
        [COUNT]: 0, // 파일 리스트 총 개수
        [LIST]: [], // 파일 리스트
        [ITEM]: {}, // 파일 항목 (상세보기)
        [PAGE]: { no: 1 }, // 페이지 정보
    },
    getters: {
        // 총 개수 가져오기
        [COUNT](state: { count: any }) {
            return state.count;
        },
        // 파일 리스트 가져오기
        [LIST](state: { list: any }) {
            return state.list;
        },
        // 파일 항목 (상세보기) 가져오기
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        // 파일 리스트 업데이트
        [LIST](
            state: { count: any; list: any; page: any },
            { count, files, page }: any,
        ) {
            state.count = count;
            state.list = files;
            if (page) state.page = page; // 페이지 정보 업데이트
        },
        // 파일 항목 업데이트
        [ITEM](state: { item: any; list: any[] }, { file }: any) {
            state.item = file[0] || file; // 첫 번째 파일 혹은 파일 자체로 업데이트
            mergeList(state.list, state.item, "id"); // 리스트와 항목을 병합
        },
    },
    actions: {
        // 파일 리스트 가져오기
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            console.log("- file getList", filter);

            const promise = await list(
                { dispatch, loading },
                `file/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { count, files } }) => {
                if (success) {
                    commit(LIST, { files, page: _.cloneDeep(page) }); // 페이지 정보는 cloneDeep으로 복사
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return files;
            });
            return promise;
        },

        // 파일 상세보기 가져오기
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `file/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { file } }) => {
                if (success) {
                    commit(ITEM, { file });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return file;
            });
            return promise;
        },

        // 새 파일 업로드
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            file: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `file`,
                { file },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    // 추가적으로 필요한 동작 처리
                }
                return success;
            });
            return promise;
        },

        // 파일 수정
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            file: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `file/${file.id}`,
                { file },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let files = state.list.map((item: { id: any }) =>
                        item.id !== file.id ? item : file,
                    );
                    // 파일 리스트 갱신
                    // commit(LIST, { count: state.count, files });
                }
                return success;
            });
            return promise;
        },

        // 파일 삭제
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(dispatch, `file/${id}`, null, sync).then(
                ({ common: { success, error }, body: { info } }) => {
                    if (success) {
                        let files = state.list.filter(
                            (item: { id: any }) => item.id !== id,
                        );
                        // 삭제된 파일을 제외한 새로운 리스트 갱신
                        // commit(LIST, { count: state.count - 1, files });
                    }
                    return success;
                },
            );
            return promise;
        },
    },
};
