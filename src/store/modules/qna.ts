import { list, item, post, put, del } from "@/api";
import {
    LIST,
    ITEM,
    COUNT,
    PAGE,
    QNA_ITEM,
    mergeList,
} from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

export default {
    namespaced: true, // namespaced 대신 namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
        [QNA_ITEM]: {}, // 새로운 state 추가
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
        [QNA_ITEM](state: { qnaWithAnswers: any }) {
            return state.qnaWithAnswers; // qnaWithAnswers getter 추가
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, qnas, page }: any,
        ) {
            state.count = count;
            state.list = qnas;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { qna }: any) {
            state.item = qna[0] || qna;
            mergeList(state.list, state.item, "id");
        },
        [QNA_ITEM](state: { qnaWithAnswers: any }, qnaWithAnswers: any) {
            state.qnaWithAnswers = qnaWithAnswers; // qnaWithAnswers mutation 추가
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "a.reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `qna/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { count, qnas } }) => {
                if (success) {
                    qnas = _.each(qnas, (v) => {
                        v.reg_date = dayjs(v.reg_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.mod_date = dayjs(v.mod_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.reg_date_short = dayjs(v.reg_date).format(
                            "YYYY-MM-DDs",
                        );
                        v.mod_date_short = dayjs(v.mod_date).format(
                            "YYYY-MM-DD",
                        );
                        return v;
                    });
                    commit(LIST, {
                        count: count[0].count,
                        qnas,
                        page: _.cloneDeep(page),
                    });
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return qnas;
            });
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `qna/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { qna } }) => {
                if (success) {
                    commit(ITEM, { qna });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return qna;
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            qna: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `qna`,
                {
                    qna,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    qna.id = info.insertId;
                    commit(ITEM, { qna });
                }
                return { success, error, qna };
            });
            return promise;
        },

        async setItem(
            { state, commit, dispatch, loading = true }: any,
            qna: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `qna/${qna.id}`,
                {
                    qna,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { qna });
                }
                return { success, error, qna };
            });
            return promise;
        },

        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(dispatch, `qna/${id}`, null, sync).then(
                ({ common: { success, error }, body: { info } }) => {
                    if (success) {
                        let qnas = state[LIST].filter(
                            (item: { id: any }) => item.id !== id,
                        );
                        commit(LIST, { count: state.count - 1, qnas });
                    }
                    return { success, error };
                },
            );
            return promise;
        },

        // QNA 목록 조회 (답변 개수 포함)
        async getQnaListWithAnswersCount(
            { state, commit, dispatch, loading = true },
            { filter = "1=1", limit = "10000", page },
            sync = true,
        ) {
            console.log("====> qna page", page);

            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
            }

            const promise = await list(
                { dispatch, loading },
                `qna/list/with-answers-count/${filter}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { count, qnas } }) => {
                if (success) {
                    // console.log("====> qna count", count);
                    qnas = _.each(qnas, (v) => {
                        v.content = v.content.replace(/\n/g, "<br />");
                        v.reg_date = dayjs(v.reg_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.mod_date = dayjs(v.mod_date).format(
                            "YYYY-MM-DD HH:mm:ss",
                        );
                        v.reg_date_short = dayjs(v.reg_date).format(
                            "YYYY-MM-DDs",
                        );
                        v.mod_date_short = dayjs(v.mod_date).format(
                            "YYYY-MM-DD",
                        );
                        return v;
                    });
                    commit(LIST, {
                        count: count[0].count,
                        qnas,
                        page: _.cloneDeep(page),
                    });
                } else {
                    commit(LIST, []);
                }
                return { success, qnas };
            });
            return promise;
        },

        // 특정 QNA와 답변 목록 조회 (새로운 액션 추가)
        async getQnaWithAnswers(
            { commit, dispatch }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading: true },
                `qna/with-answers/${id}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { qnaWithAnswers } }) => {
                    if (success) {
                        commit(QNA_ITEM, qnaWithAnswers);
                    } else {
                        commit(QNA_ITEM, {});
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return { success, qnaWithAnswers };
                },
            );
            return promise;
        },
    },
};
