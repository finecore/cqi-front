import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, season_premiums, page }: any,
            sync = true,
        ) {
            state.count = count;
            state.list = season_premiums;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { room_fee }: any) {
            state.item = room_fee[0] || room_fee;

            mergeList(state.list, state.item, "room_id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            { page, place_id = "", begin = "", end = "" },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `season/premium/${place_id}/${begin}/${end}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { season_premiums } }) => {
                    if (success) {
                        commit(LIST, {
                            count: season_premiums?.length,
                            season_premiums,
                        });
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return season_premiums;
                },
            );
            return promise;
        },

        async getItem(
            { state, commit, dispatch, loading = true }: any,
            { id },
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `season/premium/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room_fee } }) => {
                if (success) {
                    commit(ITEM, { room_fee });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return room_fee;
            });
            return promise;
        },

        async newItem(
            { state, commit, dispatch, loading = true }: any,
            room_fee: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `season/premium`,
                {
                    room_fee,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let season_premiums = state.list.concat(room_fee);
                    commit(LIST, { count: state.count++, season_premiums });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            room_fee: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `season/premium/${room_fee.id}`,
                {
                    room_fee,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { room_fee });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `season/premium/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let season_premiums = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count--, season_premiums });
                }
                return success;
            });
            return promise;
        },
    },
};
