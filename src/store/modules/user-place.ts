import { list, item, post, put, del } from "@/api";
import {
    LIST,
    ITEM,
    COUNT,
    PAGE,
    SELECTED,
    mergeList,
} from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
        [SELECTED]: 0,
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
        [SELECTED](state: { [x: string]: any }) {
            return state[SELECTED];
        },
    },
    mutations: {
        [LIST](state: { [x: string]: any }, { count, user_places, page }: any) {
            state[COUNT] = count;
            state[LIST] = user_places;
            if (page) state[PAGE] = page;
        },
        [ITEM](state: { [x: string]: any }, { user_place }: any) {
            state[ITEM] = user_place[0] || user_place;
            mergeList(state[LIST], state[ITEM], "id");
        },
        [SELECTED](state: { [x: string]: any }, id: any) {
            state[SELECTED] = id;
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `user/place/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, user_places },
                }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            user_places,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, { user_places: [] });
                    }
                    return user_places;
                },
            );
            return promise;
        },
        async getListTemp(
            { state, commit, dispatch, loading = true },
            {
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `user/place/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, user_places },
                }) => {
                    return { success, count, user_places };
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `user/place/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { user_place } }) => {
                if (success) {
                    commit(ITEM, { user_place });
                } else {
                    commit(ITEM, { user_place: {} });
                }
                return user_place;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            user_place: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `user/place`,
                {
                    user_place,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            user_place: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `user/place/${user_place.id}`,
                {
                    user_place,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { user_place });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `user/place/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return success;
            });
            return promise;
        },
    },
};
