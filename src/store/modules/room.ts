import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state: { list: any }, { rooms }: any) {
            state.list = rooms;
        },
        [ITEM](state: { item: any; list: any[] }, { room }: any) {
            state.item = room;
            // mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getSearch(
            { state, commit, dispatch, loading = true },
            {
                filter = "1=1",
                value1 = " ",
                value2 = " ",
                order = "a.name",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `room/list/search/${filter}/${value1}/${value2}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { rooms } }) => {
                if (success) {
                    commit(LIST, {
                        rooms,
                        page: 1,
                    });
                    return { success, error, rooms };
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
            });
            return promise;
        },
        async getAllList(
            { state, commit, dispatch, loading = true }: any,
            place_id: any,
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `room/all/${place_id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { rooms } }) => {
                if (success) {
                    commit(LIST, {
                        count: rooms.length,
                        rooms,
                    });
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return { success, count: rooms.length, rooms };
            });
            return promise;
        },

        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "a.name",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `room/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { rooms } }) => {
                if (success) {
                    commit(LIST, {
                        count: rooms.length,
                        rooms,
                        page: _.cloneDeep(page),
                    });
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return { success, count: rooms.length, rooms };
            });
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `room/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { room } }) => {
                if (success) {
                    room = room[0] || room;
                    commit(ITEM, { room });
                }
                return { success, error, room: room[0] || {} };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            room: { id: any },
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `room`,
                { room },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    room.id = info.insertId;
                    commit(ITEM, { room });
                }
                return { success, error, room };
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            room: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `room/${room.id}`,
                {
                    room,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { room });
                }
                return { success, error, room };
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(dispatch, `room/${id}`, null, sync).then(
                ({ common: { success, error }, body: { info } }) => {
                    if (success) {
                        let rooms = state.list.filter(
                            (item: { id: any }) => item.id !== id,
                        );
                        commit(LIST, { count: state.count - 1, rooms });
                    }
                    return success;
                },
            );
            return promise;
        },
    },
};
