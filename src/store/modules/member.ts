import { list, item, post, put, del } from "@/api";
import {
    LIST,
    ITEM,
    COUNT,
    PAGE,
    SELECTED,
    mergeList,
} from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";
import { MEMBER } from "@/store/mutation_types";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { [x: string]: any }) {
            return state[COUNT];
        },
        [LIST](state: { [x: string]: any }) {
            return state[LIST];
        },
        [ITEM](state: { [x: string]: any }) {
            return state[ITEM];
        },
    },
    mutations: {
        [LIST](
            state: { [x: string]: any; count: any; page: any },
            { count, members, page }: any,
            sync = true,
        ) {
            state.count = count;
            state[LIST] = members;
            if (page) state.page = page;
        },
        [ITEM](state: any, { member }: any) {
            if (member) state[ITEM] = member;

            mergeList(state[LIST], state[ITEM], "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 1,
                filter = "1=1",
                order = "id",
                desc = "asc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `member/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, members } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            members,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return { success, error, members };
                },
            );
            return promise;
        },

        async idCheck(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/idcheck/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member } }) => {
                member = member[0] || member;

                if (success) {
                    commit(ITEM, { member });
                    // 로그인 정보 업데이트
                    commit("member", { member }, { root: true });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return { success, error, member };
            });
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = false,
        ) {
            const promise = await item(
                { dispatch, loading },
                `member/crud/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { member } }) => {
                member = member[0] || member;

                if (success) {
                    commit(ITEM, { member });
                    // 로그인 정보 업데이트
                    commit("member", { member }, { root: true });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return { success, error, member };
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            member: any,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `member`,
                { member },
                sync,
            ).then(({ common: { success, error }, body: { info, member } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return { success, member };
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            member: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `member/crud/${member.id}`,
                {
                    member,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    member = { ...state[ITEM], ...member };
                    // console.log("member setItem success", member);
                    commit(ITEM, { member });
                    // 로그인 정보 업데이트
                    commit("member", { member }, { root: true });
                }
                return { success, info, member };
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `member/crud/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    dispatch("getList", { page: 1 });
                }
                return success;
            });
            return promise;
        },
    },
};
