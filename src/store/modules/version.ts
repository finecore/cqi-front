import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state, { count, versions, page }) {
            state.count = count;
            state.list = versions;
            if (page) state.page = page;
        },
        [ITEM](state, { version }) {
            state.item = version[0] || version;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page = 0,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `version/list/${filter}/${order}/${desc}/${limit}`,
                {
                    page,
                },
                sync,
            ).then(
                ({ common: { success, error }, body: { count, versions } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            versions,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return versions;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `version/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { version } }) => {
                if (success) {
                    commit(ITEM, { version });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return version;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            version,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `version`,
                {
                    version,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    version.id = info.insertId;
                    commit(ITEM, { version });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            version,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `version/${version.id}`,
                {
                    version,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { version });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `version/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let versions = state.list.filter((item) => item.id !== id);
                    commit(LIST, { count: state.count - 1, versions });
                }
                return success;
            });
            return promise;
        },
    },
};
