import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수
import _ from "lodash";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, isc_sets, page }: any,
        ) {
            state.count = count;
            state.list = isc_sets;
            if (page) state.page = page;
        },
        [ITEM](state: { item: any; list: any[] }, { isc_set }: any) {
            state.item = isc_set[0] || isc_set;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                order = "reg_date",
                desc = "desc",
                limit = "10000",
            },
            sync = true,
        ) {
            if (page) {
                const { no = 1, size = getSessionStroge("rowSize") } = page;
                limit = (no - 1) * size + "," + size;
                if (page.order) order = page.order;
                if (page.desc) desc = page.desc;
            }

            const promise = await list(
                { dispatch, loading },
                `isc/set/list/${filter}/${order}/${desc}/${limit}`,
                {},
                sync,
            ).then(
                ({ common: { success, error }, body: { count, isc_sets } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            isc_sets,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return isc_sets;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `isc/set/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { isc_set } }) => {
                if (success) {
                    commit(ITEM, { isc_set });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return isc_set;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            isc_set: { id: any },
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `isc/set`,
                {
                    isc_set,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    isc_set.id = info.insertId;
                    let isc_sets = state.list.concat(isc_set);
                    commit(LIST, { count: state.count++, isc_sets });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            isc_set: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `isc/set/${isc_set.id}`,
                {
                    isc_set,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let isc_sets = state.list.map((item: { id: any }) =>
                        item.id !== isc_set.id ? item : isc_set,
                    );
                    commit(LIST, { count: state.count, isc_sets });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync: boolean = false,
        ) {
            const promise = await del(
                dispatch,
                `isc/set/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let isc_sets = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count--, isc_sets });
                }
                return success;
            });
            return promise;
        },
    },
};
