import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state: { count: any }) {
            return state.count;
        },
        [LIST](state: { list: any }) {
            return state.list;
        },
        [ITEM](state: { item: any }) {
            return state.item;
        },
    },
    mutations: {
        [LIST](
            state: { count: any; list: any; page: any },
            { count, qna_answers, page }: any,
        ) {
            state[COUNT] = count;
            state[LIST] = qna_answers;
            if (page) state[PAGE] = page;
        },
        [ITEM](state: { item: any; list: any[] }, { qna_answer }: any) {
            state[ITEM] = qna_answer[0] || qna_answer;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true }: any,
            qna_id: any,
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `qna/answer/list/${qna_id}`,
                {},
                sync,
            ).then(
                ({
                    common: { success, error },
                    body: { count, qna_answers },
                }) => {
                    if (success) {
                        qna_answers = _.each(qna_answers, (v) => {
                            v.answer_content = v.answer_content.replace(
                                /\n/g,
                                "<br />",
                            );
                            v.answer_date = dayjs(v.answer_date).format(
                                "YYYY-MM-DD HH:mm:ss",
                            );
                            return v;
                        });
                        commit(LIST, {
                            count: qna_answers?.length,
                            qna_answers,
                        });
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return qna_answers;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `qna/answer/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { qna_answer } }) => {
                if (success) {
                    commit(ITEM, { qna_answer });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return qna_answer;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true }: any,
            qna_answer: { id: any },
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `qna/answer`,
                {
                    qna_answer,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    qna_answer.id = info.insertId;
                    commit(ITEM, { qna_answer });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true }: any,
            qna_answer: { id: any },
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `qna/answer/${qna_answer.id}`,
                {
                    qna_answer,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { qna_answer });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true }: any,
            id: any,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `qna/answer/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let qna_answers = state.list.filter(
                        (item: { id: any }) => item.id !== id,
                    );
                    commit(LIST, { count: state.count - 1, qna_answers });
                }
                return success;
            });
            return promise;
        },
    },
};
