import {
  LIST,
  ITEM,
  COUNT,
  PAGE,
  SELECTED,
  SEARCH_LIST,
  PLACE_SEARCH_TEXT,
} from '@/store/mutation_types';
import { list, item, post, put, del } from '@/api';
import { getSessionStroge } from '@/constants/constants'; // 한 페이지당 row 수
import _ from 'lodash';

export default {
  namespaced: true, // namespaced instead namespace
  state: {
    [COUNT]: 0,
    [LIST]: [],
    [ITEM]: {},
    [PAGE]: { no: 1 },
    [SELECTED]: 0,
    [SEARCH_LIST]: [],
    [PLACE_SEARCH_TEXT]: { text: '', sido: '' },
  },
  getters: {
    [COUNT](state: { [x: string]: any }) {
      return state[COUNT];
    },
    [LIST](state: { [x: string]: any }) {
      return state[LIST];
    },
    [ITEM](state: { [x: string]: any }) {
      return state[ITEM];
    },
    [SELECTED](state: { [x: string]: any }) {
      return state[SELECTED];
    },
    [SEARCH_LIST](state: { [x: string]: any }) {
      return state[SEARCH_LIST];
    },
    [PLACE_SEARCH_TEXT](state: { [x: string]: any }) {
      return state[PLACE_SEARCH_TEXT];
    },
  },
  mutations: {
    [LIST](
      state: { count: any; list: any; page: any },
      { count, list, page }: any
    ) {
      state[LIST] = list;
      if (count) state[COUNT] = count;
      if (page) state[PAGE] = page;
    },
    [ITEM](state: { item: any; list: any[] }, { place }: any) {
      state[ITEM] = place;
    },
    [SELECTED](state: { [x: string]: any }, { id, text }: any) {
      if (id !== undefined) state[SELECTED] = id;
      if (text !== undefined) state[SEARCH_LIST] = text;
    },
    [PLACE_SEARCH_TEXT](state: { [x: string]: any }, { text, sido }: any) {
      state[PLACE_SEARCH_TEXT] = { text, sido };
    },
    [SEARCH_LIST](
      state: { count: any; list: any; page: any },
      { count, list, page }: any
    ) {
      state[SEARCH_LIST] = list;
      if (count) state[COUNT] = count;
      if (page) state[PAGE] = page;
    },
  },
  actions: {
    async getList(
      { state, commit, dispatch, loading = true },
      { page, filter = '1=1', order = 'name', desc = 'asc', limit = '10000' },
      sync = true
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        if (!limit) limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      const promise = await list(
        { dispatch, loading },
        `place/list/${filter}/${order}/${desc}/${limit}`,
        {},
        sync
      ).then(
        ({ common: { success, error }, body: { count, places: list } }) => {
          if (success) {
            commit(LIST, {
              count: count[0].count,
              list,
              page: _.cloneDeep(page),
            }); // page 는 화면에서 변경 되므로 clone 한다.
          } else {
            commit(LIST, []);
            return dispatch('setApiErr', error, { cqi: true });
          }
          return list;
        }
      );
      return promise;
    },

    async getListSearch(
      { state, commit, dispatch, loading = true },
      {
        page,
        text = '',
        sido = '',
        order = 'a.rank',
        desc = 'desc',
        limit = '10',
        type = LIST,
      },
      sync = true
    ) {
      if (page) {
        const { no = 1, size = getSessionStroge('rowSize') } = page;
        if (!limit) limit = (no - 1) * size + ',' + size;
        if (page.order) order = page.order;
        if (page.desc) desc = page.desc;
      }

      // 데이터가 11개 조회되었다면, 다음 페이지가 존재하는 것이므로 isNext = true로 처리할 수 있습니다.
      // 클라이언트에서 11번째 데이터를 버리고 나머지만 보여주면 됩니다.
      limit += 1;

      const promise = await list(
        { dispatch, loading },
        `place/list/search/${text}/${sido}/${order}/${desc}/${limit}`,
        {},
        sync
      ).then(
        ({ common: { success, error }, body: { count, places: list } }) => {
          if (success) {
            commit(type, {
              count: count[0].count,
              list,
              page: _.cloneDeep(page),
            });
            commit(PLACE_SEARCH_TEXT, { text, sido });
          } else {
            commit(type, []);
            return dispatch('setApiErr', error, { cqi: true });
          }
          return {
            success,
            error,
            list,
            isNext: list.length >= limit,
          };
        }
      );
      return promise;
    },

    async getPreLoginAllList(
      { state, commit, dispatch, loading = true },
      sync = false
    ) {
      const promise = await list(
        { dispatch, loading: false },
        `place/all/prelogin`,
        {},
        sync
      ).then(({ common, body: { places } }) => {
        let { success, error } = common || {
          success: false,
          error: {},
        };

        if (success) {
          commit(LIST, {
            count: places.length,
            list: places,
            page: 1,
          });
        } else {
          commit(LIST, []);
          return dispatch('setApiErr', error, { root: true });
        }
        return { success, count: places.length, places };
      });
      return promise;
    },

    async getItem(
      { state, commit, dispatch, loading = true }: any,
      id: any,
      sync = true
    ) {
      const promise = await item(
        { dispatch, loading },
        `place/${id}`,
        {},
        sync
      ).then(({ common: { success, error }, body: { place } }) => {
        if (success) {
          place = place[0] || place;
          commit(ITEM, { place });
        }
        return { success, error, place };
      });
      return promise;
    },
    async newItem(
      { state, commit, dispatch, loading = true }: any,
      place: any,
      sync = true
    ) {
      const promise = await post(
        { dispatch, loading },
        `place`,
        {
          place,
        },
        sync
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          dispatch('getList', { page: state.page });
        }
        return success;
      });
      return promise;
    },
    async setItem(
      { state, commit, dispatch, loading = true }: any,
      place: { id: any },
      sync = true
    ) {
      const promise = await put(
        { dispatch, loading },
        `place/${place.id}`,
        {
          place,
        },
        sync
      ).then(({ common: { success, error }, body: { info } }) => {
        if (success) {
          let list = state.list.map((item: { id: any }) =>
            item.id !== place.id ? item : place
          );

          commit(LIST, { count: state.count, list });
        }
        return success;
      });
      return promise;
    },
    async delItem(
      { state, commit, dispatch, loading = true }: any,
      id: any,
      sync = true
    ) {
      const promise = await del(dispatch, `place/${id}`, null, sync).then(
        ({ common: { success, error }, body: { info } }) => {
          if (success) {
            dispatch('getList', { page: state.page });
          }
          return success;
        }
      );
      return promise;
    },
  },
};
