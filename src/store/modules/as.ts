import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state, { count, ases, page }) {
            state.count = count;
            state.list = ases;
            if (page) state.page = page;
        },
        [ITEM](state, { as }) {
            state.item = as[0] || as;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                between = "a.reg_date",
                begin = dayjs().add(-3, "year").format("YYYY-MM-DD"),
                end = dayjs().add(1, "day").format("YYYY-MM-DD"),
            },
            sync = true,
        ) {
            const { no = 1, size = getSessionStroge("rowSize") } = page;
            let limit = (no - 1) * size + "," + size;

            const promise = await list(
                { dispatch, loading },
                `as/list/${filter}/${between}/${begin}/${end}/${limit}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { count, ases } }) => {
                if (success) {
                    commit(LIST, {
                        ases,
                        page: _.cloneDeep(page),
                    }); // page 는 화면에서 변경 되므로 clone 한다.
                } else {
                    commit(LIST, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return ases;
            });
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `as/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { as } }) => {
                if (success) {
                    commit(ITEM, { as });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return as;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            as,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `as`,
                { as },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    as.id = info.insertId;
                    commit(ITEM, { as });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            as,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `as/${as.id}`,
                {
                    as,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { as });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(dispatch, `as/${id}`, null, sync).then(
                ({ common: { success, error }, body: { info } }) => {
                    if (success) {
                        let ass = state.list.filter((item) => item.id !== id);
                        commit(LIST, { count: state.count - 1, ass });
                    }
                    return success;
                },
            );
            return promise;
        },
    },
};
