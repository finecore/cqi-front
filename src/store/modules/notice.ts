import { list, item, post, put, del } from "@/api";
import { LIST, ITEM, COUNT, PAGE, mergeList } from "@/store/mutation_types";
import { getSessionStroge } from "@/constants/constants"; // 한 페이지당 row 수

import _ from "lodash";
import dayjs from "dayjs";

import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from "@/api/config";

export default {
    namespaced: true, // namespaced instead namespace
    state: {
        [COUNT]: 0,
        [LIST]: [],
        [ITEM]: {},
        [PAGE]: { no: 1 },
    },
    getters: {
        [COUNT](state) {
            return state.count;
        },
        [LIST](state) {
            return state.list;
        },
        [ITEM](state) {
            return state.item;
        },
    },
    mutations: {
        [LIST](state, { count, notices, page }) {
            state.count = count;
            state.list = notices;
            if (page) state.page = page;
        },
        [ITEM](state, { notice }) {
            state.item = notice[0] || notice;
            mergeList(state.list, state.item, "id");
        },
    },
    actions: {
        async getList(
            { state, commit, dispatch, loading = true },
            {
                page,
                filter = "1=1",
                between = "a.reg_date",
                begin = dayjs().add(-1, "year").format("YYYY-MM-DD"),
                end = dayjs().add(1, "day").format("YYYY-MM-DD"),
                limit = 1000,
                all = 1,
            },
            sync = true,
        ) {
            const promise = await list(
                { dispatch, loading },
                `notice/list/${filter}/${between}/${begin}/${end}/${limit}/${all}`,
                {
                    page,
                },
                sync,
            ).then(
                ({ common: { success, error }, body: { count, notices } }) => {
                    if (success) {
                        commit(LIST, {
                            count: count[0].count,
                            notices,
                            page: _.cloneDeep(page),
                        }); // page 는 화면에서 변경 되므로 clone 한다.
                    } else {
                        commit(LIST, []);
                        return dispatch("setApiErr", error, { cqi: true });
                    }
                    return notices;
                },
            );
            return promise;
        },
        async getItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await item(
                { dispatch, loading },
                `notice/${id}`,
                {},
                sync,
            ).then(({ common: { success, error }, body: { notice } }) => {
                if (success) {
                    commit(ITEM, { notice });
                } else {
                    commit(ITEM, []);
                    return dispatch("setApiErr", error, { cqi: true });
                }
                return notice;
            });
            return promise;
        },
        async newItem(
            { state, commit, dispatch, loading = true },
            notice,
            sync = true,
        ) {
            const promise = await post(
                { dispatch, loading },
                `notice`,
                {
                    notice,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    notice.id = info.insertId;
                    commit(ITEM, { notice });
                }
                return success;
            });
            return promise;
        },
        async setItem(
            { state, commit, dispatch, loading = true },
            notice,
            sync = true,
        ) {
            const promise = await put(
                { dispatch, loading },
                `notice/${notice.id}`,
                {
                    notice,
                },
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    commit(ITEM, { notice });
                }
                return success;
            });
            return promise;
        },
        async delItem(
            { state, commit, dispatch, loading = true },
            id,
            sync = true,
        ) {
            const promise = await del(
                dispatch,
                `notice/${id}`,
                null,
                sync,
            ).then(({ common: { success, error }, body: { info } }) => {
                if (success) {
                    let notices = state.list.filter((item) => item.id !== id);
                    commit(LIST, { count: state.count - 1, notices });
                }
                return success;
            });
            return promise;
        },
    },
};
