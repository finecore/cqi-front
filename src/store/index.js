import { createStore } from 'vuex';

// modules
import main from './modules/main';
import ws from './modules/ws';
import error from './modules/error';
import com from './modules/com';
import auth from './modules/auth';
import user from './modules/user';
import preferences from './modules/preferences';
import company from './modules/company';
import place from './modules/place';
import device from './modules/device';
import roomSaleReport from './modules/room-sale-report';
import userPlace from './modules/user-place';
import iscSet from './modules/isc-set';
import iscState from './modules/isc-state';
import iscStateLog from './modules/isc-state-log';
import iscKeyBox from './modules/isc-key-box';
import version from './modules/version';
import file from './modules/file';
import subscribe from './modules/subscribe';
import placeSubscribe from './modules/place-subscribe';
import notice from './modules/notice';
import as from './modules/as';
import room from './modules/room';
import roomState from './modules/room-state';
import roomStateLog from './modules/room-state-log';
import roomType from './modules/room-type';
import roomReserv from './modules/room-reserv';
import roomSale from './modules/room-sale';
import roomFee from './modules/room-fee';
import roomInterrupt from './modules/room-interrupt';
import seasonPremium from './modules/season-premium';
import mmsMo from './modules/mms-mo';
import mailReceiver from './modules/mail-receiver';
import syncLog from './modules/sync-log';
import sms from './modules/sms';
import member from './modules/member';
import snsLogin from './modules/sns-login';
import memberPreference from './modules/member-preferences';
import memberReview from './modules/member-review';
import roomSalePay from './modules/room-sale-pay';
import memberLike from './modules/member-like';
import memberPoint from './modules/member-point';
import memberRead from './modules/member-read';
import memberPointLog from './modules/member-point-log';
import placeImgs from './modules/place-imgs';
import qna from './modules/qna';
import qnaAnswer from './modules/qna-answer';

const debug = import.meta.env.VITE_MODE !== 'PROD';

const plugins = []; // [createPersistedState()];

const store = createStore({
  modules: {
    main,
    ws,
    error,
    com,
    auth,
    user,
    version,
    file,
    preferences,
    company,
    place,
    device,
    roomSaleReport,
    userPlace,
    iscSet,
    iscState,
    iscStateLog,
    iscKeyBox,
    subscribe,
    placeSubscribe,
    notice,
    as,
    room,
    roomState,
    roomStateLog,
    roomType,
    roomReserv,
    roomSale,
    roomFee,
    roomInterrupt,
    seasonPremium,
    mmsMo,
    mailReceiver,
    syncLog,
    sms,
    member,
    snsLogin,
    memberPreference,
    memberReview,
    roomSalePay,
    memberLike,
    memberPoint,
    memberRead,
    memberPointLog,
    placeImgs,
    qna,
    qnaAnswer,
  },
  strict: debug,
  plugins: plugins,
});

export default store;
