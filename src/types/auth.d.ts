import { RegDate } from '@/types';

export interface User extends RegDate {
  id: string;                   //아이디
  pwd: string;                  //비밀번호 SHA256
  type: number;                 //타입 (1:아이크루, 2:대리점, 3:업소, 4:PMS)
  name: string;                 //이름
  level: number;                //권한 (0:슈퍼관리자(ICT), 1:업주(책임자), 2:매니저(관리자), 3:카운터(사용자), 4:메이드, 9:뷰어)
  place_id: number | null;      //업소 코드 참조
  hp: string | null;            //휴대전화번호
  tel: string | null;           //전화번호
  email: string | null;         //임일
  dept: string | null;          //부서
  rank: string | null;          //직위
  daily_report_mail_yn: string; //일간 매출정보 메일수신여부 ('Y', 'N')
  isg_sale_mail_yn: string;     //무인 매출정보 메일수신여부 ('Y', 'N')
  ota_mo_mail_yn: string;       //OTA 자동 예약 정보 메일 수신 여부 (0:안함, 1:모두수신, 2:실패만수신)
  licence_yn: string;           //라이선스 부여 여부 ('Y', 'N')
  licence_type: number;         //라이선스 타입 (1:고정 접속, 2:유동 접속)
  use_yn: string;               //사용여부 ('Y', 'N')
  last_login_date: string;      //마지막 로그인 일시
}
