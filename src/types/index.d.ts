/** 기본 타입 */
export interface BaseType {
  _tp_: string;
}

/** 등록, 수정 일시 */
export interface RegDate {
  reg_date?: string; //등록 일시
  mod_date?: string | null; //수정 일시
}

/** Page */
export interface Page {
  no: number; //현재 페이지 번호
  size: number; //페이지당 건수
  pageTotal: number; //전체 페이지 수
}

/** PopupParam */
export interface PopupParam {
  title: string;
  theme: string;
  size: string;
  align: string;
  pin: boolean;
  link?: string;
  saveAction?: UserAction;
  showClose?: boolean; // 닫기 버튼 표시 여부. 미지정시 보임. 명시적으로 false 지정해야 안보임.
}

/** User */
export interface User extends RegDate {
  id: string; //아이디
  pwd: string; //비밀번호 SHA256
  type: number; //타입 (1:아이크루, 2:대리점, 3:업소, 4:PMS)
  name: string; //이름
  level: number; //권한 (0:슈퍼관리자(ICT), 1:업주(책임자), 2:매니저(관리자), 3:카운터(사용자), 4:메이드, 9:뷰어)
  place_id: number | null; //업소 코드 참조
  hp: string | null; //휴대전화번호
  tel: string | null; //전화번호
  email: string | null; //임일
  dept: string | null; //부서
  rank: string | null; //직위
  daily_report_mail_yn: string; //일간 매출정보 메일수신여부 ('Y', 'N')
  isg_sale_mail_yn: string; //무인 매출정보 메일수신여부 ('Y', 'N')
  ota_mo_mail_yn: string; //OTA 자동 예약 정보 메일 수신 여부 (0:안함, 1:모두수신, 2:실패만수신)
  licence_yn: string; //라이선스 부여 여부 ('Y', 'N')
  licence_type: number; //라이선스 타입 (1:고정 접속, 2:유동 접속)
  use_yn: string; //사용여부 ('Y', 'N')
  last_login_date: string; //마지막 로그인 일시
}

/** 업소 */
export interface Place extends RegDate {
  id: number; //업소 ID
  company_id: number; //회사 코드 참조
  type: string; //업소 종류(H:호텔, M:모텔, P:펜션, E:기타)
  name: string; //업소 명
  business_num: string; //사업자번호
  tel: string; //전화번호
  post: string; //우편번호
  addr: string; //주소
  addr_detl: string; //상세주소
  latitude: string; //위도
  longitude: string; //경도
  sido: string; //시도명
  gugun: string; //구군명
  expire_date: string; //유효기간(계약만료일)
  last_event_date: string; //마지막 이벤트 업데이트 시간
}

/** 업소구독 */
export interface PlaceSubscribe extends RegDate {
  id: number; //고유번호
  place_id: number; //업소 ID
  subscribe_id: number; //구독 ID
  subscribe_state: number; //구독상태 (0:미구독, 1:구독, 2:구독취소). (신규추가 ej 2023-11-23)
  price: number; //가격 (구독수 X 구독요금)
  begin_date: string; //구독 시작일
  end_date: string; //구독 종료일 (구독취소시 '구독 취소일')
  trial_end_date: string; //무료 서비스 종료일
  license_copy: number; //구독 수

  // discount?: number;                //할인요금 (TODO: 사용하지 않는다. 할인은 다른 시스템으로 개발 ej 2023-11-23)
  // valid_yn?: number;                //유효 여부//구독 서비스 상태(0:평가서비스, 1:정상서비스) (TODO: 배치에서 사용하는가? ej 2023-11-23)
  // apply_begin_date?: string;        //서비스 신청 시작일시. (TODO: 사용안함예정 ej 2023-11-23)
  // apply_end_date?: string;          //서비스 신청 종료일시. (TODO: 사용안함예정 ej 2023-11-23)
  // apply_license_copy?: number;      //라이센스 신청 사본 수. (TODO: 사용안함예정 ej 2023-11-23)
  // is_apply?: number;                //구독 신규및 수정 신청 여부(0:신규신청, 1:변경신청, 2:구독완료)  (TODO: 사용안함예정 ej 2023-11-23)

  name?: string; //서비스명
  pay_yn?: string; //유료여부
  pay_type?: string; //지불 유형 (D:일간, M:월간, Y:년간)
  code?: string; //관리코드
  discription?: string | null; //서비스 설명
  default_fee?: number; //기본 요금
  license_yn?: string; //라이센스 여부
  place_name?: string; //업소명
}

/** 구독 */
export interface Subscribe extends RegDate {
  id: number; //고유번호
  name: string; //서비스 명
  code: string | null; //관리 코드
  discription: string | null; //설명
  pay_yn: string; //유료 여부
  pay_type: string; //지불 유형 (D:일간, M:월간, Y:년간)
  default_fee: number; //기본요금
  license_yn: string; //라이센스 여부 (Y, N)
  default_license: number; //기본 라이센스 수
  use_yn: string; //사용 여부 (Y, N)
}

/** 객실 */
export interface Room extends RegDate {
  id: number; //고유번호
  place_id: number;
  room_type_id: number | null;
  name: string; //객실명
  count: number; //방 갯수
  floor: number; //층수
  gid: number | null;
  lid: number | null;
  card_barcode: number | null; //카드 바코드
  reserv_yn: number; //예약 가능 여부(0:불가, 1:가능)
  doorlock_id: string | null; //도어락 장비 아이디

  type_name?: string; //객실타입명
  sale?: number; //RoomSale.stay_type과 같다. (1:숙박, 2:대실, 3:장기). 0:공실
  isc_sale_1?: number; //숙박무인판매 여부(0:허용, 1:중지)
  isc_sale_2?: number; //대실무인판매 여부(0:허용, 1:중지)
  isc_sale_3?: number; //예약무인판매 여부(0:허용, 1:중지)
  room_id?: number; //객실ID
}

/** 객실 타입 */
export interface RoomType extends RegDate {
  id: number; //고유번호
  place_id: number; //업소 id 참조
  name: string; //타입명
  disc: string | null; //타입 설명
  person: number; //기본 수용 인원(명)
  default_fee_stay: number; //숙박 기본요금
  default_fee_rent: number; //대실 기본요금
  person_add_fee: number; //인원 추가 요금(1인당)
  reserv_discount_fee_stay: number; //숙박 예약 할인요금
  reserv_discount_fee_rent: number; //대실 예약 할인요금
  card_add_fee: number; //카드 추가 요금
  over_time_fee_stay: number; //숙박 시간 초과 요금(시간당)
  over_time_fee_rent: number; //대실 시간 초과 요금(시간당)
  ota_name_1: string | null; //OTA 등록 객실명 1
  ota_name_2: string | null; //OTA 등록 객실명 2
  ota_name_3: string | null; //OTA 등록 객실명 3
  ota_name_4: string | null; //OTA 등록 객실명 4
  ota_name_5: string | null; //OTA 등록 객실명 5
}

/** 객실 상태 */
export interface RoomState extends RegDate {
  id: number; //객실 상태 ID
  room_id: number; //객실 ID
  room_sale_id: number | null; //객실 판매 room_sale 참조 id
  channel: string; //이벤트 발생 채널 (front, device)
  dnd: number; //방해금지 (0:OFF, 1:ON)
  clean: number; //청소요청 (0:없음, 1:요청)
  clean_change_time: string | null; //청소 요청/완료 시간
  fire: number; //화재발생 (0:없음, 1:발생)
  emerg: number; //비상호출 (0:없음, 1:발생)
  sale: number; //RoomSale.stay_type과 같다. (1:숙박, 2:대실, 3:장기). 0:공실 (room_sale_id 같이 확인)
  sale_change_time: string; //입/퇴실/공실 전환 시간
  // isc_sale: number;                 //객실별 자판기 판매 여부 (삭제 대상)
  isc_sale_1: number; //숙박무인판매 여부(0:허용, 1:중지)
  isc_sale_2: number; //대실무인판매 여부(0:허용, 1:중지)
  isc_sale_3: number; //예약무인판매 여부(0:허용, 1:중지)
  key: number; //0:없음, 1:고객키, 2:마스터키 3:청소키
  key_change_time: string | null; //키값 변동 시간
  outing: number; //외출여부 (0:없음, 1:외출)
  signal: number; //통신상태 (0:정상, 1:이상)
  theft: number; //도난센서 (0:없음, 1:감지)
  emlock: number; //EM LOCK (0:잠김, 1:열림)
  door: number; //도어상태 (0:닫힘, 1:열림)
  car_call: number; //차량호출 (0:없음, 1:호출)
  airrcon_relay: number; //에어컨릴레이 (0:OFF, 1:ON)
  main_relay: number; //메인릴레이 (0:OFF, 1:ON)
  use_auto_power_off: number; //자동 전원 차단 사용 여부(0:안함, 1:사용)
  bath_on_delay: number; //욕실등 ON 지연시간 (0:OFF, 1~129:분)
  num_light: number; //넘버등제어 (0:OFF, 1:SLOW, 2:FAST, 3:ON)
  chb_led: number; //CHB LED제어 (0:OFF, 1:SLOW, 2:FAST, 3:ON)
  car_ss1: number; //차량 SS1 (0:없음, 1:감지)
  car_ss2: number; //차량 SS2 (0:없음, 1:감지)
  car_ss3: number; //차량 SS3 (0:없음, 1:감지)
  shutter: number; //셔터상태 (0:정지, 1:내림가동중, 2:내림완료, 3:올림가동중, 4:올림완료)
  toll_gate: number; //정산기문 (0:닫힘, 1:열림)
  entrance: number; //입실경로 (0:매니저, 1:정산기)
  air_sensor_no: number; //센서번호 (0~7)
  air_set_temp: number; //설정온도 (0~63)
  air_set_min: number; //온도 설정 최소값
  air_set_max: number; //온도 설정 최대값
  air_temp: number; //현재온도 (0~63)
  air_preheat: number; //예열/예냉상태 (0:OFF, 1:ON)
  air_heat_type: number; //냉/난방 (0:난방, 1:냉방)
  air_fan: number; //팬제어 (0:OFF, 1:약, 2:중, 3:강)
  air_power: number; //전원상태 (0:OFF, 1:ON, 2:KEY에연동)
  air_power_type: number; //전원기본설정 (0:OFF, 1:ON, 2:KEY에연동)
  main_power_type: number; //객실 전원 설정 (0:키에연동, 1:투입, 2:차단)
  light: number | null; //전등상태 (0:OFF, 1:ON, 2:UNKNOW, 3:UNUSED)
  dimmer: string | null; //딤머상태 (0:OFF, 1~10:ON(설정값), 14:UNKNOW, 15:UNUSED)
  curtain: string | null; //커튼상태 (0:CLOSE, 1~10:ON(설정값), 14:UNKNOW, 15:UNUSED)
  boiler_no: number; //보일러 번호 (0~7)
  boiler_set_temp: number; //보일러 설정온도 (0~63)
  boiler_temp: number; //보일러 현재온도 (0~63)
  boiler_type: number; //냉/난방 (0:온수, 1:난방)
  boiler_heating: number; //가동중 (0:정지, 1:가동)
  boiler_thermo: number; //실온선택 (0:바닥, 1:실내)
  boiler_power: number; //전원상태 (0:OFF, 1:ON)
  notice: string | null; //객실 표시
  notice_opacity: number; //표시 투명도
  notice_display: number; //객실 표시 여부 (0:표시안함, 1:표시)
  temp_key_1: number; //키에 연동 시 사용중 온도
  temp_key_2: number; //키에 연동 시 외출중
  temp_key_3: number; //키에 연동 시 공실
  temp_key_4: number; //키에 연동 시 청소중
  temp_key_5: number; //키에 연동 시 청소대기
  inspect: number; //인스펙트 상태 (0:없음, 1:점검대기, 2:점검중, 3:점검완료)
  qr_key_yn: number; //QR코드 사용 여부 (0:사용안함, 1:사용)
  keyless: number; //키리스 입실시 전원강제설정 (0:off, 1:on)

  place_id: number; //업소ID
  name: string; //룸이름

  room_type_id: number | null; //룸타입ID
  floor: number | null; //층
  state: string | null; //판매 상태 (A:정상, B:취소, C:완료)
  sale: string | null; //판매  (1:stay, 2:rent, 3:long, 5:reserv)
  sale_channel: string | null; //매출발생채널 (front, isc, api)
  stay_type: number | null; //투숙형태 (1:숙박, 2:대실, 3:장기)
  check_in: string | null; //입실시간
  check_out: string | null; //퇴실시간
  type_name: string | null; //룸타입명
  sale_pay_id: number | null; //판매결제ID
  rollback: string | null; //롤백여부 (0:없음, 1:입실취소, 2:퇴실취소)
}

/** 객실 상태 변경 로그 */
export interface RoomStateLog {
  id: number; //로그 ID
  room_id: number; //객실 ID
  data: object; // data (db json_type)
  reg_date: string; //등록 일시
  name: string; //객실이름
  type_name: string; //객실타입이름
}

/** 객실 예약 */
export interface RoomReserv extends RegDate {
  id: number;
  place_id: number; //업소 고유번호
  room_type_id: number | null; //객실 타입 고유번호
  room_id: number | null; //객실 id 참조
  reserv_num: string; //예약번호 야놀자16자리 또는 여기어때 8자리
  state: string; //예약 상태 (A:정상, B:취소, C:완료)
  stay_type: number; //투숙형태 (1:숙박, 2:대실, 3:장기)
  ota_code: number | null; //예약 OTA (1:야놀자, 2:여기어때 등등)
  ota_type: number | null; //OTA 예약 형태 (1:OTA 자동예약, 2: OTA 수동 예약)
  mms_mo_num: number | null; //MMS_MO 참조 num
  name: string | null; //예약자명
  hp: string; //예약자 전화번호(안심번호)
  reserv_date: string; //예약 일시
  check_in_exp: string; //입실 예정일시
  check_out_exp: string; //퇴실 예정 일시
  room_fee: number; //객실 판매 단가
  reserv_fee: number; //예약객실 공급단가
  prepay_ota_amt: number; //OTA 예약 금액
  prepay_cash_amt: number; //선수(예약) 현금 금액
  prepay_card_amt: number; //선수(예약) 카드 금액
  visit_type: number | null; //방문 형태 (1:도보, 2:자가용, 3:대중교통)
  memo: string | null; //메모

  room_name: string | null; //객실명
  floor: number | null; //층
  room_type_name: string; //룸타입명
  default_fee_stay: number; //숙박 기본요금
  default_fee_rent: number; //대실 기본요금
  reserv_discount_fee_stay: number; //숙박 예약 할인요금
  reserv_discount_fee_rent: number; //대실 예약 할인요금
}

/** 예약 alias. */
export type Booking = {
  [K in keyof RoomReserv]: RoomReserv[K];
};

/** 객실 판매(매출) */
export interface RoomSale extends RegDate {
  id: number;
  room_id: number;
  user_id: string | null; //근무자 아이디
  phone: string | null; //전화번호
  member_id: string | null; //회원 아이디 (추후 적용)',
  channel: string; //매출 발생 채널  (front,isc,api)
  serialno: string | null; //매출 발생 장비 일련번호
  state: string; //판매 상태 (A:정상, B:취소, C:완료)
  stay_type: number; //투숙형태 (1:숙박, 2:대실, 3:장기)
  prev_stay_type: number; //수박형태 변경 시 이전 숙박형태 (1:숙박, 2:대실, 3:장기)
  person: number; //투숙 인원
  time_option_id: number; //입실 옵션 적용 아이디
  room_reserv_id: number; //객실 예약 테이블 참조 id
  check_in_exp: string; //체크인 예정 일시
  check_out_exp: string; //체크아웃 예정 일시
  check_in: string; //체크인 일시
  check_out: string; //체크아웃 일시
  default_fee: number; //기본 요금 (판매 당시의 기본 요금 정보)
  add_fee: number; //추가 요금
  option_fee: number; //옵션 요금
  prepay_ota_amt: number; //OTA 예약금
  prepay_cash_amt: number; //예약 선금 현금
  prepay_card_amt: number; //예약 선금 카드
  prepay_point_amt: number; //포인트 선결제 금액
  pay_card_amt: number; //카드 결제 금액
  pay_cash_amt: number; //현금 결제 금액
  pay_point_amt: number; //포인트 사용 금액
  save_point: number; //적립 포인트
  alarm: number; //모닝콜 0:없음, 1:알림
  alarm_hour: number; //모닝콜 시간
  alarm_min: number; //모닝콜 분
  alarm_memo_play: number; //객실 알람 시 메모 음성 지원
  alarm_term: number; //알람 반복 간격(분)
  alarm_repeat: number; //알람 반복 횟수
  memo: string | null; //메모
  car_no: string | null; //차량 번호
  card_approval_num: string | null; //카드 승인 번호
  card_merchant: string | null; //카드 가맹점
  card_no: string | null; //카드 번호
  card_accepter_name: string | null; //신용카드 매입사이름(예:삼성마스터카드, 비씨카드)
  comment: string | null; //비고
  qr_key_phone: string | null; //QR코드 키발송 휴대전화번호

  room_name: string | null; //객실명
  room_type_id: number | null; //룸타입ID
  place_id: number | string; //업소ID
  sale: number | null; //객실판매 (0:없음, 1:판매)
  user_name: string | null; //근무자명

  stay_num?: number; //숙박일수. (1:1박2일)
  stay_short_num?: number; //대실시간.
  stay_long_num?: number; //장기숙박일수. (1:1박2일)
}

/** 객실 시간대 금액 */
export interface RoomFee extends RegDate {
  id: number; //일련번호
  channel: number; //적용 채널
  room_type_id: number; //객실 타입 id 참조
  day: number; //요일 구분(월~일 1~7) 구분자(|). (참고)dayjs()는 0:일, 1:월요일~6:토요일.
  stay_type: number; //대실 숙박 구분
  begin: string; //시작 시간 (시분) 4자라
  end: string; //종료 시간 (시분) 4자라
  add_fee: number; //추가 요금
}

/** 객실 테마 */
export interface RoomTheme extends RegDate {
  id: number; //일련번호
  theme: string; //적용 채널
  name: string; //객실 타입 id 참조
}

/** 시즌 프리미엄 */
export interface SeasonPremium extends RegDate {
  id: number; //일련번호
  place_id: number; //업소 id 참조
  title: string; //제목
  begin: string; //기간 시작일 (월일)
  end: string; //기간 종료일 (월일)
  premium: number; //프리미엄 가격
  add_stay_time: number; //시즌 숙박 시간 조정 (+,-) 00 시간
  add_rent_time: number; //시즌 대실 시간 조정 (+,-) 00 시간
}

/** 공지사항 */
export interface Notice extends RegDate {
  id: number;
  place_id: number; //업소고유번호 (0:전체공지, >0:특정업소공지)
  type: number; //타입 (1:공지사항, 2:업데이트사항, 3:기타)
  open: number; //업소공개 여부 (0:비공개, 1:공개)
  important: number; //중요알림 여부 (1:일반, 2:중요)
  display: number; //업소알림 여부 (0:없음, 1:알림)
  display_time: string; //업소알림 표시시간 (HH:mm)
  display_repeat: number; //업소알림 일일반복여부 (0:없음, 1:일간반복)
  title: string; //타이틀
  content: string; //내용
  user_id: string; //등록자 ID
  begin_date: string; //게시 시작 일시
  end_date: string; //게시 종료 일시
}

/** 업체 공지사항 */
export interface PlaceNotice extends RegDate {
  id: number; //ID
  place_id: number; //업소 고유 번호(0:전체공지, 0초과:특정업소공지)
  type: number; //타입 (1:공지사항, 2:메모)
  state: number; //상태 (1:등록, 2:확인, 3:처리중, 9:처리완료)
  title: string; //타이틀
  content: string; //내용
  user_id: string; //등록자 ID
}

/** 숙박 뉴스 */
export interface StayNews {
  title: string; //제목
  link: string; //링크
  description: string; //채널제목
  language: string; //언어
  copyright: string; //copyright
  lastBuildDate: string; //최종작성일
  items: StayNewsItem[]; //기사목록
}

/** 숙박 뉴스 항목  */
export interface StayNewsItem {
  id: number; //id
  title: string; //제목
  link: string; //링크
  description: string; //내용
  author: string; //작성자
  pubDate: string; //기사일
}

/** 게시물 이전/다음 */
export interface PrevNext {
  tag: string; //태그 ("UP", "DOWN")
  id: number; //기준 ID
  reg_date: string; //등록일시
  title: string; //이름 또는 제목
  content: string; //설명 또는 내용
}

/** 층 */
export interface Floor {
  no: number; //층 번호
  name: string; //층 이름
  room_count: number; //층 객실수
  room_infos: string; //객실정보들
  rooms?: FloorRoom[]; //객실들
  sync?: boolean; //sync 여부
}

/** 층의 객실 */
export interface FloorRoom {
  id: number; //고유번호
  name: string; //객실명
}

/** 이번 주기, 이전 주기 데이터 묶음 */
export interface NowAndPrevious<T> {
  now: T; //이번 주기 데이터
  previous: T; //이전 주기 데이터
}

/**
 * Device
 */
export interface Device extends RegDate {
  id: number; //고유번호
  place_id: number; //업소 id 참조
  type: string; //구분 (01:IDM, 02:ISG, 10:RPT)
  maker: string; //제조사
  model: string; //모델 코드(01)
  name: string; //장비명
  pms: string; //pms 업체명
  serialno: string; //시리얼번호
  connect: number; //서버연결 여부(0:정상1:비정상)
  server_ip: string; //소켓 서버 접속 IP
  pwd: string; //비밀번호
  sale_stop: number; //판매 중지 (0:판매, 1:중지)
  expire_date: string | null; //인증 유효 기간
  curr_version_id: number | null; //현재 버전 정보 id
  next_version_id: number | null; //다음 업데이트 버전 정보 id
  next_version_update_date: string | null; //다음 버전 업데이트 일시
}

/**
 * IscState
 */
export interface IscState extends RegDate {
  serialno: string; //기기 일련번호
  channel: string; //채널
  state: number; //0:판매대기중, 1: 판매 가능한 객실이 없음 또는 판매 중지 상태
  step: number; //" 1 : 판매대기 단계\n 2 : 성인인증 모드\n 3 : 예약 확인 단계\n 4 : 객실 선택 단계 (예약 시 스킵)\n 5 : 객실 선택 확인 단계 (예약 시 스킵)\n 6 : 결제 단계 (예약 시 스킵)\n 7 : 결제 확인 단계 및 카드키 토출\n 8 : 영수증 출력 단계 (고객 선택 시)"'
  start_time: string | null; //'고객 조작 시작 시간 YYYY-MM-DD HH:mm (고객 조작 시 초기화)'
  end_time: string | null; //'고객 조작 종료 시간 YYYY-MM-DD HH:mm  (고객 조작 시 초기화)'
  minor_mode: number; //성인인증모드 0:해제, 1:설정
  minor_state: number; //성인인증상태 0:대기상태, 1:성인인증진행중, 2:성인인증완료, 3:성인인증실패
  minor_auth_type: number; //0:사람의 개입 없이 고객이 직접 셀프인증, 1:관제 직원이 확인후 관제인증
  minor_auto_disable: number; //'0' COMMENT '0 : 미사용\\n1 : 성인인증 완료시 ISG가 자동으로 minor_auth 항목을 0으로 변경합니다.'
  door: number; //정산기 문 (1:열림,  0:닫힘)
  motion: number; //모션감지 (0:미감지, 1:감지)
  rent_sale_count: number; //현재 대실로 판매중인 객실 수
  stay_sale_count: number; //
  language: number | null; //1:한국어, 2:영어, 3:중국어, 4:일본어
  voice_call: number; //'0: 없음, 1:음성지원요청, 2:음성지원중, 3:음성지원종료.'
  setting: number; //'0: 없음, 1:자판기 설정중'
  reserv_num: string | null; //예약번호
  stay_type: number; //'1:숙박, 2:대실 // 현재 선택한 유형'
  room_id: number; //'선택한 객실의 room id'
  pay_type: string; //결제방법 1:현금, 2:카드결제
  pay_amt: number; //'결제할 금액'
  reserv_error_code: string | null; //예약번호 오류코드
  reserv_error_msg: string | null; //예약번호 오류 메세지
  dspl_signal: number | null; //'좌측 지페방출기 0 : 통신 정상 // 1 : 통신 단절'
  dspl_state: number | null; //'좌측 지페방출기 0: 대기상태 , 1 : 방출동작중'
  dspl_save_money: number; //'좌측 지페방출기 잔여 지폐 금액'
  dspl_not_change: number; //'좌측 지페방출기 미방출금액 (사용 안함)'
  dspl_change_unit: number; //'좌측 지페방출기 방출 권종'
  dspl_change_req_cnt: number; //'좌측 지페방출기 방출 요청 장수'
  dspl_change_res_cnt: number; //'좌측 지페방출기 방출 완료 장수'
  dspl_error_code: string | null; //좌측 지페방출기 방출기 에러코드 // 0:정상
  dspl_error_msg: string | null; //좌측 지페방출기 사람이 알아볼수잇는 에러코드 설명 (지폐걸림 , 지폐없음 , 도둑감지 등등)'
  dspr_signal: number | null; //'우측 지페방출기 0 : 통신 정상 // 1 : 통신 단절'
  dspr_state: number | null; //'우측 지페방출기 0: 대기상태 , 1 : 방출동작중'
  dspr_save_money: number; //'우측 지페방출기 잔여 지폐 금액'
  dspr_not_change: number; //'우측 지페방출기 미방출금액'
  dspr_change_unit: number; //'우측 지페방출기 방출 권종',
  dspr_change_req_cnt: number; //'우측 지페방출기 방출 요청 장수'
  dspr_change_res_cnt: number; //'우측 지페방출기 방출 완료 장수'
  dspr_error_code: string | null; //'우측 지페방출기 방출기 에러코드 // 0:정상',
  dspr_error_msg: string | null; //우측 지페방출기 사람이 알아볼수잇는 에러코드 설명 (지폐걸림 , 지폐없음 , 도둑감지 등등)
  act_signal: number; //'0 : 통신 정상 // 1 : 통신 단절'
  act_state: number | null; //'0: 입수 금지상태 // 1: 입수 대기상태'
  act_input_fee: number; //'현재 입수된 금액 합계 (결제 완료시 0으로 초기화)'
  act_input_unit: number; //'입수된 권종'
  act_error_code: string | null; //1:왼쪽 지폐방출기, 2:오른쪽 지폐 방출기
  act_error_msg: string | null; //사람이 알아볼수잇는 에러코드 설명 (지폐걸림 , 지폐없음 , 도둑감지 등등)
  crd_signal: number | null; //'0 : 통신 정상 // 1 : 통신 단절'
  crd_state: number | null; //'0: 대기상태 1 : 카드 투입대기중  2 : 결제중   3 : 카드제거(SamsungPay 생략)  4 : 결제 성공 5 : 결제실패 6 : 사용중지'
  crd_type: number | null; //'인식된 카드 유형 1 : IC카드 // 2 : 삼성페이 (초기화시점 : 결제완료/실패시)'
  crd_card_no: string | null; //카드번호 (앞 6자리) ex : 1234-56**-****-****'
  crd_approval_no: string | null; //승인번호 8자리
  crd_error_code: string | null; //방출기 에러코드 // 0:정상
  crd_error_msg: string | null; //사람이 알아볼수있는 에러코드 설명 (한도초과 , IC리딩실패)등
  prt_signal: number | null; //'0 : 통신 정상 // 1 : 통신 단절',
  prt_state: number; //'0 : 미출력 // 1 : 출력  (초기화시점 : 결제완료시 )'
  prt_error_code: string | null; //방출기 에러코드 0:정상
  prt_error_msg: string | null; //사람이 알아볼수잇는 에러코드 설명 (용지없음 , 용지걸림 등)
  key_state: number; //카드키 상태 (0:토출 대기, 1:키토출중, 2:키토출완료, 3:키토출실패)
  key_gid: number; //토출 키 gid
  key_did: number; //토출 키 did

  isc_id: number; //isc ID, device.id 임. isc_state 테이블에 id 없음
  place_id: number; //업소 ID
  isc_name: string; //장비명. device.name 임.
  place_name: string; //업소명. place.name 임.
}

export interface PayAmount {
  prepay_ota_amt: number; //OTA 예약금
  prepay_cash_amt: number; //예약 선금 현금
  prepay_card_amt: number; //예약 선금 카드
  prepay_point_amt: number; //포인트 선결제 금액
  pay_card_amt: number; //카드 결제 금액
  pay_cash_amt: number; //현금 결제 금액
  pay_point_amt: number; //포인트 사용 금액
}

/**
 * 숙박형태별 통계
 */
export interface StayCount {
  standard: number; //숙박
  shortTime: number; //대실
  longTime: number; //장기
}

/**
 * 객실 뷰(room_view), 테마(room_theme) 포함
 */
export interface RoomView extends RegDate {
  id: number; //고유번호
  place_id: number; //업소 id 참조
  theme_id: number; //객실 테마 id 참조
  title: string; //뷰 타이틀
  comment: string; //뷰 설명
  all_room: number; //객실 등록 시 전체 객실 여부(0:나머지 객실, 1:전체객실)
  room_order: number; //객실추가순서 (1:객실명순서, 2:객실명역순, 3:객실타입순서, 4:객실타입역순)
  rows: number; //층수
  cols: number; //가로 객실수
  col_gap: number; //객실 간격
  row_gap: number; //층별 간격
  width: number; //객실 가로 크기
  height: number; //객실 세로 크기
  order: number; //뷰 순서
  theme?: string; //테마. room_theme
}

/**
 * 객실 뷰 아이템(room_view_item)
 */
export interface RoomViewItem extends RegDate {
  id: number; //일련번호
  view_id: number; //뷰 id 참조
  room_id: number; //객실 id 참조
  row: number; //Row 순서
  col: number; //컬럼순서
  room_name: string; //객실 이름
  type_name: string; //객실타입 이름
  drag_fixed: boolean; //드래그앤드랍 고정여부.
  drag_moved: boolean; //드래그앤드랍 이동여부.
}

/**
 * 객실 인터럽트.
 */
export interface RoomInterrupt extends RegDate {
  room_id: number; //업소 id 참조
  channel: number; //작업 채널 (1:WEB, 2:자판기, 3:MOBILE)
  user_id: string; //사용자 아이디
  token: string; //인터럽트 건 사용자(장비) 토큰
  state: number; //설정 기능 중지 여부 (0:없음, 1:중지)
  sale: number; //판매 기능 중지 여부 (0:없음, 1:중지)
}

/**
 * ID, Name 인터페이스
 */
export interface IdName {
  id: number;
  name: string;
}

/**
 * 객실 아이템 (room_view_item, room, room_type, room_stae, room_sale)
 */
export interface RoomItem {
  view_item_id: number; //일련번호
  view_id: number; //뷰 id 참조
  room_id: number; //객실 id 참조
  row: number; //Row 순서
  col: number; //컬럼순서

  active: boolean; //객실 활성 여부
  room_id: number; //객실 ID
  room_name; //객실 이름
  count: number; //방 갯수
  floor: number; //층수
  card_barcode: number | null; //카드 바코드
  reserv_yn: number; //예약 가능 여부(0:불가, 1:가능)
  doorlock_id: string | null; //도어락 장비 아이디

  room_type_id: number; //객실타입 ID
  room_type_name; //객실타입 이름
  person: number; //기본 수용 인원(명)
  default_fee_stay: number; //숙박 기본요금
  default_fee_rent: number; //대실 기본요금
  person_add_fee: number; //인원 추가 요금(1인당)
  reserv_discount_fee_stay: number; //숙박 예약 할인요금
  reserv_discount_fee_rent: number; //대실 예약 할인요금
  card_add_fee: number; //카드 추가 요금
  over_time_fee_stay: number; //숙박 시간 초과 요금(시간당)
  over_time_fee_rent: number; //대실 시간 초과 요금(시간당)

  room_state_id: number; //객실 상태 ID
  dnd: number; //방해금지 (0:OFF, 1:ON)
  clean: number; //청소요청 (0:없음, 1:요청)
  clean_change_time: string | null; //청소 요청/완료 시간
  fire: number; //화재발생 (0:없음, 1:발생)
  emerg: number; //비상호출 (0:없음, 1:발생)
  sale: number; //객실판매 (0:없음, 1:판매)
  key: number; //0:없음, 1:고객키, 2:마스터키 3:청소키
  key_change_time: string | null; //키값 변동 시간

  outing: number; //외출여부 (0:없음, 1:외출)
  signal: number; //통신상태 (0:정상, 1:이상)
  theft: number; //도난센서 (0:없음, 1:감지)
  emlock: number; //EM LOCK (0:잠김, 1:열림)
  door: number; //도어상태 (0:닫힘, 1:열림)
  car_call: number; //차량호출 (0:없음, 1:호출)
  airrcon_relay: number; //에어컨릴레이 (0:OFF, 1:ON)
  main_relay: number; //메인릴레이 (0:OFF, 1:ON)
  use_auto_power_off: number; //자동 전원 차단 사용 여부(0:안함, 1:사용)
  entrance: number; //입실경로 (0:매니저, 1:정산기)
  air_temp: number; //현재온도 (0~63)
  notice: string | null; //객실 표시
  notice_opacity: number; //표시 투명도
  notice_display: number; //객실 표시 여부 (0:표시안함, 1:표시)
  temp_key_1: number; //키에 연동 시 사용중 온도
  temp_key_2: number; //키에 연동 시 외출중
  temp_key_3: number; //키에 연동 시 공실
  temp_key_4: number; //키에 연동 시 청소중
  temp_key_5: number; //키에 연동 시 청소대기
  inspect: number; //인스펙트 상태 (0:없음, 1:점검대기, 2:점검중, 3:점검완료)

  room_sale_id: number | null; //판매 ID. 없으면 0.
  sale_cahnnel: string | null; //판매 채널. 없으면 null.
  state: string | null; //판매 상태 (A:정상B:취소C:완료) 미판매는 null.
  stay_type: number | null; //투숙형태 (1:숙박, 2:대실, 3:장기)
  check_in_exp: string | null; //체크인 예정 시간
  check_out_exp: string | null; //체크아웃 예정 시간
  check_in: string | null; //체크인 시간
  check_out: string | null; //체크아웃 시간
  alarm: number | null; //모닝콜 0:없음, 1:알림
  alarm_hour: number | null; //모닝콜 시간
  alarm_min: number | null; //모닝콜 분
  alarm_memo_play: number | null; //객실 알람 시 메모 음성 지원
  alarm_term: number | null; //알람 반복 간격(분)
  alarm_repeat: number | null; //알람 반복 횟수
  memo: string | null; //메모
  car_no: string | null; //차량 번호
}

/** Preference */
export interface Preference extends RegDate {
  id: number; //ID
  place_id: number; //업소ID
  place_name: string; //업소명
  svc_mode: number; //서비스 모드 (0:Air, 1:Pro)
  pms: string | null; //PMS 연동 시 업체명
  pms_api_key: string | null; //PMS 업체 접속 API KEY
  pms_socket_access_key: string | null; //PMS 업체 로컬 PC 에서 접속 시 고유 KEY (소켓 접속 토큰 발행용)
  pms_company_id: string | null; //PMS 연동 시 업소 아이디',
  inspect_use: number; //객실 인스펙트 사용 여부 (0: 사용 안함, 1: 퇴실 상태만 사용, 2: 항상 사용)
  inspect_done_show_time: number; //점검완료 표시 시간 (분)
  rent_fee_unit: number; //0:불가, 1~10000:요금 대실판매금액 (천원단위)
  stay_fee_unit: number; //0:불가, 1~10000:요금 숙박판매금액 (천원단위)
  change_cnt: number; //0~63 거스름돈 보충 장수(10장 단위 적용)
  chage_unit: number; //0:미사용, 1:5천원, 2:1만원, 3:1천원 거그름돈 단위
  voice: number; //0:미사용, 1:사용  음성사용
  voice_opt: string | null; //음성 옵션
  voice_rate: number; //음성 재생 속도
  voice_name: string; //음성 타입 ko-KR-Standard-A ~ D, ko-KR-Wavenet-A ~ D'
  voice_type_name_play: number; //객실 타입명 음성 출력여부(0: 안함, 1:출력)
  preheat_yn: number; //예열/예냉여부 (0:OFF, 1:ON)
  use_rent_begin: number; //대실 가능 시작 시간
  use_rent_end: number; //대실 가능 종료 시간
  cancel_time: number; //입실취소 활성화 유지 기간 (분)
  stay_time_type: number; //숙박 시간 타입 (0: 이용 시간기준, 1:퇴실 시간 기준)
  stay_time: number; //기본 숙박 시간
  stay_time_weekend: number; //주말 숙박 시간
  rent_time_am: number; //평일 오전 대실 시간
  rent_time_pm: number; //평일 오후 대실 시간
  rent_time_am_weekend: number; //주말 오전 대실 시간
  rent_time_pm_weekend: number; //주말 오후 대실 시간
  stay_type_color: string | null; //숙박 형태 컬러 값
  key_type_color: string | null; //키 컬러 값
  room_name_color: string | null; //객실명 컬러 값
  time_bg_color: string | null; //시간 배경 컬러 값
  show_type_name: number; //객실 타입명 보이기여부(0:숨기기, 1:보이기)
  font_size: number; //폰트 사이즈
  font_size_1: number; //좌측 메뉴 폰트 사이즈
  font_size_2: number; //팝업 레이어 폰트 사이즈
  font_size_3: number; //객실 정보 폰트 사이즈
  air_power_type: number; //0:KEY 에연동, 1:ON, 2:OFF
  air_set_temp: number; //0 ~ 63   설정온도
  air_set_min: number; //온도 설정 최소값
  air_set_max: number; //온도 설정 최대값
  temp_key_1: number; //키에 연동 시 사용중
  temp_key_2: number; //키에 연동 시 외출중
  temp_key_3: number; //키에 연동 시 공실
  temp_key_4: number; //키에 연동 시 청소중
  temp_key_5: number; //키에 연동 시 청소대기
  interrupt_use: number; //인터럽트 사용 여부 0:안함, 1:사용
  interrupt_release: number; //인터럽트 자동 해제 시간 설정 (분)
  place_lock_use: number; //자리비움 자동 잠금 여부 0:안함, 1:사용
  place_lock_time: number; //자리비움 시 자동 잠김 시간 설정 (분)
  auto_check_out: number; //자동 퇴실 사용 여부 0: 없음, 1:사용
  auto_check_out_door_open: number; //사용 시간 경과 후 출입문이 열리면 자동 퇴실처리 (0: 사용안함,1:사용)
  auto_check_out_key_out: number; //사용 시간 경과 후 손님키 제거 시 자동 퇴실처리 (0: 사용안함,1:사용)
  auto_check_in: number; //손님키 삽입 시 자동 입실 (0:사용안함, 1:대실만 사용, 2:숙박만 사용, 3:대실/숙박 사용)
  user_key_out_rent: number; //손님키 제거시 대실 설정 (0:외출, 1:퇴실)
  user_key_out_stay: number; //손님키 제거시 숙박 설정 (0:외출, 1:퇴실)
  user_key_out_delay_time: number; //손님키 삽입 후 바로 제거 시 상태 변경 딜레이 시간(분)  CCU 에 설정 해야함.
  master_key_out_type: number; //마스터키 제거 시 자동퇴실 및 공실 처리 여부 (0:사용안함, 1:대실, 2:숙박, 3:대실+숙박)
  clean_key_out_type: number; //청소키 제거 시 공실로 변경 여부 (0:사용안함, 1:대실, 2:숙박, 3:대실+숙박)
  clean_key_out_type_force: number; //청소키 제거 시 무조건 공실 처리 여부 (0:사용안함, 1:사용)
  check_out_cancle_time: number; //퇴실 취소 가능 시간(분)
  can_use_check_in_time: string; //숙박 사전 체크인 시간 (입실 시간 대비 00시 00분)
  pre_check_in_time_clean: number; //예약 객실 청소 시간(분)
  day_sale_end_time: number; //일일 매출 정산 시간 (24시간)
  data_reload_term: number; //브라우저에서 데이터 리로드 기간(분), 네트웤 유실로 웹소켓으로 반영되지 않은 데이터를 채 워준다.
  no_sale_user_key_in_power_off_time: number; //체크인 미 상태에서 손님키 삽입 시 몇분 후 전원 차단
  clean_key_in_power_off_time: number; //청소키 삽입 시 몇분 후 전원 차단
  user_key_in_check_out_power_off_time: number; //손님키 삽입 상태 체크아웃 시 몇분 후 전원 차단
  page_reload: number; //화면 강제 리로드 기능 0:없음, 1:리로드 (업데이트가 있을때 사용)
  isc_sale: number; //자판기 판매 여부 (0:판매, 1:중지, 2:숙박만 무인판매, 3:대실만 무인판매)
  isc_sale_1: number; //숙박 무인 판매 여부(0: 허용, 1:중지)
  isc_sale_2: number; //대실 무인 판매 여부(0: 허용, 1:중지)
  isc_sale_3: number; //예약 무인 판매 여부(0: 허용, 1:중지)
  speed_checkin: number; //바로 체크인 기능 (0:사용안함, 1:사용)
  tooltip_show: number; //화면 도움말 툴팁 보기 (0:숨기기, 1:보기, 2:주요정보만보기)
  show_air_temp: number; //온도 보이기 여부(0:숨기기, 1:보이기)
  use_car_call: number; //차량호출 기능 사용여부(0:사용안함, 1:사용)
  use_emerg: number; //비상호출 기능 사용여부(0:사용안함, 1:사용)
  layout_width_left: number; //좌측 영역 넓이 설정
  reserv_auto_assign_room: number; //예약 시 객실 자동 배정 (0: 사용안함, 1:사용)
  expire_pre_alert_day: number; //유효기간 만료전 알림 일 수
  expire_check_interval: number; //유효기간 만료 확인 간격 (분)
  default_pay_type: number; //체크인 기본 요금 타입 (1:현금, 2:카드)
  check_out_alarm_time: number; //퇴실 예정 시간 알림 시작 시간(분)
  data_keep_day_sale: number; //매출 이력 보관 기간(0 이면 삭제 안함)
  data_keep_day_state: number; //객실 이력 보관 기간(0 이면 삭제 안함)
  call_alert_term: number; //차량호출 / 비상호출 알림 지연 시간(분)
  cash_point_rate: number; //현금 결제 시 적립 포인트율
  card_point_rate: number; //카드 결제 시 적립 포인트율
  reserv_point_rate: number; //예약 시 적립 포인트율
  ota_place_name: string | null; //OTA 업체에 등록된 업소명 (예약문자 자동 연동시 이글 업소명과 매핑 시 사용)
  ota_place_name_1: string | null; //야놀자에 등록된 업소명 (예약문자 자동 연동시 이글 업소명과 매핑 시 사용)
  ota_place_name_2: string | null; //여기어때에 등록된 업소명 (예약문자 자동 연동시 이글 업소명과 매핑 시 사용)
  ota_place_name_3: string | null; //네이버에 등록된 업소명 (예약문자 자동 연동시 이글 업소명과 매핑 시 사용)
  isg_point_rate: number; //무인 판매 시 적립 포인트율
  pay_add_point: number; //결제 시 추가 적립 포인트
  point_use_type: number; //포인트 사용 타입 (0:현금/카드 혼합사용, 1:포인트 단독사용(객실요금 이상 사용))
  point_pay_type: number; //포인트 사용 옵션 설정 (1:포인트 단독사용, 2:현금/카드 혼합사용)
  point_check_phone: number; //체크인 시 휴대폰번호 확인 알림 (0:사용안함, 1:사용)
  point_add_use: number; //체크인 시 적립 포인트 추가 기능 (0:사용안함, 1:사용)
  car_call_display: number; //차량번호 객실메모에 표시 여부 (0:안함, 1:표시)
  sale_view_type: number; //매출 집계 보기 옵션 선택 항목 (1:고객기준, 2:매출기준)
  reserv_notice_day: number; //객실화면에 표시할 예약의 일자 범위(1~999 일 이내)
  room_move_no_key: number; //객실이동 시 손님키 있는 객실 제외 여부 (0:안함, 1:제외)
  show_sale_change_time: number; //공실 경과 시간 표시 여부 (1:표시 0:숨김)
  use_sale_cancel_comment: number; //입실 취소 사유 입력 여부 (0:안함, 1:사용)
  room_log_show_type: string | null; //메인 화면 객실 이력 표시 여부 (설명이 맞나?)
  checkqin_url: string | null; //크루 이글 접속 url
  reserv_num_send_qr_code: number; //예약번호 QR코드 발송 여부 (0:안함, 1:사용)
  doorlock_send_qr_code: number; //도어락 OTP 번호 QR코드 발송 여부 (0: 안함, 1:사용)
  doorlock_appid: string | null; //도어락 APPID
  doorlock_username: string | null; //도어락 사용자 이름(로그인 사용자)
  doorlock_userid: string | null; //도어락 사용자 ID(로그인 사용자)
  doorlock_password: string | null; //도어락 사용자 비밀번호(로그인 사용자)
  doolock_qr_add: string | null; //도어락 QR 코드 화면 광고 이미지 경로(API 서버)
  use_kiosk_keyless: number; //키오스크 키리스 인식 (0:미사용, 1:사용)
  main_power_type: number; //객실 전원 설정 여부 (0:안함, 1:사용)
  default_add_point_rate: number; //기본 적립 포인트 비율(요금 대비 %)
}

export interface TimeOption extends RegDate {
  id: number; //고유번호
  place_id: number; //업소 아이디
  name: string; //서비스 명
  stay_type: number; //투숙형태 (1:숙박, 2:대실, 3:장기)
  begin: number; //시작 시간
  end: number; //종료 시간
  use_time: number; //이용 시간
  use_time_type: number; //이용/퇴실 시간 선택 (1: 이용시간, 2: 퇴실시간)
  add_fee: number; //추가/할인 요금
  room_types: string | null; //적용 객실 타입 목록
  sale_isc: number; //무인 판매 여부 (0:판매, 1:금지)

  room_type_ids: number[]; //적용 객실 타입 ID 목록
}

/** Mileage */
export interface Mileage extends RegDate {
  id: number; //고유ID
  place_id: number; //업소ID
  phone: string; //휴대전화번호(숫자만)
  point: number; //가용 포인트
  member_id: string | null; //회원 아이디 (추후 적용)

  place_name?: number; //업소명
  member_name?: string; //관리자이름
}

/** MileageLog */
export interface MileageLog {
  id: number; //고유ID
  place_id: number; //업소 고유 번호 참조
  mileage_id: number; //마일리지 포인트 참조
  phone: string; //휴대전화번호(숫자만)
  user_id: string; //변경 사용자 id
  change_point: number; //변동 포인트
  point: number; //변동 포인트
  type: number; //구분 (1:관리자변경, 2:사용자적립, 3:사용자사용)
  reg_date: string; //생성일시

  place_name: number; //업소명
  member_id: string; //관리자ID
  member_name: string; //관리자이름
}

/** 온동. */
export interface RoomTemputure {
  air_temp: number; //현재 온도 air_temp
  air_set_temp: number; //설정 온도 air_set_temp
  temp_key_1: number; //사용중 온도 temp_key_1
  temp_key_2: number; //외출 온도 temp_key_2
  temp_key_3: number; //공실 온도 temp_key_3
  temp_key_4: number; //청소/점검중 온도 temp_key_4
  temp_key_5: number; //청소/점검대기 온도 temp_key_5
}

/**
 * key, name 인터페이스
 */
export interface KeyName {
  key: string;
  name: string;
}

export interface NumberValue {
  value: number;
  name: string;
}

export interface StringValue {
  value: string;
  name: string;
}

export interface AnyValue {
  value: any;
  name: string;
}

export interface Snackbar {
  value: boolean; // true: show, false: hide.
  message: string; // 메세지.
  timeout: number; // ms초.
}

/**
 * newItem, setItem, delItem 결과 인터페이스.
 */
export interface CudResult<T extends object> {
  success: boolean;
  item: T | null;
}

/** 숙박형태 아이템. */
export interface StayItem {
  name: string;
  color: string;
}

/** 사용자 액션 */
export interface UserAction {
  disabled: boolean;
  action: () => void;
}
