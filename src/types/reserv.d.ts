import { BaseType } from '.';

/** 기간별 예약 수 */
export interface ReservCount {
  place_id: number;               //업소ID
  group1: number;                 //그룹1
  group2: number;                 //그룹2
  count: number;                  //예약 수
}

/** OTA별 예약 수 */
export interface OtaReservCount extends BaseType {
  place_id: number;               //업소ID
  group_num: number;              //기간그룹(일/주/월) 번호. 일=dayOfYear, 주=weekOfYear, 월=monthOfYear
  total_count: number;            //전체 수
  ota_1_count: number;            //OTA 1(야놀자) 수
  ota_2_count: number;            //OTA 2(여기어때) 수
  ota_3_count: number;            //OTA 3(네이버) 수
  ota_etc_count: number;          //OTA 나머지 수
}

/** 숙박형태별 예약 수 */
export interface StayReservCount extends BaseType {
  place_id: number;               //업소ID
  group_num: number;              //기간그룹(일/주/월) 번호. 일=dayOfYear, 주=weekOfYear, 월=monthOfYear
  total_count: number;            //전체 수
  stay_1_count: number;           //stay 1(숙박) 수
  stay_2_count: number;           //stay 2(대실) 수
  stay_3_count: number;           //stay 3(장기) 수
}
