import { RegDate } from './index.d';

/** 업소 기간별 매출 룸상태 현황 */
export interface PlaceSaleStateSum {
  place_id: number;               //업소ID
  reg_date: string;               //날짜 ('YYYY-mm-dd')
  count: number;                  //전체 수
  a_count: number;                //정상 수
  b_count: number;                //취소 수
  c_count: number;                //완료 수
}


/**
 * 매출 합계 기본 정보
 */
export interface BaseSaleSum {
  count: number;                  //객실 판매 수
  total_fee: number;              //총 가격
  total_amt: number;              //전체 금액(지불한) 합
  unpaid_amt: number;             //미수금 합 (0:정상-계산완료, 양수:미수금, 음수:환불금)

  prepay_ota_amt: number;				  //선결재 ota 금액
  prepay_cash_amt: number;			  //선결재 현금 금액
  prepay_card_amt: number;			  //선결재 카드 금액
  prepay_point_amt: number;			  //선결재 포인트 금액
  pay_cash_amt: number;				    //결재 현금 금액
  pay_card_amt: number;				    //결재 카드 금액
  pay_point_amt: number;				  //결재 포인트 금액
}


/** 업소 기간별 매출 */
export interface PlaceSaleSum extends BaseSaleSum {
  place_id: number;               //업소ID
  reg_date: string;               //날짜 ('YYYY-mm-dd')
}

/** 업소객실 매출 합계 */
export interface SaleSumRollup extends BaseSaleSum {
  id: number | null;                    //업소/객실 ID. null 이면 with rollup 합계임.
  stay_type: number | null;             //투숙형태 (1:숙박, 2:대실, 3:장기). null 이면 with rollup 합계임.
  default_fee: number;                  //기본요금 합
  add_fee: number;                      //추가요금 합

  // count: number;                        //건수
  // pay_cash_amt: number;                 //현금결제 합
  // pay_card_amt: number;                 //카드결제 합
  // pay_point_amt: number;                //포인트사용 합
  // prepay_ota_amt: number;               //OTA 예약금 합
  // prepay_cash_amt: number;              //예약선금 현금 합
  // prepay_card_amt: number;              //예약선금 카드 합
  // prepay_point_amt : number;            //포인트 선결제 합
}

/** 객실 기간 매출 */
export interface RoomSaleSum extends BaseSaleSum {
  room_id: number;                      //객실 ID
  room_name: string;                    //객실 이름
  stay_type: number | null;             //투숙형태 (1:숙박, 2:대실, 3:장기)

  prepay_ota_cnt: number = 0;           //선결재 OTA 건수
  prepay_cash_cnt: number = 0;          //선결재 현금 건수
  prepay_card_cnt: number;              //선결재 카드 건수
  prepay_point_cnt: number;             //선결재 포인트 건수
  pay_cash_cnt: number = 0;             //결재 현금 건수
  pay_card_cnt: number;                 //결재 카드 건수
  pay_point_cnt: number;                //결재 포인트 건수
  default_fee: number;                  //기본 비용 합
  add_fee: number;                      //추가 비용 합
  option_fee: number;                   //옵션 비용 합

  // count: number;                        //방ID + 투숙형태 카운트
  // prepay_ota_amt: number;               //선결재 OTA 합
  // prepay_cash_amt: number;              //선결쟤 현금 합
  // prepay_card_amt: number;              //선결재 카드 합
  // prepay_point_amt: number;             //선결재 포인트 합
  // pay_cash_amt: number;                 //결재 현금 합
  // pay_card_amt: number;                 //결재 카드 합
  // pay_point_amt: number;                //결재 포인트 합
  // total_fee: number;                    //전체 비용(내야할) 합
  // total_amt: number;                    //전체 지불금액 합
  // unpaid_amt: number;                   //미수금 합
};

/** 기간별 매출 합계 */
export interface SaleRangeSum extends BaseSaleSum {
  place_id: number | null;              //업소ID
  group_num: number                     //그룹번호
  group_year: number                    //그룹년도
  group_sub: number                     //그룹 추가정보
  group_display: string                 //그룹표시값
  default_fee: number;                  //기본요금 합
  add_fee: number;                      //추가요금 합
}


/**
 * SalePaySum
 */
export interface SalePaySum extends BaseSaleSum {
  sale_id: number;					      //sale ID
  serialno: string;					      //기기시리얼번호
  room_id: number;                //객실 ID
  room_name: string;					    //객실이름
  type_name: string;					    //객실유형이름
  floor: number;                  //층
  state: string;						      //상태 (A:정상, B:취소, C:완료)
  stay_type: number;					    //숙박유형 (1:숙박, 2:대실, 3:장기)
  check_in: string;				        //체크인 시간 또는 예정일시
  check_out: string;              //체크아웃 시간 또는 예정일시
  reg_date: string;					      //판매일시
  car_no: string | null;          //자동차번호
  default_fee: number;				    //기본가격
  add_fee: number;					      //추가가격
  option_fee: number;				      //옵션가격

  // count: number;                  //객실 판매 수. (room_sale 기준이므로 항싱 1)
  // total_fee: number;              //총 가격
  // total_amount: number;           //총 지불금액
  // unpaid_amount: number;          //미납금액. 0이면 정상, 양수이면 추가 비용 필요. 음수이면 환불 필요?
  // prepay_ota_amt: number;				  //선결재 ota 금액
  // prepay_cash_amt: number;			  //선결재 현금 금액
  // prepay_card_amt: number;			  //선결재 카드 금액
  // prepay_point_amt: number;			  //선결재 포인트 금액
  // pay_cash_amt: number;				    //결재 현금 금액
  // pay_card_amt: number;				    //결재 카드 금액
  // pay_point_amt: number;				  //결재 포인트 금액

  paid_count: number;				      //전체 결재 수단 수
  prepay_ota_cnt: number;				  //ota 선결재 수
  prepay_cash_cnt: number;			  //현금 선결재 수
  prepay_card_cnt: number;			  //카드 선결재 수
  prepay_point_cnt: number;			  //포인트 선결재 수
  pay_cash_cnt: number;				    //현금 결재 수
  pay_card_cnt: number;				    //카드 결재 수
  pay_point_cnt: number;				  //포인트 결재 수
  total_prepay_ota_amt: number;		//전체 선결재 ota 금액
  total_prepay_cash_amt: number;	//전체 선결재 현금 금액
  total_prepay_card_amt: number;	//전체 선결재 카드 금액
  total_prepay_point_amt: number;	//전체 선결재 포인트 금액
  total_pay_cash_amt: number;			//전체 결재 현금 금액
  total_pay_card_amt: number;			//전체 결재 카드 금액
  total_pay_point_amt: number;		//전체 결재 포인트 금액
}
