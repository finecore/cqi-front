import { useEffect, useState } from "react";
import Router, { useRouter } from "next/router";

export default function useSubMenuActive(num: number) {
  const router = useRouter();

  let subMenuElement: any;

  if (typeof document !== "undefined") {
    subMenuElement = document.getElementsByClassName(
      "sub_menu",
    ) as HTMLCollectionOf<HTMLElement>;
  }

  useEffect(() => {
    // 현재 gnb active 설정
    const menuHeight =
      subMenuElement[num].getElementsByTagName("li")[0].clientHeight;
    const menuLength = subMenuElement[num].children.length;
    const totalHeight = menuHeight * menuLength + 26;

    subMenuElement[num].style.height = totalHeight + "px";
    subMenuElement[num].classList.add("active");

  }, []);

  return;
}
