import React, { useState, useCallback } from "react";

export default function useInputs(initial: any) {
  const [value, setter] = useState<any>(initial);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setter(event.target.value);
  };

  // useCallBack
  //   useCallback은 특정 함수를 새로 만들지 않고 재사용하고 싶을때 사용한다.
  //   객체는 렌더링되면 완전히 새로운 객체가되어버린다. 새로운 객체가만들어지면 리액트는 렌더링을 시킨다.
  //   함수도 객체다.
  //   함수 안에서 사용하는 상태 혹은 props 가 있다면 꼭, deps 배열안에 포함시켜야 된다.
  const handleRecycle = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setter(event.target.value);
    },
    [],
  );

  /**
   * @TODO
   * validation 추가필요
   */

  return {
    value,
    onRecycle: handleRecycle,
    onChange: handleChange,
  };
}
