import { NextRouter } from "next/router";
import { useEffect } from "react";

const usePageName = (router: NextRouter) => {
  // console.log("- usePageName", { router });

  let pageName = "";

  if (router && router["components"] && router["components"][router.pathname]) {
    pageName = router["components"][router.pathname]?.Component.name;
    if (router.pathname === pageName) pageName = "";
  }

  // console.log("- usePageName", router.pathname, pageName);

  return router.pathname + "/" + pageName;
};

export default usePageName;
