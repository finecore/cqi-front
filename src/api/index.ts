import { api, reCleateAjax } from '@/utils/api-util';
import { HOST_URL, API_LOCAL_URL, API_REMOTE_URL } from './config';
import _ from 'lodash';

import { getDataSyncManager } from '@/main';

// 기본 로컬 baseURL
const ajax = reCleateAjax('');

const noSyncUrls = ['ws', 'login', 'interrupt', 'qr']; // 동기화 제외 URL

// list
const list = async (config: any, url: string, data: any, sync: boolean) => {
  const { dispatch } = config;

  try {
    // `data`가 객체라면 이를 쿼리 스트링으로 변환
    const res = await ajax.get(`/${url}`, {
      params: data && typeof data === 'object' ? { ...data } : data,
      ...config,
    });

    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// item
const item = async (config: any, url: string, data: any, sync: boolean) => {
  const { dispatch } = config;

  try {
    const res = await ajax.get(`/${url}`, {
      params: data && typeof data === 'object' ? { ...data } : data,
      ...config,
    });

    return api.checkResponse(dispatch, config, res);
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// 원격서버 동기화 처리
const syncData = async (
  method: string,
  config: any,
  url: string,
  data: any
) => {
  try {
    const urls = url.split('/');
    const isNoSyncUrl = urls.some((url) => noSyncUrls.includes(url));

    // 동기화 안하는 Url 인지 체크.
    if (!isNoSyncUrl) {
      console.log(`====> ${method.toUpperCase()} 동기화, url: ${url}`);
      await getDataSyncManager().doSync(
        method,
        config,
        API_REMOTE_URL,
        url,
        _.cloneDeep(data),
        true
      );
    }
  } catch (err) {
    console.error(`====> ${method.toUpperCase()} 원격서버 동기화 오류`, err);
  }
};

// post
const post = async (
  config: any,
  url: string,
  data: any,
  sync: boolean = true
) => {
  const { dispatch } = config;

  console.log('====> post ', `${url}`, { url, data, sync });
  try {
    const res = await ajax.post(`/${url}`, data, {
      ...config,
    });

    const result = api.checkResponse(dispatch, config, res);

    // 원격서버 동기화
    if (sync) {
      syncData('post', config, url, data);
    }

    return result;
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// put
const put = async (
  config: any,
  url: string,
  data: any,
  sync: boolean = true
) => {
  const { dispatch } = config;
  console.log('====> put', { url, data, sync });

  try {
    const res = await ajax.put(`/${url}`, data, {
      ...config,
    });

    const result = api.checkResponse(dispatch, config, res);

    // 원격서버 동기화
    if (sync) {
      syncData('put', config, url, data);
    }

    return result;
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

// del
const del = async (
  config: any,
  url: string,
  data: any,
  sync: boolean = true
) => {
  const { dispatch } = config;
  console.log('>==== del', { url, data, sync });

  try {
    const res = await ajax({
      url: `${url}`,
      method: 'delete',
      data: { ...data },
      ...config,
    });

    const result = api.checkResponse(dispatch, config, res);

    // 원격서버 동기화
    if (sync) {
      syncData('delete', config, url, data);
    }

    return result;
  } catch (error) {
    return api.errorRequest(dispatch, error);
  }
};

export { list, item, post, put, del };
