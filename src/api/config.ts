const debug = import.meta.env.NODE_ENV !== 'production';

const HOST = import.meta.env.VITE_HOST;
const PORT = import.meta.env.VITE_PORT;

const API_LOCAL_HOST = import.meta.env.VITE_API_LOCAL_HOST;
const API_LOCAL_PORT = import.meta.env.VITE_API_LOCAL_PORT;
const SOC_LOCAL_HOST = import.meta.env.VITE_SOC_LOCAL_HOST;
const SOC_LOCAL_PORT = import.meta.env.VITE_SOC_LOCAL_PORT;

const API_REMOTE_HOST = import.meta.env.VITE_API_REMOTE_HOST;
const API_REMOTE_PORT = import.meta.env.VITE_API_REMOTE_PORT;
const SOC_REMOTE_HOST = import.meta.env.VITE_SOC_REMOTE_HOST;
const SOC_REMOTE_PORT = import.meta.env.VITE_SOC_REMOTE_PORT;

const HOST_URL = HOST + ':' + PORT; // ngrok 는 port 럾다

const API_LOCAL_URL = API_LOCAL_HOST + ':' + API_LOCAL_PORT; // ngrok 는 port 럾다
const SOC_LOCAL_URL = SOC_LOCAL_HOST + ':' + SOC_LOCAL_PORT; // ngrok 는 port 럾다

const API_REMOTE_URL = API_REMOTE_HOST + ':' + API_REMOTE_PORT; // ngrok 는 port 럾다
const SOC_REMOTE_URL = SOC_REMOTE_HOST + ':' + SOC_REMOTE_PORT; // ngrok 는 port 럾다

console.log('-> Config ', {
  debug,
  HOST_URL,
  API_LOCAL_URL,
  SOC_LOCAL_URL,
  API_REMOTE_URL,
  SOC_REMOTE_URL,
});

export {
  debug,
  HOST_URL,
  API_LOCAL_URL,
  SOC_LOCAL_URL,
  API_REMOTE_URL,
  SOC_REMOTE_URL,
};
