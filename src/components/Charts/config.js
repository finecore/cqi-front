export const basicOptions = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  responsive: true
};
export let blueChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          suggestedMin: 60,
          suggestedMax: 125,
          padding: 20,
          fontColor: "#2380f7"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.1)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 20,
          fontColor: "#2380f7"
        }
      }
    ]
  }
};

export let orangeChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          suggestedMin: 50,
          suggestedMax: 110,
          padding: 20,
          fontColor: "#ff8a76"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(220,53,69,0.1)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 20,
          fontColor: "#ff8a76"
        }
      }
    ]
  }
};

export let yearChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)",
          zeroLineWidth: 2
        },
        ticks: {
          beginAtZero: false,
          stepSize: 2,
          suggestedMin: 0,
          suggestedMax: 10,
          padding: 20,
          fontColor: "#4d5763"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          padding: 20,
          fontColor: "#ffffff"
        }
      }
    ]
  }
};

export let monthChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)",
          zeroLineWidth: 2
        },
        ticks: {
          stepSize: 1,
          suggestedMin: 0,
          suggestedMax: 10,
          padding: 20,
          fontColor: "#4d5763"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          padding: 20,
          fontColor: "#ffffff"
        }
      }
    ]
  }
};

export let saleRoomMonthChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,0)",
          zeroLineColor: "rgba(41,46,53,1)",
          zeroLineWidth: 1
        },
        ticks: {
          display: false,
          stepSize: 100,
          suggestedMin: 50,
          suggestedMax: 400,
          padding: 20,
          fontColor: "#4d5763"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          fontColor: "rgba(0,0,0,0)",
          lineHeight: 1
        }
      }
    ]
  }
};

export let saleIsgMonthChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,0)",
          zeroLineWidth: 0
        },
        ticks: {
          display: false,
          stepSize: 100,
          suggestedMin: 50,
          suggestedMax: 400,
          padding: 20,
          fontColor: "#4d5763"
        }
      }
    ],

    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          drawTicks: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          padding: 20,
          fontColor: "#ffffff"
        }
      }
    ]
  }
};

export let barChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        gridLines: {
          drawBorder: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          stepSize: 25,
          suggestedMin: 0,
          suggestedMax: 100,
          padding: 20,
          fontColor: "#4d5763"
        }
      }
    ],
    xAxes: [
      {
        barThickness: 3,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 5,
          fontColor: "#ffffff"
        }
      }
    ]
  }
};

export let doughnutChartOptions = {
  ...basicOptions,
  tooltips: {
    enabled: false,
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  cutoutPercentage: 70
};

export let pieChartOptions = {
  ...basicOptions,
  tooltips: {
    enabled: false,
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  }
};

export let roomBarChartOptions = {
  ...basicOptions,
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  scales: {
    yAxes: [
      {
        gridLines: {
          drawBorder: false,
          color: "rgba(41,46,53,1)",
          zeroLineColor: "rgba(41,46,53,1)"
        },
        ticks: {
          suggestedMin: 0,
          suggestedMax: 100,
          padding: 10,
          fontColor: "#4d5763",
          min: 0,
          max: 2000,
          stepSize: 500,
          callback: function(value) {
            return (value / 500) * 25 + "%";
          }
        }
      }
    ],
    xAxes: [
      {
        barThickness: 24,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 5,
          fontColor: "#ffffff"
        }
      }
    ]
  }
};
