import React from "react";

import { FormControl } from "react-bootstrap";
import CloseIcon from "@material-ui/icons/Close";

import cx from "classnames";

import "./styles.scss";

class SearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "", clearBtn: false };
  }

  inputNode = null;

  handleChange = (value) => {
    this.setState({
      value,
      clearBtn: value ? true : false,
    });

    this.props.onSearch(value);
  };

  handleFocus = (event) => {
    this.setState({
      clearBtn: this.state.value ? true : false,
    });
  };

  handleBlur = (event) => {
    setTimeout(() => {
      this.setState({
        clearBtn: false,
      });
    }, 200);
  };

  handleClear = () => {
    this.setState({
      value: "",
      clearBtn: false,
    });
  };

  componentDidMount = () => {
    const { value } = this.props;

    this.setState({
      value,
    });
  };

  render() {
    return (
      <div className={cx(this.props.colWidth || "col-md-3")}>
        <FormControl
          name={this.props.name || "search"}
          componentClass="input"
          placeholder={this.props.placeholder || "검색어를 입력하세요"}
          label="text"
          className={cx("form-control input-ms-clearBtn")}
          value={this.state.value}
          onChange={(evt) => this.handleChange(evt.target.value)}
          onFocus={(evt) => this.handleFocus(evt.target.value)}
          onBlur={(evt) => this.handleBlur(evt.target.value)}
          maxLength={this.props.maxLength || 999}
        />
        {this.state.clearBtn ? (
          <button type="button" className="btn-input-clearBtn" onClick={this.handleClear}>
            <CloseIcon />
          </button>
        ) : null}
      </div>
    );
  }
}
export default SearchInput;
