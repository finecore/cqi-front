import React, { Component } from 'react';
import { connect } from 'react-redux';
import stackTrace from 'stack-trace';

// UI for notification
import { withSnackbar } from 'notistack';
import CustomizedStackSnackbar from 'components/Notification/CustomizedStackSnackbar';

import { actionCreators as authAction } from 'actions/auth';
import { actionCreators as errorAction } from 'actions/error';
import { actionCreators as mailAction } from 'actions/mail';

import { envProps } from 'config/configureStore';

import moment from 'moment';
import _ from 'lodash';

import '@material-ui/core/styles';
import './styles.scss';

/**
 * 공통 오류 핸들러.
 *
 * @class ErrorBoundary
 * @extends {Component}
 */
class ErrorBoundary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
      error: {},
      info: {},
    };

    this.mailSendDelay = false;
    this.lastMailContent = '';
    this.lastErrorMessage = '';
    this.timer = null;
  }

  handleDismiss = (isLogout) => {
    console.log('--> ErrorBoundary::handleDismiss isLogout', isLogout);
    const { closeSnackbar } = this.props;

    if (isLogout && this.props.auth.isLogined) {
      if (!envProps.IS_LOCAL) {
        setTimeout(function () {
          this.props.handleLogout();
        }, 60 * 1000);
      } else this.props.handleLogout();
    }

    this.setState({
      hasError: false,
      error: {},
      info: {},
    });

    closeSnackbar();
  };

  sendErrMail = (type, message) => {
    let subject = `${process.env.REACT_APP_MODE && process.env.REACT_APP_MODE !== 'PROD' ? '[' + process.env.REACT_APP_MODE + ']' : ''} Manager ${type || ''} (${process.env.REACT_APP_SVR_NAME})`;

    const {
      auth: { user },
      place: { item },
    } = this.props;

    console.info('- sendErrMail', { subject, message }, this.mailSendDelay);

    // if (process.env.REACT_APP_MODE !== "DEV") {
    if (message && message.length > 3 && !this.mailSendDelay && user && item) {
      this.lastMailContent = _.cloneDeep(message);

      let content = moment().format('YYYY-MM-DD HH:mm:ss') + '<br/><br/>';
      content += `[${item.name}] ${user.id} <br/><br/>`;
      content += message;

      var trace = stackTrace.get();

      let file = trace[1].getFileName().replace(__dirname, '');
      let line = trace[1].getLineNumber() + ':' + trace[1].getColumnNumber();

      console.info('- file', { file, line });

      content += `<br/><br/>${file} ${line}`;

      this.mailSendDelay = true;

      let mail = {
        type: 1, // 메일 타입 (0: 고객메일, 1: 관리자메일, 9: 서버메일)
        to: process.env.REACT_APP_MAIL_ERR_TO_ADDR,
        from: process.env.REACT_APP_MAIL_FROM_ADDR,
        subject,
        content,
      };

      this.props.sendMail({ mail });

      setTimeout(() => {
        this.mailSendDelay = false;
      }, 60 * 1000);
    }
    // }
  };

  componentDidMount = () => {};

  componentDidCatch(error, info) {
    console.log('--> ErrorBoundary::catch ', error, info);

    // 개발/테스트 시만 표시 및 메일 발송.
    if (process.env.REACT_APP_MODE !== 'PROD') {
      const {
        error: { code, logout },
        enqueueSnackbar,
      } = this.props;

      const autoHideDuration = envProps.IS_LOCAL ? 10000 : 3000;

      this.setState({
        hasError: true,
        error: error,
        info: info,
      });

      // Stacked Snack Bar.
      CustomizedStackSnackbar({
        variant: 'error',
        message: error.toString(),
        detail: info.componentStack,
        handleDismiss: () => {
          // 인증 오류는 로그아웃 시킨다.
          this.handleDismiss((!envProps.IS_LOCAL && code === 401) || logout);
        },
        autoHideDuration,
        enqueueSnackbar,
        anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
        callbackData: error,
      });

      // 메일 발송.
      let content = 'ErrorBoundary::catch<br/><br/>' + error.toString();
      content +=
        '<br/><br/>' +
        info.componentStack.toString().split(' in ').join('<br/>');

      // if (content.indexOf("DraggableCore") === -1) this.sendErrMail("catch", content);

      // 오류 상태 초기화.
      setTimeout(() => {
        if (error.toString() === this.state.error.toString()) {
          this.handleDismiss((!envProps.IS_LOCAL && code === 401) || logout);
        }
      }, autoHideDuration);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { enqueueSnackbar } = nextProps;

    if (nextProps.error && nextProps.error !== this.props.error) {
      let { error } = nextProps;

      let { auth } = this.props;

      console.info(' ErrorBoundary::error --->', error);

      if (error) {
        let { code, message, detail, config = {}, logout } = error;

        // 로그 아웃 상태에서 인증 정보 오류는 보여주지 않는다.
        if (code === 401 && !auth.isLogined) {
          return false;
        }

        if (
          message &&
          message !== 'undefined' &&
          message.indexOf('/mail/') === -1
        ) {
          if (envProps.SERVICE_MODE !== 'PROD') {
            message = '[' + code + '] ' + message;
            if (detail) {
              const {
                sqlMessage = undefined,
                sql = undefined,
                ...arg
              } = detail;
              if (sqlMessage) detail = sqlMessage + '\n\n' + sql + ' ' + arg;
            }
          } else {
            message = message.replace(/(\[(.*?)\])?/, '');
            message = '[' + code + '] ' + message;
            // message = "[" + code + "] 요청 처리중 오류가 발생 하였습니다.";
          }

          console.info('  CustomizedStackSnackbar ', { code, message, detail });

          if (this.lastErrorMessage !== message) {
            const autoHideDuration = envProps.IS_LOCAL ? 10000 : 3000;

            // Stacked Snack Bar.
            CustomizedStackSnackbar({
              variant: 'default',
              message,
              detail: detail ? '\n' + JSON.stringify(detail) : null,
              handleDismiss: (data) => {
                // 인증 오류는 로그아웃 시킨다.
                this.handleDismiss(code === 401 || logout);
              },
              autoHideDuration,
              enqueueSnackbar,
              anchorOrigin: { vertical: 'bottom', horizontal: 'left' },
              callbackData: error,
            });

            this.setState({
              error: error,
            });

            // 메일 발송.
            if (code === 500 && error.message !== 'Network Error') {
              let content = JSON.stringify(error);
              this.sendErrMail('error', content);
            }

            if (this.timer) clearTimeout(this.timer);

            // 오류 상태 초기화.
            this.timer = setTimeout(() => {
              if (this.lastErrorMessage === message) {
                this.lastErrorMessage = '';
              }
            }, autoHideDuration);

            this.lastErrorMessage = message;
          }

          // logger.info("-------------------------------------------------");
          // logger.info("- ErrorBoundary::error" + { code, message, detail, logout });
          // logger.info("-------------------------------------------------\n");
        }
      }
    }
  }

  render() {
    // 개발 시 상세 오류 화면으로 전환.
    if (this.state.hasError && envProps.IS_LOCAL) {
      return (
        <div className="Error">
          <h1>처리중 오류가 발생 했습니다.</h1>
          <div className="bg">
            <p className="code">{this.state.error.toString()}</p>
            {this.state.info.componentStack.split(' in ').map((value, i) => {
              if (value.replace(/\s/gi, ''))
                return <p key={i}> {i + ' ' + value}</p>;
              return '';
            })}
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

const mapStateToProps = (state, ownProps) => {
  const { auth, place, error } = state;
  return {
    auth,
    place,
    error,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleLogout: () => dispatch(authAction.doLogout()),
    handleClearError: () => dispatch(errorAction.clearError()),
    sendMail: (mail) => dispatch(mailAction.newMail(mail)),
  };
};

export default withSnackbar(
  connect(mapStateToProps, mapDispatchToProps)(ErrorBoundary)
);
