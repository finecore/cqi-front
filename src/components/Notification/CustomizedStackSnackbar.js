import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import DoneIcon from "@material-ui/icons/Done";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";

import { withStyles } from "@material-ui/core/styles";
import { IconButton } from "@material-ui/core";

const { Fragment } = React;

const styles1 = (theme) => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  default: {
    backgroundColor: "#607D8B",
  },
  warning: {
    backgroundColor: amber[700],
  },
  show: {
    fontSize: 20,
  },
  icon: {
    fontSize: 20,
    color: "#eee",
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: "flex",
    alignItems: "center",
  },
});

// const ConfirmButton = withStyles({
//   root: {
//     background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
//     borderRadius: 3,
//     border: 0,
//     color: "white",
//     height: 48,
//     padding: "0 30px",
//     boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
//   },
//   label: {
//     textTransform: "capitalize",
//   },
// })(Button);

function CustomizedStackSnackbar({ variant, message, detail, handleDismiss, autoHideDuration = 5000, enqueueSnackbar, linkButton, anchorOrigin, callbackData }) {
  const env = process.env.NODE_ENV;

  if (detail) {
    try {
      if (!(detail instanceof Array)) detail = JSON.parse(detail);
      if (!(detail instanceof Array)) detail = detail.split("\\n");
    } catch (e) {
      detail = null;
    }
  } else {
    detail = null;
  }

  if (!anchorOrigin)
    anchorOrigin = {
      vertical: "bottom",
      horizontal: "left",
    };

  const content = (
    <div className="client-snackbar-content">
      <span className={styles1.message}>{message ? message : "처리 중 오류가 발생 했습니다."}</span>
      {detail && (
        <div>
          <br />
          <span className={styles1.message}>
            {detail.map((line, i) => {
              return (
                <span key={i}>
                  {line}
                  <br />
                </span>
              );
            })}
          </span>
        </div>
      )}
    </div>
  );

  let isShow = false;

  const closeHandeler = () => {
    console.log("- closeHandeler", { isShow });
    if (handleDismiss) handleDismiss(callbackData, isShow);
  };

  const options = {
    variant,
    autoHideDuration,
    anchorOrigin,
    onClose: closeHandeler,
    action: (
      <Fragment>
        {/* <ConfirmButton
          onClick={() => {
            handleDismiss(callbackData, true);
          }}
        >
          <DoneIcon className={withStyles.show} />
        </ConfirmButton> */}
        {linkButton ? (
          <IconButton
            key="show"
            aria-label="Show"
            color="inherit"
            className={withStyles.show}
            onClick={() => {
              console.log("- handleDismiss");
              isShow = true;
            }}
          >
            <DoneIcon className={withStyles.show} />
          </IconButton>
        ) : null}
        <IconButton
          key="close"
          aria-label="Close"
          className={withStyles.close}
          onClick={() => {
            console.log("- handleDismiss");
            isShow = false;
          }}
        >
          <CloseIcon className={withStyles.icon} />
        </IconButton>
      </Fragment>
    ),
  };

  return enqueueSnackbar(content, options);
}

export default CustomizedStackSnackbar;
