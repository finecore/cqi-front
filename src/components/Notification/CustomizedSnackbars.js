import React from "react";
import PropTypes from "prop-types";
import Snackbar from "@material-ui/core/Snackbar";
import { withStyles } from "@material-ui/core/styles";

import CustomizedSnackbarContent from "./CustomizedSnackbarContent";

const styles1 = (theme) => ({
  margin: {
    margin: theme.spacing.unit,
    background: "red",
  },
});

class CustomizedSnackbars extends React.Component {
  state = {
    open: false,
  };

  componentDidMount() {
    this.setState({ open: this.props.open });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.open !== this.props.open) this.setState({ open: nextProps.open });
  }

  render() {
    const { classes, isContent = false, open = false, message, onClose, type, anchorOrigin = { vertical: "bottom", horizontal: "center" }, autoHideDuration = 6000 } = this.props;

    this.handleClick = () => {
      this.setState({ open: true });
    };

    this.handleClose = (event, reason) => {
      console.log("- CustomizedSnackbars::handleClose", event);

      if (reason === "holding") {
        this.setState({ open: false });
        console.log("- CustomizedSnackbars Holding..");
      }

      onClose();
    };

    if (this.state.open || open)
      return (
        <div>
          {!isContent && (
            <Snackbar anchorOrigin={anchorOrigin} open={true} autoHideDuration={autoHideDuration} onClose={(event) => this.handleClose(event)}>
              <CustomizedSnackbarContent variant={type} message={message} onClose={(event) => this.handleClose(event)} />
            </Snackbar>
          )}

          {isContent && <CustomizedSnackbarContent className={classes.margin} variant={type} message={message} onClose={(event) => this.handleClose(event)} />}
        </div>
      );
    else return <div />;
  }
}

CustomizedSnackbars.propTypes = {
  isContent: PropTypes.bool,
  open: PropTypes.bool,
  message: PropTypes.string.isRequired,
  onClose: PropTypes.func,
  anchorOrigin: PropTypes.object,
  autoHideDuration: PropTypes.number,
  type: PropTypes.oneOf(["success", "warning", "error", "info"]).isRequired,
};

export default withStyles(styles1)(CustomizedSnackbars);
