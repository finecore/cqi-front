import SocketLog from '..model/socket-log';
import dispatcher from 'dispatcher/dispatcher';
import request from 'request';

/**
 * Socket Helper Class.
 */
class SocketHelper {
  constructor(socket) {
    this.socket = socket;
  }

  // put log data.
  log(type, json, callback) {
    const socketLog = {
      type: type || '0', // 전송 방향 (0: 수신, 1: 송신)
      ip: this.socket.remoteAddress,
      port: this.socket.remotePort,
      data: json,
    };

    SocketLog.putSocketLog(socketLog, (err, rows) => {
      callback(err, rows);
    });
  }

  // route handle.
  route(json, callback) {
    if (!json || !json.headers || !json.headers.method) {
      callback(null, json);
    } else {
      const {
        headers: { method, url, token, serialno, channel = 'device' },
        body = {},
      } = json;

      // room state json object -> stringify
      if (url === 'room/state' && body.state) {
        body.state = JSON.stringify(body.state);
      }

      var laddr = this.socket.localAddress
        ? this.socket.localAddress.split(':')
        : 'localhost';

      console.log('- DEV ip :' + laddr);

      const host = 'https://' + laddr + ':' + process.env.PORT;
      console.log('- HOST : ', host);

      const req = {
        url: host + '/' + url,
        method: method,
        headers: { serialno, channel, 'x-access-token': xtoken, token }, // channel = test 는 checkPermission 을 하지 않는다.
        body: body,
        json: true,
      };

      console.log('-> route req: ', req);

      request(req, function (err, res, data) {
        //console.log("-> route res : ", data, " err: ", err);
        callback(err, data);
      });
    }
  }
}

module.exports = SocketHelper;
