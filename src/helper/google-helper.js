import axios from 'axios';
// import _ from "lodash";

/**
 * Google Helper Class.
 */
class GoogleHelper {
  constructor(socket) {
    this.hostMap = 'https://maps.googleapis.com/maps/api/geocode/json';
    this.hostGeo = 'https://www.googleapis.com/geolocation/v1/geolocate';

    // Google API Key
    this.serviceKey = 'AIzaSyB5cRvFhCeOY9VH8t28NZ_1gfP2Gk_TCzM';
    this.language = 'ko';
  }

  // 위/경도 를 주소로 변환.
  async gpsToAddr(latitude, longitude) {
    console.log('- GoogleHelper toAddr', latitude, longitude);

    if (latitude && longitude) {
      let promise = await axios
        .get(
          this.hostMap +
            `?key=${this.serviceKey}&language=${this.language}&latlng=${latitude},${longitude}`
        )
        .then((res) => res.data || [])
        .then((data) => {
          if (data.results.length) {
            let location = data.results[1].formatted_address.split(' '); // 기본주소.

            // 우편 번호 주소 반환.
            data.results.map(function (v, k) {
              // stress addreass
              if (
                v.types.join(' ').indexOf('street_address') > -1 &&
                v.formatted_address
              ) {
                location = v.formatted_address.split(' ');
                location.shift(); // 대한민국 제거.

                location[0] = location[0].replace(/광역/g, '');
                location[0] = location[0].replace(/특별/g, '');

                console.log(
                  '위경도 -> 주소 : ',
                  latitude + ',' + longitude,
                  ' -> ',
                  location.join(' ')
                );

                return false;
              }
            });

            return location;
          }
          return '';
        })
        .catch((error) => console.error('- google api error', error));

      return promise;
    } else {
      return '';
    }
  }

  // 주소 를 위경도로 변환.
  async addrToGps(addr) {
    let url = `${this.hostMap}?key=${
      this.serviceKey
    }&sensor=false&language=ko&address=${encodeURIComponent(addr)}`;

    console.log('- addrToGps', url);

    const promise = await axios
      .get(url)
      .then((res) => res.data)
      .then((data) => {
        const { results = [] } = data;
        console.log('주소 -> 위경도 : ', results[0]);

        return results[0];
      })
      .catch((error) => console.error('- google api error', error));

    return promise;
  }

  // 현재 브라우저 IP 로 위치 정보 가져오기.
  async ipToPosition() {
    let url = `${this.hostGeo}?key=${this.serviceKey}`;

    console.log('- ipToPosition', url);

    const promise = await axios
      .post(url) // POST
      .then((res) => {
        let { location, accuracy } = res.data;

        console.log('IP -> 위경도:', location, ' 정확도:', accuracy);

        return location;
      })
      .catch((error) => console.error('- google api error', error));

    return promise;
  }
}

module.exports = GoogleHelper;
