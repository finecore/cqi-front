import ClickOutside from 'click-outside-vue3/src/v-click-outside';

module.exports = defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(ClickOutside, {});
});
