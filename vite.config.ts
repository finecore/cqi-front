import { defineConfig, type ConfigEnv, type UserConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import vuetify from 'vite-plugin-vuetify';
import { VitePWA } from 'vite-plugin-pwa';
import dotenv from 'dotenv';

// ✅ 환경 변수 로드
dotenv.config();

export default defineConfig((config: ConfigEnv): UserConfig => {
  const { mode } = config;
  dotenv.config({ path: `.env.${mode.toLowerCase()}` });

  const HOST = process.env.VITE_REMOTE_API_HOST || 'https://localhost';
  const PORT = Number(process.env.VITE_REMOTE_API_PORT) || 4001;
  const API_LOCAL_URL = `${HOST}:${PORT}`;

  console.log('➡️ Proxy API_LOCAL_URL:', API_LOCAL_URL);

  return {
    define: {
      'import.meta.env': { ...process.env },
    },
    plugins: [
      vue(),
      vuetify({ autoImport: true }),
      VitePWA(),
      // ⛔ Babel 플러그인 제거
    ],
    server: {
      proxy: {
        '/api': {
          target: API_LOCAL_URL,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ''),
          secure: false,
          ws: true,
        },
      },
      host: HOST.replace(/^https?:\/\//, ''),
      port: PORT,
      open: true,
      watch: {
        usePolling: true,
        ignored: ['**/.env.*'],
      },
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
        '@components': path.resolve(__dirname, './src/components'),
      },
      extensions: ['.js', '.json', '.jsx', '.mjs', '.ts', '.tsx', '.vue'],
    },
    build: {
      commonjsOptions: {
        esmExternals: true,
      },
      rollupOptions: {
        external: ['crypto', 'fsevents'],
        output: {
          manualChunks: {
            vendor: ['vue', 'vuetify'],
          },
        },
      },
    },
    optimizeDeps: {
      esbuildOptions: {
        format: 'esm',
      },
      exclude: ['fsevents'],
    },
    esbuild: {
      target: 'esnext',
    },
  };
});
